module.exports = {
    getFields: function(body) { 
        // go through the json and find the fields that are there. Put them in an array that will go back to the caller.
        
        var result = [];

        if(body.Id)
            result.push("Id");

        if(body.AdjustPriceToParent__c)
            result.push("AdjustPriceToParent__c");

        if(body.AdjustPriceToSchool__c)
            result.push("AdjustPriceToSchool__c");

        if(body.Code__c)
            result.push("Code__c");

        if(body.Code_MAS90__c)
            result.push("Code_MAS90__c");

        if(body.Confirmation__c)
            result.push("Confirmation__c");

        if(body.CreatedById)
            result.push("CreatedById");

        if(body.CreatedDate)
            result.push("CreatedDate");

        if(body.Description__c)
            result.push("Description__c");

        if(body.DFLAdjusted__c)
            result.push("DFLAdjusted__c");

        if(body.DFLPrice__c)
            result.push("DFLPrice__c");

        if(body.DFLTax__c)
            result.push("DFLTax__c");

        if(body.LastModifiedById)
            result.push("LastModifiedById");

        if(body.OWProduct__c)
            result.push("OWProduct__c");

        if(body.Price_to_Parent__c)
            result.push("Price_to_Parent__c");

        if(body.Price_to_School__c)
            result.push("Price_to_School__c");

        if(body.PrintOrder__c)
            result.push("PrintOrder__c");

        if(body.Name)
            result.push("Name");

        if(body.ProductName__c)
            result.push("ProductName__c");

        if(body.SeasonProduct_ID__c)
            result.push("SeasonProduct_ID__c");

        if(body.Shortcut__c)
            result.push("Shortcut__c");

        if(body.TaxParent__c)
            result.push("TaxParent__c");

        if(body.TaxSchool__c)
            result.push("TaxSchool__c");

        if(body.TestProduct__c)
            result.push("TestProduct__c");

        return result;
    },
    getUpdateFields: function(body) { 
        // go through the json and find the fields that are there. Put them in an array that will go back to the caller.
        // columns are double quoted on purpose. Postgres likes to convert to lowercase without them. SF has camel.
        var result = [];

        if(body[0].AdjustPriceToParent__c)
            result.push("\"AdjustPriceToParent__c\"="+body[0].AdjustPriceToParent__c);

        if(body[0].AdjustPriceToSchool__c)
            result.push("\"AdjustPriceToSchool__c\"="+body[0].AdjustPriceToSchool__c);

        if(body[0].Code__c)
            result.push("\"Code__c\"='"+body[0].Code__c+"'");

        if(body[0].Code_MAS90__c)
            result.push("\"Code_MAS90__c\"='"+body[0].Code_MAS90__c+"'");

        if(body[0].Confirmation__c)
            result.push("\"Confirmation__c\"='"+body[0].Confirmation__c+"'");

        if(body[0].Description__c)
            result.push("\"Description__c\"='"+body[0].Description__c+"'");

        if(body[0].DFLAdjusted__c)
            result.push("\"DFLAdjusted__c\"="+body[0].DFLAdjusted__c);

        if(body[0].DFLPrice__c)
            result.push("\"DFLPrice__c\"="+body[0].DFLPrice__c);

        if(body[0].DFLTax__c)
            result.push("\"DFLTax__c\"="+body[0].DFLTax__c);

        if(body[0].LastModifiedById)
            result.push("\"LastModifiedById\"='"+body[0].LastModifiedById+"'");

        if(body[0].OWProduct__c)
            result.push("\"OWProduct__c\"='"+body[0].OWProduct__c+"'");

        if(body[0].Price_to_Parent__c)
            result.push("\"Price_to_Parent__c\"="+body[0].Price_to_Parent__c);

        if(body[0].Price_to_School__c)
            result.push("\"Price_to_School__c\"="+body[0].Price_to_School__c);

        if(body[0].PrintOrder__c)
            result.push("\"PrintOrder__c\"="+body[0].PrintOrder__c);

        if(body[0].Name)
            result.push("\"Name\"='"+body[0].Name+"'");

        if(body[0].ProductName__c)
            result.push("\"ProductName__c\"='"+body[0].ProductName__c+"'");

        if(body[0].SeasonProduct_ID__c)
            result.push("\"SeasonProduct_ID__c\"='"+body[0].SeasonProduct_ID__c+"'");

        if(body[0].Shortcut__c)
            result.push("\"Shortcut__c\"='"+body[0].Shortcut__c+"'");

        if(body[0].TaxParent__c)
            result.push("\"TaxParent__c\"="+body[0].TaxParent__c);

        if(body[0].TaxSchool__c)
            result.push("\"TaxSchool__c\"="+body[0].TaxSchool__c);

        if(body[0].TestProduct__c)
            result.push("\"TestProduct__c\"='"+body[0].TestProduct__c+"'");

        return result;
    }
    
};