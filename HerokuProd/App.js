var express = require('express'),
     http = require('http'),
     request = require('request'),
     bodyParser = require('body-parser'),
            //errorHandler = require('express-error-handler'),
 errorHandler = require('errorhandler'),
    app = express();

 
var dbOperations = require("./dbOperations.js");
var logFmt = require("logfmt");

app.set('views', __dirname + '/views') ;

// Authenticator
app.use(function(req, res, next) {
    var auth;

    // check whether an autorization header was send    
    if (req.headers.authorization) {
      // only accepting basic auth, so:
      // * cut the starting "Basic " from the header
      // * decode the base64 encoded username:password
      // * split the string at the colon
      // -> should result in an array
      auth = new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
    }

    // checks if:
    // * auth array exists 
    // * first value matches the expected user 
    // * second value the expected password
    if (!auth || auth[0] !== 'rpadmin@redpointcrm.com' || auth[1] !== 'MM8-TCG-m2e-vks') {
        // any of the tests failed
        // send an Basic Auth request (HTTP Code: 401 Unauthorized)
        res.statusCode = 401;
        // MyRealmName can be changed to anything, will be prompted to the user
        res.setHeader('WWW-Authenticate', 'Basic realm="MyRealmName"');
        // this will displayed in the browser when authorization is cancelled
        res.end('Unauthorized');
    } else {
        // continue with processing, user was authenticated
        next();
    }
});

app.use(bodyParser.json());         
app.use(bodyParser.urlencoded({ extended: true })); 

app.get('/' , function(req,res) {
    res.sendfile('views/index.html');
} );

app.get('/db/readRecords', function(req,res){
    dbOperations.getRecords(req,res);
});

app.get('/db/getRecordsForForm', function(req,res){
    dbOperations.getRecordsForForm(req,res);
});

app.get('/db/getRecordsForForms', function(req,res){
    dbOperations.getRecordsForForms(req,res);
});

app.get('/db/getRecordsForSeason', function(req,res){
    dbOperations.getRecordsForSeason(req,res);
});

app.get('/db/addRecord', function(req,res){
    dbOperations.addRecord(req,res);
});

app.post('/db/upsertRecords', function(req,res){
    dbOperations.upsertRecords(req,res);
});

app.get('/db/deleteRecord', function(req,res){
    dbOperations.delRecord(req,res);
});

app.get('/db/getConfirmationProductsForConfirm', function(req,res){
    dbOperations.getConfirmationProductsForConfirm(req,res);
});

app.get('/db/getConfirmationProduct', function(req,res){
    dbOperations.getConfirmationProduct(req,res);
});

app.post('/db/addConfirmationProducts', function(req,res){
    dbOperations.addConfirmationProducts(req,res);
});

app.post('/db/updateConfirmationProducts', function(req,res){
    dbOperations.updateConfirmationProducts(req,res);
});

app.get('/db/deleteConfirmationProducts', function(req,res){
    dbOperations.deleteConfirmationProducts(req,res);
});

app.set('port', process.env.PORT || 3001);

app.use(express.static(__dirname + '/client')); 
app.use(errorHandler());
app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
