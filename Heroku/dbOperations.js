module.exports = {
    getRecords: function(req, res) {    
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var query = client.query("select * from orderlines");

        query.on("row", function (row, result) { 
            result.addRow(row); 
        });

        query.on("end", function (result) {          
            client.end();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(result.rows, null, "    ") + "\n");
            res.end();  
        });
    },
    getRecordsForForm: function(req, res) {    
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var query = client.query("select * from orderlines where formid = '"+req.query.formid+"'");

        query.on("row", function (row, result) { 
            result.addRow(row); 
        });

        query.on("end", function (result) {          
            client.end();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(result.rows, null, "    ") + "\n");
            res.end();  
        });
    },
    getRecordsForForms: function(req, res) {    
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var formids = req.query.formids;
        var idArr = formids.split(",");
        var formattedIds = "(";
        var i = 0;
        for(i;i<idArr.length;i++)
        {
            var idStr = idArr[i];
            formattedIds = formattedIds + "'"+idStr+"',";
        }
        formattedIds = formattedIds.slice(0, -1);
        formattedIds = formattedIds +")";

        var query = client.query("select * from orderlines where formid in "+formattedIds);

        query.on("row", function (row, result) { 
            result.addRow(row); 
        });

        query.on("end", function (result) {          
            client.end();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(result.rows, null, "    ") + "\n");
            res.end();  
        });
    },
    getRecordsForSeason: function(req, res) {    
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var query = client.query("select * from orderlines where seasonid = '"+req.query.seasonid+"'");

        query.on("row", function (row, result) { 
            result.addRow(row); 
        });

        query.on("end", function (result) {          
            client.end();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(result.rows, null, "    ") + "\n");
            res.end();  
        });
    },
    addRecord : function(req, res){
        var pg = require('pg');  
        
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";

        var client = new pg.Client(conString);

        client.connect();
        var query = client.query("insert into orderlines (formid, seasonid, suffix, hv, code, description, qty, price, createdid, createddate, lastmodid, lastmoddate) "+ 
                                "values ('"+req.query.formid+"','"+req.query.seasonid+"','"+req.query.suffix+"','"+req.query.hv+"','"+
                                    req.query.code+"','"+req.query.desc+"','"+req.query.qty+"','"+req.query.price+"','"+req.query.createdid+"','"+
                                    req.query.createddate+"','"+req.query.lastmodid+"','"+req.query.lastmoddate+"')");
    
        query.on("end", function (result) {          
            client.end(); 
            res.write('Success');
            res.end();  
        });

    },
    upsertRecords : function(req, res){
        var pg = require('pg');  

        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);
        client.connect();

        //var arr = JSON.parse(req.query.array);
        var arr = req.body;
        //console.log("body: "+body);
        //var arr = JSON.parse(body);
        console.log("**** Array: ");
        console.log(arr);

        var queryCount = arr.length;

        var i = 0;
        for(i=0;i<arr.length;i++)
        {
            var lineno = arr[i].lineno;
            var formid = arr[i].formid;
            var seasonid = arr[i].seasonid;
            var suffix = arr[i].suffix;
            var hv = arr[i].hv;
            var paymt = arr[i].paymt;
            var code = arr[i].code;
            var description = arr[i].description;
            var qty = arr[i].qty;
            var price = arr[i].price;
            var wholesale = arr[i].wholesale;
            var total = arr[i].total;
            var createdid = arr[i].createdid;
            var createddate = arr[i].createddate;
            var lastmodid = arr[i].lastmodid;
            var lastmoddate = arr[i].lastmoddate;
            var grandtotal = arr[i].grandtotal;
            var shipping = arr[i].shipping;
            var shipvia = arr[i].shipvia;
            var discount = arr[i].discount;
            var salestax = arr[i].salestax;

            function endHandler () {
               queryCount--; // decrement count by 1
               if (queryCount === 0) {
                   // queries have ended, lets close the connection.
                   client.end();
               }
            }

            if(lineno) // update
            {
                var query = client.query("update orderlines set formid='"+formid+"', seasonid='"+seasonid+"', suffix='"+suffix+"', hv='"+
                        hv+"', paymt='"+paymt+"', code='"+code+"', description='"+description+"', qty="+qty+", price="+price+", wholesale="+
                        wholesale+", total="+total+", grandtotal="+grandtotal+", shipping="+shipping+", discount="+discount+", salestax="+
                        salestax+", lastmodid='"+lastmodid+"', lastmoddate='"+lastmoddate+"', shipvia='"+shipvia+"' where lineno="+lineno);
                query.on("end", endHandler);
            }
            else // insert
            {
                var query = client.query("insert into orderlines (formid, seasonid, suffix, hv, paymt, code, description, qty, price, wholesale, "+
                    "total, createdid, createddate, lastmodid, lastmoddate, grandtotal, shipping, discount, salestax, shipvia) "+ 
                            "values ('"+formid+"','"+seasonid+"','"+suffix+"','"+hv+"','"+paymt+"','"+
                            code+"','"+description+"',"+qty+","+price+","+wholesale+","+total+",'"+createdid+"','"+
                            createddate+"','"+lastmodid+"','"+lastmoddate+"',"+grandtotal+","+shipping+","+discount+","+salestax+",'"+shipvia+"')");
                query.on("end", endHandler);
            }
        }
        
        res.write('Success');
        res.end();
    },
    deleteRecord : function(req, res){
        var pg = require('pg');   
        
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";

        var client = new pg.Client(conString);

        client.connect();
         
        var query = client.query("Delete from orderlines where lineno = "+req.query.lineno);
    
        query.on("end", function (result) {          
            client.end(); 
            res.write('Success');
            res.end();  
        });

    },
    getConfirmationProductsForConfirm: function(req, res) {    
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var query = client.query("select * from confirmationproducts where \"Confirmation__c\" = '"+req.query.confirmid+"'");

        query.on("row", function (row, result) { 
            result.addRow(row); 
        });

        query.on("end", function (result) {          
            client.end();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(result.rows, null, "    ") + "\n");
            res.end();  
        });
    },
    getConfirmationProduct: function(req, res) {    
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var query = client.query("select * from confirmationproducts where \"Id\" = '"+req.query.recordId+"'");

        query.on("row", function (row, result) { 
            result.addRow(row); 
        });

        query.on("end", function (result) {          
            client.end();
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(JSON.stringify(result.rows, null, "    ") + "\n");
            res.end();  
        });
    },
    addConfirmationProducts : function(req, res){
        var pg = require('pg');  
        
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";

        var client = new pg.Client(conString);

        client.connect();

        var cp = require("./confirmationproduct.js");
        

        var pgp = require('pg-promise')({
           capSQL: false, // if you want all generated SQL capitalized
           pgFormatting: true
        });

        var arr = req.body;
        var queryCount = arr.length;

        var i = 0;
        for(i=0;i<arr.length;i++)
        {
            var cpFields = cp.getFields(arr[i]);
            console.log(cpFields);

            var queryString = pgp.helpers.insert(arr[i], cpFields, 'confirmationproducts');
            console.log(queryString);

            function endHandler () {
               queryCount--; // decrement count by 1
               if (queryCount === 0) {
                   // queries have ended, lets close the connection.
                   client.end();
               }
            }

            var query = client.query(queryString, function(err, result) {
                if (err) 
                {
                    client.end();
                    res.writeHead(500);
                    res.write(err.message);
                    res.end();
                }
            });
            query.on("end", endHandler);
        }

        // var queryString = pgp.helpers.insert(req.body, cpFields, 'confirmationproducts');
        // console.log(queryString);

        // var query = client.query(queryString, function(err, result) {
        //     if (err) 
        //     {
        //         client.end();
        //         res.writeHead(500);
        //         res.write(err.message);
        //         res.end();
        //     }
        // });
        // query.on("end", function (result) {          
        //     client.end(); 
        //     res.write('Success');
        //     res.end();  
        // });

        res.write('Success');
        res.end();
    },
    updateConfirmationProducts: function(req, res) {  
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var cp = require("./confirmationproduct.js");
        var cpFields = cp.getUpdateFields(req.body);
        var formattedFields = "";
        var i = 0;
        for(i;i<cpFields.length;i++)
        {
            formattedFields = formattedFields +cpFields[i]+",";
        }
        formattedFields = formattedFields.slice(0, -1);
       
        var queryString = "update confirmationproducts set " + formattedFields + " WHERE \"Id\" = '" + req.body[0].Id + "'";
        console.log(queryString);

        var query = client.query(queryString, function(err, result) {
            if (err) 
            {
                client.end();
                res.writeHead(500);
                res.write(err.message);
                res.end();
            }
        });

        query.on("end", function (result) {          
            client.end(); 
            res.write('Success');
            res.end();  
        });  
    },
    deleteConfirmationProducts : function(req, res){
        var pg = require('pg');  
      
        var conString = process.env.DATABASE_URL || "postgres://kgakmomflwdrws:fbdeb1b27d9e18e1ae46fbd26f1087cb3fd3ff14f494675aaa87d0730003ab02@ec2-23-23-111-171.compute-1.amazonaws.com:5432/d8pc9tmnvbtl2c";
        var client = new pg.Client(conString);

        client.connect();

        var confirmids = req.query.confirmids;

        var idArr = confirmids.split(",");
        var formattedIds = "(";
        var i = 0;
        for(i;i<idArr.length;i++)
        {
            var idStr = idArr[i];
            formattedIds = formattedIds + "'"+idStr+"',";
        }
        formattedIds = formattedIds.slice(0, -1);
        formattedIds = formattedIds +")";

        var queryString = "delete from confirmationproducts where \"Id\" in "+formattedIds;
        console.log("**** Delete query: "+queryString);
        
        var query = client.query(queryString, function(err, result) {
            if (err) 
            {
                client.end();
                res.writeHead(500);
                res.write(err.message);
                res.end();
            }
        });
    
        query.on("end", function (result) {          
            client.end(); 
            res.write('Success');
            res.end();  
        });
    }
};