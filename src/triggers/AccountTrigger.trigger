trigger AccountTrigger on Account (before insert, before update)
{
	if(Trigger.isInsert)
		AccountTriggerHandler.handleInsert(Trigger.new);

	if(Trigger.isUpdate)
		AccountTriggerHandler.handleUpdate(Trigger.new, Trigger.oldMap);
}