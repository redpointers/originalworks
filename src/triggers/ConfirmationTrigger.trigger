trigger ConfirmationTrigger on Confirmation__c (before insert, after insert) 
{
	if(trigger.isBefore)
		ConfirmationTriggerHandler.handleBeforeInsert(Trigger.new);

	if(trigger.isAfter)
		ConfirmationTriggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);
}