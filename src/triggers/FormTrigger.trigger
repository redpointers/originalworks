trigger FormTrigger on Form__c (before insert, before update) {

	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			FormTriggerHandler.handleBeforeInsert(Trigger.new);
		} else if(Trigger.isUpdate) {
			FormTriggerHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
		}
	}
}