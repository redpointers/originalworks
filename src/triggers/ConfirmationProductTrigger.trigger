trigger ConfirmationProductTrigger on ConfirmationProduct__c (after insert, after update) 
{
	if(Trigger.isInsert)
		ConfirmationProductTriggerHandler.handleAfterInsert(Trigger.newMap);

	if(Trigger.isUpdate)
		ConfirmationProductTriggerHandler.handleAfterUpdate(Trigger.newMap);
}