trigger OrderLineTrigger on Order__c (after insert) 
{
	OrderLineTriggerHandler.handleAfterInsert(Trigger.new);
}