<apex:page standardController="Confirmation__c" extensions="DearFamilyLetterExtension" showHeader="false" showQuickActionVfHeader="false" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false" docType="html-5.0" renderAs="pdf">
	<html>
		<head>
			<style type="text/css" media="print">
				@page {
					size: 11in 8.5in;
	        		margin-top: 1cm;

	        		@bottom-left {
	        			font-size:80%;
	        			color:#6E6E6E;
	        			padding-left:225px;
            			content: "SC: {!Info.Account__r.SchoolCode__c}";
        			}

        			@bottom-right {
	        			font-size:80%;
	        			color:#6E6E6E;
	        			padding-right:225px;
            			content: "SC: {!Info.Account__r.SchoolCode__c}";
        			}
				}

				body {
					font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
					font-size:10pt;
				}

				.right {
					float:left;
					font-size:10pt;
					padding-right:2px;
					width:45%;
				}

				.left {
					float:left;
					font-size:10pt;
					padding-left:60px;
					width:45%;
					padding-top:25px;
				}

	    		table {
	    			width:100%;
	    			border-collapse: collapse;
	    		}

	    		table,td {
	    			font-size:10pt;
	    			padding:5px;
	    			border:solid 1px #1C1C1C;
	    		}

	    		.leftBox {
	    			border:solid 1px #000000;
	    			padding:2px;
	    			font-size:9pt;
	    		}


	    		.headColumn {
	    			text-align:center;
	    		}
			</style>
		</head>
		<body>
			<div class="right">
				<p>Estimada Familia:</p>
				<p>
					{!Info.Account__r.Name} ha seleccionado a Original Works, el programa de arte n° 1 en todo el pa&iacute;s, para ofrecerle un programa muy especial en 
					nuestra escuela. El programa Original Works le permite conservar el trabajo art&iacute;stico de su hijo o hija imprimi&eacute;ndolo en diversos productos 
					a todo color y con profesional.
				</p>
				<div class="leftBox">
					Acompa&ntilde;ando la presente carta encontrar&aacute; un im&aacute;n decorativo de muestra con el trabajo art&iacute;stico de su hijo. Puede:
					<apex:outputPanel id="sbybPrice" rendered="{!Confirmation__c.RecordType.Name == 'SBYB'}">
						<ol>
							<li>Comprar y quedarse con el im&aacute;n decorativo que acompa&ntilde;a la presente por $6.00 <br/> O</li>
							<li>Devolver el im&aacute;n decorativo sin ninguna obligaci&oacute;n.</li>
							<li>
								Tambi&eacute;n puede ordenar una gran variedad de productos adicionales. <br/> Consulte la lista de precious de los productos y el folleto
								que la acompa&ntilde;a, para obtener m&aacute;s informaci&oacute;n.
							</li>
						</ol>
					</apex:outputPanel>
					<apex:outputPanel id="sssbybPrice" rendered="{!Confirmation__c.RecordType.Name == 'SS SBYB'}">
						<ol>
							<li>Comprar y quedarse con el im&aacute;n decorativo que acompa&ntilde;a la presente por $6.50 <br/> O</li>
							<li>Devolver el im&aacute;n decorativo sin ninguna obligaci&oacute;n.</li>
							<li>
								Tambi&eacute;n puede ordenar una gran variedad de productos adicionales. <br/> Consulte la lista de precious de los productos y el folleto
								que la acompa&ntilde;a, para obtener m&aacute;s informaci&oacute;n.
							</li>
						</ol>
					</apex:outputPanel>
				</div>
				<p style="text-align:center">INSTRUCCIONES PARA REALIZAR PEDIDOS:</p>
				<ol>
					<li><strong>FORMULARIO DE PEDIDO</strong>. Complete toda la informaci&oacute;n.</li>
					<li><strong>IM&Aacute;N DE MUESTRA.</strong> Para adquirir S&Oacute;LO el im&aacute; de muestra y ninguno de los dem&aacute;s productos:
						<ol type="a">
							<li>Env&iacute;e a la escuela el FORMULARIO DE PEDIDO, junto con el pago, en la bolsa verde biodegradable con cierre o en un sobre cerrado.</li>
							<li>Conserve el im&aacute;n de muestra.</li>
						</ol>
					</li>
					<li><strong>PEDIDOS DE PRODUCTOS.</strong> Para solicitar otros productos:
						<ol type="a">
							<li>Env&iacute;e a la escuela el FORMULARIO DE PEDIDO, junto con el pago, en la bolsa verde biodegradable con cierre o en un sobre cerrado.</li>
							<li>Para comprar un iPhone 5 o Cobertura de Telefono de Galaxia S3, escriban por favor "iPhone 5" o "Galaxia S3 en la linea en blanco en el fondo de la hoja de pedido.</li>
						</ol>
					</li>
					<li>Todos los pedidos deber&aacute;n enviarse a la escuela a m&aacute;s tardar el:
						<apex:outputText style="font-weight:bold;font-size:12pt;" value="{0,date, MM/dd/yyyy}">
	                		<apex:param value="{!Info.SendLookWhatsComingLetterHome__c}"/>
	            		</apex:outputText>
					</li>
					<li><strong>DEVOLUCI&Oacute;N DE IMANES.</strong>Si decide <u>no</u> comprar el im&aacute; de muestra ni ning&uacute;n otro producto:
						<ol type="a">
							<li>Anote el nombre de su hijo/a y el grado al que asiste en la escuela, en la parte superior del formulario de pedido.</li>
							<li>Env&iacute;e a la escuela el formulario de pedido y el im&aacute;n de muestra en la bolsa verde biodegradable con cierre o en un sobre.</li>
						</ol>
					</li>
				</ol>
			</div>
			<div class="left">
				<apex:messages />
    			<apex:dataTable value="{!ConfirmationProductsSpanish}" var="cproduct" headerClass="headColumn">
            		<apex:column headerValue="PRODUCTO">
                		<apex:outputText value="{!conProdToOWProdMap[cproduct.Id].NameESP__c}"/>
            		</apex:column>
            		<apex:column headerValue="PRECIO">
                		<apex:outputText value="${!cproduct.DFLAdjusted__c}"/>
            		</apex:column>
        		</apex:dataTable>
	     		<p style="text-align:center">
	     			<strong>Todos los cheques deber&aacute;n librarse a la orden de:<br/>
	     			{!Info.ChecksPayable__c}<br/><br/>
	     			Para mayor informaci&oacute;n contacte con:<br/>
	     			{!DFLCoord.Name} - {!DFLCoord.DFLPhoneNumber__c}<!--{!FormatPhone}-->
	     			</strong>
	 			</p>
     		</div>
		</body>
	</html>
</apex:page>