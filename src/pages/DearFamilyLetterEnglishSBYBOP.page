<apex:page standardController="Confirmation__c" extensions="DearFamilyLetterExtension" showHeader="false" showQuickActionVfHeader="false" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false" docType="html-5.0" renderAs="pdf">
	<html>
		<head>
			<style type="text/css" media="print">
            @page {
                size: 8.5in 11in;
                margin-top: 1cm;

                @bottom-right {
                    font-size:80%;
                    color:#6E6E6E;
                    padding-left:225px;
                    content: "SC: {!Info.Account__r.SchoolCode__c}";
                }
            }

            body {
                font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
            	font-size:10pt;
            }

            table {
                width:100%;
                border-collapse: collapse;
            }

            table,td {
            	font-size:9pt;
                padding:5px;
                border:solid 1px #1C1C1C;
            }

            .box {
            	border:solid 1px #000000;
            }

            .contact {
            	text-align:center;
            	border:solid 1px #000000;
            }
        </style>
		</head>
		<body>
			<p>Dear Family:</p>
        <p>
            We are pleased to announce that {!Info.Account__r.Name} has selected ORIGINAL WORKS to run a very special program at our school. Through the Original Works 
            program, you will have the opportunity to purchase a variety of products featuring your child's very own artwork.
        </p>
        <div class="box">
        	<p>
        		We hope you will take advantage of the opportunity to participate in this truly unique program. For further details, please review the product &amp; price list and ordering instructions below:
        	</p>
        	<ol>
        		<li>Completely fill out the enclosed <strong>ORDER FORM.</strong> Place a "T" on back of art to indicate TOP.</li>
        		<li>Make your check payable to: <strong>{!Info.ChecksPayable__c}</strong></li>
        		<li>
        			Place the ORDER FORM, PAYMENT and ARTWORK in the green biodegradable poly bag or in a separate envelope for safe transport 
        			back to the school.
    			</li>
        		<li>
        			Return the order to the school no later than: 
        			<strong>
        				<apex:outputText value="{0,date, MMMM dd',' yyyy}">
                    		<apex:param value="{!Info.SendLookWhatsComingLetterHome__c}"/>
                		</apex:outputText>
    				</strong>
    			</li>
        		<li>
        			<em>
        				Please note that images on products such as Magnets, Mouse Pads &amp; Purse Mirrors are produced full bleed. This means that names 
        				or dates placed closer than 1” from the edge of the artwork paper may be partially trimmed on the final product(s).
        			</em>
        		</li>
        	</ol>
        </div>
        <h4 style="text-align:center">PRODUCT PRICE LIST</h4>
        <table>
            <apex:repeat value="{!ConfirmationProducts}" var="cproduct">
            	<tr>
                	<td style="width:45%">{!cproduct.ProductName__c}</td>
                	<td style="width:10%;text-align:center">${!cproduct.DFLAdjusted__c}</td>
                	<td style="width:45%">{!cproduct.Description__c}</td>
            	</tr>
        	</apex:repeat>
        </table>
        <br/><br/>
        <div class="contact">
        	<p><strong>FOR FURTHER INFORMATION PLEASE CONTACT:</strong></p>
        	{!DFLCoord.Name} - {!FormatPhone} {!DFLCoord.DFLEmail__c}
        </div>
		</body>
	</html>
</apex:page>