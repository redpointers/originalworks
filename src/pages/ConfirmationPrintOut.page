<apex:page standardController="Confirmation__c"  extensions="ConfirmationDocumentExtension" showQuickActionVfHeader="false" standardStylesheets="false" renderAs="pdf">
    <apex:stylesheet value="{!URLFOR($Resource.ReportCSS, 'Confirmation.css')}"/>
    <head><!--
        <style type="text/css" media="print">
            @page {
                    size: 8.5in 11in;
                    margin:1cm;
            }

            body {
                font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size:10pt;
            }
            .columns {
                width:50%;
                float:left;
            }    
        </style>-->
    </head>
    <body>
        <div class="columns">
            <h3 style="text-align:center">{!Info.RecordType.Name} CONFIRMATION</h3>
            <br/>
            Code: {!Info.Account__r.SchoolCode__c} Season: {!Info.Season__r.Name} Date/Initials:
            <br/><br/>
            <strong>SCHOOL INFORMATION</strong><br/>
            Name: {!Info.Account__r.Name}<br/>
            County: {!Info.Account__r.County__c}<br/>
            Street: {!Info.Account__r.BillingStreet}<br/>
            City/St/Zip: {!Info.Account__r.BillingCity}, {!Info.Account__r.BillingState} {!Info.Account__r.BillingPostalCode}<br/>
            Phone: {!SchoolPhoneNumber}<br/>
            Fax: {!SchoolFaxNumber}<br/>
            School Type: {!Info.Account__r.Type__c}<br/>
            <p>
                If running the program as a fundraiser, please list how the funds will be used. 
                We will then print this information for you on the customized Dear Family Letter:
            </p>
            <br/><hr/><br/>
            <strong>COORDINATOR HOME INFORMATION</strong><br/>
            Name: {!Coordinator.Name}<br/>
            Telephone: {!CoordPhoneNumber}<br/>
            Cell Phone: {!CoordMobilePhoneNumber}<br/>
            Street: {!Coordinator.MailingStreet}<br/>
            City/State/Zip: {!Coordinator.MailingCity}, {!Coordinator.MailingState} {!Coordinator.MailingPostalCode}<br/>
            Email: {!Coordinator.Email}<br/><br/>
            Alter. Coordinator: {!CoordinatorTwo.Name}<br/>
            Telephone: {!CoordSecPhone}<br/><br/>
            Art Teacher: {!Art.Name}<br/>
            Telephone: {!ArtPhone}<br/>
            <br/>
            <em>(This is the name &amp; phone &#35; printed on customized letter to families)</em><br/><br/>
                DFL Coord: {!DFL.Name}<br/>
                Phone: {!DFLPhoneNumber}<br/>
            <br/>
            <strong>BILLING INFORMATION</strong><br/>
            <em>(Complete if DIFFERENT from school address)</em>
            <p>
                Organization:<br/>
                Billing Contact:<br/>
                Street:<br/>
                City/St/Zip:<br/>
                Billing Phone:<br/>
                Billing email:<br/>
            </p>
            <p><b>Additional Program Materials (i.e., lesson plans and special instructions)</b><br/>        
                {!Info.ProgramMaterials__c}
            </p>
        </div>
        <div class="columns">
            <h4 style="text-align:center">
                Original Works<br/>
                54 Caldwell Rd., Stillwater, NY 12170<br/>
                Phone 800-421-0020 FAX 518-584-9293<br/>
                www.originalworks.com
            </h4>
            <p style="text-align:center"> <apex:image url="{!$Resource.BallotBox}" width="12" height="12"/> Revision Date 
                <apex:outputText value="{0,Date, MM/dd/yyyy}">
                    <apex:param value="{!Info.LastModifiedDate}"/>
                </apex:outputText> 
            </p>
            <table>
                <tr>
                    <td>Confirmation Date:</td>
                    <td><apex:outputText value="{0,Date, MM/dd/yyyy}">
                            <apex:param value="{!Info.DateOpened__c}"/>
                        </apex:outputText>
                    </td>
                </tr>
                <tr>
                    <td>Sales Rep:</td>
                    <td>{!Info.SalesRep__r.Name}</td>
                </tr>
                <tr>
                    <td>Rep Phone:</td>
                    <td>{!RepPhoneNumber}</td>
                </tr>
                <tr>
                    <td>Rep email:</td>
                    <td>{!Info.SalesRep__r.Email}</td>
                </tr>
            </table><br/>
            <strong>PROGRAM INFORMATION</strong><br/>
            Families Make Check Payable to:<br/><br/>{!Info.ChecksPayable__c}<br/><br/>
            Number of Students Participating: {!Info.ActualPop__c}<br/>
            Last Day of Semester:
            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                <apex:param value="{!Info.LastSchoolDate__c}"/>
            </apex:outputText><br/>
            Date Classes Resume:
            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                <apex:param value="{!Info.ResumeDate__c}"/>
            </apex:outputText><br/>
            Dates School Closed: {!Info.ClosedDates__c}
            <br/><br/>
            <apex:outputPanel id="OPTable" rendered="{!Confirmation__c.RecordType.Name == 'OP'}">
                <table style="border:solid #000000;">
                    <tr>
                        <th>SCHEDULE INFORMATION</th>
                        <th>DATE</th>
                    </tr>
                    <tr>
                        <td>OWY Sends Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Artwork Begins:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.ArtworkBegins__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Announcement Letter Home:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendLookWhatsComingLetterHome__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Artwork Packages Home:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendArtworkPackagesHome__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Deadline for Family Orders</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.DeadlineForOrders__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Coordinator Sends Orders/Artwork to OWY:</b></td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.CoordinatorSendsArtworkToOWY__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Receives Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYReceivesOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Orders/Artwork</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendOrdersArtworkShippingFormToOWY__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
            <apex:outputPanel id="SBYBTable" rendered="{!Confirmation__c.RecordType.Name == 'SBYB'}">
                <table style="border:solid #000000;">
                    <tr>
                        <th>SCHEDULE INFORMATION</th>
                        <th>DATE</th>
                    </tr>
                    <tr>
                        <td>OWY Sends Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Artwork Begins:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.ArtworkBegins__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Announcement Letter Home:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendLookWhatsComingLetterHome__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Coordinator Sends artwork to OWY:</b></td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.CoordinatorSendsArtworkToOWY__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Receives artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYReceivesArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends magnets/artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives magnets/artwork</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Deadline for Family Orders</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.DeadlineForOrders__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Orders/Artwork to OWY:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendOrdersArtworkShippingFormToOWY__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Receives Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYReceivesOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
            <apex:outputPanel id="GalleryTable" rendered="{!Info.RecordType.Name == 'Gallery'}">
                <table style="border:solid #000000;">
                    <tr>
                        <th>SCHEDULE INFORMATION</th>
                        <th>DATE</th>
                    </tr>
                    <tr>
                        <td>School emails class list for artwork labels:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolEmailsClassListForArtLabels__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Artwork Begins:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.ArtworkBegins__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send "Look What's Coming Letter" Home:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendLookWhatsComingLetterHome__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Coord Sends artwork to OWY:</b></td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.CoordinatorSendsArtworkToOWY__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Receives artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYReceivesArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends magnets/artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives magnets/artwork</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Deadline for Online Orders</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                               <apex:param value="{!Info.DeadlineForOrders__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                                <tr>
                        <th>Manual Orders</th>
                    </tr>
                    <tr>
                        <td>School Submits Manual Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolSendsManualOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Receives Manual Orders:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYReceivesManualOrders__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends Completed Orders:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsCompletedOrders__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Late Orders Due:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.LateOrdersDue__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
            <apex:outputPanel id="TileWallTable" rendered="{!Confirmation__c.RecordType.Name == 'Tile Wall'}">
                <table style="border:solid #000000;">
                    <tr>
                        <th>SCHEDULE INFORMATION</th>
                        <th>DATE</th>
                    </tr>
                    <tr>
                        <td>OWY Sends Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Program Materials:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesProgramMaterials__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Artwork Begins:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.ArtworkBegins__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Announcement Letter Home:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendLookWhatsComingLetterHome__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Send Artwork Packages Home:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SendArtworkPackagesHome__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>Deadline for Family Orders</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.DeadlineForOrders__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Coordinator Sends Orders/Artwork to OWY:</b></td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.CoordinatorSendsArtworkToOWY__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Receives Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYReceivesOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>OWY Sends Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.OWYSendsOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td>School Receives Orders/Artwork:</td>
                        <td>
                            <apex:outputText value="{0,Date, MM/dd/yyyy}">
                                <apex:param value="{!Info.SchoolReceivesOrdersArtwork__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                </table>
            </apex:outputPanel>
            <p>We recommend using a trackable carrier such as UPS or FedEx for all shipments to Original Works.</p>
            <p>**Please contact your Sales representative if Dates or Pricing Needs Adjustment.**</p>
            <apex:outputPanel id="SBYBInfo" rendered="{!Confirmation__c.RecordType.Name == 'SBYB' || Confirmation__c.RecordType.Name == 'Gallery'}">
                <p>
                    School is responsible for shipping the artwork and order forms to our facility. If the school is unable to complete the program
                    and send in orders, the school will be responsible for paying 100% of the SBYB magnets produced.
                </p>
            </apex:outputPanel>
        </div>
        <h3 style="text-align:center; page-break-before:always">Product Pricing Information</h3>
        <p>
            Listed below is our current product line and associated pricing. Most programs use the suggested Retail Price, 
            however you are free to set prices that are higher or lower than the Suggested Retail Price.
        </p>
        <apex:dataTable value="{!ConfirmationProducts}" var="cproduct" style="width:100%;border-collapse:collapse">
             <apex:column headerValue="Product" style="border:solid #000000">
                 <apex:outputText value="{!cproduct.ProductName__c}"/>
             </apex:column>
             <apex:column headerValue="School Price" style="border:solid #000000;text-align:center">
                 <apex:outputText value="${!cproduct.AdjustPriceToSchool__c}"/>
             </apex:column>
            <apex:column headerValue="Suggested Retail" style="border:solid #000000;text-align:center">
                 <apex:outputText value="${!cproduct.DFLPrice__c}"/>
             </apex:column>
            <apex:column headerValue="Price To Families" style="border:solid #000000;text-align:center">
                 <apex:outputText value="${!cproduct.AdjustPriceToParent__c}"/>
             </apex:column>
         </apex:dataTable>
        <p><em>Wholesale prices are subject to change without notice.</em></p>
        <h3 style="text-align:center; page-break-before:always">FINANCE &amp; TAX INFORMATION</h3>
        <p><apex:image url="{!$Resource.BallotBox}" width="12" height="12"/>
            Yes. Our School or school district requires we issue a Purchase Order to Original Works in order to 
            be invoiced. The Purchase Order Number is:
        </p>
        <p>
            If your school is located in <b>CA, CO, FL, GA, MD, NY, SC, MI, NC, TN, TX, or VA,</b> please review the following
            and check the appropriate box.
        </p>
        <p><apex:image url="{!$Resource.BallotBox}" width="12" height="12"/>
            We are TAX EXEMPT and our tax exempt form is attached or will be mailed. We understand that without a tax 
            exempt form on file that we will be charged sales tax.
        </p>
        <p><apex:image url="{!$Resource.BallotBox}" width="12" height="12"/> Purchases made by this organization are TAXABLE. We will be charged tax based on the SCHOOL COST.</p>
        <p style="text-align:center">
            PLEASE CONSULT YOUR BUSINESS OFFICE FOR YOUR SALES TAX STATUS AND SCHOOL TAX POLICY.<br/>IT IS THE
            RESPONSIBILITY OF THE SCHOOL OR ORGANIZATION TO INCREASE THE PRICE TO FAMILIES TO COVER ANY ASSOCIATED TAX LIABILITY.
        </p>
        <p>
            Please advise your Sales Representative immediately if you need to change your scheduled dates or pricing information. If orders arrive AFTER 
            your scheduled date, order processing will be delayed until the next processing slot becomes available. This could result in orders not being 
            shipped from Original Works until after the holidays. During the Fall season Late Artwork Orders (Form C Orders) will not be processed until after the holidays.
        </p>
        <p>
            Please Note: Free shipping and handling is offered on all wholesale orders of $300.00 or more.<br/>For orders less than $300.00, a $25.00 shipping and 
            handling fee will apply and pre-payment of the order is required. 
        </p>
        Coordinator: Sign, date &amp; return (keep a copy for your records)<br/><br/>
        <p style="word-spacing:1px">Signature: _________________________________________________Date: ________________</p>
        <p style="word-spacing:1px">School Name: ___________________________________________________________________</p>
    </body>
</apex:page>