<apex:page standardController="Confirmation__c" extensions="SeasonForecastSummaryReportController" showHeader="false" sidebar="false" docType="html-5.0" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false" renderAs="pdf">
	<head>
		<title>Season Forecast Summary</title>
		<style type="text/css" media="print">
			@page {
        size: 11in 8.5in;
        margin-top: 4cm;

        @top-center {
          content: element(pageHeader);
        }

        @bottom-left {
          font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
          font-size:60%;
          content: "OWY_SeasonForecast_Summary v1.02 08/10/2016 The CRM Connection, LLC (Denver, CO)";
        }

        @bottom-center {
          font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
          font-size:60%;
          content: "page " counter(page) " of " counter(pages);
        }

        @bottom-right {
        	font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
          font-size:60%;
          content: "{!Month(Today())}/{!Day(Today())}/{!Year(Today())}";	
        }
      }

      body {
        font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
        color:#B00A0A;
      }

      div.pageHeader {
        font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
        position: running(pageHeader);
    	}

      table {
        width:100%;
        border-bottom:solid 3px #B00A0A;
        padding-top:10px;
        padding-bottom:10px;
      }

      .schoolColor {
      	color:#000000;
      }

      .col1 {
        width:25%;
      }

      .col1half {
        width:12%;
      }

      .col2, .col3, .col4, .col5, .col6, .col7, .col8, .col9 {
        width:10%;
        text-align: center;
      }

      .headerBorder, .bottomBorder {
         border-bottom:solid 1px #B00A0A;
      }

      .box {
        margin-top:15px;
        width:25%;
        border:solid 1px #B00A0A; 
        margin-left:35%;
        margin-right:30%;
        text-align:center;
      }
		</style>
	</head>
	<div class="pageHeader">
		<h2 style="text-align:center;color:#B00A0A">Season Forecast Report - Detail</h2>
		<h3 style="color:#B00A0A">Forecast Season: {!season}</h3>
		<h3 style="color:#B00A0A">Confirmation Status: {!status}</h3>
	</div>
	<body>
    {!queryConfirmations}
    {!separateByHouse}
    <apex:outputText value="{!DetailHouseReps}" escape="false"/>
    <table>
      <tbody>
        <tr>
          <td class="col1" colspan="2">House Totals:</td>
          <td class="col2">
            <apex:outputText value="{0,Number,###,###,###,###}">
              <apex:param value="{!FLOOR(housePopulationTotal[0])}"/>
            </apex:outputText>
          </td>
          <td class="col3">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!housePopulationTotal[1]}"/>
            </apex:outputText>
          </td>
          <td class="col4">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[2]}"/>
            </apex:outputText>
          </td>
          <td class="col5">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!housePopulationTotal[4]}"/>
            </apex:outputText>
          </td>
          <td class="col6">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!housePopulationTotal[3]}"/>
            </apex:outputText>
          </td>
          <td class="col7">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[5]}"/>
            </apex:outputText>
          </td>
          <td class="col8">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!housePopulationTotal[6]}"/>
            </apex:outputText>
          </td>
          <td class="col9"></td>
        </tr>
      </tbody>  
    </table>
    <div class="box">
      <apex:outputText value="% OP of Total: {0,number,.00}%">
        <apex:param value="{!housePopulationTotal[7]}"/>
      </apex:outputText>
    </div>
    <apex:outputText value="{!DetailNonHouseReps}" escape="false"/>
    <table>
      <tbody>
        <tr>
          <td class="col1">House Totals:</td>
          <td class="col2">
            <apex:outputText value="{0,Number,###,###,###,###}">
              <apex:param value="{!FLOOR(nonHousePopulationTotal[0])}"/>
            </apex:outputText>
          </td>
          <td class="col3">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!nonHousePopulationTotal[1]}"/>
            </apex:outputText>
          </td>
          <td class="col4">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[2]}"/>
            </apex:outputText>
          </td>
          <td class="col5">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!nonHousePopulationTotal[4]}"/>
            </apex:outputText>
          </td>
          <td class="col6">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!nonHousePopulationTotal[3]}"/>
            </apex:outputText>
          </td>
          <td class="col7">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[5]}"/>
            </apex:outputText>
          </td>
          <td class="col8">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!nonHousePopulationTotal[6]}"/>
            </apex:outputText>
          </td>
          <td class="col9"></td>
        </tr>
      </tbody>  
    </table>
    <div class="box"> 
      <apex:outputText value="% OP of Total: {0,number,.00}%">
        <apex:param value="{!nonHousePopulationTotal[7]}"/>
      </apex:outputText>
    </div>
    <table>
      <tbody>
        <tr>
          <td class="col1">Grand Totals:</td>
          <td class="col2">
            <apex:outputText value="{0,Number,###,###,###,###}">
              <apex:param value="{!FLOOR(grandTotals[0])}"/>
            </apex:outputText>
          </td>
          <td class="col3">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[1]}"/>
            </apex:outputText>
          </td>
          <td class="col4">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[2]}"/>
            </apex:outputText>
          </td>
          <td class="col5">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[4]}"/>
            </apex:outputText>
          </td>
          <td class="col6">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[3]}"/>
            </apex:outputText>
          </td>
          <td class="col7">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[5]}"/>
            </apex:outputText>
          </td>
          <td class="col8">
            <apex:outputText value="{0,Number,Currency}">
              <apex:param value="{!grandTotals[6]}"/>
            </apex:outputText>
          </td>
          <td class="col9"></td>
        </tr>
      </tbody>
    </table>
    <table>
      <tbody>
        <tr>
          <td>Season Revenue Goal -- House:</td>
          <td>% of Goal:</td>
          <td>%</td>
        </tr>
        <tr>
          <td>Season Revenue Goal -- Total:</td>
          <td>% of Goal:</td>
          <td>%</td>
        </tr>
      </tbody>
    </table>
	</body>
</apex:page>