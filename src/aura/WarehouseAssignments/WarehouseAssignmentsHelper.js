({
	initialize : function(component) {
		var action = component.get("c.headTable");

		/***********Format the current date for the date pickers default value***********/
		var today = new Date();
		var month;
		var day;

		if((today.getMonth() + 1) < 10) {
			month  = "0" + (today.getMonth() + 1);
		} else {
			month = today.getMonth() + 1;
		}

		if(today.getDate() < 10) {
			day = "0" + today.getDate();
		} else {
			day = today.getDate();
		}

		var formatted = today.getFullYear() + "-" + month + "-" + day;
		/************************************************************************************/
		
		action.setCallback(this, function(actionResult) {
			component.set('v.mainList', actionResult.getReturnValue());
			var list = actionResult.getReturnValue();

			var viewportHeight = window.innerHeight - 280;
            var mainHeight = viewportHeight + "px";
            var subHeight = ( (viewportHeight - 142) / 2) + "px";

            document.getElementById("mainTable").style.height = mainHeight;
			document.getElementById("stillwaterTable").style.height = subHeight;
			document.getElementById("amsterdamTable").style.height = subHeight;	

			component.set('v.stillwaterDate',  formatted);
			component.set('v.amsterdamDate', formatted);

			this.getMainOrderCount(component, list);
			this.getMainValue(component, list);
			this.getMainArtwork(component, list);
	            
		});
		$A.enqueueAction(action);

		this.stillwaterTable(component, formatted); 
		this.amsterdamTable(component, formatted);
		this.setupSearch(component);
	},

	/*************************************** Buttons ***************************************/

	resetItems : function(component) {
		var searchFocus = component.find("searchDiv");

		component.set("v.searchValue",'');

		$A.util.removeClass(searchFocus, "slds-is-open"); 

		this.initialize(component);
	},

	/*************************************** Warehouse Tables ***************************************/

	resetMainTable : function(component) {
		var action = component.get("c.headTable");

		action.setCallback(this, function(actionResult) {
			component.set('v.mainList', actionResult.getReturnValue());
			var list = actionResult.getReturnValue();

			this.getMainOrderCount(component, list);
			this.getMainValue(component, list);
			this.getMainArtwork(component, list);
		});
		$A.enqueueAction(action);
	},

	stillwaterTable : function(component, changeDate) {
		var action = component.get("c.TableBasedOnTransferDate");

		action.setParams({wh:'ST', transferDate:changeDate});
		action.setCallback(this, function(actionResult) {
		
			component.set('v.stillwater', actionResult.getReturnValue());
			var list = actionResult.getReturnValue();

			this.getStillwaterOrderCount(component, list);
			this.getStillwaterValue(component, list);
			this.getStillwaterArtwork(component, list);
		});
		$A.enqueueAction(action);
	},

	amsterdamTable : function(component, changeDate) {
		var action = component.get("c.TableBasedOnTransferDate");

		action.setParams({wh:'AM', transferDate:changeDate});
		action.setCallback(this, function(actionResult) {
		
			component.set('v.amsterdam', actionResult.getReturnValue());
			var list = actionResult.getReturnValue();

			this.getAmsterdamOrderCount(component, list);
			this.getAmsterdamValue(component, list);
			this.getAmsterdamArtwork(component, list);
		});
		$A.enqueueAction(action);
	},

	/*************************************** Record Manipulation ***************************************/

	saveSelected : function(component, event) {

		var selectedRecords = [];

		selectedRecords = component.get("v.mainSelected");

	    var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
	    
	    var index = parseInt(rowIndex);           
	    var table = document.getElementById("mainTableRows");    
	    var row = table.rows[index]; 

	  	row.className = 'slds-is-selected';
		selectedRecords.push(row.id);

	    component.set("v.mainSelected", selectedRecords);
	},

	unSelected : function(component, event) {
		var selectedRecords = component.get("v.mainSelected");
	
		var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
		var index = parseInt(rowIndex);           
	    var table = document.getElementById("mainTableRows");    
	    var row = table.rows[index]; 

	  	row.className = '';
	  	
	  	var removedRow = selectedRecords.filter(function(e) { return e !== row.id });
	  	
	  	component.set("v.mainSelected", removedRow);
	},

	saveSTSelected : function(component, event) {

		var selectedRecords = [];

		selectedRecords = component.get("v.stillwaterSelected");

	    var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
	    
	    var index = parseInt(rowIndex);           
	    var table = document.getElementById("stillwaterTableRows");    
	    var row = table.rows[index]; 

	  	row.className = 'slds-is-selected';
		selectedRecords.push(row.id);

	    component.set("v.stillwaterSelected", selectedRecords);
	},

	unSTSelected : function(component, event) {
		var selectedRecords = component.get("v.stillwaterSelected");
	
		var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
		var index = parseInt(rowIndex);           
	    var table = document.getElementById("stillwaterTableRows");    
	    var row = table.rows[index]; 

	  	row.className = '';
	  	
	  	var removedRow = selectedRecords.filter(function(e) { return e !== row.id });
	  	
	  	component.set("v.stillwaterSelected", removedRow);
	},

	saveAMSelected : function(component, event) {

		var selectedRecords = [];

		selectedRecords = component.get("v.amsterdamSelected");

	    var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
	    
	    var index = parseInt(rowIndex);           
	    var table = document.getElementById("amsterdamTableRows");    
	    var row = table.rows[index]; 

	  	row.className = 'slds-is-selected';
		selectedRecords.push(row.id);

	    component.set("v.amsterdamSelected", selectedRecords);
	},

	unAMSelected : function(component, event) {
		var selectedRecords = component.get("v.amsterdamSelected");
	
		var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
		var index = parseInt(rowIndex);           
	    var table = document.getElementById("amsterdamTableRows");    
	    var row = table.rows[index]; 

	  	row.className = '';
	  	
	  	var removedRow = selectedRecords.filter(function(e) { return e !== row.id });
	  	
	  	component.set("v.amsterdamSelected", removedRow);
	},

	stillwaterWarehouse : function(component) {
		var rows = component.get("v.mainSelected");
		var transfer = component.find("stillwaterTransferDate").get("v.value");

		var action = component.get("c.toStillwater");
		action.setParams({idList:rows, tDate:transfer});

		action.setCallback(this, function(actionResult) {
			component.set("v.mainSelected", []);
			component.set("v.stillwaterSelected", []);
			this.stillwaterTable(component,transfer);
			this.resetMainTable(component);
		});
		$A.enqueueAction(action);
		
	},

	amsterdamWarehouse : function(component) {
		var rows = component.get("v.mainSelected");
		var transfer = component.find("amsterdamTransferDate").get("v.value");

		var action = component.get("c.toAmsterdam");
		action.setParams({idList:rows, tDate:transfer});

		action.setCallback(this, function(actionResult) {
			component.set("v.mainSelected", []);
			component.set("v.amsterdamSelected", []);
			this.amsterdamTable(component, transfer); 
			this.resetMainTable(component);
		});
		$A.enqueueAction(action);
	},

	undoTransferST : function(component) {
		var subtable = component.get("v.stillwaterSelected");
		var transfer = component.find("stillwaterTransferDate").get("v.value");
       
		var action = component.get("c.toMainTable");
		action.setParams({idList:subtable});
		
		action.setCallback(this, function(actionResult) {
			this.stillwaterTable(component, transfer);  
			this.resetMainTable(component);
		});
		$A.enqueueAction(action);	
	},
    
    undoTransferAM : function(component) {
		var subtable = component.get("v.amsterdamSelected");
		var transfer = component.find("amsterdamTransferDate").get("v.value");
        
		var action = component.get("c.toMainTable");
		action.setParams({idList:subtable});
		
		action.setCallback(this, function(actionResult) {
			this.amsterdamTable(component, transfer); 
			this.resetMainTable(component);
		});
		$A.enqueueAction(action);	
	},

	/*************************************** Table Summaries ***************************************/

	getMainOrderCount : function(component, list) {
		var action = component.get("c.getTableCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.mainOrder', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getMainValue : function(component, list) {
		var action = component.get("c.getTableValue");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.mainValue', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getMainArtwork : function(component, list) {
		var action = component.get("c.getArtworkCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.mainArt', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getStillwaterOrderCount : function(component, list) {
		var action = component.get("c.getTableCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.stillwaterOrder', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getStillwaterValue : function(component, list) {
		var action = component.get("c.getTableValue");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.stillwaterValue', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getStillwaterArtwork : function(component, list) {
		var action = component.get("c.getArtworkCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.stillwaterArt', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getAmsterdamOrderCount : function(component, list) {
		var action = component.get("c.getTableCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.amsterdamOrder', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getAmsterdamValue : function(component, list) {
		var action = component.get("c.getTableValue");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.amsterdamValue', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	getAmsterdamArtwork : function(component, list) {
		var action = component.get("c.getArtworkCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.amsterdamArt', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	/*************************************** Search Bar ***************************************/

	findName : function(component, event, helper) {
		var searchKey = event.getParam("searchKey");
	    var action = component.get("c.findByName");
	    
	    action.setParams({"searchKey": searchKey});
	    
	    action.setCallback(this, function(a) {
	        component.set("v.schoolCode", a.getReturnValue());
	    });
	    $A.enqueueAction(action);
	},

	search: function(component, event, helper) {
        var myEvent = $A.get("e.c:SearchKeyChange");
        myEvent.setParams({"searchKey": event.target.value});
        myEvent.fire();
    },

    setupSearch : function(component) {
    	var action = component.get("c.findByName");
        
        action.setCallback(this, function(a) {
            component.set("v.schoolCode", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    showDropdown : function(component) {
    	var searchFocus = component.find("searchDiv");

		$A.util.addClass(searchFocus, "slds-is-open");
    },

    chosenSearchValue : function(component, event) {
    	var val = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
	    var searchFocus = component.find("searchDiv");

	    component.set("v.searchValue", val);
	    component.set("v.schoolCode", []);

		$A.util.removeClass(searchFocus, "slds-is-open"); 

	    var action = component.get("c.formsBySchoolMain");
	    action.setParams({school:val});

	    action.setCallback(this, function(actionResult) {
            component.set("v.mainList", actionResult.getReturnValue());
            var list = actionResult.getReturnValue();

			this.getMainOrderCount(component, list);
			this.getMainValue(component, list);
			this.getMainArtwork(component, list);
			        
            component.set("v.stillwaterDate", null);
			component.set("v.amsterdamDate", null);
        });
        $A.enqueueAction(action);
	
		this.searchStillwater(component, val);
        this.searchAmsterdam(component, val);

    },

    searchStillwater : function(component, sch) {
    	
    	var action = component.get("c.formsBySchool");
	    action.setParams({school:sch, wh:'ST'});

	    action.setCallback(this, function(actionResult) {
            component.set("v.stillwater", actionResult.getReturnValue());
            var list = actionResult.getReturnValue();
            
			this.getStillwaterOrderCount(component, list);
			this.getStillwaterValue(component, list);
			this.getStillwaterArtwork(component, list);
        });
        $A.enqueueAction(action);
    },

    searchAmsterdam : function(component, sch) {
    	var action = component.get("c.formsBySchool");
	    action.setParams({school:sch, wh:'AM'});

	    action.setCallback(this, function(actionResult) {
            component.set("v.amsterdam", actionResult.getReturnValue());
            var list = actionResult.getReturnValue();

			this.getAmsterdamOrderCount(component, list);
			this.getAmsterdamValue(component, list);
			this.getAmsterdamArtwork(component, list);

        });
        $A.enqueueAction(action);
    }
})