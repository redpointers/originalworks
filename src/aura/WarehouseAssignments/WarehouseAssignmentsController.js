({
	doInit : function(component, event, helper) {
		helper.initialize(component);
	},

	/*************************************** Search Bar ***************************************/

	keyChange : function(component, event, helper) {
		helper.findName(component, event, helper);
	},

	searchKeyChange : function(component, event, helper) {
		helper.search(component, event, helper);
	},

	selectSearchValue : function(component, event, helper) {
		helper.chosenSearchValue(component, event);
	},

	revealDropdown : function(component, event, helper) {
		helper.showDropdown(component);    
	},

	/*************************************** Buttons ***************************************/

	transferToStillwater : function(component, event, helper) {
		helper.stillwaterWarehouse(component);
	},

	transferToAmsterdam : function(component, event, helper) {
		helper.amsterdamWarehouse(component);
	},

	transferToMainTableFromST : function(component, event, helper) {
		helper.undoTransferST(component);
	},
    
    transferToMainTableFromAM : function(component, event, helper) {
		helper.undoTransferAM(component);
	},

	clearSchool : function(component, event, helper) {
		helper.resetItems(component);
	},

	/*************************************** Transfer ***************************************/

	changeSTDate : function(component, event, helper) {
		var tDate = component.find("stillwaterTransferDate").get("v.value");
		helper.stillwaterTable(component, tDate);
	},

	changeAMDate : function(component, event, helper) {
		var tDate = component.find("amsterdamTransferDate").get("v.value");
		helper.amsterdamTable(component, tDate);
	},

	/*************************************** Record Manipulation ***************************************/

	selectRow : function(component, event, helper) {     
		helper.saveSelected(component, event);
	},

	deselectRow : function(component, event, helper) {
		helper.unSelected(component, event);
	},

	selectSTRow : function(component, event, helper) {     
		helper.saveSTSelected(component, event);
	},

	deselectSTRow : function(component, event, helper) {
		helper.unSTSelected(component, event);
	},

	selectAMRow : function(component, event, helper) {     
		helper.saveAMSelected(component, event);
	},

	deselectAMRow : function(component, event, helper) {
		helper.unAMSelected(component, event);
	}
})