({
	setup : function(component, event) {
		component.set("v.numLines", 0);

        var action = component.get("c.getOrdersForForm");

        var recId = component.get("v.recordId");
        
        action.setParams({
            "formId": recId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lines = JSON.parse(JSON.stringify(response.getReturnValue()));
                if(lines && lines.length > 0)
                {
                    var i = 0;
                    var objects = [];
                    for(i=0; i< lines.length; i++)
                    {
                        console.log("Line: "+lines[i]);
                        objects.push({lineno:lines[i].lineno, suffix:lines[i].suffix, hv:lines[i].hv, pmt:lines[i].paymt, 
                            code:lines[i].code, description:lines[i].description, qty:lines[i].qty, price:lines[i].price, 
                            total:lines[i].total, wholesale:lines[i].wholesale});
                    }
                    component.set("v.tableLines", objects);
                    component.set("v.numLines", objects.length);
                }
                else
                {
                    component.set("v.tableLines", null);
                    component.set("v.numLines", 0);
                }
            }
            else {
                console.log("Get Form Lines failed with state: " + state);
                this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
	}
})