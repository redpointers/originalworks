({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	showAll : function(component, event, helper) {
		var comps=component.find("showAllDialog");
        $A.util.removeClass(comps, "slds-hide");
        $A.util.addClass(comps, "slds-show");
	},
	closeDialog : function(component, event, helper) {
		var comps=component.find("showAllDialog");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
	}
})