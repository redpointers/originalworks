({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	selectSeason : function(component, event, helper) {
		var season = component.find("seasonSelect").get("v.value");
		component.set("v.selectedSeason", season);
	},
	cancel : function(component, event, helper) {
		var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
	},
	copy : function(component, event, helper) {
		helper.copy(component, event);
	}
})