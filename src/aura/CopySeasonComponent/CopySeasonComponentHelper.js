({
    setup : function(component, event) {

        var recordId = component.get("v.recordId");

        var action = component.get("c.getHasProducts");

        action.setParams({
            "recordId": recordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var hasProds = response.getReturnValue();
                
                if(hasProds == false)
                    this.getSeasonData(component,event);
                else
                {
                    var mess = "This Season already contains products. Other Season products may not be copied into it.";
                    component.set("v.message",mess);

                    var comps=component.find("messagePanel");
                    $A.util.removeClass(comps, "slds-hide");
                    $A.util.addClass(comps, "slds-show");

                    comps=component.find("mainPanel");
                    $A.util.removeClass(comps, "slds-show");
                    $A.util.addClass(comps, "slds-hide");

                    comps=component.find("copyButton");
                    $A.util.removeClass(comps, "slds-show");
                    $A.util.addClass(comps, "slds-hide");
                }
            }
            else {
                console.log("Get Has prods Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

    },
	getSeasonData : function(component, event) {

        var recordId = component.get("v.recordId");

		var action = component.get("c.getSeasons");

		action.setParams({
            "recordId": recordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var seasons = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.seasons", seasons);
                component.set("v.selectedSeason", seasons[0].Id);
            }
            else {
                console.log("Get Seasons Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

        var action2 = component.get("c.getCurrentSeason");

        action2.setParams({
            "recordId": recordId
        });

        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var season = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.currentSeason", season);
            }
            else {
                console.log("Get Current Season Failed with state: " + state);
            }
        });

        $A.enqueueAction(action2);
	},
    copy : function(component, event) {

        var recordId = component.get("v.recordId");
        var selectedSeason = component.get("v.selectedSeason");

        var action = component.get("c.copyProducts");

        action.setParams({
            "oldRecordId": recordId,
            "copyRecordId": selectedSeason
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var seasonId = response.getReturnValue();
                
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();

                this.viewRecord(component, event);
            }
            else {
                console.log("Get copyProducts Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

    },
    viewRecord : function(component, event) {
        var recordId = component.get("v.recordId");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else{
            window.location.href = '/one/one.app#/sObject/'+recordId+'/view'
        }
    }
})