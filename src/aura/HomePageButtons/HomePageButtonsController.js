({
	goToProductionPlanner : function(component, event, helper) {
		helper.navigateProductionPlanner(component, event);
	},

	goToWarehouse : function(component, event, helper) {
		helper.navigateWarehouseAssignments(component, event);
	},

	goToGreensheet : function(component, event, helper) {
		helper.navigateGreensheet(component, event);
	}
})