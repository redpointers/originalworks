({
	navigateProductionPlanner : function(component, event) {
    var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:ProductionPlanner",
        });
    	evt.fire();    
	},

	navigateWarehouseAssignments : function(component, event) {
		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:WarehouseAssignments",
        });
    	evt.fire();
	},

    navigateGreensheet : function(component, event) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:Greensheet",
        });
        evt.fire();
    }
})