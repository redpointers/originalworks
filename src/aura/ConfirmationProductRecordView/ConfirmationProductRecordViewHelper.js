({
	setup : function(component, event) {
		component.set("v.numLines", 0);

        var action = component.get("c.getProduct");

        var recId = component.get("v.recordId");
        
        action.setParams({
            "recordId": recId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var product = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.product", product);

                this.getLastModString(component, product);
                this.getCreatedByString(component, product);
                this.getConfirmString(component, product);
                this.getSPString(component, product);
                this.getOWPString(component, product);
            }
            else {
                console.log("Get ConfirmationProduct failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
	},
    getLastModString : function(component, product)
    {
        var action = component.get("c.getUserDateString");

        action.setParams({
            "userId" : product.LastModifiedById,
            "dtObj" :  JSON.stringify(product.LastModifiedDate)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lastMod = response.getReturnValue();
                component.set("v.lastMod", lastMod);
            }
            else {
                console.log("Get Last Mod String failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    getCreatedByString : function(component, product)
    {
        var action = component.get("c.getUserDateString");

        action.setParams({
            "userId" : product.CreatedById,
            "dtObj" :  JSON.stringify(product.CreatedDate)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var createdBy = response.getReturnValue();
                component.set("v.createdBy", createdBy);
            }
            else {
                console.log("Get createdBy String failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    getConfirmString : function(component, product)
    {
        var action = component.get("c.getConfirmationName");

        action.setParams({
            "confirmId" : product.Confirmation__c
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var confirmName = response.getReturnValue();
                component.set("v.confirmName", confirmName);
            }
            else {
                console.log("Get confirmName String failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    getSPString : function(component, product)
    {
        var action = component.get("c.getSeasonProductName");

        action.setParams({
            "spId" : product.SeasonProduct_Id__c
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var spName = response.getReturnValue();
                component.set("v.spName", spName);
            }
            else {
                console.log("Get spName String failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    getOWPString : function(component, product)
    {
        var action = component.get("c.getOWProductName");

        action.setParams({
            "owId" : product.OWProduct__c
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var owName = response.getReturnValue();
                component.set("v.owName", owName);
            }
            else {
                console.log("Get owName String failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    save : function(component, event) {
        console.log("In helper.save");
        var product = component.get("v.product");
        var action = component.get("c.updateProduct");

        action.setParams({
            "jsonObj": JSON.stringify(product)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log("**** Update Success");
            }
            else {
                console.log("Update failed with state: " + state);
                // this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    }
})