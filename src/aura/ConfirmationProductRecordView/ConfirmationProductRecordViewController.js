({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
    cancel : function(component, event, helper) {
        var product = component.get("v.product");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": product.Confirmation__c,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
    },
    save : function(component, event, helper) {
        helper.save(component, event);
    },
	goToConfirmationRecord : function(component, event, helper) {
		var product = component.get("v.product");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": product.Confirmation__c,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
	},
	goToSeasonRecord : function(component, event, helper) {
		var product = component.get("v.product");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": product.SeasonProduct_Id__c,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
	},
	goToOWRecord : function(component, event, helper) {
		var product = component.get("v.product");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": product.OWProduct__c,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
	}
})