({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	filter : function(component, event, helper) {
		helper.filter(component, event);
	},
	warehouseChange : function(component, event, helper) {
		var radioId = event.getSource().getLocalId();

        if(radioId == "allRadio")
        {
        	component.set("v.filterWHAll", true);
        	component.set("v.filterWHAM", false);
        	component.set("v.filterWHST", false);
        }
        else if(radioId == "amRadio")
        {
        	component.set("v.filterWHAll", false);
        	component.set("v.filterWHAM", true);
        	component.set("v.filterWHST", false);
        }
        else if(radioId == "stRadio")
        {
        	component.set("v.filterWHAll", false);
        	component.set("v.filterWHAM", false);
        	component.set("v.filterWHST", true);
        }
	},
    handleMenuSelect : function(component, event, helper) {
        var val = event.getParam("value");
        if(val.startsWith("Export"))
        {
            var index = val.indexOf("_");
            var formId = val.substring(index+1);
            console.log("Id: "+formId);
            helper.export(component, event, formId);
        }
        else if(val.startsWith("Scan"))
        {
            var index = val.indexOf("_");
            var formId = val.substring(index+1);
            console.log("Id: "+formId);
            helper.markScanned(component, event, formId, true);
        }
        else if(val.startsWith("UnScan"))
        {
            var index = val.indexOf("_");
            var formId = val.substring(index+1);
            console.log("Id: "+formId);
            helper.markScanned(component, event, formId, false);
        }

    }
})