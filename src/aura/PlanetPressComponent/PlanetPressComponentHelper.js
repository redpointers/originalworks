({
	setup : function(component, event) {

		var filterCode = null;
		component.set("v.filterCode",filterCode);

		var filterScanned = false;
		component.set("v.filterScanned",filterScanned);

		var filterCreated = false;
		component.set("v.filterCreated",filterCreated);

		var filterWHAll = true;
		component.set("v.filterWHAll",filterWHAll);

		var filterWHAM = false;
		component.set("v.filterWHAM",filterWHAM);

		var filterWHST = false;
		component.set("v.filterWHST",filterWHST);

		//this.toggleSpinner(component,event);

		var action = component.get("c.getForms");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var forms = JSON.parse(JSON.stringify(response.getReturnValue()));
                
                var objects = component.get("v.warehouseData");
		        if(!objects)
		            objects = [];

                var i=0;
                for(i=0;i<forms.length;i++)
                {
                	objects.push({id:forms[i].Id, code:forms[i].Confirmation__r.Account__r.SchoolCode__c, form:forms[i].FormType__c, seq:forms[i].FormSequence__c, 
                		warehouse:forms[i].WarehouseAssigned__c, wareAss:forms[i].WarehouseTransferDate__c, scanned:forms[i].isScanned__c, 
                		file:forms[i].isPPFileCreated__c});
                }

                component.set("v.tableLines", objects);
                component.set("v.formData", objects);
                component.set("v.lineCount", objects.length);
                //this.toggleSpinner(component,event);
            }
            else {
                console.log("Get Forms Failed with state: " + state);
                //this.toggleSpinner(component,event);
            	this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
	},
	filter : function(component, event) {
		var filterCode = component.get("v.filterCode");
		var filterScanned = component.get("v.filterScanned");
		var filterCreated = component.get("v.filterCreated");
		var filterWHAll = component.get("v.filterWHAll");
		var filterWHAM = component.get("v.filterWHAM");
		var filterWHST = component.get("v.filterWHST");
		var forms = component.get("v.formData");

		var useFilterCode = false;
		if(filterCode != null && filterCode.length > 0)
			useFilterCode = true;

		var wh = 'AMST';

		if(filterWHAll == true)
			wh = 'AMST';
		else if(filterWHAM == true)
			wh = 'AM';
		else if(filterWHST == true)
			wh = 'ST';

		var objects = [];

		var i=0;
        for(i=0;i<forms.length;i++)
        {
        	if(((useFilterCode && forms[i].code == filterCode) || useFilterCode == false) && 
        		wh.includes(forms[i].warehouse))
			{
				var alreadyPushed = false;
				if(filterScanned==false && alreadyPushed==false)
				{
					objects.push({id:forms[i].id, code:forms[i].code, form:forms[i].form, seq:forms[i].seq, 
    					warehouse:forms[i].warehouse, wareAss:forms[i].wareAss, scanned:forms[i].scanned,file:forms[i].file});
					alreadyPushed = true;
				}
				else
				{
					if(forms[i].scanned == true && alreadyPushed==false)
					{
						objects.push({id:forms[i].id, code:forms[i].code, form:forms[i].form, seq:forms[i].seq, 
    						warehouse:forms[i].warehouse, wareAss:forms[i].wareAss, scanned:forms[i].scanned,file:forms[i].file});
						alreadyPushed = true;
					}
				}

				if(filterCreated==false && alreadyPushed==false)
				{
					objects.push({id:forms[i].id, code:forms[i].code, form:forms[i].form, seq:forms[i].seq, 
    					warehouse:forms[i].warehouse, wareAss:forms[i].wareAss, scanned:forms[i].scanned,file:forms[i].file});
					alreadyPushed = true;
				}
				else
				{
					if(forms[i].file == true && alreadyPushed==false)
					{
						objects.push({id:forms[i].id, code:forms[i].code, form:forms[i].form, seq:forms[i].seq, 
    						warehouse:forms[i].warehouse, wareAss:forms[i].wareAss, scanned:forms[i].scanned,file:forms[i].file});
						alreadyPushed = true;
					}
				}
			}
        }

		component.set("v.tableLines", objects);

	},
    toggleSpinner: function(cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
	export : function(component, event, formId) {
		
		var formData = component.get("v.tableLines");
		var i=0;
		var thisForm = null;
		for(i=0;i<formData.length;i++)
		{
			if(formData[i].id == formId)
			{
				thisForm = formData[i];
				break;
			}
		}

		var scanned = thisForm.scanned;
		if(scanned == false)
		{
			var errors = ["The Form must be scanned before a Planet Press file can be created."];
            this.errorToast(component,event, errors);
            return;
		}

		this.toggleSpinner(component,event);

		var schoolCode = thisForm.code;
		var formType = thisForm.form;
		var seq =thisForm.seq;

		var action = component.get("c.getExport");

		action.setParams({
            "formId": formId,
            "schoolCode": schoolCode
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var data = JSON.parse(JSON.stringify(response.getReturnValue()));

                this.toggleSpinner(component,event);

                var results = encodeURI(data);
                var link = document.createElement('a');

                link.setAttribute('href', results);
                var todayDate = new Date();
                var month = ("0" + (todayDate.getMonth() + 1)).slice(-2);
                var day = ("0" + (todayDate.getDate())).slice(-2);
                var year = todayDate.getFullYear().toString().substr(2,2);
                
                link.setAttribute('download', schoolCode+"_"+formType+seq+month+day+year);

                link.click();

                // remove the row from the table
                var formData = component.get("v.tableLines");
				var i=0;
				var thisForm = null;
				for(i=0;i<formData.length;i++)
				{
					if(formData[i].id == formId)
					{
						formData.splice(i,1);
						break;
					}
				}

				component.set("v.tableLines",formData);
            }
            else {
                console.log("Get Export Failed with state: " + state);
                this.toggleSpinner(component,event);
                this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
	},
	markScanned : function(component, event, formId, mark) {
		this.toggleSpinner(component,event);

		var action = component.get("c.getMarkScanned");

		action.setParams({
            "formId": formId,
            "mark": mark
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result==true)
                {
                	var formData = component.get("v.tableLines");
					var i=0;
					var thisForm = null;
					for(i=0;i<formData.length;i++)
					{
						if(formData[i].id == formId)
						{
							thisForm = formData[i];
							break;
						}
					}

					thisForm.scanned = mark;
					component.set("v.tableLines",formData);

					formData = component.get("v.formData");
					i=0;
					thisForm = null;
					for(i=0;i<formData.length;i++)
					{
						if(formData[i].id == formId)
						{
							thisForm = formData[i];
							break;
						}
					}

					thisForm.scanned = mark;
					component.set("v.formData",formData);
                }
                this.toggleSpinner(component,event);

            }
            else {
                console.log("Get Export Failed with state: " + state);
                this.toggleSpinner(component,event);
                this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
	},
	errorToast : function(component, event, errors) {
        var errStr = '';
        if (errors)
        {
            if (errors[0])
            {
                if(errors[0].message)
                    errStr = errStr + errors[0].message;
                else
                    errStr = errStr + errors[0];
            }
            console.log("errors[0]: " + errStr);
            if (errors[1])
            {
                if(errors[1].message)
                    errStr = errStr + errors[1].message;
                else
                    errStr = errStr + errors[1];
            }
            console.log("errors[1]: " + errStr);
        }

        component.set("v.errorMessage", errStr);

        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent)
        {
            toastEvent.setParams({
                "title": "Error",
                "message": errStr,
                "type": "error"
            });
            toastEvent.fire();
        }
        else
        {
            var comps=component.find("errorDialog");
            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
    }
})