({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	cancel : function(component, event, helper) {
		var recId = component.get("v.recordId");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
	},
	toAccount : function(component, event, helper) {
		var recId = component.get("v.confirmation.Account__c");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
	},
	toSeason : function(component, event, helper) {
		var recId = component.get("v.confirmation.Season__c");

        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
	},
    closeErrorDialog : function(component, event, helper) {
        var comps=component.find("errorDialog");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
    },
    submitConfirmDialog : function(component, event, helper) {
        var addEvent = $A.get("e.c:ConfirmationEvent");
        addEvent.setParams({ "result": true });
        addEvent.fire();

        var comps=component.find("confirmDialog");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
    },
    cancelConfirmDialog : function(component, event, helper) {
        var addEvent = $A.get("e.c:ConfirmationEvent");
        addEvent.setParams({ "result": false });
        addEvent.fire();

        var comps=component.find("confirmDialog");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
    },
	save : function(component, event, helper) {
        helper.save(component,event, true);
	},
    saveAndPrint : function(component, event, helper) {
        helper.saveAndPrint(component, event);
    },
    suffixPressed : function(component, event, helper) {
        var suffix = component.get("v.suffix");
        var lastKey = suffix.slice(-1);

        if(lastKey == "+") 
        {
            suffix = suffix.substring(0,suffix.length-1);

            var hv = component.get("v.hv");

            window.setTimeout(
                $A.getCallback(function() {
                    var hvField = component.find("hvField");
                    console.log(hvField);
                    hvField.focus();
                }), 100
            );
                        
            component.set("v.suffix", suffix);
        }
        else if(lastKey == "-") // Key Pad plus
        {
            var suffixField = component.find("suffixField");
            suffixField.set("v.value",null);
        }
    },
    hvPressed : function(component, event, helper) {
        var hv = component.get("v.hv");
        var lastKey = hv.slice(-1);

        if(lastKey == "+") // Key Pad plus
        {
            hv = hv.substring(0,hv.length-1);

            window.setTimeout(
                $A.getCallback(function() {
                    var codeField = component.find("codeField");
                    codeField.focus();
                }), 100
            );
                        
            component.set("v.hv", hv);
        }
        else if(lastKey == "-") // Key Pad plus
        {
            var hvField = component.find("hvField");
            hvField.set("v.value",null);
        }
    },
    codeTabPressed : function(component, event, helper) {

        var code = component.get("v.code");
        var lastKey = code.slice(-1);

        if(lastKey == "+") // Key Pad plus
        {
            code = code.substring(0,code.length-1);

            window.setTimeout(
                $A.getCallback(function() {
                    var qtyField = component.find("qtyField");
                    qtyField.focus();
                }), 100
            );
                        
            component.set("v.code", code);

            helper.fillInLine(component, event);
        }
        else if(lastKey == "-") // Key Pad plus
        {
            var codeField = component.find("codeField");
            codeField.set("v.value",null);
        }
        else if(lastKey == "=")
        {
            helper.openProductsDialog(component, event);
        }
    },
    qtyTabPressed : function(component, event, helper) {
        var qty = component.get("v.qty");
        var lastKey = qty.slice(-1);

        if(lastKey == "+") // Key Pad plus
        {
            qty = qty.substring(0,qty.length-1);
            component.set("v.qty", qty);

            if(qty && qty > 10)
            {
                var message = "You entered a large quantity. Is that correct?";
                helper.confirmToast(component,event, message, "Large Quantity");
                return;
            }

            window.setTimeout(
                $A.getCallback(function() {
                    var suffixField = component.find("suffixField");
                    suffixField.focus();
                }), 100
            );
                        
            helper.processLine(component, event);
        }
        else if(lastKey == "-") // Key Pad plus
        {
            var qtyField = component.find("qtyField");
            qtyField.set("v.value",null);
        }
    },
    rowSelected : function(component, event, helper) {
        var targetElement = event.target || event.srcElement;

        var targetId = targetElement.id;
        if(!targetId) // bad click. Nothing to do here
            return;

        var id = "tr_"+targetElement.id;
        console.log("Id: "+id);
        var lines = component.get("v.tableLines");
        var lineSuffix = lines[targetElement.id].suffix;
        var lineId = "tr_"+lines[targetElement.id].suffix+"_"+targetElement.id;

        console.log("row dom "+lineId);
        document.getElementById(lineId).className = "slds-is-selected";

        var i =0;
        for(i =0 ; i<lines.length;i++)
        {
            var thisSuffix = lines[i].suffix;
            var thisLineId = "tr_"+lines[i].suffix+"_"+i;

            if(i != targetElement.id && lineSuffix != thisSuffix)
            {
                console.log("unselect thisLineId "+thisLineId);
                document.getElementById(thisLineId).className = "";
            }
            else if(i != targetElement.id && lineSuffix == thisSuffix)
            {
                console.log("select thisLineId "+thisLineId);
                document.getElementById(thisLineId).className = "slds-is-selected";
            }
        }

        helper.rowSelected(component, event, targetElement.id);
    },
    handleLineSummaryChange  : function(component, event, helper) {
        var comps=component.find("lineSummary");
        $A.util.toggleClass(comps, "slds-hide");
    },
    deleteAllLines : function(component, event, helper) {
        helper.deleteAllLines(component, event);
    },
    formSelected : function(component, event, helper) {
        var targetElement = event.target || event.srcElement;

        var targetId = targetElement.id;
        if(!targetId) // bad click. Nothing to do here
            return;

        var id = "form_"+targetElement.id;
        console.log("Id: "+id);
        var lines = component.get("v.forms");
        var lineForm = lines[targetElement.id].FormType__c;
        var lineSeq = lines[targetElement.id].FormSequence__c;
        var lineId = "form_"+lineForm+"_"+lineSeq;

        console.log("row dom "+lineId);
        document.getElementById(lineId).className = "slds-is-selected";

        var i =0;
        for(i =0 ; i<lines.length;i++)
        {
            var thisLineForm = lines[i].FormType__c;
            var thisLineSeq = lines[i].FormSequence__c;
            var thisLineId = "form_"+thisLineForm+"_"+thisLineSeq;

            if(i != targetElement.id && (lineForm != thisLineForm || lineSeq != thisLineSeq) )
            {
                console.log("unselect thisLineId "+thisLineId);
                document.getElementById(thisLineId).className = "";
            }
        }

        helper.formSelected(component, event, targetElement.id);
    },
    openProductsDialog : function(component, event, helper) {
        helper.openProductsDialog(component, event);
    },
    closeProductsDialog : function(component, event, helper) {
        helper.closeProductsDialog(component, event);
    },
    handleCodeChange : function(component, event, helper) {
        console.log("Got Code Change");
        var code = event.getParam("code");
        component.set("v.code", code);

        helper.closeProductsDialog(component, event);
        helper.fillInLine(component, event);

        window.setTimeout(
            $A.getCallback(function() {
                var codeField = component.find("codeField");
                codeField.focus();
            }), 100
        );
    },
    handleConfirmation : function(component, event, helper) {
        console.log("Got Confirmation");
        var result = event.getParam("result");
        component.set("v.confirmResult", true);
        
        if(result == true)
            helper.processLine(component, event);
    }
})