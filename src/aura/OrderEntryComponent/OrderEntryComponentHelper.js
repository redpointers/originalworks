({
	setup : function(component, event) {
       
        var action = component.get("c.getConfirmation");

        var recId = component.get("v.recordId");
        
        action.setParams({
            "recordId": recId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var confirm = response.getReturnValue();
                component.set("v.confirmation", response.getReturnValue());
                component.set("v.seasonId", confirm.Season__c);

                var showSBYB = false;
                var name = confirm.RecordType.Name;
                console.log("SBYB: "+name);

                if(name == "SBYB")
                    showSBYB = true;

                component.set("v.showSBYB", showSBYB);
                
                this.getProductData(component, event);

                var addEvent = $A.get("e.c:SeasonEvent");
                addEvent.setParams({ "seasonId": recId });
                addEvent.fire();
            }
            else {
                //this.toggleSpinner(component, event);
                console.log("Get Confirmation Failed with state: " + state);
                // var errors = ["Survey Questions could not be retrieved"];
                // this.errorToast(component,event, errors);
            }
        });

        $A.enqueueAction(action);

        var action2 = component.get("c.getForms");
        action2.setParams({
            "recordId": recId
        });

        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var forms = response.getReturnValue();
                component.set("v.forms", response.getReturnValue());

                if(forms && forms.length > 0)
                {
                    component.set("v.selectedForm", forms[0]);
                    this.getFormLines(component, event);
                }
                else
                {
                    this.noFormsError(component);
                }
            }
            else {
                console.log("Get Forms Failed with state: " + state);
                this.noFormsError(component);
            }
        });

        $A.enqueueAction(action2);

        component.set("v.qty", 0);
		component.set("v.pmt", "O");
        component.set("v.orderCount", 0);
        component.set("v.totalValue", 0);
        component.set("v.totalLineItems", 0);
        component.set("v.orderValue", 0);
        component.set("v.selectedSuffix", null);
        component.set("v.selectedLineItems", 0);
        component.set("v.selectedItemCount", 0);
        component.set("v.selectedSubTotal", 0);
	},
    getProductData : function(component, event) {
        var recordId = component.get("v.recordId");
        
        var action = component.get("c.getProductsForConfirmation");

        action.setParams({
            "confId": recordId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var products = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.products", products);
                //this.toggleSpinner(component, event);
            }
            else {
                //this.toggleSpinner(component, event);
                console.log("Get Product Data Failed with state: " + state);
                // var errors = ["Product data could not be retrieved"];
                // this.errorToast(component,event, errors);
            }
        });

        $A.enqueueAction(action);
    },
    fillInLine : function(component, event) {
        var code = component.get("v.code");
        var qty = component.get("v.qty");
        var products = component.get("v.products");
        var hv = component.get("v.hv");
        if(hv)
        {
            hv = hv.toUpperCase();
            component.set("v.hv", hv);
        }

        if(!hv || (hv != 'V' && hv != 'H' ))
        {
            var errors = ["H or V must be either an 'H' or a 'V'"];
            this.errorToast(component,event, errors);
            return;
        }

        if(code)
        {
            var foundCode = false;
            var i = 0;
            for(i = 0;i<products.length;i++)
            {
                if(products[i].Shortcut__c == code) 
                {
                    foundCode = true;
                    component.set("v.description", products[i].Description__c);
                    component.set("v.price", products[i].Price_to_Parent__c);
                    component.set("v.wholesale", products[i].Price_to_School__c);
                    if(qty)
                    {
                        component.set("v.total", qty*products[i].Price_to_Parent__c);
                    }

                    return;
                }
            }

            if(foundCode == false)
            {
                var errors = ["There are no matching products for that code"];
                this.errorToast(component,event, errors);
                return;
            }
        }
        else
        {
            var errors = ["You must enter a valid code value"];
            this.errorToast(component,event, errors);
            return;
        }
    },
    processLine : function(component, event) {
        var multiEntry = component.get("v.multiEntry");
        var suffix = component.get("v.suffix");
        var hv = component.get("v.hv");
        var pmt = component.get("v.pmt");
        var code = component.get("v.code");
        var description = component.get("v.description");
        var qty = component.get("v.qty");
        if(!qty)
        {
            var qtyField = component.find("qtyField");
            qty = qtyField.get("v.value");
        }

        if(!suffix || !hv || !code || !qty)
        {
            var errors = ["Suffix, H or V, Code, and Quantity must be filled in"];
            this.errorToast(component,event, errors);
            return;
        }

        if(qty && qty <= 0)
        {
            var errors = ["Qty must be greater than zero"];
            this.errorToast(component,event, errors);
            return;
        }

        var price = component.get("v.price");
        var wholesale = component.get("v.wholesale");
        component.set("v.total", qty*price);
        var total = component.get("v.total");

        var products = component.get("v.products");
        var codeToUse = null;
        for(i = 0;i<products.length;i++)
        {
            if(products[i].Shortcut__c == code) 
            {
                codeToUse = products[i].Code__c;
                break;
            }
        }

        var objects = component.get("v.tableLines");
        if(!objects)
            objects = [];

        if(multiEntry == false)
        {        
            objects.push({suffix:suffix, hv:hv, pmt:pmt, code:codeToUse, description:description, qty:qty, price:price, total:total, wholesale:wholesale});
        }
        else
        {
            var i = 0;
            for(i=0; i< qty; i++)
            {
                var thisSuffix = null;
                try
                {
                    thisSuffix = Number(suffix)+i;
                    objects.push({suffix:thisSuffix, hv:hv, pmt:pmt, code:codeToUse, description:description, qty:1, price:price, total:total, wholesale:wholesale});
                }
                catch(err)
                {
                    var errors = ["The Suffix must be a numeric value"];
                    this.errorToast(component,event, errors);
                    return;
                }
            }
        }

        component.set("v.tableLines", objects);

        this.calculateSummaryFields(component, event);
    },
    errorToast : function(component, event, errors) {
        var errStr = '';
        if (errors)
        {
            if (errors[0])
            {
                if(errors[0].message)
                    errStr = errStr + errors[0].message;
                else
                    errStr = errStr + errors[0];
            }
            console.log("errors[0]: " + errStr);
            if (errors[1])
            {
                if(errors[1].message)
                    errStr = errStr + errors[1].message;
                else
                    errStr = errStr + errors[1];
            }
            console.log("errors[1]: " + errStr);
        }

        component.set("v.errorMessage", errStr);

        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent)
        {
            toastEvent.setParams({
                "title": "Error",
                "message": errStr,
                "type": "error"
            });
            toastEvent.fire();
        }
        else
        {
            var comps=component.find("errorDialog");
            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
    },
    confirmToast : function(component, event, message, title) {
        component.set("v.confirmMessage", message);
        component.set("v.confirmTitle", title);

        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent)
        {
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": "warning"
            });
            toastEvent.fire();
        }
        else
        {
            var comps=component.find("confirmDialog");
            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
    },
    save : function(component, event, goToForm) {
        var tableLines = component.get("v.tableLines");
        var form = component.get("v.selectedForm");
        var orderCount = component.get("v.orderCount");

        console.log("*** Order Count: "+orderCount);

        if(form.GreenSheet__c != null && form.GreenSheet__c == true)
        {
            var errors = ["The selected form is a Green Sheet. Changes cannot be made to it."];
            this.errorToast(component,event, errors);
            return;
        }
        
        var action = component.get("c.saveOrders");

        action.setParams({
            "form": form,
            "inOrders": JSON.stringify(tableLines),
            "orderCount": JSON.stringify(orderCount) 
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                if(goToForm == true)
                {
                    var recId = form.Id;
                    this.goToRecord(recId); 
                }               
            }
            else {
                console.log("Save Failed with state: " + state);
                this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    rowSelected : function(component, event, selectedRow) {
        var lines = component.get("v.tableLines");
        var lineSuffix = lines[selectedRow].suffix;

        console.log("lineSuffix "+lineSuffix);

        var lineItems = 0;
        var itemCount = 0;
        var subTotal = 0;     
    
        var i =0;
        for(i =0 ; i<lines.length;i++)
        {
            var thisSuffix = lines[i].suffix;
            console.log("thisSuffix "+thisSuffix);
            if(i == selectedRow)
            {
                itemCount = itemCount + Number(lines[i].qty);
                subTotal = subTotal + lines[i].total;
                lineItems++;
            }
            else if(i != selectedRow && lineSuffix == thisSuffix)
            {
                itemCount = itemCount + Number(lines[i].qty);
                subTotal = subTotal + lines[i].total;
                lineItems++;
            }
        }

        console.log("lineItems "+lineItems+" itemCount "+itemCount+' subTotal '+subTotal);

        component.set("v.selectedSuffix", lineSuffix);
        component.set("v.selectedLineItems", lineItems);
        component.set("v.selectedItemCount", itemCount);
        component.set("v.selectedSubTotal", subTotal);
    },
    deleteAllLines : function(component, event) {
        component.set("v.tableLines", null);
        component.set("v.selectedSuffix", null);
        component.set("v.selectedLineItems", 0);
        component.set("v.selectedItemCount", 0);
        component.set("v.selectedSubTotal", 0);
        component.set("v.qty", 0);
        component.set("v.pmt", "O");
        component.set("v.orderCount", 0);
        component.set("v.totalValue", 0);
        component.set("v.totalLineItems", 0);
        component.set("v.orderValue", 0);
        component.set("v.wholesale", 0);
        component.set("v.total", 0);
    },
    formSelected : function(component, event, selectedRow) {
        var lines = component.get("v.forms");

        var selectedForm = lines[selectedRow];
    
        component.set("v.selectedForm", selectedForm);

        this.getFormLines(component, event);
    },
    getFormLines : function(component, event) {
        var selectedForm = component.get("v.selectedForm");
        var seasonId = component.get("v.seasonId");

        console.log("**** SeasonID: "+seasonId);

        var action = component.get("c.getOrdersForForm");

        action.setParams({
            "formId": selectedForm.Id 
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lines = JSON.parse(JSON.stringify(response.getReturnValue()));
                if(lines && lines.length > 0)
                {
                    var i = 0;
                    var objects = [];
                    for(i=0; i< lines.length; i++)
                    {
                        console.log("Line: "+lines[i]);
                        objects.push({lineno:lines[i].lineno, suffix:lines[i].suffix, hv:lines[i].hv, pmt:lines[i].paymt, 
                            code:lines[i].code, description:lines[i].description, qty:lines[i].qty, price:lines[i].price, 
                            total:lines[i].total, wholesale:lines[i].wholesale, seasonid:seasonId});
                    }
                    component.set("v.tableLines", objects);
                    this.calculateSummaryFields(component, event);
                }
                else
                {
                    component.set("v.tableLines", null);
                }
            }
            else {
                console.log("Get Form Lines failed with state: " + state);
                this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
    },
    openProductsDialog : function(component, event) {
        var comps=component.find("productsDialog");
        $A.util.removeClass(comps, "slds-hide");
        $A.util.addClass(comps, "slds-show");
    },
    closeProductsDialog : function(component, event) {
        var comps=component.find("productsDialog");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
    },
    calculateSummaryFields : function(component, event) {
        var objects = component.get("v.tableLines");

        var i = 0;
        var suffixArray = [];
        for (var i = 0; i < objects.length; i++) {
            suffixArray[i] = objects[i].suffix;
        }

        var uniqueArray = Array.from(new Set(suffixArray));

        var orderCount = uniqueArray.length;

        var orderValue = 0;
        var orderValueWholesale = 0;
        var lineCount = objects.length;
        
        for(i =0; i < objects.length; i++)
        {
            orderValue = orderValue+objects[i].total;
            orderValueWholesale = orderValueWholesale+objects[i].wholesale;
        }

        component.set("v.totalValue", orderValue);
        component.set("v.orderValue", orderValue);
        component.set("v.totalValueWholesale", orderValueWholesale);
        component.set("v.orderValueWholesale", orderValueWholesale);
        component.set("v.totalLineItems", lineCount);
        component.set("v.orderCount", orderCount);
    },
    noFormsError : function(component) {
        var comps=component.find("mainPanel");
        $A.util.toggleClass(comps, "slds-hide");

        var errors = ["There are no Forms associated with this Confirmation. "," Please create a Form and then process the Order Entry."];
        this.errorToast(component,event, errors);
    },
    saveAndPrint : function(component, event) {
        this.save(component, event, false);

        var selectedForm = component.get("v.selectedForm");
        var type = selectedForm.FormType__c;

        if(selectedForm.GreenSheet__c != null && selectedForm.GreenSheet__c == true)
        {
            // The save method above will show an error to the user. Just get out of here.
            return;
        }

        if(type == "A" || type == "B" || type == "C" || type == "D" || type == "E" || type == "F" || type == "G")
            this.boxSlipReport(component, event);

        if(type == "B" || type == "C" || type == "D" || type == "G")
            this.financeReport(component, event);

        if(type == "B" || type == "C" || type == "D" || type == "G")
            this.recapReport(component, event); 
    },
    goToRecord : function(recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        if(navEvt){
            navEvt.setParams({
            "recordId": recId,
            "slideDevName": "detail"
            });
            navEvt.fire();
        }
        else
        {
            sforce.one.navigateToSObject(recId,"detail");
        }
    },
    boxSlipReport : function(component, event) {
        var selectedForm = component.get("v.selectedForm");

        var url = "/apex/OrderEntryPDF?id="+selectedForm.Id;
        var navEvt = $A.get("e.force:navigateToURL");

        if(navEvt) {
            navEvt.setParams({
                "url": url
            });
            navEvt.fire();
        }
        else {
            var win = window.open(url, '_blank');
            console.log("Win: "+win);
        }
    },
    financeReport : function(component, event)
    {
        var selectedForm = component.get("v.selectedForm");

        var url = "/apex/OrderEntryFinancePDF?id="+selectedForm.Id;
        var navEvt = $A.get("e.force:navigateToURL");

        if(navEvt) {
            navEvt.setParams({
                "url": url
            });
            navEvt.fire();
        }
        else {
            var win = window.open(url, '_blank');
        }
    },
    recapReport : function(component, event)
    {
        var selectedForm = component.get("v.selectedForm");

        var url = "/apex/OrderEntryRecapPDF?id="+selectedForm.Id;
        var navEvt = $A.get("e.force:navigateToURL");

        if(navEvt) {
            navEvt.setParams({
                "url": url
            });
            navEvt.fire();
        }
        else {
            var win = window.open(url, '_blank');
        }
    }

})