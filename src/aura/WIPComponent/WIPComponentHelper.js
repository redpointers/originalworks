({
	setup : function(component, event) {

		var compData = {sbybRcvd:0, tvRcvd:0, sbybOE:0, tvOE:0, ovOE:0, sbybWH:0, tvWH:0, ovWH:0, sbybShipped:0, tvShipped:0, ovShipped:0, sbybWIP:0, tvWIP:0, ovWIP:0};
		component.set("v.companyTotals", compData);

        var action = component.get("c.getSeasons");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var seasons = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.seasons", seasons);
                this.getCurrentSeason(component, event);
            }
            else {
                console.log("Get Seasons Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);

        var action1 = component.get("c.getStartDate");

        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	var nowDate = response.getReturnValue();
            	component.set("v.thruDate",nowDate);
            }
            else {
                console.log("GetStartDate Failed with state: " + state);
            }
        });

        $A.enqueueAction(action1);

        var action2 = component.get("c.getWarehouses");

        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var warehouses = response.getReturnValue();
                component.set("v.warehouses", response.getReturnValue());

                var objects = component.get("v.warehouseData");
		        if(!objects)
		            objects = [];

                var i=0;
                for(i=0;i<warehouses.length;i++)
                {
                	objects.push({name:warehouses[i], sbybWHAssign:0, sbybShipped:0, ordersWHAssignTV:0, ordersWHShippedTV:0, ordersWHAssignOV:0, ordersWHShippedOV:0, sbybWip:0, wipTV:0, wipOV:0});
                }
            }
            else {
                console.log("Get Warehouses Failed with state: " + state);
            }
        });

        $A.enqueueAction(action2);
	},
    getCurrentSeason : function(component, event) {
        var action = component.get("c.getCurrentSeason");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var seasonId = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.currentSeason", seasonId);
                component.set("v.selectedSeason", seasonId);
                component.find("seasonSelect").set("v.value", seasonId);
            }
            else {
                console.log("Get Current Seasons Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
    },
	showData : function(component, event) {
       
       	var seasonId = component.get("v.selectedSeason");

        var action = component.get("c.getFormsForSeason");

        action.setParams({
            "seasonId": seasonId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var forms = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.forms", forms);

                var action2 = component.get("c.getConfirmsForSeason");

                action2.setParams({
                    "seasonId": seasonId
                });

                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if (component.isValid() && state === "SUCCESS") {
                        var confirms = JSON.parse(JSON.stringify(response.getReturnValue()));
                        component.set("v.confirms", confirms);

                        this.getOrders(component, event, forms, confirms);
                    }
                    else {
                        console.log("Get confirms Failed with state: " + state);
                    }
                });

                $A.enqueueAction(action2);
            }
            else {
                console.log("Get Forms Failed with state: " + state);
            }
        });

        $A.enqueueAction(action);
	},
	getOrders : function(component, event, forms, confirms) {
		this.toggleSpinner(component,event);

		var seasonId = component.get("v.selectedSeason");
		var warehouseData = component.get("v.warehouseData");
		var thruDate = component.get("v.thruDate");
		
		var action2 = component.get("c.getOrdersForSeason");
        action2.setParams({
            "seasonId": seasonId
        });

        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var orders = JSON.parse(JSON.stringify(response.getReturnValue()));

                var newWarehouseData = [];

                var w = 0;
                for(w=0;w<warehouseData.length;w++)
            	{
            		var thisWarehouse = warehouseData[w];
            		// reset data
            		thisWarehouse.sbybWHAssign = 0;
            		thisWarehouse.sbybShipped = 0;
            		thisWarehouse.sbybWip = 0;
            		thisWarehouse.ordersWHAssignTV = 0;
            		thisWarehouse.ordersWHShippedTV = 0;
            		thisWarehouse.ordersWHAssignOV = 0;
            		thisWarehouse.ordersWHShippedOV = 0;
            		thisWarehouse.wipTV = 0;
            		thisWarehouse.wipOV = 0;

            		var magsTotal = 0;

	                var formIndex=0;
	                for(formIndex=0;formIndex<forms.length;formIndex++)
	                {
	                	var formId = forms[formIndex].Id;
	                	var formWarehouse = forms[formIndex].WarehouseAssigned__c;
	                	var confirmType = forms[formIndex].Confirmation__r.RecordType.Name;
	                	var formType = forms[formIndex].FormType__c;
	                	var formShipDate = null;
	                	if(forms[formIndex].ActualShip__c != null)
	                		formShipDate = forms[formIndex].ActualShip__c;

                		if(thisWarehouse.name != formWarehouse)
                			continue; // form is not in this warehouse

                		var magsForForm = 0;
                		if(forms[formIndex].MagsBought__c!=null)
                			magsForForm = forms[formIndex].MagsBought__c;

                		// add in the SBYB magnets
                		if(confirmType == "SBYB")
                			thisWarehouse.sbybWHAssign = thisWarehouse.sbybWHAssign + magsForForm;
                		if(confirmType == "SBYB" && formShipDate != null && formShipDate <= thruDate)
		                	thisWarehouse.sbybShipped = thisWarehouse.sbybShipped + magsForForm;

                		if(formType == "B") // total up the magnets from the Form B's
	                		magsTotal = magsTotal + magsForForm;

                		var i=0;
                		for(i=0;i<orders.length;i++)
                		{
                			var order = orders[i];
                			if(order.formid == formId) // if the forms match, then we're in the right warehouse. order doesn't have warehouse, form does
                			{
                				if(confirmType == "SBYB")
		                			thisWarehouse.sbybWHAssign = thisWarehouse.sbybWHAssign+order.qty;

		                		if(confirmType == "SBYB" && formShipDate != null && formShipDate <= thruDate)
		                			thisWarehouse.sbybShipped = thisWarehouse.sbybShipped+order.qty;

                				thisWarehouse.ordersWHAssignOV = thisWarehouse.ordersWHAssignOV+order.grandtotal;
	                			
	                			if(formShipDate != null && formShipDate <= thruDate)
                					thisWarehouse.ordersWHShippedOV = thisWarehouse.ordersWHShippedOV+order.grandtotal;
	                		}
                		}
                	}

                	thisWarehouse.ordersWHAssignTV = thisWarehouse.ordersWHAssignOV+(magsTotal * 4);
                	thisWarehouse.ordersWHShippedTV = thisWarehouse.ordersWHShippedOV + (magsTotal * 4);

                	thisWarehouse.sbybWip = thisWarehouse.sbybWHAssign - thisWarehouse.sbybShipped;
                	thisWarehouse.wipTV = thisWarehouse.ordersWHAssignTV - thisWarehouse.ordersWHShippedTV;
                	thisWarehouse.wipOV = thisWarehouse.ordersWHAssignOV - thisWarehouse.ordersWHShippedOV;
	                	
	                newWarehouseData.push(thisWarehouse);
                }

                component.set("v.warehouseData", newWarehouseData);

                this.calculateCompanyTotals(component, event, newWarehouseData, confirms);

                var comps=component.find("resultsArea");
		        $A.util.removeClass(comps, "slds-hide");
		        $A.util.addClass(comps, "slds-show");

		        this.toggleSpinner(component,event);
            }
            else {
            	this.toggleSpinner(component,event);

                console.log("ShowData Failed with state: " + state);
                var errors = action.getError();
                if (errors)
                {
                    if (errors[0])
                    {
                        if(errors[0].message)
                            console.log(errors[0].message);
                        else
                           console.log(errors[0]);
                    }
                    
                    if (errors[1])
                    {
                        if(errors[1].message)
                            console.log(errors[1].message);
                        else
                            console.log(errors[1]);
                    }
                    ;
                }
            }
        });

        $A.enqueueAction(action2);
	},
	calculateCompanyTotals : function(component, event, newWarehouseData, confirms) {

        var forms = component.get("v.forms");

        var i = 0;

		// These are unknown
		var sbybRcvd = 0;
		var tvRcvd = 0;
		var sbybOE = 0;
		var tvOE = 0;
		var ovOE = 0;
        var mags = 0;

        for(i=0;i<forms.length;i++)
        {
            var form = forms[i];

            if(form.ArtworkCount__c)
                sbybRcvd = sbybRcvd + form.ArtworkCount__c;

            if(form.ArtworkCountOE__c)
                sbybOE = sbybOE + form.ArtworkCountOE__c;

            if(form.MagsBought__c)
                mags = mags + form.MagsBought__c;

            if(form.OrderValueWholesale__c && form.MagsBought__c)
                ovOE = ovOE + form.OrderValueWholesale__c - (form.MagsBought__c*4);
            else if(form.OrderValueWholesale__c)
                ovOE = ovOE + form.OrderValueWholesale__c;
        }

        for(i=0;i<confirms.length;i++)
        {
            var confirm = confirms[i];

            if(confirm.Projection__c)
                tvRcvd = tvRcvd + (confirm.Projection__c);

            if(confirm.TotalRevenue__c)
            {
                tvOE = tvOE + (confirm.TotalRevenue__c);
            }
        }

		var sbybWH = 0;
		var sbybShipped = 0;
		var tvWH = 0;
		var ovWH = 0;
		var tvShipped = 0;
		var ovShipped = 0;
		var sbybWIP = 0;
		var tvWIP = 0;
		var ovWIP = 0;

		for(i=0;i<newWarehouseData.length;i++)
		{
			var ware = newWarehouseData[i];
			sbybWH = sbybWH + ware.sbybWHAssign;
			sbybShipped = sbybShipped + ware.sbybShipped;
			tvWH = tvWH + ware.ordersWHAssignTV;
			ovWH = ovWH + ware.ordersWHAssignOV;
			tvShipped = tvShipped + ware.ordersWHShippedTV;
			ovShipped = ovShipped + ware.ordersWHShippedOV;

		}

		sbybWIP = sbybWH - sbybShipped;
		tvWIP = tvWH - tvShipped;
		ovWIP = ovWH - ovShipped;

		var compData = {sbybRcvd:sbybRcvd, tvRcvd:tvRcvd, sbybOE:sbybOE, tvOE:tvOE, ovOE:ovOE, sbybWH:sbybWH, tvWH:tvWH, ovWH:ovWH, sbybShipped:sbybShipped, 
			tvShipped:tvShipped, ovShipped:ovShipped, sbybWIP:sbybWIP, tvWIP:tvWIP, ovWIP:ovWIP};
		component.set("v.companyTotals", compData);
	},
    print: function(component, event) {
        var seasonId = component.get("v.selectedSeason");
        var thruDate = component.get("v.thruDate");
        //yyyy-MM-dd HH:mm:ss
//2017-04-14T20:23:53.789Z

       	var dateStr = thruDate.replace("T", " ");
       	var index = dateStr.indexOf(".");
       	dateStr = dateStr.substring(0,index-1);

        var url = "/apex/WIPReportPDF?seasonid="+seasonId+"&date="+dateStr;
        console.log("url: "+url);

        var navEvt = $A.get("e.force:navigateToURL");

        if(navEvt) {
            navEvt.setParams({
                "url": url
            });
            navEvt.fire();
        }
        else {
            var win = window.open(url, '_blank');
            console.log("Win: "+win);
        }
    },
    toggleSpinner: function(cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    }
})