({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	selectSeason : function(component, event, helper) {
		var season = component.find("seasonSelect").get("v.value");
		component.set("v.selectedSeason", season);
	},
	showData : function(component, event, helper) {
		helper.showData(component, event);
	},
	print : function(component, event, helper) {
		helper.print(component, event);
	}
})