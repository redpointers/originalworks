({
	suffixPressed : function(component, event, helper) {
		var suffix = component.get("v.suffix");
		var lastKey = suffix.slice(-1);

        if(lastKey == "+") 
        {
        	suffix = suffix.substring(0,suffix.length-1);

        	var hv = component.get("v.hv");

            window.setTimeout(
                $A.getCallback(function() {
                    var hvField = component.find("hvField");
                    console.log(hvField);
                    hvField.focus();
                    // hvField.set("v.value",null);
                    // hvField.set("v.placeholder",hv);
                }), 100
            );
                        
            component.set("v.suffix", suffix);
        }
        else if(lastKey == "-") // Key Pad plus
        {
        	var suffixField = component.find("suffixField");
        	suffixField.set("v.value",null);
        }
    },
    hvPressed : function(component, event, helper) {
        var hv = component.get("v.hv");
		var lastKey = hv.slice(-1);

        if(lastKey == "+") // Key Pad plus
        {
        	hv = hv.substring(0,hv.length-1);

            window.setTimeout(
                $A.getCallback(function() {
                    var codeField = component.find("codeField");
                    codeField.focus();
                }), 100
            );
                        
            component.set("v.hv", hv);
        }
        else if(lastKey == "-") // Key Pad plus
        {
        	var hvField = component.find("hvField");
        	hvField.set("v.value",null);
        }
    },
    codePressed : function(component, event, helper) {
        var code = component.get("v.code");
		var lastKey = code.slice(-1);

        if(lastKey == "+") // Key Pad plus
        {
        	code = code.substring(0,code.length-1);

            window.setTimeout(
                $A.getCallback(function() {
                    var qtyField = component.find("qtyField");
                    qtyField.focus();
                }), 100
            );
                        
            component.set("v.code", code);
        }
        else if(lastKey == "-") // Key Pad plus
        {
        	var codeField = component.find("codeField");
        	codeField.set("v.value",null);
        }
    },
    qtyPressed : function(component, event, helper) {
        var qty = component.get("v.qty");
		var lastKey = qty.slice(-1);

        if(lastKey == "+") // Key Pad plus
        {
        	qty = qty.substring(0,qty.length-1);

            window.setTimeout(
                $A.getCallback(function() {
                    var suffixField = component.find("suffixField");
                    suffixField.focus();
                }), 100
            );
                        
            component.set("v.qty", qty);
        }
        else if(lastKey == "-") // Key Pad plus
        {
        	var qtyField = component.find("qtyField");
        	qtyField.set("v.value",null);
        }
    }
})