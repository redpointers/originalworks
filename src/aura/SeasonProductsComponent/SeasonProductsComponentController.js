({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	handleSeasonChange : function(component, event, helper) {
        var seasonId = event.getParam("seasonId");
        component.set("v.seasonId", seasonId);
        helper.setup(component, event);
    },
    rowSelected : function(component, event, helper) {
        var targetElement = event.target || event.srcElement;

        var targetId = targetElement.id;
        if(!targetId) // bad click. Nothing to do here
            return;

        var id = "tr_"+targetElement.id;
        
        helper.rowSelected(component, event, targetElement.id);
    }
})