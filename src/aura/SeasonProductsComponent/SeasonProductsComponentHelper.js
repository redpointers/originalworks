({
	setup : function(component, event) {
		var confId = component.get("v.seasonId");
        
        var action = component.get("c.getProductsForConfirmation");

        action.setParams({
            "confId": confId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var products = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.sproducts", products);
            }
            else {
                //this.toggleSpinner(component, event);
                console.log("Get Product Data Failed with state: " + state);
                // var errors = ["Product data could not be retrieved"];
                // this.errorToast(component,event, errors);
            }
        });

        $A.enqueueAction(action);
	},
    rowSelected : function(component, event, selectedRow) {
        var products = component.get("v.sproducts");
        var code = products[selectedRow].Shortcut__c;
        
        var addEvent = $A.get("e.c:ProductEvent");
        addEvent.setParams({ "code": code });
        addEvent.fire();
    }
})