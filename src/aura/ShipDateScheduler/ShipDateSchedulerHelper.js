({
	/*************************************** Initialize ***************************************/
	initialize : function(component) {
		/***********Format the current date for the date pickers default value***********/
		var today = new Date();
		var month;
		var day;

		if((today.getMonth() + 1) < 10) {
			month  = "0" + (today.getMonth() + 1);
		} else {
			month = today.getMonth() + 1;
		}

		if(today.getDate() < 10) {
			day = "0" + today.getDate();
		} else {
			day = today.getDate();
		}

		var formatted = today.getFullYear() + "-" + month + "-" + day;
		/************************************************************************************/
		var action = component.get("c.dailyRevenueReport");

		action.setCallback(this, function(actionResult) {
			component.set('v.dailyRevenue', actionResult.getReturnValue());
			this.leftTableRecords(component, formatted);
			this.rightTableRecords(component, formatted);
			/*this.setupChangeLog(component);*/

			var viewportHeight = window.innerHeight - 320;
        	var height = viewportHeight + "px";

        	document.getElementById("leftTable").style.height = height;
        	document.getElementById("rightTable").style.height = height;
           
        	component.set('v.todaysLeft', formatted);
        	component.set('v.todaysRight', formatted);
		});
		$A.enqueueAction(action);
	},

	/*************************************** Table Information ***************************************/

	leftTableRecords : function(component, byDate) {
		var action = component.get("c.listLeftForms");
		action.setParams({filterDate:byDate});

		action.setCallback(this, function(actionResult) {
			component.set("v.leftList", actionResult.getReturnValue());
			var list = actionResult.getReturnValue();
        
			this.leftCountForTable(component, list);
		});
		$A.enqueueAction(action);
	},

	rightTableRecords : function(component, byDate) {
		var action = component.get("c.listRightForms");
		action.setParams({filterDate:byDate});

		var arrayDate = byDate.split('-');

		var formatDate = arrayDate[1] + '/' + arrayDate[2];

		action.setCallback(this, function(actionResult) {
			component.set("v.rightList", actionResult.getReturnValue());
			var list = actionResult.getReturnValue();
			var btn = component.find("scheduleBtn");
	
			this.rightCountForTable(component, list);
			btn.set("v.label", formatDate);
		});
		$A.enqueueAction(action);
	},

	leftCountForTable : function(component, list) {
		var action = component.get("c.rowCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.leftCount', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	rightCountForTable : function(component, list) {
		var action = component.get("c.rowCount");
		action.setParams({forms:list});

		action.setCallback(this, function(actionResult) {
			component.set('v.rightCount', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	/*************************************** Selected Information ***************************************/

	leftSelected : function(component, event) {
	    var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");

	    var index = parseInt(rowIndex);           
	    var table = document.getElementById("leftRows");    
	    var row = table.rows[index]; 
        
        for(var i = 0; i < table.rows.length; i++) {
            table.rows[i].className = '';
        }
            
	  	row.className = 'slds-is-selected';
	    component.set("v.selected", row.id);
	   	this.formLeftInformation(component, row.id);     
	},

	rightSelected : function(component, event) {
	    var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");

	    var index = parseInt(rowIndex);           
	    var table = document.getElementById("rightRows");    
	    var row = table.rows[index]; 
        
        for(var i = 0; i < table.rows.length; i++) {
            table.rows[i].className = '';
        }
            
	  	row.className = 'slds-is-selected';
	    component.set("v.selected", row.id);
	   	this.formRightInformation(component, row.id);     
	},

	formLeftInformation : function(component, form) {
		var list = component.get("v.leftList");

		var action = component.get("c.selectedInformation");
		action.setParams({forms:list, selected:form});

		action.setCallback(this, function(actionResult) {
			var returnForm = actionResult.getReturnValue();

			component.set("v.schoolCode", returnForm.Confirmation__r.Account__r.SchoolCode__c);
			component.set("v.lastDay", returnForm.Confirmation__r.LastSchoolDate__c);
			component.set("v.resumeDay", returnForm.Confirmation__r.ResumeDate__c);
			component.set("v.ftRequest", returnForm.Confirmation__r.FTType__c);
			component.set("v.closedDates", returnForm.Confirmation__r.ClosedDates__c);

			if(returnForm.Confirmation__r.RecordType.Name == 'OP') {
				component.set("v.leftOP", returnForm.Confirmation__r.Projection__c);
				component.set("v.leftSBYB", 0);
				component.set("v.leftGallery", 0);
				component.set("v.leftTile", 0);
			}

			if(returnForm.Confirmation__r.RecordType.Name == 'SBYB' || returnForm.Confirmation__r.RecordType.Name == 'SS SBYB') {
				component.set("v.leftOP", 0);
				component.set("v.leftSBYB", returnForm.Confirmation__r.Projection__c);
				component.set("v.leftGallery", 0);
				component.set("v.leftTile", 0);
			}

			if(returnForm.Confirmation__r.RecordType.Name == 'Gallery') {
				component.set("v.leftOP", 0);
				component.set("v.leftSBYB", 0);
				component.set("v.leftGallery", returnForm.Confirmation__r.Projection__c);
				component.set("v.leftTile", 0);
			}

			if(returnForm.Confirmation__r.RecordType.Name == 'Tile Wall') {
				component.set("v.leftOP", 0);
				component.set("v.leftSBYB", 0);
				component.set("v.leftGallery", 0);
				component.set("v.leftTile", returnForm.Confirmation__r.Projection__c);
			}

			component.set("v.leftTotal", returnForm.Confirmation__r.Projection__c);
		});
		$A.enqueueAction(action);	
	},

	formRightInformation : function(component, form) {
		var list = component.get("v.rightList");

		var action = component.get("c.selectedInformation");
		action.setParams({forms:list, selected:form});

		action.setCallback(this, function(actionResult) {
			var returnForm = actionResult.getReturnValue();

			component.set("v.schoolCode", returnForm.Confirmation__r.Account__r.SchoolCode__c);
			component.set("v.lastDay", returnForm.Confirmation__r.LastSchoolDate__c);
			component.set("v.resumeDay", returnForm.Confirmation__r.ResumeDate__c);
			component.set("v.ftRequest", returnForm.Confirmation__r.FTType__c);
			component.set("v.closedDates", returnForm.Confirmation__r.ClosedDates__c);

			if(returnForm.Confirmation__r.RecordType.Name == 'OP') {
				component.set("v.rightOP", returnForm.Confirmation__r.Projection__c);
				component.set("v.rightSBYB", 0);
				component.set("v.rightGallery", 0);
				component.set("v.rightTile", 0);
			}

			if(returnForm.Confirmation__r.RecordType.Name == 'SBYB' || returnForm.Confirmation__r.RecordType.Name == 'SS SBYB') {
				component.set("v.rightOP", 0);
				component.set("v.rightSBYB", returnForm.Confirmation__r.Projection__c);
				component.set("v.rightGallery", 0);
				component.set("v.rightTile", 0);
			}

			if(returnForm.Confirmation__r.RecordType.Name == 'Gallery') {
				component.set("v.rightOP", 0);
				component.set("v.rightSBYB", 0);
				component.set("v.rightGallery", returnForm.Confirmation__r.Projection__c);
				component.set("v.rightTile", 0);
			}

			if(returnForm.Confirmation__r.RecordType.Name == 'Tile Wall') {
				component.set("v.rightOP", 0);
				component.set("v.rightSBYB", 0);
				component.set("v.rightGallery", 0);
				component.set("v.rightTile", returnForm.Confirmation__r.Projection__c);
			}

			component.set("v.rightTotal", returnForm.Confirmation__r.Projection__c);
		});
		$A.enqueueAction(action);	
	},

	/*************************************** Reports and Buttons ***************************************/

	rightDateReassignment : function(component) {
		var row = component.get("v.selected");
		var changeDate = component.get("v.todaysRight");
		var recDate = component.get("v.todaysLeft");
		var list = component.get("v.leftList");
		
		var action = component.get("c.updateForm");
		action.setParams({forms:list, stringId:row, newDate:changeDate});

		$A.enqueueAction(action);	

		this.rightTableRecords(component, changeDate);
		this.leftTableRecords(component, recDate);
	},

	setupChangeLog : function(component) {
		var action = component.get("c.changeLogReport");

		action.setCallback(this, function(actionResult) {
			component.set('v.changeLog', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	}
})