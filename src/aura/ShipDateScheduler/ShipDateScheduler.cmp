<aura:component implements="force:appHostable,flexipage:availableForAllPageTypes" access="global" controller="ShipDateSchedulerController">
	<!--
		- author: Redpoint
		- purpose: The Ship Date Scheduler provides a listing of forms based on either the Actual Ship date from the Form object
		-			or the Fast Tracked ship date from the Confirmation the form is attached to. Users filter the tables using 
		-			date pickers and can give a form a new ship date by the press of the Date button. Users can also go directly
		-			to two different reports, the Daily Revenue and the Change Log. Clicking on the Daily Rev button will open a 
		-			new tab into the Daily Revenue report that will display the ship dates and total revenues for the entire current
		-			season. The Change Log will track the changes of the new and old ship dates of the forms.
		- date: 4/10/2017
		- notes: 4/18/2017: OW has determined that they do not need the Change Log at this time and has stated 
		-        that work should be stopped on creating the Log and focused elsewhere.
    -->

    <!--Initialize-->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>

    <!--Header attributes-->
    <aura:attribute name="schoolCode" type="String"/>
    <aura:attribute name="lastDay" type="Date"/>
    <aura:attribute name="resumeDay" type="Date"/>
    <aura:attribute name="ftRequest" type="Date"/>
    <aura:attribute name="closedDates" type="String"/>
    <aura:attribute name="dailyRevenue" type="String"/>
    <aura:attribute name="changeLog" type="String"/>

    <!--Table Attributes-->
    <aura:attribute name="leftList" type="List"/>
    <aura:attribute name="rightList" type="List"/>
    <aura:attribute name="todaysLeft" type="Date"/>
    <aura:attribute name="todaysRight" type="Date"/>
    <aura:attribute name="selected" type="String"/>

    <!--Summaries-->
    <aura:attribute name="leftCount" type="Integer" default="0"/>
    <aura:attribute name="leftOP" type="Double" default="0"/>
    <aura:attribute name="leftSBYB" type="Double" default="0"/>
    <aura:attribute name="leftGallery" type="Double" default="0"/>
    <aura:attribute name="leftTile" type="Double" default="0"/>
    <aura:attribute name="leftTotal" type="Double" default="0"/>
    <aura:attribute name="rightCount" type="Double" default="0"/>
    <aura:attribute name="rightOP" type="Double" default="0"/>
    <aura:attribute name="rightSBYB" type="Double" default="0"/>
    <aura:attribute name="rightGallery" type="Double" default="0"/>
    <aura:attribute name="rightTile" type="Double" default="0"/>
    <aura:attribute name="rightTotal" type="Double" default="0"/>

    <div class="slds-page-header">
    	<div class="slds-grid">
        	<div class="slds-col slds-has-flexi-truncate">
        		<div class="slds-media slds-no-space slds-grow">
                    <div class="slds-media__body">
                        <div class="slds-grid">
                        	<div class="slds-col slds-size--1-of-4">
                        		<p class="slds-text-title">Code:</p>
                        		<ui:outputText value="{!v.schoolCode}"/>
                        	</div>
                        	<div class="slds-col slds-size--1-of-4">
                        		<p class="slds-text-title">Last Day:</p>
                        		<ui:outputDate value="{!v.lastDay}" format="MM/dd/yy"/>
                        	</div>
                        	<div class="slds-col slds-size--1-of-4">
                        		<p class="slds-text-title">Resume Day:</p>
                        		<ui:outputDate value="{!v.resumeDay}" format="MM/dd/yy"/>
                        	</div>
                        	<div class="slds-col slds-size--1-of-4">
                        		<p class="slds-text-title">F/T Request:</p>
                        		<ui:outputDate value="{!v.ftRequest}" format="MM/dd/yy"/>
                        	</div>
                        	<div class="slds-col slds-size--1-of-4">
                        		<p class="slds-text-title">Closed Dates:</p>
                        		<ui:outputText value="{!v.closedDates}"/>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slds-col slds-no-flex slds-grid slds-align-top">
                <div class="slds-button-group" role="group">
                	<a class="slds-button slds-button--neutral" href="{!v.dailyRevenue}" target="_blank">Daily Revenue</a>
                	<!--<a class="slds-button slds-button-neutral" href="{!v.changeLog}" target="_blank">Change Log</a>-->
                </div>
            </div>
    	</div>
	</div>
	<div class="slds-p-vertical--small"></div>
	<div class="slds-grid">
		<div class="slds-col slds-p-horizontal--large slds-size--2-of-3 slds-text-align--right">
			<ui:inputDate aura:id="leftDate" class="datePickerWidth" format="MM/dd/yy" value="{!v.todaysLeft}" select="{!c.changeLeftListing}" displayDatePicker="true"/>
			<table class="slds-table slds-table--bordered slds-table--fixed-layout headerBorder">
				<thead class="slds-text-title--caps">
					<th scope="col" class="slds-text-align--center headercol1">Type</th>
					<th scope="col" class="slds-text-align--center headercol2">Code</th>
					<th scope="col" class="slds-text-align--center headercol3">State</th>
					<th scope="col" class="slds-text-align--center headercol4">Days</th>
					<th scope="col" class="slds-text-align--center headercol5">Proj.</th>
					<th scope="col" class="slds-text-align--center headercol6">Rec</th>
					<th scope="col" class="headercol7"></th>
					<th scope="col" class="slds-text-align--center headercol8">Ship</th>
					<th scope="col" class="slds-text-align--center headercol9">FT</th>
					<th scope="col" class="headercol10"></th>
				</thead>
			</table>
			<div aura:id="leftTable" id="leftTable" class="slds-scrollable--y bodyBorder">
				<table id="leftRows" class="slds-table slds-table--bordered slds-table--col-bordered slds-table--fixed-layout">
					<tbody>
						<aura:iteration items="{!v.leftList}" var="left" indexVar="leftIndex">
							<tr class="mouseIcon" id="{!left.Id}" data-data="{!leftIndex}" onclick="{!c.selectLeftRow}">
								<th class="slds-text-align--center col1" scope="row" data-label="Select Row">
									{!left.Confirmation__r.RecordType.Name == 'OP' ? 'O' : left.Confirmation__r.RecordType.Name == 'SBYB' ? 'S' : left.Confirmation__r.RecordType.Name == 'SS SBYB' ? 'SS' : left.Confirmation__r.RecordType.Name == 'Gallery' ? 'G' : left.Confirmation__r.RecordType.Name == 'Tile Wall' ? 'T' : ''}
								</th>
								<td class="slds-text-align--center col2">{!left.Confirmation__r.Account__r.SchoolCode__c}</td>
								<td class="slds-text-align--center col3">{!left.Confirmation__r.Account__r.BillingState}</td>
								<td class="slds-text-align--center col4">{!left.ShipDays__c}</td>
								<td class="slds-text-align--center col5">
									<ui:outputCurrency class="uiPointerEvent" value="{!left.OrderValueWholesale__c == null || left.OrderValueWholesale__c == 0 ? left.Confirmation__r.Projection__c : left.OrderValueWholesale__c}"/>
								</td>
								<td class="slds-text-align--center col6">
									<ui:outputDate class="uiPointerEvent" value="{!left.Confirmation__r.OWYReceivesOrdersArtwork__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col7">
									<ui:outputDate class="uiPointerEvent" value="{!left.ActualReceive__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col8">
									<ui:outputDate class="uiPointerEvent" value="{!left.Confirmation__r.OWYSendsOrdersArtwork__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col9">
									<ui:outputDate class="uiPointerEvent" value="{!left.Confirmation__r.FTActual__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col10">{!left.Confirmation__r.FTType__c == 'Internal' ? 'I' : ''}</td>
							</tr>
						</aura:iteration>
					</tbody>
				</table>
			</div>
			<div class="slds-grid slds-text-align--left">
				<div class="slds-col">
					<label>Count:</label>
					<ui:outputNumber value="{!v.leftCount}"/>
				</div>
				<div class="slds-col">
					<label>OP:</label>
					<ui:outputCurrency value="{!v.leftOP}"/>
				</div>
				<div class="slds-col">
					<label>SBYB:</label>
					<ui:outputCurrency value="{!v.leftSBYB}"/>
				</div>
				<div class="slds-col">
					<label>Gallery:</label>
					<ui:outputCurrency value="{!v.leftGallery}"/>
				</div>
				<div class="slds-col">
					<label>Tile Wall:</label>
					<ui:outputCurrency value="{!v.leftTile}"/>
				</div>
				<div class="slds-col">
					<label>Total:</label>
					<ui:outputCurrency value="{!v.leftTotal}"/>
				</div>
			</div>
		</div>
		<div class="slds-col slds-p-horizontal--small slds-align-middle">
			<div class="slds-grid slds-grid--vertical">
				<div class="slds-col">
					<ui:button aura:id="scheduleBtn" class="slds-button" label="" press="{!c.changeShippingDate}">
						<c:svgIcon class="slds-icon-text-default" svgPath="/resource/SLDS221/assets/icons/utility-sprite/svg/symbols.svg#forward" category="utility" size="small" name="forward"/>
                    
					</ui:button>
				</div>
			</div>
		</div>
		<div class="slds-col slds-p-horizontal--large slds-size--2-of-3 slds-text-align--right">
			<ui:inputDate aura:id="rightDate" class="datePickerWidth" format="MM/dd/yy" value="{!v.todaysRight}" select="{!c.changeRightListing}" displayDatePicker="true"/>
			<table class="slds-table slds-table--bordered slds-table--fixed-layout headerBorder">
				<thead class="slds-text-title--caps">
					<th scope="col" class="slds-text-align--center headercol1">Type</th>
					<th scope="col" class="slds-text-align--center headercol2">Code</th>
					<th scope="col" class="slds-text-align--center headercol3">State</th>
					<th scope="col" class="slds-text-align--center headercol4">Days</th>
					<th scope="col" class="slds-text-align--center headercol5">Proj.</th>
					<th scope="col" class="slds-text-align--center headercol6">Rec</th>
					<th scope="col" class="headercol7"></th>
					<th scope="col" class="slds-text-align--center headercol8">Ship</th>
					<th scope="col" class="slds-text-align--center headercol9">FT</th>
					<th scope="col" class="headercol10"></th>
				</thead>
			</table>
			<div aura:id="rightTable" id="rightTable" class="slds-scrollable--y bodyBorder">
				<table id="rightRows" class="slds-table slds-table--bordered slds-table--fixed-layout slds-table--col-bordered">
					<tbody>
						<aura:iteration items="{!v.rightList}" var="right" indexVar="rightIndex">
							<tr class="mouseIcon" id="{!right.Id}" data-data="{!rightIndex}" onclick="{!c.selectRightRow}">
								<th class="slds-text-align--center col1" scope="row" data-label="Select Row">
									{!right.Confirmation__r.RecordType.Name == 'OP' ? 'O' : right.Confirmation__r.RecordType.Name == 'SBYB' ? 'S' : right.Confirmation__r.RecordType.Name == 'SS SBYB' ? 'SS' : right.Confirmation__r.RecordType.Name == 'Gallery' ? 'G' : right.Confirmation__r.RecordType.Name == 'Tile Wall' ? 'T' : ''}
								</th>
								<td class="slds-text-align--center col2">{!right.Confirmation__r.Account__r.SchoolCode__c}</td>
								<td class="slds-text-align--center col3">{!right.Confirmation__r.Account__r.BillingState}</td>
								<td class="slds-text-align--center col4">{!right.right.ShipDays__c}</td>
								<td class="slds-text-align--center col5">
									<ui:outputCurrency class="uiPointerEvent" value="{!right.OrderValueWholesale__c == null || right.OrderValueWholesale__c == 0 ? right.Confirmation__r.Projection__c : right.OrderValueWholesale__c}"/>
								</td>
								<td class="slds-text-align--center col6">
									<ui:outputDate class="uiPointerEvent" value="{!right.Confirmation__r.OWYReceivesOrdersArtwork__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col7">
									<ui:outputDate class="uiPointerEvent" value="{!right.ActualReceive__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col8">
									<ui:outputDate class="uiPointerEvent" value="{!right.Confirmation__r.OWYSendsOrdersArtwork__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col9">
									<ui:outputDate class="uiPointerEvent" value="{!right.Confirmation__r.FTActual__c}" format="MM/dd/yy"/>
								</td>
								<td class="slds-text-align--center col10">{!right.Confirmation__r.FTType__c == 'Internal' ? 'I' : ''}</td>
							</tr>
						</aura:iteration>
					</tbody>
				</table>
			</div>
			<div class="slds-grid slds-text-align--left">
				<div class="slds-col">
					<label>Count:</label>
					<ui:outputNumber value="{!v.rightCount}"/>
				</div>
				<div class="slds-col">
					<label>OP:</label>
					<ui:outputCurrency value="{!v.rightOP}"/>
				</div>
				<div class="slds-col">
					<label>SBYB:</label>
					<ui:outputCurrency value="{!v.rightSBYB}"/>
				</div>
				<div class="slds-col">
					<label>Gallery:</label>
					<ui:outputCurrency value="{!v.rightGallery}"/>
				</div>
				<div class="slds-col">
					<label>Tile Wall:</label>
					<ui:outputCurrency value="{!v.rightTile}"/>
				</div>
				<div class="slds-col">
					<label>Total:</label>
					<ui:outputCurrency value="{!v.rightTotal}"/>
				</div>
			</div>
		</div>
	</div>
</aura:component>