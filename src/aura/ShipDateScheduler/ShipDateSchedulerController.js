({
	doInit : function(component, event, helper) {
		helper.initialize(component);
	},

	selectLeftRow : function(component, event, helper) {     
		helper.leftSelected(component, event);
	},

	selectRightRow : function(component, event, helper) {     
		helper.rightSelected(component, event);
	},

	changeLeftListing : function(component, event, helper) {
		var newDate = component.find("leftDate").get("v.value");
		helper.leftTableRecords(component, newDate);
	},

	changeRightListing : function(component, event, helper) {
		var newDate = component.find("rightDate").get("v.value");
		helper.rightTableRecords(component, newDate);
	},

	changeShippingDate : function(component, event, helper) {
		helper.rightDateReassignment(component);
	}
})