({
	doInit : function(component, event, helper) {
		helper.setup(component, event);
	},
	showAll : function(component, event, helper) {
		var comps=component.find("showAllDialog");
        $A.util.removeClass(comps, "slds-hide");
        $A.util.addClass(comps, "slds-show");
	},
	closeDialog : function(component, event, helper) {
		var comps=component.find("showAllDialog");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
	},
	showRecord : function(component, event, helper) {
		var targetElement = event.target || event.srcElement;

        var targetId = targetElement.id;
console.log("**** Targetid: "+targetId);
        if(!targetId) // bad click. Nothing to do here
            return;

		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:ConfirmationProductRecordView",
            componentAttributes: {
                recordId : targetId
            }
        });

        console.log("**** Before firing");
    	evt.fire();
	}
})