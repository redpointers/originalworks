({
	setup : function(component, event) {
		component.set("v.numLines", 0);

        var action = component.get("c.getProductsForConfirm");

        var recId = component.get("v.recordId");
        
        action.setParams({
            "confirmId": recId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var products = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.products", products);

                if(products)
                	component.set("v.numLines", products.length);
               	else
               		component.set("v.numLines", 0);
            }
            else {
                console.log("Get ConfirmationProduct Lines failed with state: " + state);
                //this.errorToast(component,event, response.getError());
            }
        });

        $A.enqueueAction(action);
	}
})