<aura:component implements="force:appHostable,flexipage:availableForAllPageTypes" access="global" controller="ProductionPlannerController">
    <!--
		- author: Redpoint
		- purpose: Takes input from a set of criteria that is used to create a SOQL query. The query
		-				results are then displayed back to the user. 
		- date: 2/1/2017
		- notes: Component is broken into three main sections, the top first section is a header that contains 
		-			the summary counts of the information shown in the table and where the buttons are located. 
		-			The middle section displays the different filters that can be applied to the data.The bottom section 
		-			is the table of form data that a user can click on get to a specific record.
  -			03/22/2017: Modified the Projection column to only show a value if the form is of type B.
    -->
    <!--Initialize-->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
    <aura:attribute name="seasons" type="List"/>
    
    <!--List view-->
    <aura:attribute name="results" type="List"/>
    <aura:attribute name="stringResults" type="String"/>
    <aura:attribute name="columnNumber" type="Integer"/>
    <aura:attribute name="csv" type="String"/>
    
    <!--Summaries-->
    <aura:attribute name="count" type="Integer" default="0"/>
    <aura:attribute name="revenue" type="Decimal" default="0"/>
    <aura:attribute name="magnetsProjected" type="Decimal" default="0"/>
    <aura:attribute name="magnetsMade" type="Double" default="0"/>
    <aura:attribute name="magnetsDollarValue" type="Decimal" default="0"/>
    <aura:attribute name="orderValue" type="Decimal" default="0"/>
    <aura:attribute name="totalValue" type="Decimal" default="0"/>
    
    <div class="slds-page-header">
        <div class="slds-grid">
            <div class="slds-col slds-has-flexi-truncate">
                <div class="slds-media slds-no-space slds-grow">
                    <div class="slds-media__body">
                        <div class="slds-grid">
                            <div class="slds-col">
                                <p class="slds-text-title">Count:</p>
                                <ui:outputNumber value="{!v.count}"/>
                            </div>
                            <div class="slds-col">
                                <p class="slds-text-title">Proj. Rev:</p>
                                <ui:outputCurrency value="{!v.revenue}"/>
                            </div>
                            <div class="slds-col">
                                <p class="slds-text-title">SBYB Mags Proj:</p>
                                <ui:outputNumber value="{!v.magnetsProjected}"/>
                            </div>
                            <div class="slds-col">
                                <p class="slds-text-title">SBYB Mags Made:</p>
                                <ui:outputNumber value="{!v.magnetsMade}"/>
                            </div>
                            <div class="slds-col">
                                <p class="slds-text-title">Mag Dollar Value:</p>
                                <ui:outputCurrency value="{!v.magnetsDollarValue}"/>
                            </div>
                            <div class="slds-col">
                                <p class="slds-text-title">Order Value:</p>
                                <ui:outputCurrency value="{!v.orderValue}"/>
                            </div>
                            <div class="slds-col">
                                <p class="slds-text-title">Total Value:</p>
                                <ui:outputCurrency value="{!v.totalValue}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slds-col slds-no-flex slds-grid slds-align-top">
                <div class="slds-button-group" role="group">
                    <ui:button class="slds-button" label="Apply Filters" press="{!c.filteredSearch}"/>
                    <ui:button class="slds-button" label="Clear" press="{!c.clearSearch}"/>
                    <ui:button class="slds-button" label="Export" press="{!c.exportQuery}"/><a href="#"/>
                </div>
            </div>
        </div>
    </div>
    <div class="slds-grid--vertical">
        <div class="slds-grid slds-text-align--center">
            <div class="slds-col slds-p-around--small slds-size--2-of-3">
                <ui:inputSelect label="Schedule Date Fields" aura:id="chosenDate">
                    <ui:inputSelectOption label="Rec. Date" text="ActualReceive__c"/>
                    <ui:inputSelectOption label="Rec. Date Proj." text="EstimatedReceived__c"/>
                    <ui:inputSelectOption label="Order Entry" text="OrderEntry__c"/>
                    <ui:inputSelectOption label="Warehouse Date" text="WarehouseTransferDate__c"/>
                    <ui:inputSelectOption label="Ship Date" text="ActualShip__c"/>
                    <ui:inputSelectOption label="Ship Date Proj." text="Confirmation__r.OWYSendOrders__c"/>
                </ui:inputSelect>
                <div class="slds-grid">
                    <div class="slds-size--2-of-3 slds-p-around--small">
                        <ui:inputDate label="Start:" aura:id="startRange" format="MM/dd/yy" displayDatePicker="true" required="true"/>
                    </div>
                    <div class="slds-size--2-of-3 slds-p-around--small">
                        <ui:inputDate label="End:" aura:id="endRange" format="MM/dd/yy" displayDatePicker="true" required="true"/>
                    </div>
                </div>
            </div>
            <div class="slds-col slds-p-around--small slds-size--1-of-2">
                <ui:inputSelect label="Season" aura:id="season">
                    <ui:inputSelectOption label="" text=""/>
                    <aura:iteration items="{!v.seasons}" var="s">
                        <ui:inputSelectOption label="{!s.Name}" text="{!s.Name}"/>
                    </aura:iteration>
                </ui:inputSelect>
                <ui:inputSelect label="WH" aura:id="warehouse" multiple="true" class="multi-View1">
                    <ui:inputSelectOption label="AM" text="AM"/>
                    <ui:inputSelectOption label="ST" text="ST"/>
                </ui:inputSelect>
            </div>
            <div class="slds-col slds-p-around--small slds-size--1-of-2">
                <ui:inputSelect label="Type" aura:id="recordType" multiple="true" class="multi-View2">
                    <ui:inputSelectOption label="Gallery" text="Gallery"/>
                    <ui:inputSelectOption label="OP" text="OP"/>
                    <ui:inputSelectOption label="SBYB" text="SBYB"/>
                    <ui:inputSelectOption label="SS SBYB" text="SS SBYB"/>
                    <ui:inputSelectOption label="Tile Wall" text="Tile Wall" click="{!c.showDiv}" dblclick="{!c.hideDiv}"/>
                </ui:inputSelect>
            </div>
            <div aura:id="tileDiv" class="hideDiv slds-col slds-p-around--small slds-size--1-of-2">
                <ui:inputSelect label="Tile Size" aura:id="tileSize" multiple="true" class="multi-View3">
                    <ui:inputSelectOption label="4x4" text="Size4x4__c"/>
                    <ui:inputSelectOption label="6x6" text="Size6x6__c"/>
                    <ui:inputSelectOption label="6x8" text="Size6x8__c"/>
                </ui:inputSelect>                
            </div>
            <div class="slds-col slds-p-around--small slds-size--1-of-2">
                <ui:inputSelect label="Form" aura:id="formType" multiple="true" class="multi-View2">
                    <ui:inputSelectOption label="A" text="A"/>
                    <ui:inputSelectOption label="B" text="B"/>
                    <ui:inputSelectOption label="C" text="C"/>
                    <ui:inputSelectOption label="D" text="D"/>
                    <ui:inputSelectOption label="E" text="E"/>
                    <ui:inputSelectOption label="F" text="F"/>
                    <ui:inputSelectOption label="G" text="G"/>
                </ui:inputSelect>
            </div>
            <div class="slds-col slds-p-around--small slds-size--1-of-2">
                <ui:inputSelect label="Customer Status" aura:id="customerStatus" multiple="true" class="multi-View2">
                    <ui:inputSelectOption label="New" text="False"/>
                    <ui:inputSelectOption label="Repeat" text="True"/>
                    <ui:inputSelectOption label="Active" text="Active"/>
                    <ui:inputSelectOption label="Inactive" text="Inactive"/>
                </ui:inputSelect>
            </div>
            <div class="slds-col slds-p-around--small slds-size--1-of-2">
                <ui:inputSelect label="Stage" aura:id="objectStatus1" multiple="true" class="multi-View2">
                    <ui:inputSelectOption label="Received" text="received"/>
                    <ui:inputSelectOption label="Order Entry" text="orderEntry"/>
                    <ui:inputSelectOption label="WH Assignment" text="whAssignment"/>
                    <ui:inputSelectOption label="Shipped" text="shipped"/>
                    <ui:inputSelectOption label="Not Received" text="n_received"/>
                    <ui:inputSelectOption label="No Order Entry" text="n_orderEntry"/>
                    <ui:inputSelectOption label="No WH Assignment" text="n_whAssignment"/>
                    <ui:inputSelectOption label="Not Shipped" text="n_shipped"/>
                </ui:inputSelect>
            </div>
        </div>
        <div class="slds-grid">
            <div class="slds-col">
                <table class="slds-table slds-table--bordered slds-table--cell-buffer slds-table--fixed-layout slds-table--compact">
                    <thead>
                        <tr class="slds-text-title--caps">
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortShipCol}">Ship Act</th>
                            <th scope="col" class="slds-text-align--center" style="cursor:pointer" onclick="{!c.sortFSTypeCol}">F/T Type</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortActualCol}">Rec Act</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortSeasonCol}">Season</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortTypeCol}">Type</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortFormCol}">Form</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortFSCol}">FS</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortCodeCol}">Code</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortProjectionCol}">Projection</th>
                            <th scope="col" class="slds-text-align--center" style="cursor:pointer" onclick="{!c.sortWHCol}">WH</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortArtworkCol}">Artwork</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortStateCol}">State</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortOECol}">OE</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortWHTranCol}">WH Tran</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortSBYBCol}">SBYB $</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortOrderCol}">Order $</th>
                            <th scope="col" style="cursor:pointer" onclick="{!c.sortSalesRepCol}">Sales Rep</th>
                        </tr>
                    </thead>
                </table>
                <div aura:id="resultsTable" id="resultsTable" class="slds-scrollable--y">
                    <table class="slds-table slds-table--cell-buffer slds-table--bordered slds-table--fixed-layout textSize">
                        <tbody>
                            <aura:iteration items="{!v.results}" var="result">
                                <tr>
                                    <th scope="row">
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputDate format="MM/dd/yy" value="{!result.ActualShip__c}"/></a>
                                    </th>
                                    <td>
                                        <a class="selectRecord slds-text-align--center" href="{!'/' + result.Id}" target="__blank">{!result.Confirmation__r.FTType__c}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputDate format="MM/dd/yy" value="{!result.ActualReceive__c}"/></a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.Confirmation__r.Season__r.Name}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.Confirmation__r.RecordType.Name}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.FormType__c}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.FormSequence__c}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.Confirmation__r.Account__r.SchoolCode__c}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputCurrency value="{!result.FormType__c == 'B' || result.FormType__c == 'D' ? result.Confirmation__r.Projection__c : ''}"/></a>
                                    </td>
                                    <td>
                                        <a class="selectRecord slds-text-align--center" href="{!'/' + result.Id}" target="__blank">{!result.WarehouseAssigned__c}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.ArtworkCount__c}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.Confirmation__r.Account__r.BillingState}</a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputDate format="MM/dd/yy" value="{!result.OrderEntry__c}"/></a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputDate format="MM/dd/yy" value="{!result.WarehouseTransferDate__c}"/></a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputCurrency value="{!result.MagValueWholesale__c}"/></a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank"><ui:outputCurrency value="{!result.OrderValueWholesale__c}"/></a>
                                    </td>
                                    <td>
                                        <a class="selectRecord" href="{!'/' + result.Id}" target="__blank">{!result.Confirmation__r.SalesRep__r.Name}</a>
                                    </td>
                                </tr>
                            </aura:iteration>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</aura:component>