({
    doInit : function(component, event, helper) {
        var action = component.get("c.getSeasons");
        
        action.setCallback(this, function(actionResult) {
            component.set('v.seasons', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
    },
    
	filteredSearch : function(component, event, helper) {
        var start = component.find("startRange").get("v.value");
        var end = component.find("endRange").get("v.value");
        
        if(start == null || end == null) {
            helper.requiredFieldsMessage(component, event);
        } else {  
            helper.getQueryResults(component);
            helper.getCount(component);
            helper.getProjectedRevenue(component);
            helper.getNumberOfMagnetsMade(component);
            helper.getMagnetsProjectedPrice(component);
            helper.getMagnetsDollarValue(component);
            helper.getOrderValue(component);
            helper.getTotalValue(component);
            helper.getStringQuery(component);
        }
	},
    
    hideModal : function(component, event, helper) {
        helper.concealModal(component);
    },
    
    clearSearch : function(component, event, helper) {
        helper.clearResults(component);
    },
    
    exportQuery : function(component, event, helper) {
        helper.retrieveExport(component);
    },
    
    showDiv : function(component, event) {
        var target = component.find("tileDiv");
        $A.util.removeClass(target, 'hideDiv');
    },
    
    hideDiv : function(component, event) {
        var target = component.find("tileDiv");
        $A.util.addClass(target, 'hideDiv');
        component.find("tileSize").set("v.value", null);
    },
    
    /**************************************** Sort Columns ****************************************/
    sortShipCol : function(component, event, helper) {
        component.set("v.columnNumber",1);
        helper.sortColumns(component);
    },
    
    sortFSTypeCol : function(component, event, helper) {
        component.set("v.columnNumber",2);
        helper.sortColumns(component);
    },
    
    sortActualCol : function(component, event, helper) {
        component.set("v.columnNumber",3);
        helper.sortColumns(component);
    },
    
    sortSeasonCol : function(component, event, helper) {
        component.set("v.columnNumber",4);
        helper.sortColumns(component);
    },
    
    sortTypeCol : function(component, event, helper) {
        component.set("v.columnNumber",5);
        helper.sortColumns(component);
    },
    
    sortFormCol : function(component, event, helper) {
        component.set("v.columnNumber",6);
        helper.sortColumns(component);
    },
    
    sortFSCol : function(component, event, helper) {
        component.set("v.columnNumber",7);
        helper.sortColumns(component);
    },
    
    sortCodeCol : function(component, event, helper) {
        component.set("v.columnNumber",8);
        helper.sortColumns(component);
    },
    
    sortProjectionCol : function(component, event, helper) {
        component.set("v.columnNumber",9);
        helper.sortColumns(component);
    },

	sortWHCol : function(component, event, helper) {
        component.set("v.columnNumber",10);
        helper.sortColumns(component);
    },
    
    sortArtworkCol : function(component, event, helper) {
        component.set("v.columnNumber",11);
        helper.sortColumns(component);
    },
    
    sortStateCol : function(component, event, helper) {
        component.set("v.columnNumber",12);
        helper.sortColumns(component);
    },
    
    sortOECol : function(component, event, helper) {
        component.set("v.columnNumber",13);
        helper.sortColumns(component);
    },
    
    sortWHTranCol : function(component, event, helper) {
        component.set("v.columnNumber",14);
        helper.sortColumns(component);
    },
    
    sortSBYBCol : function(component, event, helper) {
        component.set("v.columnNumber",15);
        helper.sortColumns(component);
    },
    
    sortOrderCol : function(component, event, helper) {
        component.set("v.columnNumber",16);
        helper.sortColumns(component);
    },
    
    sortSalesRepCol : function(component, event, helper) {
        component.set("v.columnNumber",17);
        helper.sortColumns(component);
    }
})