({
    getQueryResults : function(component) {
        var schedule = component.find("chosenDate").get("v.value");
        var start = component.find("startRange").get("v.value");
        var end = component.find("endRange").get("v.value");
        var s = component.find("season").get("v.value");
        var rectype = component.find("recordType").get("v.value");
        var tsize = component.find("tileSize").get("v.value");
        var form = component.find("formType").get("v.value");
        var custStatus = component.find("customerStatus").get("v.value");
        var wh = component.find("warehouse").get("v.value");
        var stage1 = component.find("objectStatus1").get("v.value");

		var action = component.get('c.buildQuery');
        action.setParams({scheduleDate:schedule, startRange:start, endRange:end,
                          season:s, recordType:rectype, tileSize:tsize, 
                          formType:form, warehouse:wh, customerStatus:custStatus,
                          stageOne:stage1});
        
        action.setCallback(this, function(actionResult) {
            component.set('v.results', actionResult.getReturnValue());
            this.setupExport(component, actionResult.getReturnValue());
            
            var viewportHeight = window.innerHeight - 378;
            var height = viewportHeight + "px";
            document.getElementById("resultsTable").style.height = height;
        } );
        $A.enqueueAction(action);
	},
    
    clearResults : function(component) {
        
        component.find("chosenDate").set("v.value", null);
        component.find("startRange").set("v.value", null);
        component.find("endRange").set("v.value", null);
        component.find("season").set("v.value", null);
        component.find("recordType").set("v.value", null);
        component.find("tileSize").set("v.value", null);
        component.find("formType").set("v.value", null);
        component.find("customerStatus").set("v.value", null);
        component.find("warehouse").set("v.value", null);
        component.find("objectStatus1").set("v.value", null);
        component.set("v.results", null);
        component.set("v.stringResults", null);
        component.set("v.columnNumber", null);
        component.set("v.csv", null);
        component.set("v.count", 0);
        component.set("v.revenue", 0);
        component.set("v.magnetsProjected", 0);
        component.set("v.magnetsMade", 0);
        component.set("v.magnetsDollarValue", 0);
        component.set("v.orderValue", 0);
        component.set("v.totalValue", 0);
	},
    
    getStringQuery : function(component) {
        var action = component.get('c.getSQ');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.stringResults', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
    },
    
    setupExport : function(component, res) {
        var action = component.get("c.createCSV");
        action.setParams({results:res});
        
        action.setCallback(this, function(actionResult) {
       		component.set('v.csv', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);                         
    },
    
    retrieveExport : function(component) {
        var res = component.get("v.csv"); 
        var results = encodeURI(res);
        var link = document.createElement('a');
        
        link.setAttribute('href', results);
        link.setAttribute('download', 'Production Planner Export.csv');
        link.click();
    },
    
    /**************************************** Summaries ****************************************/
    getCount : function(component) {
		var action = component.get('c.getCount');
             
        action.setCallback(this, function(actionResult) {
            component.set('v.count', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    getProjectedRevenue : function(component) {
		var action = component.get('c.getProjectedRevenue');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.revenue', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    getNumberOfMagnetsMade : function(component) {
		var action = component.get('c.getMagnetsMade');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.magnetsMade', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    getMagnetsProjectedPrice : function(component) {
		var action = component.get('c.getMagnetsProjected');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.magnetsProjected', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    getMagnetsDollarValue : function(component) {
		var action = component.get('c.getMagDollarValue');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.magnetsDollarValue', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    getOrderValue : function(component) {
		var action = component.get('c.getOrderValue');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.orderValue', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    getTotalValue : function(component) {
		var action = component.get('c.getTotalValue');
        
        action.setCallback(this, function(actionResult) {
            component.set('v.totalValue', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
	},
    
    /**************************************** Sorting ****************************************/
    sortColumns : function(component) {
        var res = component.get("v.stringResults");
        var col = component.get("v.columnNumber");
      	        
        var action = component.get("c.sortOnColumn");
        action.setParams({num:col,unsorted:res});
        
        action.setCallback(this, function(actionResult) {
            component.set('v.results', actionResult.getReturnValue());
        } );
        $A.enqueueAction(action);
    },
    
    /**************************************** Message ****************************************/
    requiredFieldsMessage : function(component, event) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent)
        {
            toastEvent.setParams({
                "title": 'Required Fields',
                "message": 'Start and End date values required.',
                "type": "warning",
                "duration": "3000"
            });
            toastEvent.fire();
        }
    }
})