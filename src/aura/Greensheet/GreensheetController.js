({
	doInit : function(component, event, helper) {
		helper.initialize(component);
	},
    
    filterManager : function(component, event, helper) {
        helper.recordsByManager(component);
    },
    
    selectRow : function(component, event, helper) {     
		helper.saveSelected(component, event);
	},

	deselectRow : function(component, event, helper) {
		helper.unSelected(component, event);
	},
   
    specModification : function(component, event, helper) {
        helper.specInstructionModification(component);
    },
    
    notesModification : function(component, event, helper) {
        helper.receivingNotesModification(component);
    },

    showModal : function(component, event, helper) {
        helper.displayModal(component);
    },

    hideModal : function(component, event, helper) {
        helper.concealModal(component);
    },

    recordIsResolved : function(component, event, helper) {
        helper.recordResolution(component);
    }
})