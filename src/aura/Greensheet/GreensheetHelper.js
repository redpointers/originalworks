({
	initialize : function(component) {
		var action = component.get("c.resolveRecords");
		
		action.setCallback(this, function(actionResult) {
			component.set('v.records', actionResult.getReturnValue());
            var list = actionResult.getReturnValue();

			var viewportHeight = window.innerHeight - 260;
            var height = viewportHeight + "px";

            document.getElementById("recordTable").style.height = height;
            
            this.numberOfRows(component, list);
            this.managersDropdown(component, list);  
            this.nullHeaderInformation(component);
		});
		$A.enqueueAction(action);
	},
    
    managersDropdown : function(component, list) {
        var action = component.get("c.managerList");
        action.setParams({forms:list});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.managers', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },

    numberOfRows : function(component, list) {
        var action = component.get("c.itemCount");
        action.setParams({forms:list});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.items', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },
   
    saveSelected : function(component, event) {
	    var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        var special = component.find("specInput");
        var receiving = component.find("recInput");
        var remove = component.find("removeBtn");
        var link = component.find("navigateTo");
        
	    var index = parseInt(rowIndex);           
	    var table = document.getElementById("tableRows");    
	    var row = table.rows[index]; 
        
        for(var i = 0; i < table.rows.length; i++) {
            table.rows[i].className = '';
        }
            
	  	row.className = 'slds-is-selected';
	    component.set("v.selected", row.id);
        special.set("v.readonly", "false");
        receiving.set("v.readonly", "false");
        remove.set("v.disabled", "false");
        $A.util.removeClass(link, "disableLink");
        this.rowInformation(component, row.id);     
	},

	unSelected : function(component, event) {
	
		var rowIndex = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
		var index = parseInt(rowIndex);           
	    var table = document.getElementById("tableRows");    
	    var row = table.rows[index]; 
        var special = component.find("specInput");
        var receiving = component.find("recInput");
        var remove = component.find("removeBtn");
        var link = component.find("navigateTo");

        row.className = '';
        component.set("v.selected", "");
        special.set("v.readonly", "true");
        receiving.set("v.readonly", "true");
        remove.set("v.disabled", "true");

        $A.util.addClass(link, "disableLink");
        this.nullHeaderInformation(component);
	},

 /*************************************** Filter ***************************************/

	recordsByManager: function(component) {
        var mgr = component.find("managerFilter").get("v.value");
        var action = component.get("c.filterByManager");
        action.setParams({manager:mgr});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.records', actionResult.getReturnValue());
            this.numberOfRows(component, actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
        this.nullHeaderInformation(component);
    },

/*************************************** Row Information ***************************************/

    rowInformation : function(component, id) {
        var action = component.get("c.selectedForm");
        action.setParams({formId:id});
        
        action.setCallback(this, function(actionResult) {
            var form = actionResult.getReturnValue();
            
            this.earlyLate(component, form);
            this.instructionsCheck(component, form);
            this.dflCheck(component, form);
            this.shipAddressCheck(component, form);
            this.receivingNotesOriginal(component, form);
            this.specialInstructionsOriginal(component, form);

            component.set("v.confirmationId", form.Confirmation__c);
            component.set("v.sForm", form.Confirmation__r.Account__r.SchoolCode__c + ' ' + form.FormType__c + form.FormSequence__c);
		});
		$A.enqueueAction(action);
    },

    nullHeaderInformation : function(component) {
        component.set("v.instructions", '');
        component.set("v.dfl", '');
        component.set("v.address", '');
        component.set("v.days", '');
        component.set("v.specInstructions", '');
        component.set("v.notes", '');
        component.set("v.confirmationId", '');
        component.set("v.sForm", '');
    },
    
    earlyLate : function(component, record) {
        var action = component.get("c.daysLate");
        action.setParams({form:record});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.days', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },

    instructionsCheck : function(component, record) {
        var action = component.get("c.instructionsCheckbox");
        action.setParams({form:record});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.instructions', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },

    dflCheck : function(component, record) {
        var action = component.get("c.dflCheckbox");
        action.setParams({form:record});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.dfl', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },

    shipAddressCheck : function(component, record) {
        var action = component.get("c.shippingAddressCheckbox");
        action.setParams({form:record});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.address', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },
    
/*************************************** Editable ***************************************/
    
    receivingNotesOriginal : function(component, record) {
    	var action = component.get("c.notes");
        action.setParams({form:record});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.notes', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },

    specialInstructionsOriginal : function(component, record) {
    	var action = component.get("c.specialInstructions");
        action.setParams({form:record});
        
        action.setCallback(this, function(actionResult) {
			component.set('v.specInstructions', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
    },
    
    specInstructionModification : function(component) {
        var modified = component.get("v.specInstructions");
        var record = component.get("v.confirmationId");
        var action = component.get("c.updateSpecialInstructions");
        
        action.setParams({confId:record, specIn:modified});
        $A.enqueueAction(action);
    },
    
    receivingNotesModification: function(component) {
        var modified = component.get("v.notes");
        var record = component.get("v.selected");
        var action = component.get("c.updateReceivingNotes");
      
        action.setParams({formId:record, specIn:modified});
        $A.enqueueAction(action);
    },

    /*************************************** Modal ***************************************/

    displayModal : function(component) {
        var modal = component.find("removalModal");
        var backdrop = component.find("removalModalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealModal : function(component) {
        var modal = component.find("removalModal");
        var backdrop = component.find("removalModalBackdrop");
        
        component.set("v.note", '');
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    recordResolution : function(component) {
        var selectedForm = component.get("v.selected");
        var note = component.get("v.notes");
        var action = component.get("c.resolved");
        action.setParams({formId:selectedForm, specIn:note});
        $A.enqueueAction(action);
        this.concealModal(component);
        this.initialize(component);
    }
})