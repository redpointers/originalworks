({
	doInit : function(component, event, helper) {
		helper.initialize(component);
	},

	/*************************************** Report Generation ***************************************/

	generateItemCount : function(component, event, helper) {
		helper.itemCountSetup(component);
	},

	generatePerCap : function(component, event, helper) {
		helper.perCapSetup(component);
	},

	generateReceiving : function(component, event, helper) {
		helper.receivingSetup(component);
	},

	generateSalesAnalysis : function(component, event, helper) {
		helper.salesSetup(component);
	},

	generateSeasonForecastDetail : function(component, event, helper) {
		helper.forecastSeasonSetup(component);
	},

	generateSeasonForecastHQMGRDetail : function(component, event, helper) {
		helper.forecastHQMGRSeasonSetup(component);
	},

	generateSeasonForecastHQMGRSummary : function(component, event, helper) {
		helper.forecastHQMGRSeasonSummarySetup(component);
	},

	generateSeasonForecastSummary : function(component, event, helper) {
		helper.forecastSeasonSummarySetup(component);
	},

	generateShipping : function(component, event, helper) {
		helper.shippingSetup(component);
	},

	generateWIP : function(component, event, helper) {
		helper.wipSetup(component);
	},

	/*************************************** Modal ***************************************/

	showItemCountModal : function(component, event, helper) {
		helper.displayItemCountModal(component);
	},

	hideItemCountModal : function(component, event, helper) {
        helper.concealItemCountModal(component);
    },

    showPerCapModal : function(component, event, helper) {
		helper.displayPerCapModal(component);
	},

	hidePerCapModal : function(component, event, helper) {
        helper.concealPerCapModal(component);
    },

    showReceivingModal : function(component, event, helper) {
		helper.displayReceivingModal(component);
	},

	hideReceivingModal : function(component, event, helper) {
        helper.concealReceivingModal(component);
    },

    showSalesModal : function(component, event, helper) {
		helper.displaySalesModal(component);
	},

	hideSalesModal : function(component, event, helper) {
        helper.concealSalesModal(component);
    },

    showForecastDetailModal : function(component, event, helper) {
		helper.displayForecastDetailModal(component);
	},

	hideForecastDetailModal : function(component, event, helper) {
        helper.concealForecastDetailModal(component);
    },

    showForecastHQMGRDetailModal : function(component, event, helper) {
		helper.displayForecastHQMGRDetailModal(component);
	},

	hideForecastHQMGRDetailModal : function(component, event, helper) {
        helper.concealForecastHQMGRDetailModal(component);
    },

    showForecastHQMGRSummaryModal : function(component, event, helper) {
		helper.displayForecastHQMGRSummaryModal(component);
	},

	hideForecastHQMGRSummaryModal : function(component, event, helper) {
        helper.concealForecastHQMGRSummaryModal(component);
    },

    showForecastSummaryModal : function(component, event, helper) {
		helper.displayForecastSummaryModal(component);
	},

	hideForecastSummaryModal : function(component, event, helper) {
        helper.concealForecastSummaryModal(component);
    },

    showShippingModal : function(component, event, helper) {
		helper.displayShippingModal(component);
	},

	hideShippingModal : function(component, event, helper) {
        helper.concealShippingModal(component);
    },

    showWIPModal : function(component, event, helper) {
		helper.displayWIPModal(component);
	},

	hideWIPModal : function(component, event, helper) {
        helper.concealWIPModal(component);
    }
})