({
	initialize : function(component) {

        var action = component.get("c.getSeasons");

		action.setCallback(this, function(actionResult) {
			component.set("v.seasons", actionResult.getReturnValue());
            var list = actionResult.getReturnValue();
			var viewportHeight = window.innerHeight - 198;
        	var height = viewportHeight + "px";
	
        	document.getElementById("content").style.height = height;

        	this.dailyRev(component);
        	this.leadSourceList(component);
            this.changeLog(component);
            component.set('v.defaultSeason', list[0].Name);
            component.set('v.defaultSeasonId', list[0].Id);
            component.set('v.seasonStart', list[0].StartDate__c);
            component.set('v.seasonEnd', list[0].EndDate__c);
		});
		$A.enqueueAction(action);

        var action2 = component.get("c.defaultDate");

        action2.setCallback(this, function(actionResult) {
            component.set("v.todaysDate", actionResult.getReturnValue());
        });
        $A.enqueueAction(action2);
	},

    /*************************************** Report Generation ***************************************/

	dailyRev : function(component) {
		var action = component.get("c.dailyRevenueReport");

		action.setCallback(this, function(actionResult) {
			component.set("v.dailyRevenue", actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	leadSourceList : function(component) {
		var action = component.get("c.leadSourceReport");

		action.setCallback(this, function(actionResult) {
			component.set("v.leadSource", actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
	},

	itemCountSetup : function(component) {
		var selected = component.find("itemCountSeason").get("v.value");
        var defaultS = component.get("v.defaultSeasonId");
        var season = selected === undefined ? defaultS : selected;
        
		var action = component.get("c.itemCountReport");
		action.setParams({seasonId:season});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();

		});
		$A.enqueueAction(action);
	},

    perCapSetup : function(component) {
        var selected = component.find("perCapSeason").get("v.value");
        var defaultS = component.get("v.defaultSeason");
        var season = selected === undefined ? defaultS : selected;

        var action = component.get("c.perCapReport");
        action.setParams({seasonName:season});

        action.setCallback(this, function(actionResult) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({"url": actionResult.getReturnValue()});
            urlEvent.fire();

        });
        $A.enqueueAction(action);
    },

	receivingSetup : function(component) {
		var startparam = component.find("startDate").get("v.value");
		var endparam = component.find("endDate").get("v.value");
		var action = component.get("c.receivingReport");
		action.setParams({start:startparam, endd:endparam});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();
		});
		$A.enqueueAction(action);
	},

	salesSetup : function(component) {
		var selected = component.find("salesSeason").get("v.value");
        var defaultS = component.get("v.defaultSeason");
        var season = selected === undefined ? defaultS : selected;
		
		var action = component.get("c.salesAnalysisReport");
		action.setParams({seasonName:season});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();
		});
		$A.enqueueAction(action);
	},

	forecastSeasonSetup : function(component) {
		var selected = component.find("forecastDetailSeason").get("v.value");
		var status = component.find("forecastDetailStatus").get("v.value");
        var defaultS = component.get("v.defaultSeason");
        var season = selected === undefined ? defaultS : selected;

		var action = component.get("c.seasonForecastDetailReport");
		action.setParams({seasonName:season, cStatus:status});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();
		});
		$A.enqueueAction(action);
	},

	forecastHQMGRSeasonSetup : function(component) {
		var selected = component.find("forecastHQMGRDetailSeason").get("v.value");
		var status = component.find("forecastHQMGRDetailStatus").get("v.value");
        var defaultS = component.get("v.defaultSeason");
        var season = selected === undefined ? defaultS : selected;

		var action = component.get("c.seasonForecastHQMGRDetailReport");
		action.setParams({seasonName:season, cStatus:status});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();
		});
		$A.enqueueAction(action);
	},

	forecastHQMGRSeasonSummarySetup : function(component) {
		var selected = component.find("forecastHQMGRSummarySeason").get("v.value");
		var status = component.find("forecastHQMGRSummaryStatus").get("v.value");
        var defaultS = component.get("v.defaultSeason");
        var season = selected === undefined ? defaultS : selected;

		var action = component.get("c.seasonForecastHQMGRSummaryReport");
		action.setParams({seasonName:season, cStatus:status});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();
		});
		$A.enqueueAction(action);
	},

	forecastSeasonSummarySetup : function(component) {
		var selected = component.find("forecastSummarySeason").get("v.value");
		var status = component.find("forecastSummaryStatus").get("v.value");
        var defaultS = component.get("v.defaultSeason");
        var season = selected === undefined ? defaultS : selected;

		var action = component.get("c.seasonForecastSummaryReport");
		action.setParams({seasonName:season, cStatus:status});

		action.setCallback(this, function(actionResult) {
			var urlEvent = $A.get("e.force:navigateToURL");
    		urlEvent.setParams({"url": actionResult.getReturnValue()});
    		urlEvent.fire();
		});
		$A.enqueueAction(action);
	},

    changeLog : function(component) {
        var action = component.get("c.shipDateChangeLogReport");

        action.setCallback(this, function(actionResult) {
            component.set("v.changeLog", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },

    shippingSetup : function(component) {
        var startParam = component.find("startShip").get("v.value");
        var endParam = component.find("endShip").get("v.value");

        var action = component.get("c.shippingReport");
        action.setParams({startDate:startParam, endDate:endParam});

        action.setCallback(this, function(actionResult) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({"url": actionResult.getReturnValue()});
            urlEvent.fire();
        });
        $A.enqueueAction(action);
    },

    wipSetup : function(component) {
        var selected = component.find("wipSeason").get("v.value");
        var chosenDate = component.find("wipdate").get("v.value");
        var defaultS = component.get("v.defaultSeasonId");
        var season = selected === undefined ? defaultS : selected;

        var action = component.get("c.wipReport");
        action.setParams({seasonId:season, cDate:chosenDate});

        action.setCallback(this, function(actionResult) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({"url": actionResult.getReturnValue()});
            urlEvent.fire();
        });
        $A.enqueueAction(action);
    },

    /*************************************** Modal ***************************************/

    displayItemCountModal : function(component) {
        var modal = component.find("hiddenItemCountModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealItemCountModal : function(component) {
        var modal = component.find("hiddenItemCountModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayPerCapModal : function(component) {
        var modal = component.find("hiddenPerCapModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealPerCapModal : function(component) {
        var modal = component.find("hiddenPerCapModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayReceivingModal : function(component) {
        var modal = component.find("hiddenReceivingModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealReceivingModal : function(component) {
        var modal = component.find("hiddenReceivingModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displaySalesModal : function(component) {
        var modal = component.find("hiddenSalesModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealSalesModal : function(component) {
        var modal = component.find("hiddenSalesModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayForecastDetailModal : function(component) {
        var modal = component.find("hiddenForecastDetailModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealForecastDetailModal : function(component) {
        var modal = component.find("hiddenForecastDetailModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayForecastHQMGRDetailModal : function(component) {
        var modal = component.find("hiddenForecastHQMGRDetailModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealForecastHQMGRDetailModal : function(component) {
        var modal = component.find("hiddenForecastHQMGRDetailModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayForecastHQMGRSummaryModal : function(component) {
        var modal = component.find("hiddenForecastHQMGRSummaryModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealForecastHQMGRSummaryModal : function(component) {
        var modal = component.find("hiddenForecastHQMGRSummaryModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayForecastSummaryModal : function(component) {
        var modal = component.find("hiddenForecastSummaryModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealForecastSummaryModal : function(component) {
        var modal = component.find("hiddenForecastSummaryModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayShippingModal : function(component) {
        var modal = component.find("hiddenShippingModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealShippingModal : function(component) {
        var modal = component.find("hiddenShippingModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    },

    displayWIPModal : function(component) {
        var modal = component.find("hiddenWIPModal");
        var backdrop = component.find("modalBackdrop");

        $A.util.addClass(modal, "slds-fade-in-open");
        $A.util.addClass(backdrop, "slds-backdrop--open");
    },

    concealWIPModal : function(component) {
        var modal = component.find("hiddenWIPModal");
        var backdrop = component.find("modalBackdrop");
        
        $A.util.removeClass(modal, "slds-fade-in-open");
        $A.util.removeClass(backdrop, "slds-backdrop--open");
    }
})