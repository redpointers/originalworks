({
	extract : function(component, event, helper) {
		helper.extract(component, event);
	},
	extractRetail : function(component, event, helper) {
		helper.extractRetail(component, event);
	},
	extractGallery : function(component, event, helper) {
		helper.extractGallery(component, event);
	},
	filterSchools : function(component, event, helper) {
		helper.filterSchools(component, event);
	},
	filterForms : function(component, event, helper) {
		helper.filterForms(component, event);
	},
	filterTaxes : function(component, event, helper) {
		helper.filterTaxes(component, event);
	},
	filterSalesReps : function(component, event, helper) {
		helper.filterSalesReps(component, event);
	},
	filterQuestions : function(component, event, helper) {
		helper.filterQuestions(component, event);
	},
	filterNoMags : function(component, event, helper) {
		helper.filterNoMags(component, event);
	},
	filterHiMags : function(component, event, helper) {
		helper.filterHiMags(component, event);
	},
	toggleSpinner: function(component, event, helper) {
        helper.toggleSpinner(component, event);
    },
    reset: function(component, event, helper) {
        helper.reset(component, event);
    },
	navigate : function(component, event, helper) {
		var recordId = event.currentTarget.id;
		console.log(recordId);

        var url = "/"+recordId;
        var navEvt = $A.get("e.force:navigateToURL");
        if(navEvt){
            navEvt.setParams({
                "url": url
            });
            navEvt.fire();
        }
        else if( (typeof sforce != 'undefined') && (sforce != null) ) {
            sforce.one.navigateToURL(url);
        }
        else {
            window.location.href = url;
        }
	},
	export : function(component, event, helper) {
		helper.export(component, event);
	},
	handleExportComplete: function(component, event, helper) {
		helper.handleExportComplete(component, event);
	}
	
})