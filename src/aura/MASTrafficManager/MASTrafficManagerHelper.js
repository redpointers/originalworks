({
	extract : function(component, event) {
        this.clearTable(component);

        this.toggleSpinner(component,event);

		var action = component.get("c.extractData");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var data = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.extractData", data);
                component.set("v.extractedNormal", true);
                component.set("v.extractedRetail", false);
                component.set("v.extractedGallery", false);

                this.toggleSpinner(component,event);

                if(data)
                {
                    component.set("v.numForms", data.numForms);
                    component.set("v.numQuestion", data.numQuestions);
            		component.set("v.numSchools", data.numSchools);
                    component.set("v.numTax", data.numTaxes);
                    component.set("v.numSalesReps", data.numSalesReps);
                    component.set("v.numNoMag", data.numNoMags);
                    component.set("v.numHiMag", data.numHighMags);

                    if(!data.goodData)
                        this.noData(component, event);
                    else
                        this.successMessage(component, event, "Data Extracted", "Records have been extracted. Click on the links to view the data");
                }
                else
                {
                    this.noData(component, event);
                }
            }
            else {
                this.toggleSpinner(component,event);
                var errors = action.getError();
                if (errors)
                {
                    if (errors[0])
                    {
                        if(errors[0].message)
                            console.log(errors[0].message);
                        else
                           console.log(errors[0]);
                    }
                    
                    if (errors[1])
                    {
                        if(errors[1].message)
                            console.log(errors[1].message);
                        else
                            console.log(errors[1]);
                    }
                    ;
                }
            }
        });

        $A.enqueueAction(action);
	},
    extractRetail : function(component, event) {
        this.clearTable(component);

        this.toggleSpinner(component,event);

        var action = component.get("c.extractRetailData");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var data = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.extractData", data);
                component.set("v.extractedNormal", false);
                component.set("v.extractedRetail", true);
                component.set("v.extractedGallery", false);

                this.toggleSpinner(component,event);

                if(data)
                {
                    component.set("v.numForms", data.numForms);
                    component.set("v.numQuestion", data.numQuestions);
                    component.set("v.numSchools", data.numSchools);
                    component.set("v.numTax", data.numTaxes);
                    component.set("v.numSalesReps", data.numSalesReps);
                    component.set("v.numNoMag", data.numNoMags);
                    component.set("v.numHiMag", data.numHighMags);

                    if(!data.goodData)
                        this.noData(component, event);
                    else
                        this.successMessage(component, event, "Data Extracted", "Records have been extracted. Click on the links to view the data");
                }
                else
                {
                    this.noData(component, event);
                }
            }
            else {
                this.toggleSpinner(component,event);
            }
        });

        $A.enqueueAction(action);
    },
    extractGallery : function(component, event) {
        this.clearTable(component);

        this.toggleSpinner(component,event);

        var action = component.get("c.extractGalleryData");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var data = JSON.parse(JSON.stringify(response.getReturnValue()));
                component.set("v.extractData", data);
                component.set("v.extractedNormal", false);
                component.set("v.extractedRetail", false);
                component.set("v.extractedGallery", true);

                this.toggleSpinner(component,event);

                if(data)
                {
                    component.set("v.numForms", data.numForms);
                    component.set("v.numQuestion", data.numQuestions);
                    component.set("v.numSchools", data.numSchools);
                    component.set("v.numTax", data.numTaxes);
                    component.set("v.numSalesReps", data.numSalesReps);
                    component.set("v.numNoMag", data.numNoMags);
                    component.set("v.numHiMag", data.numHighMags);

                    if(!data.goodData)
                        this.noData(component, event);
                    else
                        this.successMessage(component, event, "Data Extracted", "Records have been extracted. Click on the links to view the data");
                }
                else
                {
                    this.noData(component, event);
                }
            }
            else 
            {
               this.toggleSpinner(component,event);
            }
        });

        $A.enqueueAction(action);
    },
    filterSchools : function(component, event) {
        var data = component.get("v.extractData");
        var numSchools = component.get("v.numSchools");

        var comps=component.find("table");

        if(numSchools > 0)
        {
            var headers = [];
            headers.push("Account");
            headers.push("School Code");
            headers.push("City/State/Zip");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];
            var acct = data.accounts;

            var i = 0;
            for(i=0;i<data.accounts.length;i++)
            {
                var rowArray = [];
                rowArray.push({id:acct[i].Id, label:acct[i].Name});
                rowArray.push(acct[i].SchoolCode__c);
                rowArray.push(acct[i].BillingCity+"/"+acct[i].BillingState+"/"+acct[i].BillingPostalCode);
                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    filterForms : function(component, event) {
        var data = component.get("v.extractData");
        var numForms = component.get("v.numForms");

        var comps=component.find("table");

        if(numForms > 0)
        {
            var headers = [];
            headers.push("Form Name");
            headers.push("Form Type");
            headers.push("Sequence");
            headers.push("Warehouse Assigned");
            headers.push("Warehouse Transfer Date");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];

            var i = 0;
            for(i=0;i<data.forms.length;i++)
            {
                var rowArray = [];
                var form = data.forms[i];

                console.log("Form Name: "+form.Name);
                rowArray.push({id:form.Id, label:form.Name});
                rowArray.push(form.FormType__c);
                rowArray.push(form.FormSequence__c);
                rowArray.push(form.WarehouseAssigned__c);
                rowArray.push(form.WarehouseTransferDate__c);
                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    filterTaxes : function(component, event) {
        var data = component.get("v.extractData");
        var numTax = component.get("v.numTax");

        var comps=component.find("table");

        if(numTax > 0)
        {
            var headers = [];
            headers.push("Account");
            headers.push("School Code");
            headers.push("City/State/Zip");
            headers.push("Tax Exempt");
            headers.push("Tax Exempt Recv'd");
            headers.push("Tax Exempt Value Until");
            headers.push("Tax Code");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];
            var i = 0;
            for(i=0;i<data.taxAccounts.length;i++)
            {
                var acct = data.taxAccounts[i];
                var rowArray = [];
                rowArray.push({id:acct.Id, label:acct.Name});
                rowArray.push(acct.SchoolCode__c);
                rowArray.push(acct.BillingCity+"/"+acct.BillingState+"/"+acct.BillingPostalCode);
                rowArray.push(acct.TaxExempt__c);
                rowArray.push(acct.TaxExemptReceived__c);
                rowArray.push(acct.TaxExemptValidUntil__c);
                rowArray.push(acct.TaxCode__c);

                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    filterSalesReps : function(component, event) {
        var data = component.get("v.extractData");
        var numSalesReps = component.get("v.numSalesReps");

        var comps=component.find("table");

        if(numSalesReps > 0)
        {
            var headers = [];
            headers.push("First Name");
            headers.push("Last Name");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];

            var i = 0;
            for(i=0;i<data.saleRepConfirms.length;i++)
            {
                var confirm = data.saleRepConfirms[i];
                var rowArray = [];
                rowArray.push({id:confirm.SalesRep__c, label:confirm.SalesRep__r.FirstName});
                rowArray.push(confirm.SalesRep__r.LastName);
                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    filterQuestions : function(component, event) {
        var data = component.get("v.extractData");
        var numQuestion = component.get("v.numQuestion");

        var comps=component.find("table");

        if(numQuestion > 0)
        {
            var headers = [];
            headers.push("Form Name");
            headers.push("Form Type");
            headers.push("Sequence");
            headers.push("Question Folders");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];

            var i = 0;
            for(i=0;i<data.questionForms.length;i++)
            {
                var form = data.questionForms[i];

                var rowArray = [];
                rowArray.push({id:form.Id, label:form.Name});
                rowArray.push(form.FormType__c);
                rowArray.push(form.FormSequence__c);
                rowArray.push(form.QuestionFolders__c);
                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    filterNoMags : function(component, event) {
        var data = component.get("v.extractData");
        var numNoMag = component.get("v.numNoMag");

        var comps=component.find("table");

        if(numNoMag > 0)
        {
            var headers = [];
            headers.push("Form Name");
            headers.push("Form Type");
            headers.push("Sequence");
            headers.push("Mags Bought");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];

            var i = 0;
            for(i=0;i<data.noMagsForms.length;i++) // TODO need to filter mags here
            {
                var form = data.noMagsForms[i];
                var rowArray = [];
                rowArray.push({id:form.Id, label:form.Name});
                rowArray.push(form.FormType__c);
                rowArray.push(form.FormSequence__c);
                rowArray.push(form.MagsBought__c);
                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    filterHiMags : function(component, event) {
        var data = component.get("v.extractData");
        var numHiMag = component.get("v.numHiMag");

        var comps=component.find("table");

        if(numHiMag > 0)
        {
            var headers = [];
            headers.push("Confirmation");
            headers.push("Form B Type");
            headers.push("Form B Mags Bought");
            headers.push("Form A Type");
            headers.push("Form A Mags Made");
            component.set("v.tableHeaders",headers);

            var tableDataMap = [];

            var i = 0;
            for(i=0;i<data.hiMagsConfirms.length;i++)
            {
                var confirm = data.hiMagsConfirms[i];
                var rowArray = [];
                var formB = {};
                var formA = {};

                var formI = 0;
                for(formI=0;formI<data.forms.length;formI++)
                {
                    var form = data.forms[formI];
                    if(form.Confirmation__c == confirm.Id && form.FormType__c == "B")
                        formB = data.forms[formI];
                    else if(form.Confirmation__c == confirm.Id && form.FormType__c == "A")
                        formA = data.forms[formI];
                }

                rowArray.push({id:confirm.Id, label:confirm.Name});
                rowArray.push(formB.FormType__c);
                rowArray.push(formB.MagsBought__c);
                rowArray.push(formA.FormType__c);
                rowArray.push(formA.ArtworkCountOE__c);
                var temp = { "key": i, "list": rowArray };
                tableDataMap.push(temp);
            }

            component.set("v.filteredRows", tableDataMap);

            $A.util.removeClass(comps, "slds-hide");
            $A.util.addClass(comps, "slds-show");
        }
        else
        {
            this.clearTable(component);
        }

    },
    clearTable : function(component)
    {
        component.set("v.tableHeaders",null);
        component.set("v.filteredRows", null);
        component.set("v.extractedNormal", false);
        component.set("v.extractedRetail", false);
        component.set("v.extractedGallery", false);

        var comps=component.find("table");
        $A.util.removeClass(comps, "slds-show");
        $A.util.addClass(comps, "slds-hide");
    },
    export : function(component, event)
    {
        var extractedNormal = component.get("v.extractedNormal");
        var extractedRetail = component.get("v.extractedRetail");
        var extractedGallery = component.get("v.extractedGallery");

        this.toggleSpinner(component, event);

        if(extractedNormal)
        {
            this.exportSchool(component, "NORMAL");
            this.exportSalesRep(component);
            this.exportInvoice(component, "NORMAL");
        }
        else if(extractedRetail) // no sales rep for retail
        {
            this.exportSchool(component, "RETAIL");
            this.exportInvoice(component, "RETAIL");
        }
        else if(extractedGallery)
        {
            this.exportSchool(component, "GALLERY");
            this.exportSalesRep(component);
            this.exportInvoice(component, "GALLERY");
        }

        this.markExported(component, event);
    },
    exportSchool : function(component, exportType)
    {
        var extractData = component.get("v.extractData");
        var accounts = extractData.accounts;
        var accountsStr = JSON.stringify(accounts);
        var taxAccounts = extractData.taxAccounts;
        var taxStr = JSON.stringify(taxAccounts);
        var questionForms = extractData.questionForms;
        var questionStr = JSON.stringify(questionForms);
        var noMagsForms = extractData.noMagsForms;
        var noMagStr = JSON.stringify(noMagsForms);
        var hiMagsConfirms = extractData.hiMagsConfirms;
        var highMagStr = JSON.stringify(hiMagsConfirms);

        var action = component.get("c.exportSchoolData");

        action.setParams({
            "dataStr": accountsStr,
            "type": exportType,
            "taxStr": taxStr,
            "questionStr": questionStr,
            "noMagStr": noMagStr,
            "highMagStr": highMagStr
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {

                // Fire event to tell the component that this is complete
                var addEvent = component.getEvent("exportCompleteEvent");
                addEvent.setParams({ "message": "All records have been exported. Check your downloads folder for the files" });
                addEvent.fire();

                var data = response.getReturnValue();
                var results = encodeURI(data);
                var link = document.createElement('a');

                var todayDate = new Date();
                var month = ("0" + (todayDate.getMonth() + 1)).slice(-2);
                var day = ("0" + (todayDate.getDate())).slice(-2);
                var year = todayDate.getFullYear().toString().substr(2,2);
                
                link.setAttribute('href', results);
                if(exportType == "GALLERY")
                    link.setAttribute('download', "MAS90SC_GALLERY_"+month+day+year);
                else if(exportType == "NORMAL")
                    link.setAttribute('download', "MAS90SC_"+month+day+year);
                else if(exportType == "RETAIL")
                    link.setAttribute('download', "MAS90SC_RETAIL_"+month+day+year);

                link.click();
            }
            else {
                console.log("Export Schools Failed with state: " + state);
                var errors = action.getError();
            }
        });

        $A.enqueueAction(action);
    },
    exportSalesRep : function(component)
    {
        var extractData = component.get("v.extractData");
        var confirms = extractData.confirms;
        var confirmsStr = JSON.stringify(confirms);
        var taxAccounts = extractData.taxAccounts;
        var taxStr = JSON.stringify(taxAccounts);
        var questionForms = extractData.questionForms;
        var questionStr = JSON.stringify(questionForms);
        var noMagsForms = extractData.noMagsForms;
        var noMagStr = JSON.stringify(noMagsForms);
        var hiMagsConfirms = extractData.hiMagsConfirms;
        var highMagStr = JSON.stringify(hiMagsConfirms);

        var action = component.get("c.exportSalesRepData");

        action.setParams({
            "dataStr":  confirmsStr,
            "taxStr": taxStr,
            "questionStr": questionStr,
            "noMagStr": noMagStr,
            "highMagStr": highMagStr
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var data = response.getReturnValue();
                var results = encodeURI(data);
                var link = document.createElement('a');

                var todayDate = new Date();
                var month = ("0" + (todayDate.getMonth() + 1)).slice(-2);
                var day = ("0" + (todayDate.getDate())).slice(-2);
                var year = todayDate.getFullYear().toString().substr(2,2);
                
                link.setAttribute('href', results);
                link.setAttribute('download', "MAS90SR_"+month+day+year);
                link.click();
            }
            else {
                console.log("Export Failed with state: " + state);
                var errors = action.getError();
            }
        });

        $A.enqueueAction(action);
    },
    exportInvoice : function(component, exportType)
    {
        var extractData = component.get("v.extractData");
        var accounts = extractData.accounts;
        var accountsStr = JSON.stringify(accounts);
        var confirms = extractData.confirms;
        var confirmsStr = JSON.stringify(confirms);
        var forms = extractData.formIds; // Id's, not the forms themselves
        var formStr = JSON.stringify(forms);
        var taxAccounts = extractData.taxAccounts;
        var taxStr = JSON.stringify(taxAccounts);
        var questionForms = extractData.questionForms;
        var questionStr = JSON.stringify(questionForms);
        var noMagsForms = extractData.noMagsForms;
        var noMagStr = JSON.stringify(noMagsForms);
        var hiMagsConfirms = extractData.hiMagsConfirms;
        var highMagStr = JSON.stringify(hiMagsConfirms);

        var action = component.get("c.exportInvoiceData");

        action.setParams({
            "acctStr":  accountsStr,
            "confirmStr":  confirmsStr,
            "formStr":  formStr,
            "type": exportType,
            "taxStr": taxStr,
            "questionStr": questionStr,
            "noMagStr": noMagStr,
            "highMagStr": highMagStr
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var data = response.getReturnValue();
                var results = encodeURI(data);
                var link = document.createElement('a');

                var todayDate = new Date();
                var month = ("0" + (todayDate.getMonth() + 1)).slice(-2);
                var day = ("0" + (todayDate.getDate())).slice(-2);
                var year = todayDate.getFullYear().toString().substr(2,2);
                
                link.setAttribute('href', results);
                if(exportType == "GALLERY")
                    link.setAttribute('download', "MAS90_GALLERY_"+month+day+year);
                else if(exportType == "NORMAL")
                    link.setAttribute('download', "MAS90_"+month+day+year);
                else if(exportType == "RETAIL")
                    link.setAttribute('download', "MAS90_RETAIL_"+month+day+year);

                link.click();
            }
            else {
                console.log("ExportInvoice Failed with state: " + state);
                var errors = action.getError();
                if (errors)
                {
                    if (errors[0])
                    {
                        if(errors[0].message)
                            console.log(errors[0].message);
                        else
                           console.log(errors[0]);
                    }
                    
                    if (errors[1])
                    {
                        if(errors[1].message)
                            console.log(errors[1].message);
                        else
                            console.log(errors[1]);
                    }
                    ;
                }
            }
        });

        $A.enqueueAction(action);
    },
    markExported : function(component, exportType)
    {
        var extractData = component.get("v.extractData");
        var forms = extractData.formIds; // Id's, not the forms themselves
        var formsStr = JSON.stringify(forms);

        var action = component.get("c.markFormsExported");

        action.setParams({
            "formStr":  formsStr
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                this.reset(component, event);
            }
            else {
                console.log("MarkExported failed with state: " + state);
                var errors = action.getError();
                if (errors)
                {
                    if (errors[0])
                    {
                        if(errors[0].message)
                            console.log(errors[0].message);
                        else
                           console.log(errors[0]);
                    }
                    
                    if (errors[1])
                    {
                        if(errors[1].message)
                            console.log(errors[1].message);
                        else
                            console.log(errors[1]);
                    }
                    ;
                }
            }
        });

        $A.enqueueAction(action);
    },
    noData : function(component, event) {
        var toastEvent = $A.get("e.force:showToast");
        console.log("Toast: "+toastEvent);
        if(toastEvent)
        {
            toastEvent.setParams({
                "title": "No Data",
                "message": "No data in this extraction",
                "type": "warning"
            });
            toastEvent.fire();
        }
    },
    successMessage : function(component, event, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent)
        {
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": "success",
                "duration": "2000"
            });
            toastEvent.fire();
        }
    },
    reset : function(component, event) {
        this.clearTable(component);

        component.set("v.extractData", null);
        component.set("v.numForms", 0);
        component.set("v.numQuestion", 0);
        component.set("v.numSchools", 0);
        component.set("v.numTax", 0);
        component.set("v.numSalesReps", 0);
        component.set("v.numNoMag", 0);
        component.set("v.numHiMag", 0);
    },
    toggleSpinner: function(cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    handleExportComplete: function(component, event) {
        var message = event.getParam("message");
        this.toggleSpinner(component, event);
        this.successMessage(component, event, "Data Exported", message);
    }

})