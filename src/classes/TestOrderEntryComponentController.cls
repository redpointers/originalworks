@isTest
private class TestOrderEntryComponentController
{
	static Confirmation__c confirm;
	static Form__c form;
	static ConfirmationProduct__c confirmProd;
	
	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		form = UnitTestDataFactory.inflateForm(confirm);
		insert form;

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c sp = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert sp;

		confirmProd = UnitTestDataFactory.inflateConfirmationProduct(confirm, sp);
		insert confirmProd;
	}

	@isTest
	static void testGetConfirmation()
	{
		setup();

		Confirmation__c con = OrderEntryComponentApexController.getConfirmation(confirm.Id);
		System.assert(con != null);
	}

	@isTest
	static void testGetForms()
	{
		setup();

		List<Form__c> forms = OrderEntryComponentApexController.getForms(confirm.Id);
		System.assert(forms.size() != null);
	}

	@isTest
	static void testGetOrders()
	{
		setup();

		List<OrderLines> orders = OrderEntryComponentApexController.getOrdersForForm(form.Id);
		System.assert(orders.size() > 0);
	}

	@isTest
	static void testGetProductsForConfirmation()
	{
		setup();

		List<ConfirmationProduct__c> prods = OrderEntryComponentApexController.getProductsForConfirmation(confirm.Id);
		System.assert(prods.size()>0);
	}

	@isTest
	static void testSave()
	{
		setup();

		String inOrders ='[{"lineno": 71,"formid": "a013B000001vCi3QAE","seasonid": "a04410000034p0EAAQ","suffix": "1234","hv": "H","code": "QN",'+
	        '"description": "Quick Notes Dry Erase Magnet","qty": 1,"price": 13.5,"createdid": "00541000001IufKAAS","createddate": "2017-04-07T21:08:59.781Z",'+
	        '"lastmodid": "00541000001IufKAAS","lastmoddate": "2017-04-10T15:29:40.939Z","paymt": "O","wholesale": 9,"total": 27}]';

		OrderEntryComponentApexController.saveOrders(form, inOrders, '1');
	}
}