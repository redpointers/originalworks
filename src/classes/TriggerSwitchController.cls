public class TriggerSwitchController {

	@AuraEnabled
	public static TriggerSwitches__c accountSetting() {
		TriggerSwitches__c tswitch = TriggerSwitches__c.getOrgDefaults();

		return tswitch;
	}

/******************************** Account ***********************************/
	/*@AuraEnabled
	public static TriggerSwitches__c setAccountSwitch(Integer param) {
		TriggerSwitches__c tswitch = TriggerSwitches__c.getOrgDefaults();
		Integer num = Integer.valueOf(param);

		if(num == 1) {

			tswitch.AccountSwitch__c = !tswitch.AccountSwitch__c;

			if(!tswitch.AccountSwitch__c) {
				tswitch.AccountBeforeInsertSwitch__c = False;
				tswitch.AccountBeforeUpdateSwitch__c = False;
				tswitch.AccountAfterInsertSwitch__c = False;
				tswitch.AccountAfterUpdateSwitch__c = False;
			} else {
				tswitch.AccountBeforeInsertSwitch__c = True;
				tswitch.AccountBeforeUpdateSwitch__c = True;
				tswitch.AccountAfterInsertSwitch__c = True;
				tswitch.AccountAfterUpdateSwitch__c = True;
			}

		} else if(num == 2) {

			tswitch.AccountBeforeInsertSwitch__c = !tswitch.AccountBeforeInsertSwitch__c;

		} else if(num == 3) {

			tswitch.AccountBeforeUpdateSwitch__c = !tswitch.AccountBeforeUpdateSwitch__c;

		} else if(num == 4) {

			tswitch.AccountAfterInsertSwitch__c = !tswitch.AccountAfterInsertSwitch__c;

		} else if(num == 5) {

			tswitch.AccountAfterUpdateSwitch__c = !tswitch.AccountAfterUpdateSwitch__c;

		}

		update tswitch;
		return tswitch;
	}*/

/******************************** OW Product ***********************************/
	@AuraEnabled
	public static TriggerSwitches__c setOWProductSwitch(Integer param) {
		TriggerSwitches__c tswitch = TriggerSwitches__c.getOrgDefaults();
		Integer num = Integer.valueOf(param);

		if(num == 1) {

			tswitch.OWProductAll__c = !tswitch.OWProductAll__c;

			if(!tswitch.OWProductAll__c) {
				tswitch.OWProductBeforeInsert__c = False;
				tswitch.OWProductBeforeDelete__c = False;
			} else {
				tswitch.OWProductBeforeInsert__c = True;
				tswitch.OWProductBeforeDelete__c = True;
			}

		} else if(num == 2) {

			tswitch.OWProductBeforeInsert__c = !tswitch.OWProductBeforeInsert__c;

		} else if(num == 3) {

			tswitch.OWProductBeforeDelete__c = !tswitch.OWProductBeforeDelete__c;

		}

		update tswitch;
		return tswitch;
	}
}