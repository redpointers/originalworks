@isTest
private class TestRecapReportController
{
	@testSetup static void methodName() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		Confirmation__c confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		Form__c form = UnitTestDataFactory.inflateForm(confirm);
		insert form;
	}

	@isTest
	static void itShould()
	{
		Form__c form = [select Id, Name from Form__c limit 1];

		ApexPages.StandardController stdController = new ApexPages.StandardController(form);
		RecapReportController frc = new RecapReportController(stdController);
		System.assert(frc.orderValue != 0);

		Map<String, List<RecapReportController.TableLine>> linesMap = frc.getSortedOrders();
		System.assert(linesMap != null);
	}
}