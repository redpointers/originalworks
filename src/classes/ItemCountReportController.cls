public with sharing class ItemCountReportController 
{
	public Season__c season{get;set;}
	public List<ItemCountTableLine> tableData{get;set;}
	public Integer grandQty{get;set;}
	public Double grandPerQty{get;set;}
	public Double grandTotal{get;set;}
	public Double grandPerTotal{get;set;}

	public ItemCountReportController(ApexPages.StandardController stdController) 
	{
		Id seasonId = stdController.getId();
        if(seasonId == null)
            seasonId = ApexPages.currentPage().getParameters().get('id');

        season = [select Id, Name from Season__c where Id =: seasonId];

        grandQty = 0;
        grandPerQty = 0;
        grandTotal = 0;
        grandPerTotal = 0;

        List<Id> formIds = null;
        
        if(Test.isRunningTest()) // This will tie the forms to fake order data in the test.
        {
         	List<Form__c> forms = WIPComponentApexController.getFormsForSeason(seasonId);
         	formIds = new List<Id>();
         	for(Form__c form : forms)
         		formIds.add(form.Id);
        }


        List<OrderLines> allOrderLines = WIPComponentApexController.getOrdersForSeason(seasonId,formIds);
        if(allOrderLines != null && allOrderLines.size()>0)
        {
        	List<OrderLines> validOrders = new List<OrderLines>();

        	for(OrderLines line : allOrderLines) {
        		if(line.formid != 'null') {
        			validOrders.add(line);
        		}
        	}
	        List<OrderLines> orderLines = getOnlyActiveConfirmOrders(validOrders);
	        tableData = calculateTableData(orderLines);
	    }
	    else
	    {
	    	tableData = new List<ItemCountTableLine>();
	    	ItemCountTableLine line = new ItemCountTableLine();
	    	line.code = '';
	    	line.name = 'No Orders for this Season';
	    	line.perQty = 0;
	    	line.perValue = 0;
	    	line.qty = 0;
	    	line.value = 0;
	    	tableData.add(line); 
	    }
	}

	private List<OrderLines> getOnlyActiveConfirmOrders(List<OrderLines> allOrderLines)
	{
		List<OrderLines> orderLines = new List<OrderLines>();

		Map<Id, OrderLines> idMap = new Map<Id, OrderLines>();

		for(OrderLines line : allOrderLines)
		{
			if(!idMap.containsKey(line.formid))
				idMap.put(line.formid, line);
		}

		Set<Id> formIds = idMap.keySet();

		Map<Id, Form__c> activeFormsMap = new Map<Id, Form__c>([select Id, Name, Confirmation__r.Status__c from Form__c where Confirmation__r.Status__c='Active' and Id in : formIds]);

		for(OrderLines line : allOrderLines)
		{
			if(activeFormsMap.containsKey(line.formid))
				orderLines.add(line);
		}

		return orderLines;
	}

	private List<ItemCountTableLine> calculateTableData(List<OrderLines> orderLines)
	{
		Map<String, ItemCountTableLine> dataMap = new Map<String, ItemCountTableLine>();

		for(OrderLines line : orderLines)
		{
			grandQty = grandQty + line.qty;

			System.debug('**** grandQty: '+grandQty);

			grandTotal = grandTotal + line.total;

			if(!dataMap.containsKey(line.code))
			{
				ItemCountTableLine ictl = new ItemCountTableLine();
				ictl.code = line.code;
				ictl.name = line.description;
				ictl.qty = line.qty;
				ictl.value = line.total;
				ictl.perQty = ((Double)ictl.qty/grandQty) * 100;

				System.debug('**** ictl.perQty: '+ictl.perQty);

				ictl.perValue = (ictl.value/grandTotal) * 100;

				dataMap.put(line.code, ictl);
			}
			else
			{
				ItemCountTableLine ictl = dataMap.get(line.code);
				ictl.qty = ictl.qty + line.qty;
				ictl.value = ictl.value + line.total;
				ictl.perQty = ((Double)ictl.qty/grandQty) * 100;

				System.debug('**** ictl.perQty: '+ictl.perQty);

				ictl.perValue = (ictl.value/grandTotal) * 100;

				dataMap.put(line.code, ictl);
			}
		}

		if(grandQty > 0)
		{
			grandPerQty = 100;
			grandPerTotal = 100;
		}

		return dataMap.values();
	}


	public class ItemCountTableLine
	{
		public String code{get;set;}
		public String name{get;set;}
		public Integer qty{get;set;}
		public Double perQty{get;set;}
		public Double value{get;set;}
		public Double perValue{get;set;}
	}	
}