@isTest
global class TestDatabaseConfirmProdCalloutMock implements HttpCalloutMock
{
	List<Id> confirmIds;

	public TestDatabaseConfirmProdCalloutMock()
	{}

	public TestDatabaseConfirmProdCalloutMock(List<Id> confirmIds)
	{
		this.confirmIds = confirmIds;
	}

	global HTTPResponse respond(HTTPRequest req) 
	{
        HTTPResponse result = new HTTPResponse();
        
		String body = '[{"Id": "a023B000001dKniQAE","AdjustPriceToParent__c": "5","AdjustPriceToSchool__c": "5","Code__c": "QN","Code_MAS90__c": "QN",'+
        	'"Confirmation__c": "a003B000001aN0HQAU","Name": "CP-747","CreatedById": "00541000001IufKAAS","Description__c": "Quick Notes Dry Erase Magnet",'+
        	'"DFLAdjusted__c": "5","DFLPrice__c": "5","DFLTax__c": "5","LastModifiedById": "00541000001IufKAAS","OWProduct__c": "a034100000BCn2tAAD",'+
        	'"Price_to_Parent__c": 13.5,"Price_to_School__c": 9,"PrintOrder__c": "1","ProductName__c": "Quick Notes Dry Erase Magnet","Shortcut__c": "11",'+
        	'"TaxParent__c": "2","TaxSchool__c": "2","TestProduct__c": false,"CreatedDate": "2017-04-11T16:28:54.000Z","LastModifiedDate": "2017-04-11T16:28:54.000Z",'+
        	'"SeasonProduct_Id__c": "a053B0000012uTSQAY"}]';

	    if(confirmIds !=null && confirmIds.size() >0)
	    {
	    	body = '[{"Id": "a023B000001dKniQAE","AdjustPriceToParent__c": "5","AdjustPriceToSchool__c": "5","Code__c": "QN","Code_MAS90__c": "QN",'+
        	'"Confirmation__c": "'+confirmIds.get(0)+'","Name": "CP-747","CreatedById": "00541000001IufKAAS","Description__c": "Quick Notes Dry Erase Magnet",'+
        	'"DFLAdjusted__c": "5","DFLPrice__c": "5","DFLTax__c": "5","LastModifiedById": "00541000001IufKAAS","OWProduct__c": "a034100000BCn2tAAD",'+
        	'"Price_to_Parent__c": 13.5,"Price_to_School__c": 9,"PrintOrder__c": "1","ProductName__c": "Quick Notes Dry Erase Magnet","Shortcut__c": "11",'+
        	'"TaxParent__c": "2","TaxSchool__c": "2","TestProduct__c": false,"CreatedDate": "2017-04-11T16:28:54.000Z","LastModifiedDate": "2017-04-11T16:28:54.000Z",'+
        	'"SeasonProduct_Id__c": "a053B0000012uTSQAY"}]';
	    }
		
		result.setBody(body);			
		result.setStatusCode(200);
		result.setStatus('OK');
		return result;
    }
}