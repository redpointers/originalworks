public class LeadSourceListReportController {
	/*
   * author: Redpoint
   * date: 4/4/2017
   * purpose: Displays all the values for the Lead Source global picklist.
   * notes:
  */

    public Account acct = new Account();

	public LeadSourceListReportController(ApexPages.StandardController stdcontroller) {
		this.acct = (Account)stdcontroller.getRecord();
	}

	public List<String> getLeadListings() {
		List<String> leads = new List<String>();
		Schema.DescribeFieldResult fieldResult = Account.LeadSource__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		for(Schema.PicklistEntry f : ple) {
  			String temp = String.valueOf(f.getLabel());
  			leads.add(temp);
   		}
   		leads.sort();
   		return leads;
	}
}