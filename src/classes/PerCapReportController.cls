public class PerCapReportController {
	/*
     * author: Redpoint
     * date: 4/24/2017
     * purpose:  
     * notes:
    */
    public List<Confirmation__c> confirmations = new List<Confirmation__c>();
    public List<Form__c> forms = new List<Form__c>();
    public Confirmation__c confirm = new Confirmation__c();
    public String season {get;set;}

    /*Schools Order Entered*/
    public Integer sbybCount {get;set;}
    public Integer opCount {get;set;}
    public Integer tileCount {get;set;}
    public Integer galleryCount {get;set;}

    /*School Population Estimate*/
	public Double sbybEstPop {get;set;}
    public Double opEstPop {get;set;}
    public Double tileEstPop {get;set;}
    public Double galleryEstPop {get;set;}  

    /*SBYB*/
    public Double sbybActualPop {get;set;}
    public Double sbybTotal {get;set;}

    /*OP*/
    public Double opTotal {get;set;}
    public Double opActualPop {get;set;}

    /*Tile Wall*/
    public Double tileTotal {get;set;}
    public Double tileActualPop {get;set;}

    /*Gallery Wall*/
    public Double galleryTotal {get;set;}
    public Double galleryActualPop {get;set;}

    public PerCapReportController(ApexPages.StandardController stdcontroller) {
    	this.confirm = (Confirmation__c)stdcontroller.getRecord();
    	this.season = ApexPages.currentPage().getParameters().get('season');
    }

    public void getInformation() {
    	seasonConfirmations();
    	seasonForms();
    	schoolCount();
    	estimatedPopulations();
    	actualPopulation();
    	totalOrdered();
    }

    public void seasonConfirmations() {
    	confirmations = [SELECT Name, RecordType.Name, SeasonPopEstimate__c
    						FROM Confirmation__c
    						WHERE (Status__c = 'Active') AND (Season__r.Name =: season)];
    }

    public void seasonForms() {
    	forms = [SELECT Confirmation__r.RecordType.Name, ArtworkCount__c, FormType__c, 
    					OrderValueWholesale__c, MagValueWholesale__c, OrderCount__c 
    				FROM Form__c
    				WHERE (Confirmation__r.Status__c = 'Active') AND (Confirmation__r.Season__r.Name =: season)
    			];
    }

   	public void schoolCount() {
   		Set<String> sbyb = new Set<String>();
   		Set<String> op = new Set<String>();
   		Set<String> tile = new Set<String>();
   		Set<String> gallery = new Set<String>();

   		for(Confirmation__c c : confirmations) {

   			if(c.RecordType.Name == 'SBYB' || c.RecordType.Name == 'SS SBYB') {
   				if(!sbyb.contains(c.Name)) {
	    			sbyb.add(c.Name);
	    		}
			}

			if(c.RecordType.Name == 'OP') {
   				if(!op.contains(c.Name)) {
	    			op.add(c.Name);
	    		}
			}

			if(c.RecordType.Name == 'Tile Wall') {
   				if(!tile.contains(c.Name)) {
	    			tile.add(c.Name);
	    		}
			}

			if(c.RecordType.Name == 'Gallery') {
   				if(!gallery.contains(c.Name)) {
	    			gallery.add(c.Name);
	    		}
			}
		}

		sbybCount = sbyb.size();
		opCount = op.size();
		tileCount = tile.size();
		galleryCount = gallery.size();
	}

	public void estimatedPopulations() {
		Double sbyb = 0;
		Double op = 0;
		Double tile = 0;
		Double gallery = 0;

   		for(Confirmation__c c : confirmations) {

   			if( (c.RecordType.Name == 'SBYB' || c.RecordType.Name == 'SS SBYB') && c.SeasonPopEstimate__c != null ) {
   				sbyb += c.SeasonPopEstimate__c;
			}

			if(c.RecordType.Name == 'OP' && c.SeasonPopEstimate__c != null) {
   				op += c.SeasonPopEstimate__c;
			}

			if(c.RecordType.Name == 'Tile Wall' && c.SeasonPopEstimate__c != null) {
   				tile += c.SeasonPopEstimate__c;
			}

			if(c.RecordType.Name == 'Gallery' && c.SeasonPopEstimate__c != null) {
   				gallery += c.SeasonPopEstimate__c;
			}
		}

		sbybEstPop = sbyb;
		opEstPop = op;
		tileEstPop = tile;
		galleryEstPop = gallery;
	}

	public void actualPopulation() {
		Double sbyb = 0;
		Double op = 0;
		Double tile = 0;
		Double gallery = 0;

		for(Form__c f : forms) {

			if( (f.Confirmation__r.RecordType.Name == 'SBYB' || f.Confirmation__r.RecordType.Name == 'SS SBYB') && (f.FormType__c == 'A') && (f.ArtworkCount__c != null)) {
				sbyb += f.ArtworkCount__c;
			}

			if( (f.Confirmation__r.RecordType.Name == 'OP')  && (f.FormType__c == 'D') && (f.OrderCount__c != null)) {
				op += f.OrderCount__c;
			}

			if( (f.Confirmation__r.RecordType.Name == 'Tile Wall')  && (f.FormType__c == 'D') && (f.OrderCount__c != null)) {
				tile += f.OrderCount__c;
			}

			if( (f.Confirmation__r.RecordType.Name == 'Gallery') && (f.FormType__c == 'G') && (f.ArtworkCount__c != null)) {
				gallery += f.ArtworkCount__c;
			}
		}

		sbybActualPop = sbyb;
		opActualPop = op;
		tileActualPop = tile;
		galleryActualPop = gallery;
	}

	public void totalOrdered() {
		Double s_total = 0;
		Double o_total = 0;
		Double t_total = 0;
		Double g_total = 0;

		for(Form__c f : forms) {
			
			if(f.Confirmation__r.RecordType.Name == 'SBYB') {
				
				if(f.OrderValueWholesale__c != null) {
					if(f.MagValueWholesale__c != null) {
						s_total += (f.OrderValueWholesale__c + f.MagValueWholesale__c);
					} else {
						s_total += f.OrderValueWholesale__c; 
					}
				}
			}

			if(f.Confirmation__r.RecordType.Name == 'OP') {
				if(f.OrderValueWholesale__c != null) {
					o_total += f.OrderValueWholesale__c;
				}
			}

			if(f.Confirmation__r.RecordType.Name == 'Tile Wall') {
				if(f.OrderValueWholesale__c != null) {
					t_total += f.OrderValueWholesale__c;
				}
			}

			if(f.Confirmation__r.RecordType.Name == 'Gallery') {
				if(f.OrderValueWholesale__c != null) {
					g_total += f.OrderValueWholesale__c;
				}
			}
		}
		sbybTotal = s_total;
		opTotal = o_total;
		tileTotal = t_total;
		galleryTotal = g_total;
	}

	public Double getSBYBPerCap() {
		if(sbybActualPop != 0 && sbybActualPop != null) {
			return (sbybTotal / sbybActualPop);
		}
		return 0;
	}

	public Double getOPPercap() {
		if(opActualPop != 0 && opActualPop != null) {
			return (opTotal / opActualPop);
		}
		return 0;
	}

	public Double getTilePercap() {
		if(tileActualPop != 0 && tileActualPop != null) {
			return (tileTotal / tileActualPop);
		}
		return 0;
	}

	public Double getGalleryPercap() {
		if(galleryActualPop != 0 && galleryActualPop != null) {
			return (galleryTotal / galleryActualPop);
		}
		return 0;
	}

	public Double getSBYBAverageOrderValue() {
		Double count = 0;
		Double ret = 0;

		for(Form__c f : forms) {

			if( (f.Confirmation__r.RecordType.Name == 'SBYB' || f.Confirmation__r.RecordType.Name == 'SS SBYB') && (f.FormType__c == 'B') && (f.OrderCount__c != null)) {
				count += f.OrderCount__c;
			}
		}

		if(count != 0) {
			ret = (sbybTotal / count);
		}

		return ret;
	}

	public Double getGalleryAverageOrderValue() {
		Double count = 0;
		Double ret = 0;

		for(Form__c f : forms) {

			if(f.Confirmation__r.RecordType.Name == 'Gallery' && f.OrderCount__c != null) {
				count += f.OrderCount__c;
			}
		}

		if(count != 0) {
			ret = (galleryTotal / count);
		}

		return ret;
	}
}