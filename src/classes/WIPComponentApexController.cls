public with sharing class WIPComponentApexController 
{
	@AuraEnabled
    public static List<Season__c> getSeasons() 
    {
        List<Season__c> seasons = [SELECT Id, Name FROM Season__c ORDER BY StartDate__c DESC];
        return seasons;
    }

    @AuraEnabled
    public static Id getCurrentSeason()
    {
        Variables__c var = Variables__c.getOrgDefaults();
        String defSeason = var.Default_Season__c;
        Season__c currentSeason = null;

        if(!Test.isRunningTest())
            currentSeason = [select Id, Name from Season__c where Name =: defSeason];
        else
            currentSeason = UnitTestDataFactory.inflateSeason(defSeason);

        return currentSeason.Id;
    }

    @AuraEnabled
    public static DateTime getStartDate() 
    {
        Datetime current = System.now(); // returns date time value in GMT time zone.
 
        Date currDate = current.date();
        Time currTime = current.time();
 
        Datetime local = Datetime.newinstance(currDate,currTime);
        return local;
    }

    @AuraEnabled
    public static List<String> getWarehouses()
    {
    	Schema.DescribeFieldResult fieldResult = Confirmation__c.Warehouse__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        List<String> warehouses = new List<String>();
	   	for( Schema.PicklistEntry f : ple)
	   	{
	   		warehouses.add(f.getValue());
	   	} 

	   	return warehouses;
    }

    @AuraEnabled
	public static List<Form__c> getFormsForSeason(Id seasonId)
	{
		List<Form__c> forms = [select Id, Name, FormType__c, ActualShip__c,WarehouseAssigned__c,MagsBought__c,Confirmation__r.RecordType.Name, ArtworkCount__c, ArtworkCountOE__c,
            OrderValueWholesale__c from Form__c where Confirmation__r.Season__c =: seasonId];
		return forms;
	}

    @AuraEnabled
    public static List<Confirmation__c> getConfirmsForSeason(Id seasonId)
    {
        List<Confirmation__c> confirms = [select Id, Name, Projection__c, SeasonPopEstimate__c, ActualPop__c, TotalRevenue__c from Confirmation__c where Season__c =: seasonId];
        return confirms;
    }

    @AuraEnabled
    public static List<OrderLines> getOrdersForSeason(Id seasonId)
    {
        return WIPComponentApexController.getOrdersForSeason(seasonId,null);
    }

    @AuraEnabled
	public static List<OrderLines> getOrdersForSeason(Id seasonId, List<Id> formIds)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getRecordsForSeason?seasonid='+seasonid);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock();
            if(formIds != null)
                mock = new TestDatabaseCalloutMock(formIds);

            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            List<OrderLines> lines = OrderEntryComponentApexController.parseJSONLinesBody(res.getBody());
            return lines;
        }
        catch(Exception e)
        {
        	throw new AuraException(e.getMessage());
        }

		return null;
	}
}