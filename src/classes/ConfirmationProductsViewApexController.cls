public with sharing class ConfirmationProductsViewApexController 
{
	@AuraEnabled
	public static List<ConfirmationProduct__c> getProductsForConfirm(Id confirmId)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getConfirmationProductsForConfirm?confirmid='+confirmId);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock(new List<Id>{confirmId});
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            List<ConfirmationProduct__c> lines = parseJSONCPBody(res.getBody());
            return lines;
        }
        catch(Exception e)
        {
        	throw new AuraException(e.getMessage());
        }

		return null;
	}

	public static List<ConfirmationProduct__c> parseJSONCPBody(String body)
    {
    	JSONParser parser = JSON.createParser(body);

    	List<ConfirmationProduct__c> lines = new List<ConfirmationProduct__c>();
    	while (parser.nextToken() != null) 
    	{
        	// Start at the array of lines.
        	if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
        	{
            	while (parser.nextToken() != null) 
            	{
               		// Advance to the start object marker to find next line object.
                	if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                	{
                		ConfirmationProduct__c line = (ConfirmationProduct__c)parser.readValueAs(ConfirmationProduct__c.class);
                    	lines.add(line);
                	}
                }
            }
        }

        return lines;
    }
}