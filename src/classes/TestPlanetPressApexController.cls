@isTest
private class TestPlanetPressApexController
{
	static Confirmation__c confirm;
	static Form__c form;
	static ConfirmationProduct__c confirmProd;
	static Season__c season;

	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();
		UnitTestDataFactory.insertPlanetPress();
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		Form__c formA = UnitTestDataFactory.inflateForm(confirm);
		formA.WarehouseAssigned__c='AM';
		formA.FormType__c = 'A';
		insert formA;

		form = UnitTestDataFactory.inflateForm(confirm);
		form.WarehouseAssigned__c='AM';
		form.FormType__c = 'B';
		form.ActualShip__c = null;
		form.isPPFileCreated__c = false;
		insert form;

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c sp = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert sp;

		confirmProd = UnitTestDataFactory.inflateConfirmationProduct(confirm, sp);
		insert confirmProd;
	}

	@isTest
	static void testGetForms()
	{
		setup();

		List<Form__c> forms = PlanetPressApexController.getForms();
		System.assert(forms.size() == 1);

	}

	@isTest
	static void testMarks()
	{
		setup();

		List<Form__c> forms = PlanetPressApexController.getForms();
		
		for(Form__c fm : forms)
		{
			Boolean success = PlanetPressApexController.getMarkScanned(fm.Id, true);
			System.assert(success);

			success = PlanetPressApexController.getMarkFileCreated(fm.Id, true);
			System.assert(success);
		}

	}

	@isTest
	static void testGetExport()
	{
		setup();

		List<Form__c> forms = PlanetPressApexController.getForms();
		String result = PlanetPressApexController.getExport(forms.get(0).Id, 'TAC');
		System.assert(result!=null);
	}
}