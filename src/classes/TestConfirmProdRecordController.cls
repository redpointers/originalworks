@isTest
private class TestConfirmProdRecordController
{
	@isTest
	static void testGetData()
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();

		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		Confirmation__c confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c seasonProd = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert seasonProd;

		List<ConfirmationProduct__c> lines = ConfirmationProductsViewApexController.getProductsForConfirm(confirm.Id);
	
		ConfirmationProduct__c cp = ConfirmationProductRecordApexController.getProduct(lines.get(0).Id);

		String userStr = ConfirmationProductRecordApexController.getUserDateString(cp.LastModifiedById, JSON.serialize(cp.LastModifiedDate));
		String confirmStr = ConfirmationProductRecordApexController.getConfirmationName(confirm.Id);
		String spStr = ConfirmationProductRecordApexController.getSeasonProductName(seasonProd.Id);
		String owStr = ConfirmationProductRecordApexController.getOWProductName(prod.Id);

		String jsonObj = JSON.serialize(cp);
		String status = ConfirmationProductRecordApexController.updateProduct(jsonObj);
		System.assert(status != null);

	}
}