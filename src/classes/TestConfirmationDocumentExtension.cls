@isTest
private class TestConfirmationDocumentExtension {
	/*
	 * author: Redpoint
	 * date: 1/31/2017
	 * purpose: Test coverage for the ConfirmationDocumentExtension class.
	 * note: 99% test coverage as of 4/6
	*/
    static Account acct = new Account();
    static Account acct2 = new Account();
    static Contact rep = new Contact();
    static Season__c season = new Season__c();
    static SeasonProduct__c seasonproduct = new SeasonProduct__c();
    static Confirmation__c conf = new Confirmation__c();
    static Confirmation__c conf2 = new Confirmation__c();

    static void setup() {
        UnitTestDataFactory.insertVariables();

        Variables__c var = Variables__c.getOrgDefaults();

        rep.FirstName = 'Test Sales';
        rep.LastName = 'Rep';
        rep.Phone = '1234569987';
        insert rep;

        acct.Name = 'Unit Test Account';
        acct.Phone = '1234567899';
        acct.Fax = '1234567855';
        acct.SalesRep__c = rep.Id;
        acct2.Name = 'Unit Test Account 2';
        season.Name = var.Default_Season__c;
        RecordType recordtype = [SELECT Id 
                                                FROM RecordType 
                                                WHERE Name = 'OP' 
                                                LIMIT 1];
        
        insert acct;
        insert acct2;
        insert season;
        seasonproduct.Season__c = season.Id;
        insert seasonproduct;

        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        conf.SalesRep__c = rep.Id;
        insert conf;

        conf2.RecordTypeId = recordtype.Id;
        conf2.Account__c = acct2.Id;
        conf2.Season__c = season.Id;
        insert conf2;
    }

    static Testmethod void testGetInfo() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);

        Confirmation__c test = confdoc.getInfo();
        System.assertEquals(conf.Id, test.Id);
    }

    static Testmethod void testGetNonNullPhoneNumbers() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);

        confdoc.getInfo();

        String testSC = confdoc.getSchoolPhoneNumber();
        System.assertEquals('(123) 456-7899', testSC);

        String testFA = confdoc.getSchoolFaxNumber();
        System.assertEquals('(123) 456-7855', testFA);

        String testRE = confdoc.getRepPhoneNumber();
        System.assertEquals('(123) 456-9987', testRE);
    }

    static Testmethod void testGetNullPhoneNumbers() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf2);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);

        confdoc.getInfo();

        String testSC = confdoc.getSchoolPhoneNumber();
        System.assertEquals('', testSC);

        String testFA = confdoc.getSchoolFaxNumber();
        System.assertEquals('', testFA);

        String testRE = confdoc.getRepPhoneNumber();
        System.assertEquals('', testRE);

        String testDFL = confdoc.getDFLPhoneNumber();
        System.assertEquals('', testDFL);

        String testCO = confdoc.getCoordPhoneNumber();
        System.assertEquals('', testCO);

        String testCO2 = confdoc.getCoordSecPhone();
        System.assertEquals('', testCO2);

        String testCOM = confdoc.getCoordMobilePhoneNumber();
        System.assertEquals('', testCOM);

        String testART = confdoc.getArtPhone();
        System.assertEquals('', testART);
    }

    static Testmethod void testGetConfirmationProducts() {
        setup();
       
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);
        
        List<ConfirmationProduct__c> confirmationproducts = new List<ConfirmationProduct__c>();
        confirmationproducts = confdoc.getConfirmationProducts();
        System.assertEquals(confirmationproducts.size(), 1);
        //System.assertEquals(confirmationproducts.size(), 0);//Data migration value
    }
    
    static Testmethod void testGetConfirmationContacts() {
        setup();
        
        Contact contact = new Contact(AccountId=acct.Id,FirstName='Test', LastName='Contact');
        insert contact;
        ConfirmationContact__c ccon = new ConfirmationContact__c();
        ccon.Confirmation__c = conf.Id;
        ccon.Contact__c = contact.Id;
        ccon.Role__c = 'DFL Coord';
        insert ccon;
       
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);
        
        List<ConfirmationContact__c> cconList = new List<ConfirmationContact__c>();
        cconList = confdoc.getConfirmationContacts();
        System.assertEquals(cconList.size(), 1);
    }
    
    static Testmethod void testGetDFL() {
       setup();
        
        Contact contact = new Contact(AccountId=acct.Id,FirstName='Test', LastName='Contact', Phone = '1234567741');
        insert contact;
        ConfirmationContact__c ccon = new ConfirmationContact__c();
        ccon.Confirmation__c = conf.Id;
        ccon.Contact__c = contact.Id;
        ccon.Role__c = 'DFL Coord';
        insert ccon;

        Contact contact2 = new Contact(AccountId=acct.Id,FirstName='Test', LastName='Contact2', Phone = '1234567741', MobilePhone = '1234569956');
        insert contact2;
        ConfirmationContact__c ccon2 = new ConfirmationContact__c();
        ccon2.Confirmation__c = conf.Id;
        ccon2.Contact__c = contact2.Id;
        ccon2.Role__c = 'Coordinator';
        insert ccon2;
      
        Contact contact3 = new Contact(AccountId=acct.Id,FirstName='Test', LastName='Contact3', Phone = '1234567741');
        insert contact3;
        ConfirmationContact__c ccon3 = new ConfirmationContact__c();
        ccon3.Confirmation__c = conf.Id;
        ccon3.Contact__c = contact3.Id;
        ccon3.Role__c = 'Coordinator Sec';
        insert ccon3;
        
        Contact contact4 = new Contact(AccountId=acct.Id,FirstName='Test', LastName='Contact4', Phone = '1234567741');
        insert contact4;
        ConfirmationContact__c ccon4 = new ConfirmationContact__c();
        ccon4.Confirmation__c = conf.Id;
        ccon4.Contact__c = contact4.Id;
        ccon4.Role__c = 'Art Teacher';
        insert ccon4;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);
        
        Contact dfl = confdoc.getDFL();
        System.assertEquals(dfl.Id, contact.Id);
        
        Contact coord = confdoc.getCoordinator();
        System.assertEquals(coord.Id, contact2.Id);
        
        Contact coord2 = confdoc.getCoordinatorTwo();
        System.assertEquals(coord2.Id, contact3.Id);
        
        Contact art = confdoc.getArt();
        System.assertEquals(art.Id, contact4.Id);

        String testDFL = confdoc.getDFLPhoneNumber();
        System.assertEquals('(123) 456-7741', testDFL);

        String testCO = confdoc.getCoordPhoneNumber();
        System.assertEquals('(123) 456-7741', testCO);

        String testCO2 = confdoc.getCoordSecPhone();
        System.assertEquals('(123) 456-7741', testCO2);

        String testCOM = confdoc.getCoordMobilePhoneNumber();
        System.assertEquals('(123) 456-9956', testCOM);

        String testART = confdoc.getArtPhone();
        System.assertEquals('(123) 456-7741', testART);
    }
    
    static Testmethod void testSaveAsPDF() {
        setup();
        
        ApexPages.currentPage().getParameters().put('Id', conf.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        ConfirmationDocumentExtension confdoc = new ConfirmationDocumentExtension(sc);
        
        String test = confdoc.getConfirmationDocument().getURL();
        System.assertEquals('/' + conf.Id, test);

        List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf.Id];
        System.assertEquals(1, attachments.size());
    }
}