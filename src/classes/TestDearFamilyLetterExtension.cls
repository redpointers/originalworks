@isTest
private class TestDearFamilyLetterExtension {
	
    static Account acct = new Account();
    static Season__c season = new Season__c();
    static SeasonProduct__c seasonproduct = new SeasonProduct__c();
    static Confirmation__c conf = new Confirmation__c();

    @isTest
    static void setup() {
        UnitTestDataFactory.insertVariables();

        Variables__c var = Variables__c.getOrgDefaults();

        acct.Name = 'Unit Test Account';
        acct.SchoolCode__c = 'UTAC';
        season.Name = var.Default_Season__c;
        RecordType recordtype = [SELECT Id 
                                    FROM RecordType 
                                    WHERE Name = 'OP' 
                                    LIMIT 1];
        
        insert acct;
        insert season;
        seasonproduct.Season__c = season.Id;
        insert seasonproduct;
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;
    }

    @isTest
    static void testGetInfo() {
        setup();
       
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);
        
        Confirmation__c test = dfle.getInfo();
        System.assertNotEquals(null, test);
        System.assertEquals(conf.Id, test.Id);
    }

    @isTest
    static void testGetConfirmationProducts() {
        setup();
       
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);
        
        List<ConfirmationProduct__c> confirmationproducts = new List<ConfirmationProduct__c>();
        confirmationproducts = dfle.getConfirmationProducts();
        System.assertEquals(1, confirmationproducts.size());
        //System.assertEquals(0, confirmationproducts.size());//Data Migration value

        List<ConfirmationProduct__c> confirmationproductsspanish = new List<ConfirmationProduct__c>();
        confirmationproductsspanish = dfle.getConfirmationProductsSpanish();
        System.assertEquals(1, confirmationproductsspanish.size());
        //System.assertEquals(0, confirmationproductsspanish.size());//Data migration value
    }

    @isTest
    static void testGetFormatPhone() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);

        dfle.phone = 5758419623.0;
        String test = dfle.getFormatPhone();
        System.assertEquals('(575) 841-9623', test);

        ApexPages.StandardController sc2 = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle2 = new DearFamilyLetterExtension(sc);

        String test2 = dfle2.getFormatPhone();
        System.assertEquals('', test2);
    }
    
    @isTest
    static void testGetDFLCoord() {
        setup();
       
        Contact contact = new Contact(AccountId=acct.Id,FirstName='Test', LastName='Contact');
        insert contact;
        ConfirmationContact__c ccon = new ConfirmationContact__c();
        ccon.Confirmation__c = conf.Id;
        ccon.Contact__c = contact.Id;
        ccon.Role__c = 'DFL Coord';
        insert ccon;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);
        
        Contact getcon = new Contact();
        getcon = dfle.getDFLCoord();
        
        System.assertEquals(getcon.Id, contact.Id);
    }

    @isTest
    static void testGetEnglishDFL() {
        setup();

        ApexPages.currentPage().getParameters().put('Id', conf.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);

        String testURL = dfle.getEnglishDFL().getURL();
        System.assertEquals('/' + conf.Id, testURL);

        String testURL2 = dfle.getEnglishDFLSBYB().getURL();
        System.assertEquals('/' + conf.Id, testURL2);

        List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf.Id];
        System.assertEquals(2, attachments.size());
    }

    @isTest
    static void testGetSpanishDFL() {
        setup();

        ApexPages.currentPage().getParameters().put('Id', conf.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);

        String testURL = dfle.getSpanishDFL().getURL();
        System.assertEquals('/' + conf.Id, testURL);

        String testURL2 = dfle.getSpanishDFLSBYB().getURL();
        System.assertEquals('/' + conf.Id, testURL2); 

        List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf.Id];
        System.assertEquals(2, attachments.size());  
    }

    @isTest
    static void testGetDFLSBYBOP() {
        setup();

        ApexPages.currentPage().getParameters().put('Id', conf.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);

        String testURL = dfle.getDFLSBYBOP().getURL();
        System.assertEquals('/' + conf.Id, testURL);

        List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf.Id];
        System.assertEquals(1, attachments.size());   
    }

// this test fails due to the Confirmation trigger not allowing multiple confirms on an Account in one season. 
    //static Testmethod void testGetDFLSBYBOPPart2() {
    //    setup();

    //    Confirmation__c conf2 = new Confirmation__c();
    //     RecordType recordtype = [SELECT Id 
    //                                FROM RecordType 
    //                                WHERE Name = 'SBYB' 
    //                                LIMIT 1];

    //    conf2.RecordTypeId = recordtype.Id;
    //    conf2.Account__c = acct.Id;
    //    conf2.Season__c = season.Id;
    //    insert conf2;

    //    ApexPages.currentPage().getParameters().put('Id', conf2.Id);
    //    ApexPages.StandardController sc = new ApexPages.StandardController(conf2);
    //    DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);

    //    String testURL = dfle.getDFLSBYBOP().getURL();
    //    System.assertEquals('/' + conf2.Id, testURL);

    //    List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf2.Id];
    //    System.assertEquals(1, attachments.size());    
    //}
    
    @isTest
    static void testGetGalleryForm() {
        setup();
       
        ApexPages.currentPage().getParameters().put('Id', conf.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);
        
        String testURL = dfle.getGalleryForm().getURL();
        System.assertEquals('/' + conf.Id, testURL);

        List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf.Id];
        System.assertEquals(1, attachments.size()); 
    }

    @isTest
    static void testGetEnglishSpanishDFLs() {
        setup();

        ApexPages.currentPage().getParameters().put('Id', conf.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        DearFamilyLetterExtension dfle = new DearFamilyLetterExtension(sc);

        String testURL = dfle.getEnglishSpanishDFLs().getURL();
        System.assertEquals('/' + conf.Id, testURL);  

        List<Attachment> attachments = [SELECT Id, Name FROM Attachment WHERE ParentId = : conf.Id];
        System.assertEquals(2, attachments.size()); 
    }
}