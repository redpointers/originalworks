@isTest
private class TestConfirmationProductTrigger
{
	static Season__c season;
	static Confirmation__c confirm;
	static Form__c form;
	static ConfirmationProduct__c confirmProd;

	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();

		Variables__c var = Variables__c.getOrgDefaults();
		String defSeason = var.Default_Season__c;
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		season = UnitTestDataFactory.inflateSeason(defSeason);
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

	}

	@isTest
	static void testInsert()
	{
		setup();

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c seasonProd = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert seasonProd;

		ConfirmationProduct__c cp = UnitTestDataFactory.inflateConfirmationProduct(confirm, seasonProd);
		insert cp;

		cp.PrintOrder__c = 10;
		update cp;

	}
}