public with sharing class ProductionPlannerController {
    /*
     * author: Redpoint
     * purpose: Apex controller for the Production Planner Lightning Component. Will
     * 				take the filtered input data from the user facing lightning component
     * 				and generate a SOQl query. The controller will also be capable of taking the
     * 				query results and generating a downloadable csv file. 
     * date: 2/2/2017
     * notes:
	*/
    
    /*************************************** Global Variables ***************************************/
    public static List<Form__c> query = new List<Form__c>();
    public static List<String> sQuery = new List<String>();
    
    /*************************************** Used for the dynamic Season picklist in the filter section ***************************************/
    @AuraEnabled
    public static List<Season__c> getSeasons() {
        List<Season__c> seasons = [SELECT Name FROM Season__c ORDER BY StartDate__c DESC];
        
        return seasons;
    }
   
    /*************************************** Methods used for building the query ***************************************/
    @AuraEnabled
    public static List<Form__c> buildQuery(String scheduleDate, String startRange, String endRange, 
                                           						String season, String recordType, String tileSize,
                                           						String formType, String warehouse, String customerStatus,
                                           						String stageOne) {
                                                                    
        String filterDate = buildScheduleRange(scheduleDate, startRange, endRange);
        String seasonPick = buildSeason(season);
        String recType = buildRType(recordType);
        String tSize = buildTSize(tileSize);
        String form = buildFType(formType);
        String wh = buildWH(warehouse);
        String cStatus = buildStatus(customerStatus);
        String stage1 = buildStage(stageOne);
        
        String summaryFields = 'SELECT Id, FormType__c, FormSequence__c, WarehouseTransferDate__c, OrderEntry__c, WarehouseAssigned__c,' +
            								'ActualReceive__c, ActualShip__c, OrderValueWholesale__c, MagValueWholesale__c, ArtworkCount__c,' + 
            								'Confirmation__r.Projection__c, Confirmation__r.FTType__c, Confirmation__r.Season__r.Name,' +
            								'Confirmation__r.RecordType.Name, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Account__r.BillingState,' + 
            								'Confirmation__r.SeasonPopEstimate__c, Confirmation__r.SalesRep__r.Name ' + 
            							   	'FROM Form__c ';
        
        String finalFilters = 'WHERE ' +  filterDate + recType + tSize + form + wh + seasonPick + cStatus + stage1;
       
        query = Database.query(summaryfields + finalFilters);
       	setSQ(summaryfields + finalFilters);

        return query;
    }
    
    public static String buildScheduleRange(String scheduleDate, String startRange, String endRange) {
        if( (startRange == null) && (endRange == null) ) {
            return ' ';
        } else 
            return  '(' + scheduleDate +  '>' + startRange + ' AND ' + scheduleDate + ' < ' + endRange + ')';
    }
    
    public static String buildSeason(String s) {
        String filter;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else {
            filter = ' AND (Confirmation__r.Season__r.Name = ' + '\'' +  s +  '\'' + ') ';
        }
        
        return filter;
    }
    
    public static String buildRType(String s) {
        List<String> split = new List<String>();
        String temp = String.valueOf(s);
        String filter = ' AND (';
        Integer i;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else if( temp.contains(';') ) {
            
            split = temp.split(';');
            
            for(i = 0; i < split.size(); i++) {
                
                filter += ' Confirmation__r.RecordType.Name = ' + '\'' + split.get(i) + '\'';
                
                if( i != split.size()-1) {
                    filter += ' OR ';
                }
               
            }
            filter += ' )';
        } else {
        	filter += ' Confirmation__r.RecordType.Name = '+ '\'' +  s +  '\'' + ' )';
        }
        
        return filter;
    }
    
    public static String buildTSize(String s) {
        List<String> split = new List<String>();
        String temp = String.valueOf(s);
        String filter = ' AND (';
        Integer i;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else if( temp.contains(';') ) {
            
            split = temp.split(';');
            
            for(i = 0; i < split.size(); i++) {
                
                filter += ' Confirmation__r.' + split.get(i) + ' = True';
                
                if( i != split.size()-1) {
                    filter += ' OR ';
                }

            }
            filter += ' )';
        } else {
        	filter += ' Confirmation__r.' +  s  + ' = True ' +  ' )';
        }

        return filter;
    }
    
    public static String buildFType(String s) {
        List<String> split = new List<String>();
        String temp = String.valueOf(s);
        String filter = ' AND (';
        Integer i;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else if( temp.contains(';') ) {
            
            split = temp.split(';');
            
            for(i = 0; i < split.size(); i++) {
                
                filter += ' FormType__c = ' + '\'' + split.get(i) + '\'';
                
                if( i != split.size()-1) {
                    filter += ' OR ';
                }
               
            }
            filter += ' )';
        } else {
        	filter += ' FormType__c = '+ '\'' +  s +  '\'' + ' )';
        }
        
        return filter;
    }
    
    public static String buildWH(String s) {
        String temp = String.valueOf(s);
        String filter;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else if( temp.contains(';') ) {
            filter = ' AND (WarehouseAssigned__c = \'AM\' OR WarehouseAssigned__c = \'ST\' ) ';
        } else {
        	filter = ' AND (WarehouseAssigned__c = ' + '\'' +  s +  '\'' + ') ';
        }
        
        return filter;
    }

    public static String buildStatus(String s) {
        List<String> split = new List<String>();
        String temp = String.valueOf(s);
        String filter = ' AND (';
        Integer i;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else if( temp.contains(';') ) {
            
            split = temp.split(';');
            
            for(i = 0; i < split.size(); i++) {
                filter += statusCheck(split.get(i));
                
                if( i != split.size()-1) {
                    filter += ' OR ';
                }
                
            }
            filter += ' )';
        } else {
            filter += statusCheck(s) + ' )';
        }
       
        return filter;
    }
    
    public static String statusCheck(String s) {
        String filter;
        
        if(s == 'True') {
            filter = 'Confirmation__r.Account__r.RepeatCustomer__c = True';
        } else if(s == 'False') {
            filter = 'Confirmation__r.Account__r.RepeatCustomer__c = False';
        } else if(s == 'Active') {
            filter = 'Confirmation__r.Status__c = \'Active\'';
        } else if(s == 'Inactive') {
            filter = 'Confirmation__r.Status__c = \'Inactive\'';
        }
        return filter;
    }
    
    public static String buildStage(String s) {
        List<String> split = new List<String>();
        String temp = String.valueOf(s);
        String filter = ' AND (';
        Integer i;
        
        if(s == '' || s == null) {
            filter = ' ';
        } else if( temp.contains(';') ) {
            
            split = temp.split(';');
            
            for(i = 0; i < split.size(); i++) {
                filter += stageCheck(split.get(i));
                
                if( i != split.size()-1) {
                    filter += ' AND ';
                }
                
            }
            filter += ' )';
        } else {
            filter += stageCheck(s) + ' )';
        }
       
        return filter;
    }
    
    public static String stageCheck(String s) {
        String filter;
        
        if(s == 'all') {
            filter = ' EmailSentReceive__c = True AND OrderEntry__c != null AND WarehouseAssigned__c != null AND EmailSentShip__c = True ';
        } else if(s == 'received') {
            filter = ' EmailSentReceive__c = True ';
        } else if(s == 'orderEntry') {
            filter = ' OrderEntry__c != null ';
        } else if(s == 'whAssignment') {
            filter = ' WarehouseAssigned__c != null ';
        } else if(s == 'shipped') {
            filter = ' EmailSentShip__c = True ';
        } else if(s == 'n_all') {
            filter = ' EmailSentReceive__c = False AND OrderEntry__c = null AND WarehouseAssigned__c = null AND EmailSentShip__c = False ';
        } else if(s == 'n_received') {
            filter = ' EmailSentReceive__c = False ';
        } else if(s == 'n_orderEntry') {
            filter = ' OrderEntry__c = null ';
        } else if(s == 'n_whAssignment') {
            filter = ' WarehouseAssigned__c = null ';
        } else if(s == 'n_shipped') {
            filter = ' EmailSentShip__c = False '; 
        } else {
            filter = ' ';
        }
        
        return filter;
    }
    
    /*************************************** Method used for sorting the table ***************************************/
    @AuraEnabled
    public static List<Form__c> sortOnColumn(Integer num, String unsorted) {
        Integer columnNumber = 0;
        columnNumber = Integer.valueOf(num);
        
        if(columnNumber == 1) {
            unsorted += ' ORDER BY ActualShip__c DESC';
        } else if(columnNumber == 2) {
            unsorted += ' ORDER BY Confirmation__r.FTType__c DESC';
        } else if(columnNumber == 3) {
            unsorted += ' ORDER BY ActualReceive__c DESC';
        } else if(columnNumber == 4) {
            unsorted += ' ORDER BY Confirmation__r.Season__r.StartDate__c DESC';
        } else if(columnNumber == 5) {
            unsorted += ' ORDER BY Confirmation__r.RecordType.Name DESC';
        } else if(columnNumber == 6) {
			unsorted += ' ORDER BY FormType__c ASC';            
        } else if(columnNumber == 7) {
            unsorted += ' ORDER BY FormSequence__c ASC';
        } else if(columnNumber == 8) {
            unsorted += ' ORDER BY Confirmation__r.Account__r.SchoolCode__c ASC';
        } else if(columnNumber == 9) {
            unsorted += ' ORDER BY Confirmation__r.Projection__c DESC';
        } else if(columnNumber == 10) {
            unsorted += ' ORDER BY WarehouseAssigned__c ASC';
        } else if(columnNumber == 11) {
            unsorted += ' ORDER BY ArtworkCount__c DESC';
        } else if(columnNumber == 12) {
            unsorted += ' ORDER BY Confirmation__r.Account__r.BillingState ASC';
        } else if(columnNumber == 13) {
            unsorted += ' ORDER BY OrderEntry__c DESC';
        } else if(columnNumber == 14) {
            unsorted += ' ORDER BY WarehouseTransferDate__c DESC';
        } else if(columnNumber == 15) {
            unsorted += ' ORDER BY MagValueWholesale__c DESC';
        } else if(columnNumber == 16) {
            unsorted += ' ORDER BY OrderValueWholesale__c DESC';
        } else if(columnNumber == 17) {
            unsorted += ' ORDER BY Confirmation__r.SalesRep__r.Name ASC';
        } 
        query = Database.query(unsorted);
        return query;
    }
    
    /*************************************** Summary Counts ***************************************/
    @AuraEnabled
    public static Integer getCount() {
        return query.size();
    }
    
    @AuraEnabled
    public static Decimal getProjectedRevenue() {
        Decimal rev = 0;
        for(Form__c q : query) {
            if(q.Confirmation__r.Projection__c != null) {
                rev += q.Confirmation__r.Projection__c;
            }
        }
        return rev;
    }
    
    @AuraEnabled 
    public static Decimal getMagnetsProjected() {
        Decimal proj = 0;
        for(Form__c q : query) {
            if((q.Confirmation__r.SeasonPopEstimate__c != null) && (q.Confirmation__r.RecordType.Name == 'SBYB') ) {
                proj += q.Confirmation__r.SeasonPopEstimate__c;
        	}
        }
        return proj;
    }
    
    @AuraEnabled
    public static Double getMagnetsMade() {
        Double num = 0;
        for(Form__c q : query) {
            if((q.ArtworkCount__c != null) && (q.Confirmation__r.RecordType.Name == 'SBYB') && (q.FormType__c == 'A')) {
                num += q.ArtworkCount__c;
            }
        }
        return num;
    }
    
    @AuraEnabled
    public static Decimal getMagDollarValue() {
        Decimal value = 0;
        for(Form__c q : query) {
            if(q.MagValueWholesale__c != null) {
                value += q.MagValueWholesale__c;
            }
        }
        return value;
    }
    
    @AuraEnabled
    public static Decimal getOrderValue() {
        Decimal value = 0;
        for(Form__c q : query) {
            if(q.OrderValueWholesale__c != null) {
                value += q.OrderValueWholesale__c;
            }
        }
        return value;
    }
    
    @AuraEnabled 
    public static Decimal getTotalValue() {
        Decimal v1 = getMagDollarValue();
        Decimal v2 = getOrderValue();
        return v1 + v2;
    }
    
    /*************************************** Miscellaneous ***************************************/
    @AuraEnabled
    public static void setSQ(String s) {
        sQuery.add(s);
    }
    
    @AuraEnabled 
    public static String getSQ() {
        return sQuery.get(0);
    }
    
    @AuraEnabled
    public static String createCSV(List<Form__c> results) {
        String file = 'data:text/csv;charset=utf-8,';
        String header = 'Ship Act, F/T Type, Rec Act, Season, Type, Form, FS, Code, Projection, '+ 
            					' WH, Artwork, State, OE, WH Tran, SBYB $, Order $, Sales Rep \n';
        
        String finalString = file + header;
        
        for(Form__c f : results) {
            String recString = f.ActualShip__c + ',' + f.Confirmation__r.FTType__c + ',' + f.ActualReceive__c + ',' + 
                						f.Confirmation__r.Season__r.Name + ',' + f.Confirmation__r.RecordType.Name + ',' +
                						f.FormType__c + ',' + f.FormSequence__c + ',' + f.Confirmation__r.Account__r.SchoolCode__c +
                						',' + f.Confirmation__r.Projection__c + ',' + f.WarehouseAssigned__c + ',' + f.ArtworkCount__c +
                						',' + f.Confirmation__r.Account__r.BillingState + ',' + f.OrderEntry__c + ',' + f.WarehouseTransferDate__c +
                						',' + f.MagValueWholesale__c + ',' + f.OrderValueWholesale__c + ',' + f.Confirmation__r.SalesRep__r.Name + '\n';
            finalString += recString;
        }
        return finalString;
    }
    
}