@isTest
private class TestSeasonForecastHQMGRSummaryController {
	/*
	 * author: Redpoint
	 * date: 4/5/2017
	 * purpose: Test coverage for the SeasonForecastHQMGRController class.
	 * note: 100% test coverage as of 4/6
	*/

	static Account acct = new Account();
    static Account acct2 = new Account();
    static Account acct3 = new Account();
    static Account acct4 = new Account();
    static Account acct5 = new Account();
    static Season__c season = new Season__c();
    static Confirmation__c conf = new Confirmation__c();
    static Contact rep = new Contact();
    static Contact mgr = new Contact();

	static void setup() {
		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP17';
        upsert var;

        mgr.FirstName = 'Test';
        mgr.LastName = 'Manager';
        insert mgr;

        rep.FirstName = 'Test';
        rep.LastName = 'Rep';
        rep.HQMGR__c = mgr.Id;
        insert rep;

		acct.Name = 'Unit Test Account';
        acct.SalesRep__c = rep.Id;
        insert acct;

        acct2.Name = 'Unit Test Account 2';
        acct2.SalesRep__c = rep.Id;
        insert acct2;

        acct3.Name = 'Unit Test Account 3';
        acct3.SalesRep__c = rep.Id;
        insert acct3;

        acct4.Name = 'Unit Test Account 4';
        acct4.SalesRep__c = rep.Id;
        insert acct4;

        acct5.Name = 'Unit Test Account 5';
        acct5.SalesRep__c = rep.Id;
        insert acct5;

        season.Name = var.Default_Season__c;
        season.OPGross__c = 1.0;
        season.OPParticipation__c = 5;
        season.SBYBGross__c = 2.0;
        season.SBYBParticipation__c = 10;
        season.GalleryGross__c = 3.0;
        season.GalleryParticipation__c = 15;
        season.TileWallGross__c = 4.0;
        Season.TileWallParticipation__c = 20;
        insert season;

        RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'OP' LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        conf.SalesRep__c = rep.Id;
        conf.Status__c = 'Active';
        conf.SeasonPopEstimate__c = 1;
        insert conf;

        ApexPages.currentPage().getParameters().put('season', season.Name);
        ApexPages.currentPage().getParameters().put('status', 'Active');
	}

	static Testmethod void testGetQueryConfirmations() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(conf);
    	SeasonForecastHQMGRSummaryController sfs = new SeasonForecastHQMGRSummaryController(sc);

    	sfs.getqueryConfirmations();

    	List<Confirmation__c> test = sfs.confirmations;
    	System.assertEquals(1, test.size());
	}

    static Testmethod void testGroupByReps() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        SeasonForecastHQMGRSummaryController sfs = new SeasonForecastHQMGRSummaryController(sc);

        Map<String, List<Confirmation__c>> test = sfs.groupByReps();
        System.assert(!test.isEmpty());

        Set<String> test2 = test.keySet();
        System.assert(test2.contains(mgr.FirstName + ' ' + mgr.LastName));
    }

    static Testmethod void testGetHouseReps() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        SeasonForecastHQMGRSummaryController sfs = new SeasonForecastHQMGRSummaryController(sc);

        sfs.getqueryConfirmations();

        String test = sfs.getHouseReps();
        System.assertNotEquals('', test);
        System.assertNotEquals(null, test);
    }

    static Testmethod void testGetDisplayReps() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        SeasonForecastHQMGRSummaryController sfs = new SeasonForecastHQMGRSummaryController(sc);

        sfs.getqueryConfirmations();

        Map<String, List<Double>> test = sfs.getDisplayReps();
        System.assert(!test.isEmpty());

        Set<String> test2 = test.keySet();
        System.assert(test2.contains(mgr.LastName + ', ' + mgr.FirstName));
    }

    static Testmethod void testTotalCounts() {
        setup();

        Confirmation__c confS = new Confirmation__c();
        confS.Account__c = acct2.Id;
        confS.Season__c = season.Id;
        confS.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1].Id;
        confS.Status__c = 'Active';
        confS.SeasonPopEstimate__c = 2;
        insert confS;

        Confirmation__c confSS = new Confirmation__c();
        confSS.Account__c = acct3.Id;
        confSS.Season__c = season.Id;
        confSS.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'SS SBYB' LIMIT 1].Id;
        confSS.Status__c = 'Active';
        confSS.SeasonPopEstimate__c = 3;
        insert confSS;

        Confirmation__c confG = new Confirmation__c();
        confG.Account__c = acct4.Id;
        confG.Season__c = season.Id;
        confG.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Gallery' LIMIT 1].Id;
        confG.Status__c = 'Active';
        confG.SeasonPopEstimate__c = 4;
        insert confG;

        Confirmation__c confT = new Confirmation__c();
        confT.Account__c = acct5.Id;
        confT.Season__c = season.Id;
        confT.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1].Id;
        confT.Status__c = 'Active';
        confT.SeasonPopEstimate__c = 5;
        insert confT;

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        SeasonForecastHQMGRSummaryController sfs = new SeasonForecastHQMGRSummaryController(sc);

        sfs.getqueryConfirmations();

        List<Double> test = sfs.totalCounts(sfs.confirmations);
        System.assertEquals(15, test.get(5));//Population total
        System.assertEquals(2, test.get(0));//sbyb population
        System.assertEquals(3, test.get(1));//ss sbyb population
        System.assertEquals(1, test.get(2));//op population
        System.assertEquals(4, test.get(3));//gallery population
        System.assertEquals(5, test.get(4));//tile wall population
        System.assertEquals(0.4, test.get(6));//sbyb projection
        System.assertEquals(0.6, test.get(7));//ss sbyb projection
        System.assertEquals(0.05, test.get(8));//op projection
        System.assertEquals(1.8, test.get(9));//gallery projection
        System.assertEquals(4.0, test.get(10));//tile wall projection
        System.assertEquals(6.85, test.get(11));//projection total

    }

    static Testmethod void testGetGrandTotals() {
        setup();

        Confirmation__c confS = new Confirmation__c();
        confS.Account__c = acct2.Id;
        confS.Season__c = season.Id;
        confS.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1].Id;
        confS.Status__c = 'Active';
        confS.SeasonPopEstimate__c = 2;
        insert confS;

        Confirmation__c confSS = new Confirmation__c();
        confSS.Account__c = acct3.Id;
        confSS.Season__c = season.Id;
        confSS.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'SS SBYB' LIMIT 1].Id;
        confSS.Status__c = 'Active';
        confSS.SeasonPopEstimate__c = 3;
        insert confSS;

        Confirmation__c confG = new Confirmation__c();
        confG.Account__c = acct4.Id;
        confG.Season__c = season.Id;
        confG.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Gallery' LIMIT 1].Id;
        confG.Status__c = 'Active';
        confG.SeasonPopEstimate__c = 4;
        insert confG;

        Confirmation__c confT = new Confirmation__c();
        confT.Account__c = acct5.Id;
        confT.Season__c = season.Id;
        confT.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1].Id;
        confT.Status__c = 'Active';
        confT.SeasonPopEstimate__c = 5;
        insert confT;

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        SeasonForecastHQMGRSummaryController sfs = new SeasonForecastHQMGRSummaryController(sc);

        sfs.getqueryConfirmations();

        List<Double> test = sfs.getgrandTotals();
        System.assertEquals(15, test.get(0));//population total
        System.assertEquals(0.4, test.get(1));//sbyb projection
        System.assertEquals(0.6, test.get(2));//ss sbyb projection
        System.assertEquals(0.05, test.get(3));//op projection
        System.assertEquals(1.8, test.get(4));//gallery projection
        System.assertEquals(4.0, test.get(5));//tile wall projection
        System.assertEquals(6.85, test.get(6));//projection total
    }
}