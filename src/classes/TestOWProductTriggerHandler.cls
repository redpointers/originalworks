@isTest
private class TestOWProductTriggerHandler {
	/*
	* author: Redpoint
	* date: 6/2/2017
	* purpose: Unit Tests for the OW Product Trigger Handler class.
	* notes: Has 100% code coverage as of 6/2.
	*/

    static void setup() {
        TriggerSwitches__c tswitch = TriggerSwitches__c.getOrgDefaults();

        tswitch.OWProductAll__c = True;
        tswitch.OWProductBeforeInsert__c = True;
        tswitch.OWProductBeforeDelete__c = True;

        upsert tswitch;
    }

    static Testmethod void testHandleDelete() {
        setup();

        List<OWProduct__c> productList = new List<OWProduct__c>();
    	OWProduct__c product1 = new OWProduct__c();
        product1.Name = 'Test This Name';
        insert product1;

        productList.add(product1);

    	try {
    		OWProduct__c testDelete = [SELECT Id, Name FROM OWProduct__c WHERE Id =: product1.Id LIMIT 1];
        	delete testDelete;
            OWProductTriggerHandler.handleDelete(productList);
    		
    	} catch(DMLException e) {
        	Boolean expectedError =  e.getMessage().contains('Record cannot be deleted.') ? true : false;
			System.AssertEquals(expectedError, true);
        }
    }
    
    static Testmethod void testHandleInsert() {
        setup();

        List<OWProduct__c> productList = new List<OWProduct__c>();

        OWProduct__c product1 = new OWProduct__c();
        product1.Name = 'Test This Name';
        insert product1;

        try {
            OWProduct__c product2 = new OWProduct__c();
            product2.Name = 'Test This Name';
            insert product2;
            OWProductTriggerHandler.handleInsert(productList);
            
        } catch(DMLException e) {
            Boolean expectedError =  e.getMessage().contains('Product already exists.') ? true : false;
            System.AssertEquals(expectedError, true);
        }
    }
}