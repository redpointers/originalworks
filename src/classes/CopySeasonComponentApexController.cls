public with sharing class CopySeasonComponentApexController 
{
	@AuraEnabled
    public static Season__c getCurrentSeason(Id recordId) 
    {
        Season__c season = [SELECT Id, Name FROM Season__c where Id =: recordId];
        return season;
    }

    @AuraEnabled
    public static Boolean getHasProducts(Id recordId) 
    {
        Integer count = [SELECT Count() FROM SeasonProduct__c where Season__c =: recordId];
        
        if(count <=0)
            return false;
        else
            return true;
    }

	@AuraEnabled
    public static List<Season__c> getSeasons(Id recordId) 
    {
        List<Season__c> seasons = [SELECT Id, Name FROM Season__c where Id != : recordId ORDER BY StartDate__c DESC];
        return seasons;
    }

    @AuraEnabled
    public static String copyProducts(Id oldRecordId, Id copyRecordId) 
    {
        List<SeasonProduct__c> prods = [SELECT Id, Name, BoxGroup__c, BoxQuantity__c, BoxSort__c, CometPRID__c, CometSeasonID__c, CostToOWY__c,
            Inventory__c, NewForSeason__c, OWProduct__c, PriceToParent__c, PriceToSchool__c, PrintOrder__c, SLXOWProductID__c, SLXSeasonID__c,
            SLXSeasonProductID__c, TestProduct__c FROM SeasonProduct__c where Season__c =: copyRecordId];

        List<SeasonProduct__c> newProds = new List<SeasonProduct__c>();
        for(SeasonProduct__c prod : prods)
        {
            SeasonProduct__c sp = new SeasonProduct__c();
            sp.Season__c = oldRecordId;
            sp.BoxGroup__c = prod.BoxGroup__c;
            sp.BoxQuantity__c = prod.BoxQuantity__c;
            sp.BoxSort__c = prod.BoxSort__c;
            sp.CometPRID__c = prod.CometPRID__c;
            sp.CometSeasonID__c = prod.CometSeasonID__c;
            sp.CostToOWY__c = prod.CostToOWY__c;
            sp.Inventory__c = prod.Inventory__c;
            sp.NewForSeason__c = prod.NewForSeason__c;
            sp.OWProduct__c = prod.OWProduct__c;
            sp.PriceToParent__c = prod.PriceToParent__c;
            sp.PriceToSchool__c = prod.PriceToSchool__c;
            sp.PrintOrder__c = prod.PrintOrder__c;
            sp.SLXOWProductID__c = prod.SLXOWProductID__c;
            sp.SLXSeasonID__c = prod.SLXSeasonID__c;
            sp.SLXSeasonProductID__c = prod.SLXSeasonProductID__c;
            sp.TestProduct__c = prod.TestProduct__c;

            newProds.add(sp);
        }

        insert newProds;

        return copyRecordId;
    }
}