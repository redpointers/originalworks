public with sharing class AccountTriggerHandler 
{
	public static void handleInsert(List<Account> newAccounts)
	{
		AccountTriggerHandler.getTaxCode(newAccounts);
	}

	public static void handleUpdate(List<Account> newAccounts, Map<Id, Account> oldMap)
	{
		AccountTriggerHandler.getTaxCode(newAccounts);
	}

	private static void getTaxCode(List<Account> newAccounts)
	{
		Variables__c vars = Variables__c.getInstance();

		List<String> states = new List<String>();
		for(Account acct : newAccounts)
		{
			states.add(acct.BillingState);
		}	

		if(states.size() > 0)
		{
			List<Tax_Code_Mapping__c> stateMappings = [select Tax_Code__c, City__c, County__c from Tax_Code_Mapping__c where State__c in : states];
			for(Account acct : newAccounts)
			{
				if(acct.BillingState != null && vars.Tax_States__c.indexOf(acct.BillingState) >=0 )
				{
					Boolean foundMapping = false;
					for(Tax_Code_Mapping__c tcm : stateMappings)
					{
						if(tcm.City__c!=null && tcm.City__c == acct.BillingCity && tcm.County__c!=null && tcm.County__c == acct.BillingCounty__c)
						{
							System.debug('**** Acct City: '+acct.BillingCity);
							System.debug('**** Acct County: '+acct.BillingCounty__c);
							acct.TaxCode__c = tcm.Tax_Code__c;
							foundMapping = true;
							break;
						}
					}

					if(!foundMapping)
					{
						for(Tax_Code_Mapping__c tcm : stateMappings)
						{
							if(tcm.City__c!=null && tcm.City__c == acct.BillingCity)
							{
								System.debug('**** CO Acct City: '+acct.BillingCity);
								acct.TaxCode__c = tcm.Tax_Code__c;
								foundMapping = true;
								break;
							}
						}
					}

					if(!foundMapping)
					{
						for(Tax_Code_Mapping__c tcm : stateMappings)
						{
							if(tcm.County__c!=null && tcm.County__c == acct.BillingCounty__c)
							{
								System.debug('**** CU Acct County: '+acct.BillingCounty__c);
								acct.TaxCode__c = tcm.Tax_Code__c;
								foundMapping = true;
								break;
							}
						}
					}
				}
			}

		}
	}
}