public with sharing class FormTriggerHandler {
    /*
    * author: Redpoint
    * date: 3/1/2017
    * purpose: When a new form is created all the forms in the Confirmations related list
    *       will be checked for the largest Form Sequence number by form type. the new forms 
    *       FormSequence__c field will be auto incremented based on the current largest Form 
    *       Sequence number of the same form type.   
    * notes: 3/3/2017 Added the handleBeforeInsertAndUpdate method.
    */
    public static void handleBeforeInsert(List<Form__c> newForms) {
        List<Confirmation__c> confs = new List<Confirmation__c>();
        List<AggregateResult> aggregatedForms = new List<AggregateResult>();
        Set<String> formType = new Set<String>();
        Set<Id> confIds = new Set<Id>();
    
        for(Form__c form : newForms) {
            if(!confIds.contains(form.Confirmation__c)) {
                confIds.add(form.Confirmation__c);
            }
        }

        confs = [   SELECT Id, Account__r.BillingState, Account__r.ShipDaysOverride__c, (SELECT Id, FormType__c, FormSequence__c FROM Forms__r) 
                    FROM Confirmation__c 
                    WHERE Confirmation__c.Id in : confIds
                ];

        for(Confirmation__c conf : confs) {
            aggregatedForms = [ SELECT FormType__c, MAX(FormSequence__c)latest 
                                FROM Form__c 
                                WHERE Confirmation__c =: conf.Id 
                                GROUP BY FormType__c];
        }

        for(AggregateResult result : aggregatedForms) {
            formType.add(String.valueOf(result.get('FormType__c')));
        }

        for(Form__c f : newForms) {

            f.ShipDays__c = populateShipDaysField(f.Confirmation__c, confs);
            String error = checkFormReceiveOrder(f, formType);

            if(error != 'no error') {
                f.addError(error);
            }

            if(!aggregatedForms.isEmpty()) {
                for(AggregateResult ar : aggregatedForms) {
                    if(formType.contains(f.FormType__c)) {
                        if(f.FormType__c == ar.get('FormType__c')) {
                            f.FormSequence__c = (Integer.valueOf(ar.get('latest')) + 1);
                        }    
                    } else {
                        f.FormSequence__c = 1;
                    }
                }
            } else {
                f.FormSequence__c = 1;
            }
        }

        formFields(newForms);
    }

    public static void handleBeforeUpdate(List<Form__c> newForms, Map<id, Form__c> oldMap) {
        formFieldUpdates(newForms, oldMap);
    }

    public static void formFields(List<Form__c> newForms) {
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault = True];
        Time hours = Time.newInstance(0,0,0,0); 
        Long days;

        for(Form__c f : newForms) {

            if(f.EstimatedReceived__c != null && f.ActualReceive__c != null) {
                Datetime startDate = Datetime.newInstance(f.EstimatedReceived__c, hours);
                Datetime endDate = DateTime.newInstance(f.ActualReceive__c,hours); 
                days = BusinessHours.diff(bh.id, startDate, endDate) / (1000*60*60*24);
            
                f.DaysLate__c = days;

                if(days >= 3) {
                    f.Greensheet__c = True;
                } else if(days < 0) {
                    f.DaysLate__c = 0;
                }
            }
        }
    }
    
    public static void formFieldUpdates(List<Form__c> newForms, Map<id, Form__c> oldMap) {
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault = True];
        Time hours = Time.newInstance(0,0,0,0); 
        Long days;

        for(Form__c f : newForms) {
            
            Form__c oldf = oldMap.get(f.Id);

            if( (f.EstimatedReceived__c != oldf.EstimatedReceived__c) || (f.ActualReceive__c != oldf.ActualReceive__c) ) {
                if(f.EstimatedReceived__c != null && f.ActualReceive__c != null) {
                    Datetime startDate = Datetime.newInstance(f.EstimatedReceived__c, hours);
                    Datetime endDate = DateTime.newInstance(f.ActualReceive__c,hours); 
                    days = BusinessHours.diff(bh.id, startDate, endDate) / (1000*60*60*24);
                
                    f.DaysLate__c = days;

                    if(days >= 3) {
                        f.Greensheet__c = True;
                    } else if(days < 0) {
                        f.DaysLate__c = 0;
                    }
                }
            } 
        }
    }

    public static Decimal populateShipDaysField(Id conf, List<Confirmation__c> cList) {
        Map<String, ShipDays__c> numberOfShippingDays = ShipDays__c.getAll();
        Decimal numDays = 0;

        for(Confirmation__c c : cList) {
            if(conf == c.Id) {
                if(c.Account__r.ShipDaysOverride__c == null && c.Account__r.BillingState != null) 
                {
                    String state = c.Account__r.BillingState;
                    try
                    {
                        ShipDays__c shipdays = numberofShippingDays.get(state);
                        numDays = shipdays.numDays__c;
                    }
                    catch(Exception e)
                    {
                        numDays = c.Account__r.ShipDaysOverride__c;
                    }
                } 
                else 
                {
                    numDays = c.Account__r.ShipDaysOverride__c;
                }
            }
        }
        return numDays;
    }

    public static String checkFormReceiveOrder(Form__c form, Set<String> types) {
        String error;

        if(form.FormType__c == 'B' && !types.contains('A')) {
                error = 'Form A must be received before a Form B can be received!';
        } else if(form.FormType__c == 'G' && !types.contains('E')) {
                error = 'Form E must be received before a Form G can be received!';
        } else {
            error = 'no error';
        }
        return error;
    }
}