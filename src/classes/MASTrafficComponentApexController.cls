public with sharing class MASTrafficComponentApexController 
{
	public static final String NORMAL='NORMAL';
	public static final String RETAIL='RETAIL';
	public static final String GALLERY='GALLERY';

	@AuraEnabled
	public static MASTrafficData extractData()
	{
		List<Form__c> forms = [select Id, Name, FormType__c, FormSequence__c, QuestionFolders__c, Confirmation__c, WarehouseAssigned__c, WarehouseTransferDate__c,
			MAS90Export__c,MagsBought__c,ArtworkCountOE__c, Confirmation__r.Account__c from 
			Form__c where MAS90Export__c = null AND WarehouseAssigned__c !=null AND WarehouseTransferDate__c != null AND CALENDAR_YEAR(ActualShip__c) > 2012 AND
			Confirmation__r.RecordType.Name != 'Gallery' AND Confirmation__r.Account__r.status__c != 'Retail Customer'];

		List<Id> confirmIds = new List<Id>();
		for(Form__c form : forms)
			confirmIds.add(form.Confirmation__c);

		List<Confirmation__c> confirms = [select Id, Name, Season__c, Season__r.Name, SalesRep__c, SalesRep__r.RepId__c, SalesRep__r.FirstName, SalesRep__r.LastName, 
			SalesRep__r.MailingStreet,SalesRep__r.MailingCity, SalesRep__r.MailingState, SalesRep__r.MailingPostalCode, SalesRep__r.Phone, SalesRep__r.Email,
			SalesRep__r.BillingStreet2__c, TestProgram__c, Account__c, RecordType.Name, ClosedDates__c, PurchasePaper__c, SalesRep__r.MASSubRepCode__c, PONumber__c,
            LastSchoolDate__c, ResumeDate__c, NumberOfReams__c from Confirmation__c where Id in : confirmIds];

        List<Id> acctIds = new List<Id>();
		for(Confirmation__c confirm : confirms)
			acctIds.add(confirm.Account__c);

        List<Account> accts = [select Id, Name, BillingStreet, BillingStreet2__c, BillingCity, BillingState, BillingPostalCode, Owner.Name, SchoolCode__c, Phone, TaxExempt__c,TaxCode__c,
        	TaxExemptReceived__c, TaxExemptValidUntil__c,Type__c, SubType__c, SalesRep__c, SalesRep__r.Email, SalesRep__r.RepId__c, Fax, SubSalesRep__r.RepId__c from Account where Id in : acctIds];

        MASTrafficData fakeData = new MASTrafficData(forms, confirms, accts);

		return fakeData;
	}

	@AuraEnabled
	public static MASTrafficData extractRetailData()
	{
		List<Form__c> forms = [select Id, Name, FormType__c, FormSequence__c, QuestionFolders__c, Confirmation__c, WarehouseAssigned__c, WarehouseTransferDate__c,
			MAS90Export__c,MagsBought__c,ArtworkCountOE__c, Confirmation__r.Account__c from 
			Form__c where MAS90Export__c = null AND WarehouseAssigned__c !=null AND WarehouseTransferDate__c != null AND 
			Confirmation__r.Account__r.status__c = 'Retail Customer'];

		List<Id> confirmIds = new List<Id>();
		for(Form__c form : forms)
			confirmIds.add(form.Confirmation__c);

		List<Confirmation__c> confirms = [select Id, Name, Season__c, Season__r.Name, SalesRep__c, SalesRep__r.RepId__c, SalesRep__r.FirstName, SalesRep__r.LastName,
			SalesRep__r.MailingStreet,SalesRep__r.MailingCity, SalesRep__r.MailingState, SalesRep__r.MailingPostalCode, SalesRep__r.Phone, SalesRep__r.Email, 
			SalesRep__r.BillingStreet2__c, TestProgram__c, Account__c, RecordType.Name, ClosedDates__c, PurchasePaper__c, SalesRep__r.MASSubRepCode__c, PONumber__c, 
            LastSchoolDate__c, ResumeDate__c, NumberOfReams__c from Confirmation__c where Id in : confirmIds];

        List<Id> acctIds = new List<Id>();
		for(Confirmation__c confirm : confirms)
			acctIds.add(confirm.Account__c);

        List<Account> accts = [select Id, Name, BillingStreet, BillingStreet2__c, BillingCity, BillingState, BillingPostalCode, Owner.Name, SchoolCode__c, Phone,TaxExempt__c,
        	TaxCode__c,TaxExemptReceived__c,TaxExemptValidUntil__c,Type__c, SubType__c, SalesRep__c, SalesRep__r.Email, SalesRep__r.RepId__c, Fax, SubSalesRep__r.RepId__c from Account where Id in : acctIds];

        MASTrafficData fakeData = new MASTrafficData(forms, confirms, accts);

		return fakeData;
	}

	@AuraEnabled
	public static MASTrafficData extractGalleryData()
	{
		List<Form__c> forms = [select Id, Name, FormType__c, FormSequence__c, QuestionFolders__c, Confirmation__c, WarehouseAssigned__c, WarehouseTransferDate__c,
			MAS90Export__c,MagsBought__c,ArtworkCountOE__c, Confirmation__r.Account__c from 
			Form__c where MAS90Export__c = null AND WarehouseAssigned__c !=null AND WarehouseTransferDate__c != null AND CALENDAR_YEAR(ActualShip__c) > 2012 AND
			Confirmation__r.RecordType.Name = 'Gallery'];

		List<Id> confirmIds = new List<Id>();
		for(Form__c form : forms)
			confirmIds.add(form.Confirmation__c);

		List<Confirmation__c> confirms = [select Id, Name, Season__c, Season__r.Name, SalesRep__c, SalesRep__r.RepId__c, SalesRep__r.FirstName, SalesRep__r.LastName,
			SalesRep__r.MailingStreet,SalesRep__r.MailingCity, SalesRep__r.MailingState, SalesRep__r.MailingPostalCode, SalesRep__r.Phone, SalesRep__r.Email, 
			SalesRep__r.BillingStreet2__c, TestProgram__c, Account__c, RecordType.Name, ClosedDates__c, PurchasePaper__c, SalesRep__r.MASSubRepCode__c, PONumber__c, 
            LastSchoolDate__c, ResumeDate__c, NumberOfReams__c from Confirmation__c where Id in : confirmIds];

        List<Id> acctIds = new List<Id>();
		for(Confirmation__c confirm : confirms)
			acctIds.add(confirm.Account__c);

        List<Account> accts = [select Id, Name, BillingStreet, BillingStreet2__c, BillingCity, BillingState, BillingPostalCode, Owner.Name, SchoolCode__c, Phone,TaxExempt__c,
        	TaxCode__c,TaxExemptReceived__c,TaxExemptValidUntil__c,Type__c, SubType__c, SalesRep__c, SalesRep__r.Email, SalesRep__r.RepId__c, Fax, SubSalesRep__r.RepId__c from Account where Id in : acctIds];

        MASTrafficData fakeData = new MASTrafficData(forms, confirms, accts);

		return fakeData;
	}

	@AuraEnabled
	public static void markFormsExported(String formStr)
	{
		try
		{
			List<Id> formIds = (List<Id>)System.JSON.deserialize(formStr, List<Id>.Class);
			List<Form__c> forms = [select Id, Name, MAS90Export__c from	Form__c where Id = : formIds];

			for(Form__c form : forms)
			{
				form.MAS90Export__c = System.today();
			}

			update forms;
		}
		catch(Exception e)
		{
			throw new AuraHandledException(e.getStackTraceString());
		}
	}

	@AuraEnabled
	public static String exportSchoolData(String dataStr, String type, String taxStr, String questionStr, String noMagStr, String highMagStr)
	{
		List<Account> finalAccounts = getNonProblemAccounts(dataStr, taxStr, questionStr, noMagStr, highMagStr);

		String file = 'data:text/csv;charset=utf-8,';
        
        String retStr = file;

        if(finalAccounts.size() <= 0)
        	return retStr;

		List<String> schoolRows = MASTrafficComponentApexController.getSchoolExportData(finalAccounts, type);
		if(schoolRows.size() > 0)
		{
			for(String row : schoolRows)
			{
				retStr = retStr + row;
			}
		}

		return retStr;
	}

	@AuraEnabled
	public static String exportSalesRepData(String dataStr, String taxStr, String questionStr, String noMagStr, String highMagStr)
	{
		List<Confirmation__c> confirms = getNonProblemConfirms(dataStr, taxStr, questionStr, noMagStr, highMagStr);
		String file = 'data:text/csv;charset=utf-8,';
        
        String retStr = file;

		List<String> repRows = MASTrafficComponentApexController.getSalesRepExportData(confirms);
		if(repRows.size() > 0)
		{
			for(String row : repRows)
			{
				retStr = retStr + row;
			}
		}

		return retStr;
	}

	@AuraEnabled
	public static String exportInvoiceData(String acctStr, String confirmStr, String formStr, String type, String taxStr, String questionStr, String noMagStr, String highMagStr)
	{
		try
		{
			List<Account> accounts = getNonProblemAccounts(acctStr, taxStr, questionStr, noMagStr, highMagStr);
			List<Confirmation__c> confirms = getNonProblemConfirms(confirmStr, taxStr, questionStr, noMagStr, highMagStr);
			List<Id> formIds = (List<Id>)System.JSON.deserialize(formStr, List<Id>.Class);

			List<Form__c> forms = [select Id, Name, FormType__c, FormSequence__c, QuestionFolders__c, Confirmation__c, WarehouseAssigned__c, WarehouseTransferDate__c,
				MAS90Export__c,MagsBought__c,ArtworkCountOE__c, Confirmation__r.Account__c from 
				Form__c where Id = : formIds];

			List<Form__c> finalForms = getNonProblemForms(forms, taxStr, questionStr, noMagStr, highMagStr);

			List<OrderLines> orders = new List<OrderLines>();

			if(finalForms!=null && finalForms.size() > 0)
			{
				List<Id> finalFormIds = new List<Id>();
				for(Form__c form : finalForms)
					finalFormIds.add(form.Id);

				orders = getOrders(finalFormIds);
			}

			String file = 'data:text/csv;charset=utf-8,';
	        
	        String retStr = file;

	        List<String> repRows = null;
	        if(type == MASTrafficComponentApexController.NORMAL)
				repRows = MASTrafficComponentApexController.getInvoiceData(accounts,confirms,finalForms, orders);
			else if(type == MASTrafficComponentApexController.GALLERY)
				repRows = MASTrafficComponentApexController.getGalleryInvoiceData(accounts,confirms,finalForms, orders);
			else if(type == MASTrafficComponentApexController.RETAIL)
				repRows = MASTrafficComponentApexController.getRetailInvoiceData(accounts,confirms,finalForms, orders);

			if(repRows.size() > 0)
			{
				for(String row : repRows)
				{
					retStr = retStr + row;
				}
			}

			return retStr;
		}
		catch(Exception e)
		{
			throw new AuraHandledException(e.getStackTraceString());
		}
	}

	private static List<String> getSchoolExportData(List<Account> accounts, String type)
    {
    	Variables__c vars = Variables__c.getInstance();

        List<String> rows = new List<String>();
        if(accounts != null && accounts.size()>0)
        {
            for(Account acct : accounts)
            {
                String row = commaString(acct.SchoolCode__c);
                row = row + commaString(acct.Name);
                row = row + commaString(acct.BillingStreet);
                row = row + commaString(acct.BillingStreet2__c);
                row = row + commaString(acct.BillingCity);
                row = row + commaString(acct.BillingState);
                row = row + commaString(acct.BillingPostalCode);
                row = row + commaString(acct.Phone);
                row = row + commaString(acct.Type__c);
                row = row + commaString(acct.SubType__c); // Grade Level
                row = row + commaString(null); // Art Teacher

                if(vars.Tax_States__c.indexOf(acct.BillingState) >= 0)
                	row = row + commaString(acct.TaxCode__c);
                else
                	row = row + commaString(null);// Tax Schedule

                row = row + commaString(acct.SalesRep__r.RepId__c);
                row = row + commaString(null); // Date and Time not used
                row = row + commaString(acct.SalesRep__r.Email);
                row = row + commaString(null); // School email
                row = row + commaString(acct.Fax);

                if(type == MASTrafficComponentApexController.GALLERY)
                {
                	row = row + commaString(null); // test Code. On confirm but not Account?
                	row = row + commaString(acct.SubSalesRep__r.RepId__c);
                	row = row + commaString(null); // Primary Contact Name?
                }

                row = row + '\n';

                rows.add(row);
            }
        }

        return rows;
    }

    private static List<String> getSalesRepExportData(List<Confirmation__c> confirms)
    {
        List<String> rows = new List<String>();
        if(confirms != null && confirms.size()>0)
        {
            for(Confirmation__c confirm : confirms)
            {
                String row =  commaString(confirm.SalesRep__r.RepId__c);
                row = row + commaString(confirm.SalesRep__r.FirstName+' '+confirm.SalesRep__r.LastName);
                row = row + commaString(confirm.SalesRep__r.MailingAddress);
                row = row + commaString(confirm.SalesRep__r.BillingStreet2__c); // Address2
                row = row + commaString(confirm.SalesRep__r.MailingCity);
                row = row + commaString(confirm.SalesRep__r.MailingState);
                row = row + commaString(confirm.SalesRep__r.MailingPostalCode);
                row = row + commaString(confirm.SalesRep__r.Phone);
                row = row + commaString(confirm.SalesRep__r.Email);
                row = row + '\n';

                rows.add(row);
            }
        }

        return rows;
    }

    private static List<String> getInvoiceData(List<Account> accounts, List<Confirmation__c> confirms, List<Form__c> forms, List<OrderLines> orders)
    {
    	Variables__c vars = Variables__c.getInstance();

        List<String> rows = new List<String>();
        if(forms != null && forms.size()>0)
        {
            for(Form__c form : forms)
            {
            	Confirmation__c confirm = MASTrafficComponentApexController.getCorrectConfirm(confirms, form);
            	if(confirm != null)
            	{
            		Account acct = MASTrafficComponentApexController.getCorrectAccount(accounts, confirm);
            		if(acct != null)
            		{
		            	for(OrderLines line : orders)
		            	{
		            		if(line.formid != form.Id)
		            			continue; // not an order on this form

		            		String row =  commaString(acct.SchoolCode__c);
		            		row = row + commaString(confirm.Season__r.Name);
		            		row = row + commaString(null); // blank
		            		row = row + commaString(confirm.PONumber__c);
		            		row = row + commaString(line.suffix+form.FormType__c+form.FormSequence__c); // Invoice No
		            		row = row + commaString(form.FormType__c);
		            		row = row + commaString(line.code);
		            		row = row + commaString(line.qty);
		            		row = row + commaString(null); // blank
		            		row = row + commaString(null); // Item Type
		            		row = row + commaString(form.WarehouseAssigned__c);
		            		row = row + commaString(confirm.SalesRep__r.RepId__c);
		            		if(vars.Tax_States__c.indexOf(acct.BillingState) >= 0)
			                	row = row + commaString(acct.TaxCode__c);
			                else
			                	row = row + commaString('NONTAX');// Tax Schedule

		            		if(confirm.SalesRep__r.MASSubRepCode__c != null)
		            			row = row + commaString('Y');
		            		else
		            			row = row + commaString('N');

		            		row = row + commaString(confirm.SalesRep__r.MASSubRepCode__c);
		            		row = row + commaString(null); // original School

		            		
		            		row = row + commaString(form.WarehouseAssigned__c);
		            		row = row + commaString(null); // Ship Via
		            		row = row + commaString(confirm.TestProgram__c);
		            		row = row + '\n';	

		            		rows.add(row);
		            	}
		            }
	            }
            }
        }

        return rows;
    }

    private static List<String> getRetailInvoiceData(List<Account> accounts, List<Confirmation__c> confirms, List<Form__c> forms, List<OrderLines> orders)
    {
    	Variables__c vars = Variables__c.getInstance();

        List<String> rows = new List<String>();
        if(forms != null && forms.size()>0)
        {
            for(Form__c form : forms)
            {
            	Confirmation__c confirm = MASTrafficComponentApexController.getCorrectConfirm(confirms, form);
            	if(confirm != null)
            	{
            		Account acct = MASTrafficComponentApexController.getCorrectAccount(accounts, confirm);
            		if(acct != null)
            		{
		            	for(OrderLines line : orders)
		            	{
		            		if(line.formid != form.Id)
		            			continue; // not an order on this form

		            		String row =  commaString(acct.SchoolCode__c); 
		            		row = row + commaString(line.suffix+form.FormType__c+form.FormSequence__c); // Invoice No
		            		row = row + commaString(line.lastmoddate);
		            		row = row + commaString(line.grandtotal);
		            		row = row + commaString(line.salestax);
		            		row = row + commaString(line.shipping);
		            		row = row + commaString(line.discount);
		            		row = row + commaString(line.shipVia); 
		            		row = row + commaString(line.code);
		            		row = row + commaString(line.qty);
		            		row = row + commaString(line.total);
		            		row = row + commaString(null); // blank
		            		row = row + commaString(line.paymt);
		            		row = row + '\n';	

		            		rows.add(row);
		            	}
		            }
	            }
            }
        }

        return rows;
    }

    private static List<String> getGalleryInvoiceData(List<Account> accounts, List<Confirmation__c> confirms, List<Form__c> forms, List<OrderLines> orders)
    {
    	Variables__c vars = Variables__c.getInstance();

        List<String> rows = new List<String>();
        if(forms != null && forms.size()>0)
        {
            for(Form__c form : forms)
            {
            	Confirmation__c confirm = MASTrafficComponentApexController.getCorrectConfirm(confirms, form);
            	if(confirm != null)
            	{
            		Account acct = MASTrafficComponentApexController.getCorrectAccount(accounts, confirm);
            		if(acct != null)
            		{
		            	for(OrderLines line : orders)
		            	{
		            		if(line.formid != form.Id)
		            			continue; // not an order on this form

		            		String row =  commaString(acct.SchoolCode__c);
		            		row = row + commaString(line.suffix+form.FormType__c+form.FormSequence__c); // Invoice No
		            		row = row + commaString(confirm.Season__r.Name);
		            		row = row + commaString(form.FormType__c);
		            		row = row + commaString(confirm.SalesRep__r.RepId__c);

		            		if(confirm.SalesRep__r.MASSubRepCode__c != null)
		            			row = row + commaString('Y');
		            		else
		            			row = row + commaString('N');

		            		row = row + commaString(confirm.SalesRep__r.MASSubRepCode__c);
		            		row = row + commaString(form.WarehouseAssigned__c);

		            		if(vars.Tax_States__c.indexOf(acct.BillingState) >= 0)
			                	row = row + commaString(acct.TaxCode__c);
			                else
			                	row = row + commaString('NONTAX');// Tax Schedule

		            		row = row + commaString(acct.SchoolCode__c);
		            		row = row + commaString(line.salestax);
		            		row = row + commaString(line.code);
		            		row = row + commaString(null); // Item Type
		            		row = row + commaString(line.qty);
		            		row = row + commaString(line.total);
		            		row = row + commaString(line.wholesale);
		            		row = row + commaString(line.paymt);

		            		row = row + '\n';	

		            		rows.add(row);
		            	}
		            }
	            }
            }
        }

        return rows;
    }

    private static String commaString(Object inObj)
	{
		if(inObj!=null)
		{
			String inStr = String.valueOf(inObj);
			return inStr+',';
		}
		else
			return ',';
	}

	private static Confirmation__c getCorrectConfirm(List<Confirmation__c> confirms, Form__c form)
	{
		for(Confirmation__c confirm :  confirms)
    	{
    		if(confirm.Id == form.Confirmation__c)
    			return confirm;
    	}

    	return null;
	}

	private static Account getCorrectAccount(List<Account> accounts, Confirmation__c confirm)
	{
		for(Account acct :  accounts)
    	{
    		if(acct.Id == confirm.Account__c)
    			return acct;
    	}

    	return null;
	}

	public static List<OrderLines> getOrders(List<Id> formIds)
	{
		String formIdStr = '';
		for(Id formId : formIds)
		{
			formIdStr = formIdStr + formId +',';
		}
		formIdStr = formIdStr.substring(0,formIdStr.length()-1); // remove last comma

		Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getRecordsForForms?formids='+formIdStr);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock(formIds);
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            List<OrderLines> lines = OrderEntryComponentApexController.parseJSONLinesBody(res.getBody());
            return lines;
        }
        catch(Exception e)
        {
        	throw new AuraException(e.getMessage());
        }

		return null;
	}

	private static List<Account> getNonProblemAccounts(String dataStr, String taxStr, String questionStr, String noMagStr, String highMagStr)
	{
		List<Account> accounts = (List<Account>)System.JSON.deserialize(dataStr, List<Account>.Class);
		List<Account> taxAccounts = (List<Account>)System.JSON.deserialize(taxStr, List<Account>.Class);
		List<Form__c> forms = (List<Form__c>)System.JSON.deserialize(questionStr, List<Form__c>.Class);
		List<Form__c> noMags = (List<Form__c>)System.JSON.deserialize(noMagStr, List<Form__c>.Class);
		List<Confirmation__c> confirms = (List<Confirmation__c>)System.JSON.deserialize(highMagStr, List<Confirmation__c>.Class);

		Map<Id, Boolean> statusMap = new Map<Id, Boolean>();
		for(Account acct : accounts)
			statusMap.put(acct.Id, false);

		if(taxAccounts !=null && taxAccounts.size()>0)
		{
			for(Account ta : taxAccounts)
			{
				for(Account acct : accounts)
				{
					if(acct.Id == ta.Id)
					{
						statusMap.put(acct.Id, true);
					}
				}
			}
		}

		if(forms !=null && forms.size()>0)
		{
			for(Form__c form : forms)
			{
				for(Account acct : accounts)
				{
					if(acct.Id == form.Confirmation__r.Account__c)
					{
						statusMap.put(acct.Id, true);
					}
				}
			}
		}

		if(noMags !=null && noMags.size()>0)
		{
			for(Form__c form : noMags)
			{
				for(Account acct : accounts)
				{
					if(acct.Id == form.Confirmation__r.Account__c)
					{
						statusMap.put(acct.Id, true);
					}
				}
			}
		}

		if(confirms !=null && confirms.size()>0)
		{
			for(Confirmation__c confirm : confirms)
			{
				for(Account acct : accounts)
				{
					if(acct.Id == confirm.Account__c)
					{
						statusMap.put(acct.Id, true);
					}
				}
			}
		}

		List<Account> finalAccounts = new List<Account>();
		for(Id acctId : statusMap.keySet())
		{
			if(statusMap.get(acctId) == false) // not a problem account
			{
				for(Account acct : accounts)
				{
					if(acct.Id == acctId)
						finalAccounts.add(acct);
				}
			}
		}

		return finalAccounts;
	}

	private static List<Confirmation__c> getNonProblemConfirms(String dataStr, String taxStr, String questionStr, String noMagStr, String highMagStr)
	{
		List<Confirmation__c> confirms = (List<Confirmation__c>)System.JSON.deserialize(dataStr, List<Confirmation__c>.Class);
		List<Account> taxAccounts = (List<Account>)System.JSON.deserialize(taxStr, List<Account>.Class);
		List<Form__c> forms = (List<Form__c>)System.JSON.deserialize(questionStr, List<Form__c>.Class);
		List<Form__c> noMags = (List<Form__c>)System.JSON.deserialize(noMagStr, List<Form__c>.Class);
		List<Confirmation__c> highConfirms = (List<Confirmation__c>)System.JSON.deserialize(highMagStr, List<Confirmation__c>.Class);

		Map<Id, Boolean> statusMap = new Map<Id, Boolean>();
		for(Confirmation__c confirm : confirms)
			statusMap.put(confirm.Id, false);

		if(taxAccounts !=null && taxAccounts.size()>0)
		{
			for(Account ta : taxAccounts)
			{
				for(Confirmation__c confirm : confirms)
				{
					if(confirm.Account__c == ta.Id)
					{
						statusMap.put(confirm.Id, true);
					}
				}
			}
		}

		if(forms !=null && forms.size()>0)
		{
			for(Form__c form : forms)
			{
				for(Confirmation__c confirm : confirms)
				{
					if(confirm.Id == form.Confirmation__c)
					{
						statusMap.put(confirm.Id, true);
					}
				}
			}
		}

		if(noMags !=null && noMags.size()>0)
		{
			for(Form__c form : noMags)
			{
				for(Confirmation__c confirm : confirms)
				{
					if(confirm.Id == form.Confirmation__c)
					{
						statusMap.put(confirm.Id, true);
					}
				}
			}
		}

		if(highConfirms !=null && highConfirms.size()>0)
		{
			for(Confirmation__c hconfirm : highConfirms)
			{
				for(Confirmation__c confirm : confirms)
				{
					if(confirm.Id == hconfirm.Id)
					{
						statusMap.put(confirm.Id, true);
					}
				}
			}
		}

		List<Confirmation__c> finalConfirms = new List<Confirmation__c>();
		for(Id confirmId : statusMap.keySet())
		{
			if(statusMap.get(confirmId) == false) // not a problem confirmation
			{
				for(Confirmation__c confirm : confirms)
				{
					if(confirm.Id == confirmId)
						finalConfirms.add(confirm);
				}
			}
		}

		return finalConfirms;
	}

	private static List<Form__c> getNonProblemForms(List<Form__c> forms, String taxStr, String questionStr, String noMagStr, String highMagStr)
	{
		List<Account> taxAccounts = (List<Account>)System.JSON.deserialize(taxStr, List<Account>.Class);
		List<Form__c> questions = (List<Form__c>)System.JSON.deserialize(questionStr, List<Form__c>.Class);
		List<Form__c> noMags = (List<Form__c>)System.JSON.deserialize(noMagStr, List<Form__c>.Class);
		List<Confirmation__c> highConfirms = (List<Confirmation__c>)System.JSON.deserialize(highMagStr, List<Confirmation__c>.Class);

		Map<Id, Boolean> statusMap = new Map<Id, Boolean>();
		for(Form__c form : forms)
			statusMap.put(form.Id, false);

		if(taxAccounts !=null && taxAccounts.size()>0)
		{
			for(Account ta : taxAccounts)
			{
				for(Form__c form : forms)
				{
					if(ta.Id == form.Confirmation__r.Account__c)
					{
						statusMap.put(form.Id, true);
					}
				}
			}
		}

		if(questions !=null && questions.size()>0)
		{
			for(Form__c qForm : questions)
			{
				for(Form__c form : forms)
				{
					if(qForm.Id == form.Id)
					{
						statusMap.put(form.Id, true);
					}
				}
			}
		}

		if(noMags !=null && noMags.size()>0)
		{
			for(Form__c noForm : noMags)
			{
				for(Form__c form : forms)
				{
					if(noForm.Id == form.Id)
					{
						statusMap.put(form.Id, true);
					}
				}
			}
		}

		if(highConfirms !=null && highConfirms.size()>0)
		{
			for(Confirmation__c hconfirm : highConfirms)
			{
				for(Form__c form : forms)
				{
					if(hconfirm.Id == form.Confirmation__c)
					{
						statusMap.put(form.Id, true);
					}
				}
			}
		}

		List<Form__c> finalForms = new List<Form__c>();
		for(Id formId : statusMap.keySet())
		{
			if(statusMap.get(formId) == false) // not a problem confirmation
			{
				for(Form__c form : forms)
				{
					if(form.Id == formId)
						finalForms.add(form);
				}
			}
		}

		return finalForms;
	}
}