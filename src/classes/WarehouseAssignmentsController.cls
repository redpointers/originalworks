public with sharing class WarehouseAssignmentsController {
    /*
     * author: Redpoint
     * purpose: Apex controller for the Warehouse Assignments Lightning Component. Most methods call queries 
     *          based on warehouse, date, and/school school. Queries under the Table Summaries section provide
     *          counts of the total number of orders, the value of those orders, and the artwork count of each 
     *          table in the component.
     * date: 2/20/2017
     * notes:
    */

    /*************************************** Populate tables ***************************************/
    @AuraEnabled
    public static List<Form__c> headTable() {
        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                        Confirmation__r.AverageOrder__c, FormType__c, FormSequence__c, ActualReceive__c, 
                        ArtworkCount__c, WarehouseTransferDate__c, WarehouseAssigned__c
                FROM Form__c
                WHERE (WarehouseAssigned__c = null) AND (WarehouseTransferDate__c = null) 
                        AND (Confirmation__r.Season__r.Name =: season) 
                        AND (OrderEntry__c != null) AND (ActualReceive__c != null)];
    }

    @AuraEnabled
    public static List<Form__c> subTable(String wh) {
        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                        Confirmation__r.AverageOrder__c, FormType__c, FormSequence__c, ActualReceive__c, 
                        ArtworkCount__c, WarehouseTransferDate__c, WarehouseAssigned__c
                FROM Form__c
                WHERE (WarehouseAssigned__c =: wh) AND (WarehouseTransferDate__c != null) 
                        AND (Confirmation__r.Season__r.Name =: season) 
                        AND (OrderEntry__c != null) AND (ActualReceive__c != null)];
    }

    @AuraEnabled
    public static List<Form__c> TableBasedOnTransferDate(String wh, String transferDate) {
        Date d = Date.valueOf(transferDate);

        return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                        Confirmation__r.AverageOrder__c, FormType__c, FormSequence__c, ActualReceive__c, 
                        ArtworkCount__c, WarehouseTransferDate__c, WarehouseAssigned__c
                FROM Form__c
                WHERE (WarehouseAssigned__c =: wh) AND (WarehouseTransferDate__c =: d) 
                        AND (OrderEntry__c != null) AND (ActualReceive__c != null)];
    }

    /*************************************** Search Bar ***************************************/
    @AuraEnabled
    public static List<Account> findByName(String searchKey) {
        String school = '%' + searchKey + '%';
        return [SELECT Id, SchoolCode__c, Name FROM Account WHERE SchoolCode__c LIKE :school LIMIT 5];
    }

    @AuraEnabled
    public static List<Form__c> formsBySchoolMain(String school) {
        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                        Confirmation__r.AverageOrder__c, FormType__c, FormSequence__c, ActualReceive__c, 
                        ArtworkCount__c, WarehouseTransferDate__c, WarehouseAssigned__c
                FROM Form__c
                WHERE (Confirmation__r.Account__r.SchoolCode__c =: school) AND (WarehouseAssigned__c = null) 
                        AND (WarehouseTransferDate__c = null) AND (Confirmation__r.Season__r.Name =: season) 
                        AND (OrderEntry__c != null) AND (ActualReceive__c != null)];
    }

    @AuraEnabled
    public static List<Form__c> formsBySchool(String school, String wh) {
        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                        Confirmation__r.AverageOrder__c, FormType__c, FormSequence__c, ActualReceive__c, 
                        ArtworkCount__c, WarehouseTransferDate__c, WarehouseAssigned__c
                FROM Form__c
                WHERE (Confirmation__r.Account__r.SchoolCode__c =: school) AND (WarehouseAssigned__c =: wh) 
                        AND (WarehouseTransferDate__c != null) AND (Confirmation__r.Season__r.Name =: season) 
                        AND (OrderEntry__c != null) AND (ActualReceive__c != null)];
    }

    /*************************************** Transfer Records ***************************************/
    @AuraEnabled
    public static void toStillwater(Id[] idList, String tDate) {
        List<Form__c> formList = new List<Form__c>();
        Date transferDate = Date.valueOf(tDate);

        for(Id i : idList) {
            formList.add([SELECT Id, WarehouseAssigned__c, WarehouseTransferDate__c FROM Form__c WHERE Id =: i]);
        }

        for(Form__c f : formList) {
            f.WarehouseAssigned__c = 'ST';
            f.WarehouseTransferDate__c = transferDate;
        }

        if(!formList.isEmpty()) {
            update formList;
        }
    }

    @AuraEnabled
    public static void toAmsterdam(Id[] idList, String tDate) {
       List<Form__c> formList = new List<Form__c>();
        Date transferDate = Date.valueOf(tDate);

        for(Id i : idList) {
            formList.add([SELECT Id, WarehouseAssigned__c, WarehouseTransferDate__c FROM Form__c WHERE Id =: i]);
        }

        for(Form__c f : formList) {
            f.WarehouseAssigned__c = 'AM';
            f.WarehouseTransferDate__c = transferDate;
        }

        if(!formList.isEmpty()) {
            update formList;
        }
    }

    @AuraEnabled
    public static void toMainTable(Id[] idList) {
        List<Form__c> formList = new List<Form__c>();

        for(Id i : idList) {
            formList.add([SELECT Id, WarehouseAssigned__c, WarehouseTransferDate__c FROM Form__c WHERE Id =: i]);
        }

        for(Form__c f : formList) {
            f.WarehouseAssigned__c = '';
            f.WarehouseTransferDate__c = null;
        }

        if(!formList.isEmpty()) {
            update formList;
        }
    }

    /*************************************** Table Summaries ***************************************/
    @AuraEnabled
    public static Integer getTableCount(List<Form__c> forms) {
        return forms.size();
    }

    @AuraEnabled
    public static Decimal getTableValue(List<Form__c> forms) {
        Decimal sum = 0;

        for(Form__c f : forms) {
            if(f.Confirmation__r.AverageOrder__c != null) {
                sum += f.Confirmation__r.AverageOrder__c;
            }
        }

        return sum;
    }

    @AuraEnabled
    public static Double getArtworkCount(List<Form__c> forms) {
        Double art = 0;

        for(Form__c f : forms) {
            if(f.ArtworkCount__c != null && f.FormType__c == 'A') {
                art += f.ArtworkCount__c;
            }
        }

        return art;
    }
}