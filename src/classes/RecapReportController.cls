public with sharing class RecapReportController 
{
    public Form__c form{get; set;}
    public Confirmation__c confirm{get;set;}
    public Account acct{get; set;}
    public List<OrderLines> oLines{get;set;}
    public List<Integer> suffixLengths{get;set;}
    public Integer numOrders{get;set;}
    public Integer numProducts{get;set;}
    public Double orderValue{get;set;}
    public Double totalValue{get;set;}

    public RecapReportController(ApexPages.StandardController stdController) 
    {
        form = [select Id, Name, FormType__c, FormSequence__c, ActualShip__c, ActualReceive__c, OrderEntry__c, ArtworkCount__c, 
            EstimatedShip__c, MagsBought__c, MagsReturned__c, Initials__c, QuestionFolders__c, ArtworkCountOE__c, Confirmation__c,
            DaysLate__c from Form__c where Id =: stdController.getId()];

        confirm = [select Id, Name, Season__c, Season__r.Name, SalesRep__c, SalesRep__r.Name, TestProgram__c, Account__c, RecordType.Name,
            ShipInstructions__c, ManInstructions__c, MatchInstructions__c, LastSchoolDate__c, ResumeDate__c from Confirmation__c where Id =: form.Confirmation__c];

        oLines = OrderEntryComponentApexController.getOrdersForForm(form.Id);

        acct = [select Id, Name, BillingState from Account where Id =: confirm.Account__c];

        numOrders = 0;
        numProducts = 0;
        orderValue = 0;
        totalValue = 0;
        if(oLines!=null && oLines.size() > 0)
        {
            numOrders = oLines.size();
            for(OrderLines ord : oLines)
            {
                numProducts = numProducts + (Integer)ord.qty;
                orderValue = orderValue + ord.total;
                totalValue = totalValue + ord.total;
            }
        }
    }

    public Map<String, List<TableLine>> getSortedOrders()
    {
    	suffixLengths = new List<Integer>();

    	Map<String, List<TableLine>> ordersMap = new Map<String, List<TableLine>>();
    	Set<String> uniqueSuffixes = new Set<String>();

    	for(OrderLines order : oLines)
    	{
    		if(!uniqueSuffixes.contains(order.suffix))
    			uniqueSuffixes.add(order.suffix);
    	}

    	for(String suf : uniqueSuffixes)
    	{
    		List<OrderLines> sufOrders = new List<OrderLines>();
    		for(OrderLines order : oLines)
	    	{
	    		if(order.suffix == suf)
	    			sufOrders.add(order);
	    	}
	    	suffixLengths.add(sufOrders.size());

	    	List<TableLine> lines = getLinesForOrders(sufOrders);

	    	ordersMap.put(suf,lines);
    	}

    	return ordersMap;
    } 

    private List<TableLine> getLinesForOrders(List<OrderLines> orders)
    {
    	List<TableLine> lines = new List<TableLine>();
    	Double sufQty = 0;
    	Double sufPrice = 0;
    	Integer index = 0;
    	for(OrderLines order : orders)
    	{
    		sufQty = sufQty + order.qty;
    		sufPrice = sufPrice + order.total;

    		TableLine tl = new TableLine();

    		if(index == 0)
    			tl.num = order.suffix;
    		else
    			tl.num = '';

    		tl.descrip = order.description;
    		tl.qty1 = String.valueOf(order.qty.format());
    		tl.price1 = RecapReportController.formatMoney(order.total.format());
    		tl.qty2 = '';
    		tl.price2 = '';

    		lines.add(tl);
    		index++;
    	}

    	TableLine lastLine = lines.get(orders.size()-1);
    	lastLine.qty2 = String.valueOf(sufQty.format());
    	lastLine.price2 = RecapReportController.formatMoney(sufPrice.format());

    	return lines;
    }

    public static String formatMoney(String s) 
    {
	    s = '$' + s;
	    if (!s.contains('.')) 
	    {
	        s = s + '.00';
	    } 
	    else 
	    {
	      Integer dPos = s.indexOf('.');
	      if (s.length() - dPos < 3) { s = s + '0'; }   
	    }

	    return s; 
	}

    public class TableLine
    {
    	public String num{get;set;}
		public String descrip{get;set;}
		public String qty1{get;set;}
		public String price1{get;set;}
		public String qty2{get;set;}
		public String price2{get;set;}

		public TableLine()
		{
			num = '';
			descrip = '';
			qty1 = '';
			price1 = '';
			qty2 = '';
			price2 = '';
		}
    }
}