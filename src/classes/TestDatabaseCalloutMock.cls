@isTest
global class TestDatabaseCalloutMock implements HttpCalloutMock
{
	List<Id> formIds;

	public TestDatabaseCalloutMock()
	{}

	public TestDatabaseCalloutMock(List<Id> formIds)
	{
		this.formIds = formIds;
	}

	global HTTPResponse respond(HTTPRequest req) 
	{
        HTTPResponse result = new HTTPResponse();
        
		String body = '[{"lineno": 71,"formid": "a013B000001vCi3QAE","seasonid": "a04410000034p0EAAQ","suffix": "1234","hv": "H","code": "QN",'+
	        '"description": "Quick Notes Dry Erase Magnet","qty": 1,"price": 13.5,"createdid": "00541000001IufKAAS","createddate": "2017-04-07T21:08:59.781Z",'+
	        '"lastmodid": "00541000001IufKAAS","lastmoddate": "2017-04-10T15:29:40.939Z","paymt": "O","wholesale": 9,"total": 27,"grandtotal": 38.3,"salestax": 1.35, "shipping": 9.95,"discount": 0,"shipvia": "UPS"}]';

	    if(formIds !=null && formIds.size() >0)
	    {
	    	body = '[{"lineno": 71,"formid": "'+formIds.get(0)+'","seasonid": "a04410000034p0EAAQ","suffix": "1234","hv": "H","code": "QN",'+
	        '"description": "Quick Notes Dry Erase Magnet","qty": 1,"price": 13.5,"createdid": "00541000001IufKAAS","createddate": "2017-04-07T21:08:59.781Z",'+
	        '"lastmodid": "00541000001IufKAAS","lastmoddate": "2017-04-10T15:29:40.939Z","paymt": "O","wholesale": 9,"total": 27,"grandtotal": 38.3,"salestax": 1.35, '+
	        '"shipping": 9.95,"discount": 0,"shipvia": "UPS"},'+
	        '{"lineno": 72,"formid": "'+formIds.get(0)+'","seasonid": "a04410000034p0EAAQ","suffix": "1235","hv": "H","code": "24NC",'+
	        '"description": "Quick Notes Dry Erase Magnet","qty": 1,"price": 13.5,"createdid": "00541000001IufKAAS","createddate": "2017-04-07T21:08:59.781Z",'+
	        '"lastmodid": "00541000001IufKAAS","lastmoddate": "2017-04-10T15:29:40.939Z","paymt": "O","wholesale": 9,"total": 27,"grandtotal": 38.3,"salestax": 1.35, '+
	        '"shipping": 9.95,"discount": 0,"shipvia": "UPS"}]';
	    }
		
		result.setBody(body);			
		result.setStatusCode(200);
		result.setStatus('OK');
		return result;
    }
}