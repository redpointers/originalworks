@isTest
private class TestItemCountReportController
{
	static Confirmation__c confirm;
	static Form__c form;
	static ConfirmationProduct__c confirmProd;
	static Season__c season;

	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		confirm.Status__c = 'Active';
		insert confirm;

		Form__c formA = UnitTestDataFactory.inflateForm(confirm);
		formA.WarehouseAssigned__c='AM';
		formA.FormType__c = 'A';
		insert formA;

		form = UnitTestDataFactory.inflateForm(confirm);
		form.WarehouseAssigned__c='AM';
		form.FormType__c = 'B';
		form.ActualShip__c = System.today().addDays(-1);
		insert form;

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c sp = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert sp;

		confirmProd = UnitTestDataFactory.inflateConfirmationProduct(confirm, sp);
		insert confirmProd;
	}

	@isTest
	static void testIt()
	{
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(season);
		ItemCountReportController cont = new ItemCountReportController(sc);

	}
}