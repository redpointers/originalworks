@isTest
private class TestWIPComponentApexController
{
	static Confirmation__c confirm;
	static Form__c form;
	static ConfirmationProduct__c confirmProd;

	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		form = UnitTestDataFactory.inflateForm(confirm);
		insert form;

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c sp = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert sp;

		confirmProd = UnitTestDataFactory.inflateConfirmationProduct(confirm, sp);
		insert confirmProd;
	}

	@isTest
	static void testGetSeasons()
	{
		setup();

		List<Season__c> seasons = WIPComponentApexController.getSeasons();
		System.assert(seasons.size() == 1);

		List<Form__c> forms = WIPComponentApexController.getFormsForSeason(seasons.get(0).Id);
		System.assert(forms.size() != null);

		List<OrderLines> orders = WIPComponentApexController.getOrdersForSeason(seasons.get(0).Id);
		System.assert(orders.size() == 1);

		OrderLines order = orders.get(0);
		Long lineno = order.lineno;
		String formid=order.formid;
		String seasonid=order.seasonid;
		String suffix=order.suffix;
		String hv=order.hv;
		String paymt=order.paymt;
		String code=order.code;
		String description=order.description;
		Integer qty = order.qty;
		Double price=order.price;
		Double wholesale=order.wholesale;
		Double total=order.total;
		Double grandtotal=order.grandtotal;
		Double salestax=order.salestax;
		Double discount=order.discount;
		Double shipping=order.shipping;
		String shipvia=order.shipvia;
		Id cd =order.createdid;
		DateTime ct=order.createddate;
		Id ld=order.lastmodid;
		Datetime lt=order.lastmoddate;
	}

	@isTest
	static void testGetDate()
	{
		setup();

		DateTime dt = WIPComponentApexController.getStartDate();
		System.assert(dt!=null);

	}

	@isTest
	static void testGetWarehouses()
	{
		setup();

		List<String> warehouses = WIPComponentApexController.getWarehouses();
		System.assert(warehouses!=null && warehouses.size() > 0);

	}
	
}