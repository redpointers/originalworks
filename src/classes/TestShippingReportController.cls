@isTest
private class TestShippingReportController {
	/*
	* author: Redpoint
	* date: 4/25/2017
	* purpose: Unit Tests for the Shipping Report Controller;
	* note: 100% code coverage as of 4/25
	*/

    static Confirmation__c conf = new Confirmation__c();
    static Confirmation__c conf2 = new Confirmation__c();
    static Season__c season = new Season__c();
    static Account acct = new Account();
    static Account acct2 = new Account();
    static Form__c form = new Form__c();
    static Form__c formS = new Form__c();
    static Form__c formA = new Form__c();
    static Form__c formB = new Form__c();
    static Form__c formC = new Form__c();
    static Form__c formD = new Form__c();
    static Form__c formF = new Form__c();

	static void setup() {
		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP17';
        upsert var;

		acct.Name = 'Unit Test Account';
        insert acct;

        acct2.Name = 'Unit Test Account 2';
        insert acct2;

        season.Name = var.Default_Season__c;
        insert season;

        RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'OP' LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;

        RecordType recordtype2 = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1];

        conf2.RecordTypeId = recordtype2.Id;
        conf2.Account__c = acct2.Id;
        conf2.Season__c = season.Id;
        insert conf2;

        form.Name = 'Unit Test Form';
        form.Confirmation__c = conf.Id;
        form.FormType__c = 'A';
        insert form;

        formS.Name = 'Unit Test';
        formS.Confirmation__c = conf2.Id;
        formS.FormType__c = 'A';
        formS.ActualShip__c = Date.newInstance(1960, 5, 1);
        formS.MagValueWholesale__c = 1;
        formS.OrderValueWholesale__c = 2;
        insert formS;

        formA.Name = 'Unit Test';
        formA.Confirmation__c = conf.Id;
        formA.FormType__c = 'A';
        formA.WarehouseAssigned__c = 'AM';
        formA.ActualShip__c = Date.newInstance(1960, 5, 1);
        formA.OrderValueWholesale__c = 2;
        insert formA;

        formB.Name = 'Unit Test';
        formB.Confirmation__c = conf.Id;
        formB.FormType__c = 'B';
        formB.WarehouseAssigned__c = 'AM';
        formB.ActualShip__c = Date.newInstance(1960, 7, 1);
        formB.OrderValueWholesale__c = 3;
        insert formB;

        formC.Name = 'Unit Test';
        formC.Confirmation__c = conf.Id;
        formC.FormType__c = 'C';
        formC.WarehouseAssigned__c = 'AM';
        formC.ActualShip__c = Date.newInstance(1960, 7, 1);
        formC.OrderValueWholesale__c = 4;
        insert formC;

        formD.Name = 'Unit Test';
        formD.Confirmation__c = conf.Id;
        formD.FormType__c = 'D';
        formD.WarehouseAssigned__c = 'ST';
        formD.ActualShip__c = Date.newInstance(1960, 3, 1);
        formD.OrderValueWholesale__c = 5;
        insert formD;

        formF.Name = 'Unit Test';
        formF.Confirmation__c = conf.Id;
        formF.FormType__c = 'F';
        formF.WarehouseAssigned__c = 'ST';
        formF.ActualShip__c = Date.newInstance(1960, 6, 1);
        formF.OrderValueWholesale__c = 6;
        insert formF;

        ApexPages.currentPage().getParameters().put('startrange', '1/1/1960');
		ApexPages.currentPage().getParameters().put('endrange', '10/1/1960');
	}

	static Testmethod void testGetShippingReportForAM() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForAM();

		System.assertEquals(3, src.formsAM.size());
		System.assertEquals('1-Jan-1960', src.start);
		System.assertEquals('1-Oct-1960', src.endd);
	}

	static Testmethod void testGetShippingReportForST() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForST();

		System.assertEquals(2, src.formsST.size());	
	}

	static Testmethod void testSublistsAM() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForAM();
		src.SubListsAM();

		System.assertEquals(2, src.splitAM.size());
	}

	static Testmethod void testSublistsST() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForST();
		src.SubListsST();

		System.assertEquals(2, src.splitST.size());
	}

	static Testmethod void testGetDynamicAM() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForAM();
		String test = src.getDynamicAM();
		System.assert(test.startsWith('<tbody>'));
		System.assert(test.endsWith('</tbody>'));
	} 

	static Testmethod void testGetDynamicST() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForST();
		String test = src.getDynamicST();
		System.assert(test.startsWith('<tbody>'));
		System.assert(test.endsWith('</tbody>'));
	} 

	static Testmethod void testTotalsByDay() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForAM();
		src.getshippingReportForST();
		src.SubListsAM();
		src.SubListsST();

		Form__c temp = [SELECT FormType__c, Confirmation__r.RecordType.Name, 
							OrderValueWholesale__c, MagValueWholesale__c
						FROM Form__c 
						WHERE Id =: formS.Id];

		List<Form__c> combined = new List<Form__c>();
		combined.addAll(src.formsAM);
		combined.addAll(src.formsST);
		combined.add(temp);

		List<Double> test = src.TotalsByDay(combined);

		System.assertEquals(4, test[0]);
		System.assertEquals(3, test[1]);
		System.assertEquals(4, test[2]);
		System.assertEquals(5, test[3]);
		System.assertEquals(6, test[4]);
		System.assertEquals(23, test[5]);
		System.assertEquals(22, test[6]);
	}

	static Testmethod void testWarehouseTotals() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		src.getshippingReportForAM();
		src.getshippingReportForST();

		List<Double> test = src.getAMWarehouseTotals();
		List<Double> test2 = src.getSTWarehouseTotals();

		System.assert(!test.isEmpty());
		System.assert(!test2.isEmpty());
	}

	static Testmethod void testFormTotalValue() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);
		
		Decimal test1 = src.formTotalValue('OP', 1, 1);
		System.assertEquals(1, test1);

		Decimal test2 = src.formTotalValue('SBYB', 1, 1);
		System.assertEquals(2, test2);
	}

	static Testmethod void testConvertMonth() {
		setup();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(form);	
		ShippingReportController src = new ShippingReportController(sc);

		String test1 = src.convertMonth(1);
		String test2 = src.convertMonth(2);
		String test3 = src.convertMonth(3);
		String test4 = src.convertMonth(4);
		String test5 = src.convertMonth(5);
		String test6 = src.convertMonth(6);
		String test7 = src.convertMonth(7);
		String test8 = src.convertMonth(8);
		String test9 = src.convertMonth(9);
		String test10 = src.convertMonth(10);
		String test11 = src.convertMonth(11);
		String test12 = src.convertMonth(12);

		System.assertEquals('Jan', test1);
		System.assertEquals('Feb', test2);
		System.assertEquals('Mar', test3);
		System.assertEquals('Apr', test4);
		System.assertEquals('May', test5);
		System.assertEquals('Jun', test6);
		System.assertEquals('Jul', test7);
		System.assertEquals('Aug', test8);
		System.assertEquals('Sep', test9);
		System.assertEquals('Oct', test10);
		System.assertEquals('Nov', test11);
		System.assertEquals('Dec', test12);
	}
}