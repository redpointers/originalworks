@isTest
private class TestOrderLinesViewApexController
{
	@isTest
	static void testGetData()
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();

		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		Confirmation__c confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		Form__c form = UnitTestDataFactory.inflateForm(confirm);
		insert form;

		List<OrderLines> lines = OrderLinesViewApexController.getOrdersForForm(form.Id);
		System.assert(lines.size() == 1);

	}
}