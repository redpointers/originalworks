@isTest (SeeAllData=false)
	/*
	 * author: Redpoint
	 * date: 1/19/2017
	 * purpose: Unit test for the AddConfirmationProductsRelatedList trigger. Update 3/10/2017: Unit test for the
     *          Confirmation Trigger Handler apex class.
	 * notes: Also, tests SetDefaultSalesRepValuesTrigger. Update 03/10/2017: The three separate confirmation 
     *          triggers have been re-worked into a trigger framework and were combined into a single apex class.
     *          Has achieved 100% test coverage as of 01/19.
	*/
private class TestConfirmationTriggers 
{
    static Account acct;
    static Season__c season;

    static void setup() 
    {
        UnitTestDataFactory.insertVariables();
        UnitTestDataFactory.insertShipDays();

        Variables__c var = Variables__c.getOrgDefaults();
        String defSeason = var.Default_Season__c;
        
        acct = UnitTestDataFactory.inflateAccount('Test Account');
        insert acct;

        season = UnitTestDataFactory.inflateSeason(defSeason);
        insert season;

        OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
        prod.Name = 'Tile';
        insert prod;

        SeasonProduct__c seasonproduct = UnitTestDataFactory.inflateSeasonProduct(prod, season);
        insert seasonproduct;

    }
    
    @isTest
    static void testAddToRelatedList() 
    {
        setup();

        Confirmation__c conf = new Confirmation__c();
        RecordType recordtype = [SELECT Id 
                                 FROM RecordType 
                                 WHERE Name = 'OP' 
                                 LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;
        
        List<ConfirmationProduct__c> confirmationproductlist = new List<ConfirmationProduct__c>();
        confirmationproductlist = [SELECT Id 
                               FROM ConfirmationProduct__c 
                               WHERE ConfirmationProduct__c.Confirmation__c =: conf.Id];
        
        System.assertEquals(confirmationproductlist.size(), 1);
        //System.assertEquals(confirmationproductlist.size(), 0);//Data migration
    } 
    
    @isTest
    static void testSetDefaultValues() 
    {
        setup();

        Confirmation__c conf = new Confirmation__c();
        Contact con = new Contact();
        RecordType recordtype = [SELECT Id 
                                 FROM RecordType 
                                 WHERE Name = 'OP' 
                                 LIMIT 1];
        
        con.AccountId = acct.Id;
        con.LastName = 'Test Rep';
        insert con;

        acct.SalesRep__c = con.Id;
        acct.SubSalesRep__c = con.Id;
        update acct;
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        insert conf;
        
        Confirmation__c checkconf = [SELECT Id,SalesRep__c, SubSalesRep__c 
                                     				FROM Confirmation__c 
                                     				WHERE Id =: conf.Id];
        
        System.assertEquals(checkconf.SalesRep__c, acct.SalesRep__c);
        System.assertEquals(checkconf.SubSalesRep__c, acct.SubSalesRep__c);

        //System.assertEquals(checkconf.SalesRep__c, null);//Data migration
        //System.assertEquals(checkconf.SubSalesRep__c, null);//Data migration
    }
    
    @isTest
    static void testNewCustomer() 
    {
        setup();

        Confirmation__c conf1 = new Confirmation__c();
        Confirmation__c conf2 = new Confirmation__c();
        RecordType recordtype = [SELECT Id 
                                 FROM RecordType 
                                 WHERE Name = 'OP' 
                                 LIMIT 1];
        
        Account check = [SELECT Id, RepeatCustomer__c FROM Account WHERE Id =: acct.Id];

        System.assertEquals(check.RepeatCustomer__c, False);
    }

    @isTest
    static void testSBYB() 
    {
        setup();

        Confirmation__c conf = new Confirmation__c();
        RecordType recordtype = [SELECT Id 
                                 FROM RecordType 
                                 WHERE Name = 'SBYB' 
                                 LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;
    }

    @isTest
    static void testGallery() 
    {
        setup();

        Confirmation__c conf = new Confirmation__c();
        RecordType recordtype = [SELECT Id 
                                 FROM RecordType 
                                 WHERE Name = 'Gallery' 
                                 LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;
    }  

    @isTest
    static void testTileWall() 
    {
        setup();

        Confirmation__c conf = new Confirmation__c();
        RecordType recordtype = [SELECT Id 
                                 FROM RecordType 
                                 WHERE Name = 'Tile Wall' 
                                 LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;
    }
}