public with sharing class ConfirmationProductRecordApexController 
{
	@AuraEnabled
	public static ConfirmationProduct__c getProduct(Id recordId)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getConfirmationProduct?recordId='+recordId);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock();
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            ConfirmationProduct__c cp = parseJSONCPBody(res.getBody());
            return cp;
        }
        catch(Exception e)
        {
        	throw new AuraException(e.getMessage());
        }

		return null;
	}

    @AuraEnabled
    public static String getUserDateString(Id userId, String dtObj)
    {
        if(dtObj == null)
            return '';

        DateTime dt = (DateTime) Json.deserialize(dtObj, DateTime.class);

        User usr = [select Name from User where Id=:userId];

        String ret = usr.Name+ ' ' + dt.format();
        return ret;
    }

    @AuraEnabled
    public static String getConfirmationName(Id confirmId)
    {
        if(String.isEmpty(confirmId))
            return '';

        Confirmation__c confirm = [select Id, Name from Confirmation__c where Id=:confirmId];

        return confirm.Name;
    }

    @AuraEnabled
    public static String getSeasonProductName(Id spId)
    {
        if(String.isEmpty(spId))
            return '';

        SeasonProduct__c confirm = [select Id, Name from SeasonProduct__c where Id=:spId];

        return confirm.Name;
    }

    @AuraEnabled
    public static String getOWProductName(Id owId)
    {
        if(String.isEmpty(owId))
            return '';
            
        OWProduct__c confirm = [select Id, Name from OWProduct__c where Id=:owId];

        return confirm.Name;
    }

    @AuraEnabled
    public static String updateProduct(String jsonObj)
    {
        ConfirmationProduct__c cp = (ConfirmationProduct__c) JSON.deserialize(jsonObj, ConfirmationProduct__c.class);

        Variables__c var = Variables__c.getOrgDefaults();

        List<ConfirmationProduct__c> lines = new List<ConfirmationProduct__c>{cp};

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'updateConfirmationProducts');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setBody(JSON.serialize(lines));
        System.debug('**** BODY: '+JSON.serialize(lines));

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock();
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        return res.getStatus();
    }

	public static ConfirmationProduct__c parseJSONCPBody(String body)
    {
    	JSONParser parser = JSON.createParser(body);

    	ConfirmationProduct__c cp = null;
    	while (parser.nextToken() != null) 
    	{
        	// Start at the array of lines.
        	if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
        	{
            	while (parser.nextToken() != null) 
            	{
               		// Advance to the start object marker to find next line object.
                	if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                	{
                		cp = (ConfirmationProduct__c)parser.readValueAs(ConfirmationProduct__c.class);
                	}
                }
            }
        }

        return cp;
    }
}