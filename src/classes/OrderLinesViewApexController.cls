public with sharing class OrderLinesViewApexController 
{
	@AuraEnabled
	public static List<OrderLines> getOrdersForForm(Id formId)
	{
        Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getRecordsForForm?formid='+formId);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock();
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            List<OrderLines> lines = OrderEntryComponentApexController.parseJSONLinesBody(res.getBody());
            return lines;
        }
        catch(Exception e)
        {
            throw new AuraException(e.getMessage());
        }

		return null;
	}
}