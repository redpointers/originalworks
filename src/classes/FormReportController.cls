public with sharing class FormReportController 
{
    public Form__c form{get; set;}
    public Confirmation__c confirm{get;set;}
    public Account acct{get; set;}
    public List<OrderLines> oLines{get;set;}
    public Integer numOrders{get;set;}
    public Integer numProducts{get;set;}
    public Integer numUniqueSuffixes{get;set;}
    public Double orderValue{get;set;}
    public Double totalValue{get;set;}
    public Double paperValue{get;set;}

    public FormReportController(ApexPages.StandardController stdController) 
    {
        Id formId = stdController.getId();
        if(formId == null)
            formId = ApexPages.currentPage().getParameters().get('id');

        form = [select Id, Name, FormType__c, FormSequence__c, ActualShip__c, ActualReceive__c, OrderEntry__c, ArtworkCount__c, 
            EstimatedShip__c, MagsBought__c, MagsReturned__c, MagValueRetail__c, Initials__c, QuestionFolders__c, ArtworkCountOE__c, Confirmation__c,
            DaysLate__c from Form__c where Id =: formId];

        confirm = [select Id, Name, Season__c, Season__r.Name, SalesRep__c, SalesRep__r.Name, TestProgram__c, Account__c, RecordType.Name, ClosedDates__c, PurchasePaper__c,
            ShipInstructions__c, ManInstructions__c, MatchInstructions__c, LastSchoolDate__c, ResumeDate__c, NumberOfReams__c from Confirmation__c where Id =: form.Confirmation__c];

        oLines = putOrderLinesInOrder(form.Id, confirm.Season__c);

        acct = [select Id, Name, BillingState, Owner.Name from Account where Id =: confirm.Account__c];

        numOrders = 0;
        numProducts = 0;
        orderValue = 0;
        totalValue = 0;
        paperValue = 0;
        numUniqueSuffixes = 0;
        if(oLines!=null && oLines.size() > 0)
        {
            Set<String> uniqueSuffixes = new Set<String>();

            numOrders = oLines.size();
            for(OrderLines ord : oLines)
            {
                numProducts = numProducts + (Integer)ord.qty;
                orderValue = orderValue + ord.total;
                totalValue = totalValue + ord.total;

                if(!uniqueSuffixes.contains(ord.suffix))
                    uniqueSuffixes.add(ord.suffix);
            }

            numUniqueSuffixes = uniqueSuffixes.size();
        }

        if(confirm.PurchasePaper__c !=null && confirm.PurchasePaper__c && confirm.NumberOfReams__c != null)
        {
            Variables__c vars = Variables__c.getInstance();
            Double costPerReam = vars.Paper_SBYB__c;

            if(confirm.RecordType.Name == 'OP')
                costPerReam = vars.Paper_OP__c;
            else if(confirm.RecordType.Name == 'Tile Wall')
                costPerReam = vars.Paper_TW4__c;

            paperValue = Integer.valueOf(confirm.NumberOfReams__c) * costPerReam;
        }
    }

    private List<OrderLines> putOrderLinesInOrder(Id formId, Id seasonId)
    {
        List<OrderLines> unOrdLines = OrderEntryComponentApexController.getOrdersForForm(form.Id);

        List<SeasonProduct__c> seasonProds = [select Id, Name, BoxSort__c, OWProduct__r.Code__c from SeasonProduct__c where Season__c =: seasonId order by BoxSort__c ASC];

        List<OrderLines> ordLines = new List<OrderLines>();

        for(SeasonProduct__c sp : seasonProds)
        {
            for(OrderLines line : unOrdLines)
            {
                if(sp.OWProduct__r.Code__c == line.code)
                    ordLines.add(line);
            }
        }

        return ordLines;
    }
}