@isTest
private class TestReportsPageController {
	/*
     * author: Redpoint
     * purpose: Unit testing for the Reports Page Lightning component controller.
     * date: 4/25/2017
     * notes: 100% code coverage as of 4/25
     *		  The WIP report has not been added to the Reports Page lightning component since
     *		  it is not completed. 
    */
	static void setup() {

		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP60';
        upsert var;
	
		Season__c season = UnitTestDataFactory.inflateSeason('SP60');
		insert season;
	}

	static Testmethod void testGetSeasons() {
		List<Season__c> toTest = [SELECT Name FROM Season__c];

		List<Season__c> test = ReportsPageController.getSeasons();

		System.assert(toTest.equals(test));
	}

	@isTest(SeeAllData='true')
	static void testDailyRevenueReport() {
		setup();
		
		String instance = URL.getSalesforceBaseURL().toExternalForm();
		String test = ReportsPageController.dailyRevenueReport();

		Report report = [SELECT Id, Name FROM Report WHERE Name = 'Daily Revenue' LIMIT 1];

		System.assert(test.endsWith('SP60'));
		System.assert(test.contains(instance));
		System.assert(test.contains(report.Id));
	}

	static Testmethod void testItemCountReport() {
		setup();

		Season__c testSeason = [SELECT Id, Name FROM Season__c WHERE Name = 'SP60'];

		String test = ReportsPageController.itemCountReport('SP60');
		System.assertEquals('/apex/ItemCountPDF?id=' + testSeason.Name, test);
	}

	static Testmethod void testPerCapReport() {
		setup();

		String test = ReportsPageController.perCapReport('SP60');
		System.assertEquals('/apex/PerCapReport?season=SP60', test);		
	}

	static Testmethod void testLeadSourceReport() {
		
		String test = ReportsPageController.leadSourceReport();
		System.assertEquals('/apex/LeadSourceListReport', test);
	}

	static Testmethod void testReceivingReport() {

		String test = ReportsPageController.receivingReport('2017-01-01', '2017-05-28');
		String expected = '/apex/ReceivingReport?endrange=5%2F28%2F2017&startrange=1%2F1%2F2017';

		System.assertEquals(expected, test);
	}

	static Testmethod void testSalesAnalysisReport() {
		setup();

		String test = ReportsPageController.salesAnalysisReport('SP60');

		System.assertEquals('/apex/SalesAnalysis?season=SP60',test);
	}

	static Testmethod void testSeasonForecastDetailReport() {

		String test = ReportsPageController.seasonForecastDetailReport('SP60', 'Active');
		String expected = '/apex/SeasonForecastDetail?season=SP60&status=Active';

		System.assertEquals(expected, test);
	}

	static Testmethod void testSeasonForecastHQMGRDetailReport() {

		String test = ReportsPageController.seasonForecastHQMGRDetailReport('SP60', 'Inactive');
		String expected = '/apex/SeasonForecastHQMGRDetail?season=SP60&status=Inactive';

		System.assertEquals(expected, test);
	}

	static Testmethod void testSeasonForecastHQMGRSummaryReport() {

		String test = ReportsPageController.seasonForecastHQMGRSummaryReport('SP60', 'Active');
		String expected = '/apex/SeasonForecastHQMGRSummary?season=SP60&status=Active';

		System.assertEquals(expected, test);
	}

	static Testmethod void testSeasonForecastSummaryReport() {

		String test = ReportsPageController.seasonForecastSummaryReport('SP60', 'Active');
		String expected = '/apex/SeasonForecastSummary?season=SP60&status=Active';

		System.assertEquals(expected, test);
	}

	@isTest(SeeAllData='true')
	static void testShipDateChangeLogReport() {
		setup();
		
		String instance = URL.getSalesforceBaseURL().toExternalForm();
		String test = ReportsPageController.shipDateChangeLogReport();

		Report report = [SELECT Id, Name FROM Report WHERE Name = 'Ship Date Change Log' LIMIT 1];

		System.assert(test.endsWith('SP60'));
		System.assert(test.contains(instance));
		System.assert(test.contains(report.Id));
	}

	static Testmethod void testShippingReport() {

		String test = ReportsPageController.shippingReport('2017-01-01', '2017-05-28');
		String expected = '/apex/ShippingReport?endrange=5%2F28%2F2017&startrange=1%2F1%2F2017';

		System.assertEquals(expected, test);
	}

	static Testmethod void testFormatDateRanges() {

		String test = ReportsPageController.formatDateRanges(Date.newInstance(1990, 5, 26));
		System.assertEquals('5/26/1990', test);
	}
}