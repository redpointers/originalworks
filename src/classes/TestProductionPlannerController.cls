@isTest
private class TestProductionPlannerController {
    /*
     * author: Redpoint
     * date: 3/9/2017
     * purpose: Unit testing the Production Planner Controller Apex class.
     * notes: Has a 82% code coverage as of 4/20.
    */

    static Confirmation__c confirm = new Confirmation__c();
    static Confirmation__c confirm2 = new Confirmation__c();
    static Confirmation__c confirm3 = new Confirmation__c();
    static Confirmation__c confirm4 = new Confirmation__c();
    static Confirmation__c confirm5 = new Confirmation__c();
    static Form__c form = new Form__c();
    static Form__c form2 = new Form__c();
    static Form__c form3 = new Form__c();
    static Form__c form4 = new Form__c();
    static Form__c form5 = new Form__c();
    static Form__c form6 = new Form__c();

    static void setup() {
        UnitTestDataFactory.insertVariables();

        Variables__c var = Variables__c.getOrgDefaults();

        UnitTestDataFactory.insertVariables();

        List<RecordType> types = [SELECT Id, Name FROM RecordType];
        RecordType opType = new RecordType();
        RecordType sbybType = new RecordType();
        RecordType galleryType = new RecordType();
        RecordType tileType = new RecordType();

        for(RecordType t : types) {
            
            if(t.Name == 'OP') {
                opType = t;
            } else if(t.Name == 'SBYB') {
                sbybType = t;
            } else if(t.Name == 'Gallery') {
                galleryType = t;
            } else if(t.Name == 'Tile Wall') {
                tileType = t;
            }
        }
        
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.RepeatCustomer__c = True;
        insert acct;

        Account acct2 = new Account();
        acct2.Name = 'Test Account 2';
        acct2.RepeatCustomer__c = False;
        insert acct2;

        Account acct3 = new Account();
        acct3.Name = 'Test Account 3';
        acct3.RepeatCustomer__c = False;
        insert acct3;

        Account acct4 = new Account();
        acct4.Name = 'Test Account 4';
        acct4.RepeatCustomer__c = False;
        insert acct4;

        Account acct5 = new Account();
        acct5.Name = 'Test Account 5';
        acct5.RepeatCustomer__c = False;
        insert acct5;

        Contact ct = UnitTestDataFactory.inflateContact(null, acct);
        insert ct;
        
        Season__c season = UnitTestDataFactory.inflateSeason(var.Default_Season__c);
        insert season;

        confirm.Season__c = season.Id;
        confirm.Account__c = acct.Id;
        confirm.RecordTypeId = sbybType.Id;
        confirm.Status__c = 'Active';
        confirm.SeasonPopEstimate__c = 89;
        insert confirm;

        confirm2.Season__c = season.Id;
        confirm2.Account__c = acct2.Id;
        confirm2.RecordType = opType;
        insert confirm2;

        confirm3.Season__c = season.Id;
        confirm3.Account__c = acct3.Id;
        confirm3.RecordType = galleryType;
        confirm3.Status__c = 'Inactive';
        confirm3.SeasonPopEstimate__c = 89;
        insert confirm3;

        confirm4.Season__c = season.Id;
        confirm4.Account__c = acct4.Id;
        confirm4.RecordTypeId = tileType.Id;
        confirm4.Status__c = 'Inactive';
        confirm4.Size6x6__c = True;
        confirm4.Size4x4__c = True;
        insert confirm4;

        confirm5.Season__c = season.Id;
        confirm5.Account__c = acct5.Id;
        confirm5.RecordTypeId = tileType.Id;
        confirm5.Size4x4__c = True;
        insert confirm5;

        form = UnitTestDataFactory.inflateForm(confirm);
        form.ActualReceive__c = Date.newInstance(1960, 01, 01);
        form.WarehouseAssigned__c = 'AM'; 
        form.EmailSentReceive__c = True;
        form.EmailSentShip__c = True;
        form.OrderEntry__c = Date.newInstance(1960, 01, 02);
        form.MagValueWholesale__c = 45;
        form.OrderValueWholesale__c = 32;
        insert form;

        form2 = UnitTestDataFactory.inflateForm(confirm2);
        form2.FormType__c = 'A';
        form2.ActualReceive__c = Date.newInstance(1961, 01, 01);
        form2.OrderEntry__c = null; 
        form2.WarehouseAssigned__c = null; 
        form2.EmailSentReceive__c = False;
        form2.EmailSentShip__c = False;
        insert form2;

        form4 = UnitTestDataFactory.inflateForm(confirm4);
        form4.FormType__c = 'C';
        form4.ActualReceive__c = Date.newInstance(1961, 01, 01);
        form4.MagValueWholesale__c = 45;
        form4.OrderValueWholesale__c = 32; 
        insert form4;
    }

    static Testmethod void TestBuildQuery() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1961-01-01', '', '', '', '', '', '', '');
        System.assertEquals(1, forms.size());
    }
    
    static Testmethod void testGetSeasons() {
        List<Season__c> seasonTest = ProductionPlannerController.getSeasons();
        List<Season__c> seasonActual = [SELECT Name FROM Season__c];

        seasonTest.sort();
        seasonActual.sort();

        System.assert(seasonActual.equals(seasonTest));
    }

    static Testmethod void testSeasonFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1961-01-01', 'SP17', '', '', '', '', '', '');
        System.assertEquals(1, forms.size());
    }

    static Testmethod void testRecordTypeFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1961-01-01', '', 'SBYB', '', '', '', '', '');
        System.assertEquals(1, forms.size());

        List<Form__c> forms2 = new List<Form__c>();
        forms2 = ProductionPlannerController.buildQuery('ActualReceive__c','1960-01-01', '1962-01-01', '', 'Gallery;OP;Tile Wall', '', '', '', '', '');
        System.assertEquals(2, forms2.size());  
    }

    static Testmethod void testTileSizeFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1960-01-01', '1962-01-01', '', 'Tile Wall', 'Size6x6__c', '', '', '', '');
        System.assertEquals(1, forms.size()); 

        List<Form__c> forms2 = new List<Form__c>();
        forms2 = ProductionPlannerController.buildQuery('ActualReceive__c','1960-01-01', '1962-01-01', '', 'Tile Wall', 'Size6x6__c;Size4x4__c', '', '', '', '');
        System.assertEquals(1, forms2.size());      
    }

    static Testmethod void testFormTypeFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1963-01-01', '', '', '', 'A', '', '', '');
        System.assertEquals(2, forms.size());

        List<Form__c> forms2 = new List<Form__c>();
        forms2 = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', 'B;G;A', '', '', '');
        System.assertEquals(2, forms2.size());      
    }

    static Testmethod void testWarehouseFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1963-01-01', '', '', '', '', 'AM', '', '');
        System.assertEquals(1, forms.size());

        List<Form__c> forms2 = new List<Form__c>();
        forms2 = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', '', 'AM;ST', '', '');
        System.assertEquals(1, forms2.size());      
    }

    static Testmethod void testStatusFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1963-01-01', '', '', '', '', '', 'False', '');
        System.assertEquals(2, forms.size());

        List<Form__c> forms2 = new List<Form__c>();
        forms2 = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1963-01-01', '', '', '', '', '', 'Inactive', '');
        System.assertEquals(1, forms2.size());

        List<Form__c> forms3 = new List<Form__c>();
        forms3 = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', '', '', 'True;Inactive', '');
        System.assertEquals(2, forms3.size());      
    }

    static Testmethod void testStageFilter() {
        setup();

        List<Form__c> forms = new List<Form__c>();
        forms = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1963-01-01', '', '', '', '', '', '', 'all');
        System.assertEquals(1, forms.size());

        List<Form__c> forms2 = new List<Form__c>();
        forms2 = ProductionPlannerController.buildQuery('ActualReceive__c','1959-01-01', '1963-01-01', '', '', '', '', '', '', 'n_all');
        System.assertEquals(1, forms2.size());

        List<Form__c> forms3 = new List<Form__c>();
        forms3 = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', '', '', '', 'received;orderEntry;whAssignment;shipped');
        System.assertEquals(1, forms3.size());  

        List<Form__c> forms4 = new List<Form__c>();
        forms4 = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', '', '', '', 'n_received;n_orderEntry;n_whAssignment;n_shipped');
        System.assertEquals(1, forms4.size());      
    }

    static Testmethod void testSortOnColumn() {
        setup();
        List<Form__c> results = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', '', '', '', '');
        String query = ProductionPlannerController.getSQ();

        List<Form__c> f1 = [SELECT Id, FormType__c, FormSequence__c, WarehouseTransferDate__c, OrderEntry__c, WarehouseAssigned__c,
                                ActualReceive__c, ActualShip__c, OrderValueWholesale__c, MagValueWholesale__c, ArtworkCount__c, 
                                Confirmation__r.Projection__c, Confirmation__r.FTType__c, Confirmation__r.Season__r.Name,
                                Confirmation__r.RecordType.Name, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Account__r.BillingState, 
                                Confirmation__r.SeasonPopEstimate__c, Confirmation__r.SalesRep__r.Name 
                            FROM Form__c
                            WHERE (ActualReceive__c >: Date.newInstance(1958, 01, 01) AND ActualReceive__c <:  Date.newInstance(1963, 01, 01))
                            ORDER BY ActualShip__c DESC];

        List<Form__c> col1 = ProductionPlannerController.sortOnColumn(1, query);
        System.assert(f1.equals(col1));

    }

    static Testmethod void testSummaries() {
        setup();

        List<Form__c> results = ProductionPlannerController.buildQuery('ActualReceive__c','1958-01-01', '1963-01-01', '', '', '', '', '', '', '');
        Integer count = ProductionPlannerController.getCount();
        Decimal projectedRevenue = ProductionPlannerController.getProjectedRevenue();
        Decimal magProjected = ProductionPlannerController.getMagnetsProjected();
        Double magnets = ProductionPlannerController.getMagnetsMade();
        Decimal magDollar = ProductionPlannerController.getMagDollarValue();
        Decimal orderValue = ProductionPlannerController.getOrderValue();
        Decimal total = ProductionPlannerController.getTotalValue();

        Decimal sum = 0;
        Decimal sum2 = 0;
        Double sum3 = 0;
        Decimal sum4 = 0;
        Decimal sum5 = 0;

        for(Form__c f : results) {
            if(f.Confirmation__r.Projection__c != null) {sum += f.Confirmation__r.Projection__c;}
            if((f.Confirmation__r.SeasonPopEstimate__c != null) && (f.Confirmation__r.RecordType.Name == 'SBYB')) { sum2 += f.Confirmation__r.SeasonPopEstimate__c;}
            if((f.ArtworkCount__c != null) && (f.Confirmation__r.RecordType.Name == 'SBYB') && (f.FormType__c == 'A')) { sum3 += f.ArtworkCount__c;}
            if(f.MagValueWholesale__c != null) { sum4 += f.MagValueWholesale__c;}
            if(f.OrderValueWholesale__c != null) { sum5 += f.OrderValueWholesale__c;}
        }

        System.assertEquals(results.size(), count);
        System.assertEquals(sum,projectedRevenue);
        System.assertEquals(sum2,magProjected);
        System.assertEquals(sum3,magnets);
        System.assertEquals(sum4,magDollar);
        System.assertEquals(sum5, orderValue);
        System.assertEquals((orderValue + magDollar), total);
    }
}