@isTest
private class TestFormTriggerHandler {
	/*
	* author: Redpoint
	* date: 3/10/2017
	* purpose: Unit Tests for the Form Trigger Handler class.
	* notes: Has 100% code coverage as of 03/10.
	*/
	static Confirmation__c confirm = new Confirmation__c();
	static Form__c form = new Form__c();
	static Form__c form2 = new Form__c();
	static Form__c form3 = new Form__c();

	static void setup() {

		UnitTestDataFactory.insertVariables();

        Variables__c var = Variables__c.getOrgDefaults();
		
		Account acct = new Account();
		acct.Name = 'Unit Testing';
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason(var.Default_Season__c);
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		form.Confirmation__c = confirm.Id;
		form.FormType__c = 'A';
		form.EstimatedReceived__c = Date.newInstance(1960, 01, 11);
		form.ActualReceive__c = Date.newInstance(1960, 01, 06);
		insert form;

		form2.Confirmation__c = confirm.Id;
		form2.FormType__c = 'A';
		form2.EstimatedReceived__c = Date.newInstance(1960, 01, 01);
		form2.ActualReceive__c = Date.newInstance(1960, 01, 12);
		insert form2;

		form3.Confirmation__c = confirm.Id;
		form3.FormType__c = 'B';
		insert form3;
	}

	static Testmethod void testHandleBeforeInsert() {
		setup();
		
		Form__c test1 = [SELECT Id, FormSequence__c, Greensheet__c, DaysLate__c  FROM Form__c WHERE Id =: form.Id];
		Form__c test2 = [SELECT Id, FormSequence__c, Greensheet__c FROM Form__c WHERE Id =: form2.Id];
		Form__c test3 = [SELECT Id, FormSequence__c FROM Form__c WHERE Id =: form3.Id];

		System.assertEquals(False, test1.Greensheet__c);
		System.assertEquals(0, test1.DaysLate__c);

		System.assertEquals(True, test2.Greensheet__c);

	}

	static Testmethod void testHandleBeforeUpdate() {
		setup();

		Form__c test1 = [SELECT Id, ActualReceive__c, EstimatedReceived__c FROM Form__c WHERE Id =: form.Id];
		test1.EstimatedReceived__c = Date.newInstance(1960, 02, 10);
		test1.ActualReceive__c = Date.newInstance(1960, 02, 09);
		update test1;

		Form__c test2 = [SELECT Id, ActualReceive__c, EstimatedReceived__c FROM Form__c WHERE Id =: form2.Id];
		test2.ActualReceive__c = Date.newInstance(1960, 01, 26);
		update test2;

		Form__c test3 = [SELECT Id, Greensheet__c FROM Form__c WHERE Id =: form2.Id];
		Form__c test4 = [SELECT Id, DaysLate__c FROM Form__c WHERE Id =: form.Id];

		System.assertEquals(True, test3.Greensheet__c);
		System.assertEquals(0, test4.DaysLate__c);
	}
}