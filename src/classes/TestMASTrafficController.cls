@isTest
private class TestMASTrafficController
{
	static Confirmation__c confirm;
	static Confirmation__c confirmRetail;
	static Confirmation__c confirmGallery;

	static Form__c form;
	static Form__c formRetail;
	static Form__c formGallery;

	static OrderLines ord;
	static OrderLines ordRetail;
	static OrderLines ordGallery;

	static ConfirmationProduct__c confirmProd;
	static ConfirmationProduct__c confirmProdRetail;
	static ConfirmationProduct__c confirmProdGallery;

	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Account acctRetail = UnitTestDataFactory.inflateAccount('Retail Account');
		acctRetail.status__c = 'Retail Customer';
		insert acctRetail;

		Account acctGallery = UnitTestDataFactory.inflateAccount('Gallery Account');
		acctGallery.status__c = 'Customer';
		insert acctGallery;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Contact ctRetail = UnitTestDataFactory.inflateContact(null, acctRetail);
		insert ctRetail;

		Contact ctGallery = UnitTestDataFactory.inflateContact(null, acctGallery);
		insert ctGallery;

		Season__c season = UnitTestDataFactory.inflateSeason('SP17');
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

		form = UnitTestDataFactory.inflateForm(confirm);
		form.MAS90Export__c = null;
		form.WarehouseAssigned__c = 'AM';
		form.WarehouseTransferDate__c = System.today();
		form.ActualShip__c = System.today();
		insert form;

		confirmRetail = UnitTestDataFactory.inflateConfirmation(acctRetail, season, ctRetail);
		insert confirmRetail;

		formRetail = UnitTestDataFactory.inflateForm(confirmRetail);
		formRetail.MAS90Export__c = null;
		formRetail.WarehouseAssigned__c = 'AM';
		formRetail.WarehouseTransferDate__c = System.today();
		formRetail.ActualShip__c = System.today();
		insert formRetail;

		RecordType rt = [select Id from RecordType where sobjecttype = 'Confirmation__c' and Name = 'Gallery'];

		confirmGallery = UnitTestDataFactory.inflateConfirmation(acctGallery, season, ctGallery);
		confirmGallery.RecordTypeId = rt.Id;
		insert confirmGallery;

		formGallery = UnitTestDataFactory.inflateForm(confirmGallery);
		formGallery.MAS90Export__c = null;
		formGallery.WarehouseAssigned__c = 'AM';
		formGallery.WarehouseTransferDate__c = System.today();
		formGallery.ActualShip__c = System.today();
		insert formGallery;

		ord = UnitTestDataFactory.inflateOrder('101', form, season);
		ordRetail = UnitTestDataFactory.inflateOrder('102', formRetail, season);
		ordGallery = UnitTestDataFactory.inflateOrder('103', formGallery, season);

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c sp = UnitTestDataFactory.inflateSeasonProduct(prod, season);
		insert sp;

		confirmProd = UnitTestDataFactory.inflateConfirmationProduct(confirm, sp);
		insert confirmProd;

		confirmProdRetail = UnitTestDataFactory.inflateConfirmationProduct(confirmRetail, sp);
		insert confirmProdRetail;

		confirmProdGallery = UnitTestDataFactory.inflateConfirmationProduct(confirmGallery, sp);
		insert confirmProdGallery;
	}

	@isTest
	static void getExtractData()
	{
		setup();

		MASTrafficData mtd = MASTrafficComponentApexController.extractData();
		System.assert(mtd != null);

		String accountsStr = System.JSON.serialize(mtd.accounts);
        String confirmsStr = System.JSON.serialize(mtd.confirms);
        String formStr = System.JSON.serialize(mtd.formIds);
        String taxStr = System.JSON.serialize(mtd.taxAccounts);
        String questionStr = System.JSON.serialize(mtd.questionForms);
        String noMagStr = System.JSON.serialize(mtd.noMagsForms);
        String highMagStr = System.JSON.serialize(mtd.hiMagsConfirms);

		String exportSchoolRes = MASTrafficComponentApexController.exportSchoolData(accountsStr, 'NORMAL', taxStr, questionStr, noMagStr, highMagStr);
		String exportSRRes = MASTrafficComponentApexController.exportSalesRepData(confirmsStr, taxStr, questionStr, noMagStr, highMagStr);
		String exportInvRes = MASTrafficComponentApexController.exportInvoiceData(accountsStr, confirmsStr, formStr, 'NORMAL', taxStr, questionStr, noMagStr, highMagStr);

	}

	@isTest
	static void getExtractRetailData()
	{
		setup();

		MASTrafficData mtd = MASTrafficComponentApexController.extractRetailData();
		System.assert(mtd != null);

		String accountsStr = System.JSON.serialize(mtd.accounts);
        String confirmsStr = System.JSON.serialize(mtd.confirms);
        String formStr = System.JSON.serialize(mtd.formIds);
        String taxStr = System.JSON.serialize(mtd.taxAccounts);
        String questionStr = System.JSON.serialize(mtd.questionForms);
        String noMagStr = System.JSON.serialize(mtd.noMagsForms);
        String highMagStr = System.JSON.serialize(mtd.hiMagsConfirms);

		String exportSchoolRes = MASTrafficComponentApexController.exportSchoolData(accountsStr, 'RETAIL', taxStr, questionStr, noMagStr, highMagStr);
		String exportInvRes = MASTrafficComponentApexController.exportInvoiceData(accountsStr, confirmsStr, formStr, 'RETAIL', taxStr, questionStr, noMagStr, highMagStr);

	}

	@isTest
	static void getExtractGalleryData()
	{
		setup();

		MASTrafficData mtd = MASTrafficComponentApexController.extractGalleryData();
		System.assert(mtd != null);

		String accountsStr = System.JSON.serialize(mtd.accounts);
        String confirmsStr = System.JSON.serialize(mtd.confirms);
        String formStr = System.JSON.serialize(mtd.formIds);
        String taxStr = System.JSON.serialize(mtd.taxAccounts);
        String questionStr = System.JSON.serialize(mtd.questionForms);
        String noMagStr = System.JSON.serialize(mtd.noMagsForms);
        String highMagStr = System.JSON.serialize(mtd.hiMagsConfirms);

		String exportSchoolRes = MASTrafficComponentApexController.exportSchoolData(accountsStr, 'GALLERY', taxStr, questionStr, noMagStr, highMagStr);
		String exportSRRes = MASTrafficComponentApexController.exportSalesRepData(confirmsStr, taxStr, questionStr, noMagStr, highMagStr);
		String exportInvRes = MASTrafficComponentApexController.exportInvoiceData(accountsStr, confirmsStr, formStr, 'GALLERY', taxStr, questionStr, noMagStr, highMagStr);

	}

	@isTest
	static void getMarkForms()
	{
		setup();

		MASTrafficData mtd = MASTrafficComponentApexController.extractGalleryData();
		String formStr = System.JSON.serialize(mtd.formIds);

		MASTrafficComponentApexController.markFormsExported(formStr);
	}
}