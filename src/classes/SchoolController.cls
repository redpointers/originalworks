public with sharing class SchoolController {

    @AuraEnabled
    public static List<Account> findByName(String searchKey) {
        String school = '%' + searchKey + '%';
        return [SELECT Id, SchoolCode__c, Name FROM Account WHERE SchoolCode__c LIKE :school LIMIT 5];
    }
}