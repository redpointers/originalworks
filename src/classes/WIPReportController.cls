public with sharing class WIPReportController 
{
	public Season__c season{get;set;}
	public DateTime thruDate{get;set;}
	public List<String> warehouses{get;set;}
	public List<WarehouseData> warehouseData{get;set;}
	public Integer sbybRcvd{get;set;}
	public Double tvRcvd{get;set;}
	public Integer sbybOE{get;set;}
	public Double tvOE{get;set;}
	public Double ovOE{get;set;}
	public Integer sbybWH{get;set;}
	public Integer sbybShipped{get;set;}
	public Double tvWH{get;set;}
	public Double ovWH{get;set;}
	public Double tvShipped{get;set;}
	public Double ovShipped{get;set;}
	public Integer sbybWIP{get;set;}
	public Double tvWIP{get;set;}
	public Double ovWIP{get;set;}

	public WIPReportController() 
	{
		Id seasonId = ApexPages.currentPage().getParameters().get('seasonid');
		String dateTimeString = ApexPages.currentPage().getParameters().get('date');

		season = [select Id, Name from Season__c where Id =: seasonid];
		DateTime gmtDT = DateTime.valueOf(dateTimeString);


		TimeZone tz = UserInfo.getTimeZone();
		thruDate = gmtDT.addSeconds(tz.getOffset(gmtDT)/1000);
		thruDate = thruDate.addSeconds(tz.getOffset(thruDate)/1000);

		warehouses = WIPComponentApexController.getWarehouses();

		List<Form__c> forms =  WIPComponentApexController.getFormsForSeason(seasonId);
		List<Confirmation__c> confirms = WIPComponentApexController.getConfirmsForSeason(seasonId);
		List<Id> formIds = new List<Id>();
		for(Form__c form : forms)
			formIds.add(form.Id);

		List<OrderLines> orders = WIPComponentApexController.getOrdersForSeason(seasonId,formIds);
		warehouseData = calculateWarehouseData(forms, orders);

		calculateCompanyTotals(forms,confirms);
	}

	private List<WarehouseData> calculateWarehouseData(List<Form__c> forms, List<OrderLines> orders)
	{
		List<WarehouseData> whData = new List<WarehouseData>();
		for(String wh : warehouses)
        {
        	WarehouseData wd = new WarehouseData(wh);

        	Integer magsTotal = 0;
            for(Form__c form : forms)
            {
            	Id formId = form.Id;
            	String formWarehouse = form.WarehouseAssigned__c;
            	String confirmType = form.Confirmation__r.RecordType.Name;
            	String formType = form.FormType__c;
            	Date formShipDate = null;
            	if(form.ActualShip__c != null)
            		formShipDate = form.ActualShip__c;

        		if(wh != formWarehouse)
        			continue; // form is not in this warehouse

        		Integer magsForForm = 0;
        		if(form.MagsBought__c!=null)
        			magsForForm = (Integer)form.MagsBought__c;

        		// add in the SBYB magnets
        		if(confirmType == 'SBYB')
        			wd.sbybWHAssign = wd.sbybWHAssign + magsForForm;
        		if(confirmType == 'SBYB' && formShipDate != null && formShipDate <= thruDate)
                	wd.sbybShipped = wd.sbybShipped + magsForForm;

        		if(formType == 'B') // total up the magnets from the Form B's
            		magsTotal = magsTotal + magsForForm;

        		for(OrderLines order : orders)
        		{
        			if(order.formid != null && order.formid != 'null' &&order.formid == formId) // if the forms match, then we're in the right warehouse. order doesn't have warehouse, form does
        			{
        				if(confirmType == 'SBYB')
                			wd.sbybWHAssign = wd.sbybWHAssign+order.qty;

                		if(confirmType == 'SBYB' && formShipDate != null && formShipDate <= thruDate)
                			wd.sbybShipped = wd.sbybShipped+order.qty;

        				wd.ordersWHAssignOV = wd.ordersWHAssignOV+order.grandtotal;
            			
            			if(formShipDate != null && formShipDate <= thruDate)
        					wd.ordersWHShippedOV = wd.ordersWHShippedOV+order.grandtotal;
            		}
        		}
            }

            wd.ordersWHAssignTV = wd.ordersWHAssignOV+(magsTotal * 4);
        	wd.ordersWHShippedTV = wd.ordersWHShippedOV + (magsTotal * 4);

        	wd.sbybWip = wd.sbybWHAssign - wd.sbybShipped;
        	wd.wipTV = wd.ordersWHAssignTV - wd.ordersWHShippedTV;
        	wd.wipOV = wd.ordersWHAssignOV - wd.ordersWHShippedOV;
            	
            whData.add(wd);
        }

        return whData;
	}

	private void calculateCompanyTotals(List<Form__c> forms, List<Confirmation__c> confirms)
	{
		sbybRcvd=0;
		tvRcvd=0;
		sbybOE=0;
		tvOE=0;
		ovOE=0;
		sbybWH=0;
		sbybShipped=0;
		tvWH=0;
		ovWH=0;
		tvShipped=0;
		ovShipped=0;
		sbybWIP=0;
		tvWIP=0;
		ovWIP=0;
		Integer mags=0;

		for(Form__c form : forms)
        {
            if(form.ArtworkCount__c != null)
                sbybRcvd = sbybRcvd + (Integer)form.ArtworkCount__c;

            if(form.ArtworkCountOE__c != null)
                sbybOE = sbybOE + (Integer)form.ArtworkCountOE__c;

            if(form.MagsBought__c != null)
                mags = mags + (Integer)form.MagsBought__c;

            if(form.OrderValueWholesale__c!=null && form.MagsBought__c!= null)
                ovOE = ovOE + form.OrderValueWholesale__c - (form.MagsBought__c*4);
            else if(form.OrderValueWholesale__c != null)
                ovOE = ovOE + form.OrderValueWholesale__c;
        }

        for(Confirmation__c confirm : confirms)
        {
            if(confirm.Projection__c != null)
                tvRcvd = tvRcvd + (confirm.Projection__c);

            if(confirm.TotalRevenue__c != null)
            {
                tvOE = tvOE + (confirm.TotalRevenue__c);
            }
        }

		for(WarehouseData ware : this.warehouseData)
		{
			sbybWH = sbybWH + (Integer)ware.sbybWHAssign;
			sbybShipped = sbybShipped + (Integer)ware.sbybShipped;
			tvWH = tvWH + ware.ordersWHAssignTV;
			ovWH = ovWH + ware.ordersWHAssignOV;
			tvShipped = tvShipped + ware.ordersWHShippedTV;
			ovShipped = ovShipped + ware.ordersWHShippedOV;

		}

		sbybWIP = sbybWH - sbybShipped;
		tvWIP = tvWH - tvShipped;
		ovWIP = ovWH - ovShipped;
	}

	public class WarehouseData
	{
		public String name{get;set;}
		public Double sbybWHAssign{get;set;}
        public Double sbybShipped{get;set;}
        public Double sbybWip{get;set;}
        public Double ordersWHAssignTV{get;set;}
        public Double ordersWHShippedTV{get;set;}
        public Double ordersWHAssignOV{get;set;}
        public Double ordersWHShippedOV{get;set;}
        public Double wipTV{get;set;}
        public Double wipOV{get;set;}

        public WarehouseData(String name)
        {
        	this.name = name;
        	sbybWHAssign = 0;
        	sbybShipped = 0;
        	sbybWip = 0;
        	ordersWHAssignTV = 0;
        	ordersWHShippedTV = 0;
        	ordersWHAssignOV = 0;
        	ordersWHShippedOV = 0;
        	wipTV = 0;
        	wipOV = 0;
        }
	}
}