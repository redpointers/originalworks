public with sharing class GreensheetController {
	/*
     * author: Redpoint
     * purpose: The apex class is used by the Greensheet Lightning component to find all the forms that
     *          have issues that need to be resolved and to provide information about a selected form.
     * date: 3/6/2017
     * notes:
    */
    
    /*************************************** Initialize ***************************************/
    @AuraEnabled
    public static List<Form__c> resolveRecords() {
        
        return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                				Confirmation__r.RecordType.Name, Confirmation__r.SalesRep__r.Name, 
                				Confirmation__r.AccountManager__r.Name, FormType__c
                FROM Form__c
                WHERE Greensheet__c = True ];
    }
    
    @AuraEnabled
    public static List<String> managerList(List<Form__c> forms) {
        List<String> managers;
        Set<String> unique = new Set<String>();
        
        for(Form__c f : forms) {
            if( (f.Confirmation__r.AccountManager__r.Name != null) && (!unique.contains(f.Confirmation__r.AccountManager__r.Name)) ) {
				unique.add(f.Confirmation__r.AccountManager__r.Name);
            }
        }
        managers = new List<String>(unique);

        return managers;
    }
    
    @AuraEnabled 
    public static Integer itemCount(List<Form__c> forms) {
        return forms.size();
    }
    
    /*************************************** Filter ***************************************/
    
    @AuraEnabled
    public static List<Form__c> filterByManager(String manager) {
        if(manager != '') {
            return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                                    Confirmation__r.RecordType.Name, Confirmation__r.SalesRep__r.Name, 
                                    Confirmation__r.AccountManager__r.Name, FormType__c
                    FROM Form__c
                    WHERE (Greensheet__c = True) AND (Confirmation__r.AccountManager__r.Name =: manager) ];
        } else
            return [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
                                    Confirmation__r.RecordType.Name, Confirmation__r.SalesRep__r.Name, 
                                    Confirmation__r.AccountManager__r.Name, FormType__c
                    FROM Form__c
                    WHERE (Greensheet__c = True) ];
    }
    
    /*************************************** Row Information ***************************************/
    
    @AuraEnabled
    public static Form__c selectedForm(String formId) {

        return [SELECT Confirmation__c, Confirmation__r.Account__r.SchoolCode__c, FormType__c, 
                    FormSequence__c, OKInstructions__c, OKDFL__c, OKShipAddress__c, DaysLate__c, 
                    Confirmation__r.SpecialInstructions__c, Note__c 
        		FROM Form__c 
        		WHERE Id =: formId];
    }
    
    @AuraEnabled
    public static String daysLate(Form__c form) {
        String val;

        if(form.DaysLate__c == null) {
            val = '';
        } else if( (form.DaysLate__c <= 2) && (form.DaysLate__c >= 0) ) {
        	val = 'On Time';    
        } else {
            val = form.DaysLate__c + ' Days Late';
        }
        return val;
    }

    @AuraEnabled
    public static String instructionsCheckbox(Form__c form) {
        String val;

        if(form.OKInstructions__c) {
            val = 'OK';
        } else {
            val = 'BAD!';
        }
        return val;
    }

    @AuraEnabled
    public static String dflCheckbox(Form__c form) {
        String val;

        if(form.OKDFL__c) {
            val = 'OK';
        } else {
            val = 'BAD!';
        }
        return val;
    }

    @AuraEnabled
    public static String shippingAddressCheckbox(Form__c form) {
        String val;

        if(form.OKShipAddress__c) {
            val = 'OK';
        } else {
            val = 'BAD!';
        }
        return val;
    }

     /*************************************** Editable Information ***************************************/

    @AuraEnabled
    public static String notes(Form__c form) {
        return form.Note__c;
    }

    @AuraEnabled
    public static String specialInstructions(Form__c form) {
        return form.Confirmation__r.SpecialInstructions__c;
    }
    
    @AuraEnabled
    public static void updateSpecialInstructions(String confId, String specIn) {
        Confirmation__c conf = [SELECT SpecialInstructions__c FROM Confirmation__c WHERE Id =: confId];
        conf.SpecialInstructions__c = specIn;
        update conf;
    }
    
    @AuraEnabled
    public static void updateReceivingNotes(String formId, String specIn) {
        Form__c form = [SELECT Note__c FROM Form__c WHERE Id =: formId];
        form.Note__c = specIn;
        update form;
    }

    /*************************************** Modal ***************************************/

    @AuraEnabled
    public static void resolved(String formId, String specIn) {
        Form__c form = [SELECT Greensheet__c, Note__c FROM Form__c WHERE Id =: formId];
        form.Greensheet__c = False;
        form.Note__c = specIn;
        update form;
    }
}