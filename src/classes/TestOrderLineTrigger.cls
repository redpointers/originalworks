@isTest
private class TestOrderLineTrigger
{
	static Confirmation__c confirm;
	static Form__c form;
	static ConfirmationProduct__c confirmProd;

	static void setup() 
	{
		UnitTestDataFactory.insertVariables();
		UnitTestDataFactory.insertShipDays();

		Variables__c var = Variables__c.getOrgDefaults();
		String defSeason = var.Default_Season__c;
		
		Account acct = UnitTestDataFactory.inflateAccount('test Account');
		insert acct;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason(defSeason);
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		insert confirm;

	}

	@isTest
	static void testInsert()
	{
		Order__c order = new Order__c();
		order.Name = '1234';
		order.Code__c = 'ON';
		order.Description__c ='test';
		order.OrderNumber__c = 12344;
		order.Orientation__c ='V';
		order.PaymentMethod__c ='O';
		order.Quantity__c =1;
		order.RetailSalesTax__c=0;
		order.Shipping__c=0;
		order.Subtotal__c=10;
		order.WholeSale__c=5;
		order.Tax__c=0;
		order.DFLPrice__c=5;

		insert order;

		OrderLineTriggerHandler.sendRecordsToHeroku(new List<Id>{order.Id});

	}
}