public with sharing class ShipDateSchedulerController {
	/*
     * author: Redpoint
     * purpose: The Ship Date Scheduler Controller provides SOQL queries that pulls
     *              relevant form information to be used by the Ship Date Scheduler component.
     * date: 4/10/2017
     * notes: The button that leads to the Change Log does not currently work. Waiting on OW to send the
     *      Change Log sample report so it can be re-created and attached to the Scheduler component.
     *      4/18/2017: OW has determined that they do not need the Change Log at this time and has stated 
     *      that work should be stopped on creating the Log and focused elsewhere.
    */

    /*************************************** Initialize ***************************************/
    @AuraEnabled
    public static List<Form__c> listLeftForms(String filterDate) {

        Date filter = Date.valueOf(filterDate);

    	return [SELECT Id, Confirmation__r.RecordType.Name, Confirmation__r.Projection__c, Confirmation__r.FTActual__c, 
                    Confirmation__r.OWYReceivesOrdersArtwork__c, Confirmation__r.OWYSendsOrdersArtwork__c, Confirmation__r.Status__c, 
                    Confirmation__r.Season__r.Name, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.FTType__c, 
                    Confirmation__r.Account__r.BillingState, Confirmation__r.LastSchoolDate__c, Confirmation__r.ResumeDate__c, 
                    Confirmation__r.ClosedDates__c,  ShipDays__c, ActualReceive__c, ActualShip__c 
    			FROM Form__c
    			WHERE (Confirmation__r.Status__c = 'Active') AND (FormType__c = 'B' OR FormType__c = 'D') AND
                      (FormSequence__c = 1) AND (ActualShip__c = NULL) AND (Confirmation__r.FTActual__c = NULL) AND
                      (Confirmation__r.OWYSendsOrdersArtwork__c =: filter)
                ORDER BY Confirmation__r.OWYSendsOrdersArtwork__c, Confirmation__r.Account__r.SchoolCode__c];
    }

    @AuraEnabled
    public static List<Form__c> listRightForms(String filterDate) {

        Date filter = Date.valueOf(filterDate);

        return [SELECT Id, Confirmation__r.RecordType.Name, Confirmation__r.Projection__c, Confirmation__r.FTActual__c, 
                    Confirmation__r.OWYReceivesOrdersArtwork__c, Confirmation__r.OWYSendsOrdersArtwork__c, Confirmation__r.Status__c, 
                    Confirmation__r.Season__r.Name, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.FTType__c,
                    Confirmation__r.Account__r.BillingState, Confirmation__r.LastSchoolDate__c, Confirmation__r.ResumeDate__c, 
                    Confirmation__r.ClosedDates__c,  ShipDays__c, ActualReceive__c, ActualShip__c 
                FROM Form__c
                WHERE (Confirmation__r.Status__c = 'Active') AND (FormType__c = 'B' OR FormType__c = 'D') AND
                      (FormSequence__c = 1) AND (ActualShip__c = NULL) AND (Confirmation__r.FTActual__c =: filter)
                ORDER BY Confirmation__r.OWYSendsOrdersArtwork__c, Confirmation__r.Account__r.SchoolCode__c];
    }
    /*************************************** Reports ***************************************/

    @AuraEnabled
    public static String dailyRevenueReport() {
        Report report = [SELECT Id, Name FROM Report WHERE Name = 'Daily Revenue' LIMIT 1];
        String instance = URL.getSalesforceBaseURL().toExternalForm();

        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return instance + '/one/one.app#/sObject/' + report.Id + '/view?a:t=1491592830779&fv0=' + season;
    }

    /*
    @AuraEnabled
    public static String changeLogReport() {
        Report report = [SELECT Id, Name FROM Report WHERE Name = 'Ship Date Change Log' LIMIT 1];
        String instance = URL.getSalesforceBaseURL().toExternalForm();

        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return instance + '/one/one.app#/sObject/' + report.Id + '/view?a:t=1491592830779&fv0=' + season; 
    }*/

    /*************************************** Table Information ***************************************/    

    @AuraEnabled
    public static Integer rowCount(List<Form__c> forms) {
    	return forms.size();
    }

    @AuraEnabled
    public static Form__c selectedInformation(List<Form__c> forms, String selected) {
        Form__c info = new Form__c();
        Id formId = Id.valueOf(selected);

        for(Form__c f : forms) {
            if(formId == f.Id) {
                info = f;
            }
        }
        return info;
    }

    /*************************************** Modify Form Data ***************************************/

    @AuraEnabled
    public static void updateForm(List<Form__c> forms, String stringId, String newDate) {
        Form__c form = new Form__c();
        Id formId = Id.valueOf(stringId);
        Date changedDate = Date.valueOf(newDate);
        
        for(Form__c f : forms) {
            if(formId == f.Id) {
                form = f;
            }
        }

        if(form.Confirmation__r.OWYSendsOrdersArtwork__c != changedDate) {
            Confirmation__c confirm = [SELECT Id, FTActual__c, FTActualPrevious__c, FTType__c, FTStatus__c FROM Confirmation__c WHERE Id =: form.Confirmation__c];
            
            if(confirm.FTActual__c != null) {
                confirm.FTActualPrevious__c = confirm.FTActual__c;
            }

            confirm.FTActual__c = changedDate;
            confirm.FTStatus__c = 'Approved';

            if(confirm.FTType__c == null || confirm.FTType__c != 'Promised') {
                confirm.FTType__c = 'Internal';
            }
            
            update confirm;
        }
    }  
}