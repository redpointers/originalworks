@isTest
private class TestShipDateSchedulerController {
	/*
     * author: Redpoint
     * purpose: Unit tests for the Ship Date Scheduler Controller.
     * date: 4/18/2017
     * notes: 100% code coverage was achieved on 4/18.
     *		The button that leads to the Change Log does not currently work. Waiting on OW to send the
     *		Change Log sample report so it can be re-created and attached to the Scheduler component.
    */

    static Confirmation__c confirm = new Confirmation__c();
    static Confirmation__c confirm2 = new Confirmation__c();
    static List<Form__c> flist = new List<Form__c>();
	static Form__c form = new Form__c();
	static Form__c form2 = new Form__c();
	static Form__c form3 = new Form__c();
	static Form__c form4 = new Form__c();

	static void setup() {

		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP60';
        upsert var;
		
		Account acct = new Account();
		acct.Name = 'Unit Test Account';
		insert acct;

		Account acct2 = new Account();
		acct2.Name = 'Unit Test Account 2';
		insert acct2;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason('SP60');
		insert season;

		Season__c season2 = UnitTestDataFactory.inflateSeason('SP61');
		insert season2;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		confirm.Status__c = 'Active';
		confirm.OWYSendsOrdersArtwork__c = Date.newInstance(1960, 01, 01);
		insert confirm;

		confirm2 = UnitTestDataFactory.inflateConfirmation(acct2, season2, ct);
		confirm2.Status__c = 'Active';
		confirm2.OWYSendsOrdersArtwork__c = Date.newInstance(1961, 01, 01);
		confirm2.FTActual__c = Date.newInstance(1961, 03, 10);
		insert confirm2;

		form = UnitTestDataFactory.inflateForm(confirm);
		form.FormType__c = 'A';
		form.FormSequence__c = 1;
		insert form;

		form2 = UnitTestDataFactory.inflateForm(confirm);
		form2.FormType__c = 'B';
		form2.FormSequence__c = 1;
		form2.ActualShip__c = null;
		insert form2;

		form3 = UnitTestDataFactory.inflateForm(confirm2);
		form3.FormType__c = 'E';
		form3.FormSequence__c = 1;
		insert form3;

		form4 = UnitTestDataFactory.inflateForm(confirm2);
		form4.FormType__c = 'D';
		form4.FormSequence__c = 1;
		form4.ActualShip__c = null;
		insert form4;

		flist.add(form);
		flist.add(form2);
		flist.add(form3);
		flist.add(form4);
	}

	static Testmethod void testListLeftForms() {
		setup();
		
		List<Form__c> test = ShipDateSchedulerController.listLeftForms('1960-01-01');
		System.assertEquals(1, test.size());
	}

	static Testmethod void testListRightForms() {
		setup();
		
		List<Form__c> test = ShipDateSchedulerController.listRightForms('1961-03-10');
		System.assertEquals(2, test.size());
		//System.assertEquals(1, test.size());//Data migration value
	}

	@isTest(SeeAllData='true')
	static void testDailyRevenueReport() {
		setup();
		
		String instance = URL.getSalesforceBaseURL().toExternalForm();
		String test = ShipDateSchedulerController.dailyRevenueReport();

		System.assert(test.endsWith('SP60'));
		System.assert(test.contains(instance));
	}

	static Testmethod void testRowCount() {
		setup();

		Integer test = ShipDateSchedulerController.rowCount(flist);
		System.assertEquals(4, test);
	}

	static Testmethod void testSelectedInformation() {
		setup();

		String sId = String.valueOf(form.Id);

		Form__c test = ShipDateSchedulerController.selectedInformation(flist, sId);
		System.assertEquals(form.Id, test.Id);		
	}

	static Testmethod void testUpdateForm() {
		setup();

		String sId = String.valueOf(form.Id);

		ShipDateSchedulerController.updateForm(flist, sId, '1961-04-18');

		Confirmation__c afterUpdate = [SELECT FTActual__c, FTStatus__c, FTType__c
								FROM Confirmation__c 
								WHERE Id =: form.Confirmation__c];

		System.assertEquals(Date.newInstance(1961, 04, 18), afterUpdate.FTActual__c);
		System.assertEquals('Approved', afterUpdate.FTStatus__c);
		System.assertEquals('Internal', afterUpdate.FTType__c);
	}
}