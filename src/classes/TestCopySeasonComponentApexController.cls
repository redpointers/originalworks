@isTest
private class TestCopySeasonComponentApexController
{
	static Season__c currentSeason;

	static void setup() 
	{
		currentSeason = UnitTestDataFactory.inflateSeason('SP17');
		insert currentSeason;

		OWProduct__c prod = UnitTestDataFactory.inflateOWProduct();
		insert prod;

		SeasonProduct__c sp = UnitTestDataFactory.inflateSeasonProduct(prod, currentSeason);
		insert sp;
	}

	@isTest
	static void testGetCurrent()
	{
		setup();

		Season__c season = CopySeasonComponentApexController.getCurrentSeason(currentSeason.Id); 
		System.assert(season!=null);
	}

	@isTest
	static void testHasProducts()
	{
		setup();

		Boolean hasProds = CopySeasonComponentApexController.getHasProducts(currentSeason.Id);
		System.assert(hasProds);
	}

	@isTest
	static void testGetOtherSeasons()
	{
		setup();

		Season__c sea1 = UnitTestDataFactory.inflateSeason('SP16');
		insert sea1;

		Season__c sea2 = UnitTestDataFactory.inflateSeason('SP15');
		insert sea2;

		List<Season__c> otherSeasons = CopySeasonComponentApexController.getSeasons(currentSeason.Id);
		System.assert(otherSeasons.size()==2);
	}

	@isTest
	static void testCopy()
	{
		setup();

		Season__c sea1 = UnitTestDataFactory.inflateSeason('SP16');
		insert sea1;

		String idStr = CopySeasonComponentApexController.copyProducts(sea1.Id, currentSeason.Id);
		
		List<SeasonProduct__c> sProds = [select Id from SeasonProduct__c where Season__c =: sea1.Id];
		System.assert(sProds.size()==1);
	}
}