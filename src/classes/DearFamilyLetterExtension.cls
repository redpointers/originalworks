public class DearFamilyLetterExtension {
	/*
	 * author: Redpoint
	 * date: 1/20/2017
	 * purpose: Pulls the data necessary to create the Dear Family Letters for the 
	 * 				OP, SBYB, and Gallery confirmation records. Also contains the methods 
	 * 				that take the DFL, renders it as a PDF and saves it as an attachment to 
	 * 				the confirmation record.
	 * notes: 3/24/2017, added three methods getEnglishDFL, getSpanishDFL, and getEnglishSpanishDFL 
     *          that now saves each VF page type (Spanish/English) on a button click from
     *          the DearFamilyLetterGenerator page. 
	*/
    public Confirmation__c conf;
    public Decimal phone {get;set;}
    public Map<Id, OWProduct__c> conProdToOWProdMap{get;set;}
        
    public DearFamilyLetterExtension(ApexPages.StandardController stdcontroller) {
        this.conf = (Confirmation__c)stdcontroller.getRecord();
    }

    public Confirmation__c getInfo() {
        return [SELECT Account__r.Name, ChecksPayable__c, SendLookWhatsComingLetterHome__c,
                        Designation__c, Account__r.SchoolCode__c 
                FROM Confirmation__c WHERE Id =: conf.Id];
    }
    
    public List<ConfirmationProduct__c> getConfirmationProducts() {
        Variables__c var = Variables__c.getOrgDefaults();

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getConfirmationProductsForConfirm?confirmid='+conf.Id);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock(new List<Id>{conf.Id});
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'There was a problem getting products associated with this confirmation.'));
            }
        }

        List<ConfirmationProduct__c> lines = parseJSONCPBody(res.getBody());

        if(lines.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'There are no products associated with this confirmation.'));
        }

        return lines;
        
    }
    
    public List<ConfirmationProduct__c> getConfirmationProductsSpanish() {
        List<ConfirmationProduct__c> conprod = getConfirmationProducts();
        List<Id> owProdIds = new List<Id>();
        for(ConfirmationProduct__c cp : conprod)
            owProdIds.add(cp.OWProduct__c);
        
        List<OWProduct__c> owprod = [select Id, Name, NameESP__c, DescriptionESP__c from OWProduct__c where Id in : owProdIds];

        conProdToOWProdMap = new Map<Id, OWProduct__c>(); 

        for(ConfirmationProduct__c cp : conprod)
        {
            for(OWProduct__c op : owprod)
            {
                if(op.Id == cp.OWProduct__c)
                {
                    conProdToOWProdMap.put(cp.Id, op);
                }
            }
        }

        return conprod;
        //List<ConfirmationProduct__c> conprod = new List<ConfirmationProduct__c>();
        //conprod = [SELECT OWProduct__r.NameESP__c,DFLAdjusted__c, OWProduct__r.DescriptionESP__c 
        //                FROM ConfirmationProduct__c
        //                WHERE ConfirmationProduct__c.Confirmation__c =: conf.Id
        //                ORDER BY PrintOrder__c ASC];
        //if(conprod.isEmpty()) {
        //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'There are no products associated with this confirmation.'));
        //}
        //return conprod;
    }
    
    public Contact getDFLCoord() {
        List<ConfirmationContact__c> confirmationcon = new List<ConfirmationContact__c>();
        Contact DFLCoord = new Contact();
        
        confirmationcon = [SELECT Role__c,Contact__c
                           				FROM ConfirmationContact__c
                          				WHERE ConfirmationContact__c.Confirmation__c =: conf.Id];
        
        for(ConfirmationContact__c cc : confirmationcon) {
            if(cc.Role__c == 'DFL Coord') {
                DFLCoord = [SELECT Id, Name, DFLEmail__c, DFLPhoneNumber__c  FROM Contact WHERE Id =: cc.Contact__c];
            }
        }

        //phone = DFLCoord.DFLPhoneNumber__c;

        return DFLCoord;
    }

    public String getFormatPhone() {
        String format = String.valueOf(phone);
        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        }
        return '';
    }

    public pageReference getEnglishDFL() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        Attachment attach = new Attachment();
        PageReference pdf = Page.DearFamilyLetterEnglishOP;
        pdf.getParameters().put('Id', recordId);

        Blob body;
        Id rid = Id.valueOf(recordId);

        Confirmation__c confirmation = [SELECT Account__r.SchoolCode__c, RecordType.Name 
                                            FROM Confirmation__c 
                                            WHERE Confirmation__c.Id =: rid];

        String attachName = confirmation.Account__r.SchoolCode__c + ' ' + confirmation.RecordType.Name + ' ' + 'DFL.pdf'; 
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + conf.Id);
        return recordpage;
    }

    public pageReference getEnglishDFLSBYB() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        Attachment attach = new Attachment();
        PageReference pdf = Page.DearFamilyLetterEnglishSBYB;
        pdf.getParameters().put('Id', recordId);

        Blob body;
        Id rid = Id.valueOf(recordId);

        Confirmation__c confirmation = [SELECT Account__r.SchoolCode__c, RecordType.Name 
                                            FROM Confirmation__c 
                                            WHERE Confirmation__c.Id =: rid];

        String attachName = confirmation.Account__r.SchoolCode__c + ' ' + confirmation.RecordType.Name + ' ' + 'DFL.pdf'; 
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + conf.Id);
        return recordpage;
    }

    public PageReference getSpanishDFL() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        
        Attachment attach = new Attachment();
        PageReference pdf = Page.DearFamilyLetterSpanishOP;
        pdf.getParameters().put('Id', recordId);

        Blob body;
        Id rid = Id.valueOf(recordId);

        Confirmation__c confirmation = [SELECT Account__r.SchoolCode__c, RecordType.Name 
                                            FROM Confirmation__c 
                                            WHERE Confirmation__c.Id =: rid];

        String attachName = confirmation.Account__r.SchoolCode__c + ' ' + confirmation.RecordType.Name + ' ' + 'DFL ESP.pdf'; 
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + conf.Id);
        return recordpage;
    }

    public PageReference getSpanishDFLSBYB() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        
        Attachment attach = new Attachment();
        PageReference pdf = Page.DearFamilyLetterSpanishSBYB;
        pdf.getParameters().put('Id', recordId);

        Blob body;
        Id rid = Id.valueOf(recordId);

        Confirmation__c confirmation = [SELECT Account__r.SchoolCode__c, RecordType.Name 
                                            FROM Confirmation__c 
                                            WHERE Confirmation__c.Id =: rid];

        String attachName = confirmation.Account__r.SchoolCode__c + ' ' + confirmation.RecordType.Name + ' ' + 'DFL ESP.pdf'; 
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + conf.Id);
        return recordpage;
    }

    public PageReference getDFLSBYBOP() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        
        Attachment attach = new Attachment();
        PageReference pdf = Page.DearFamilyLetterEnglishSBYBOP;
        pdf.getParameters().put('Id', recordId);

        String attachName;
        Blob body;
        Id rid = Id.valueOf(recordId);

        Confirmation__c confirmation = [SELECT Account__r.SchoolCode__c, RecordType.Name 
                                            FROM Confirmation__c 
                                            WHERE Confirmation__c.Id =: rid];

        if(confirmation.RecordType.Name == 'SBYB') {
            attachName = confirmation.Account__r.SchoolCode__c + ' ' + confirmation.RecordType.Name + ' OP ' + ' ' + 'DFL.pdf'; 
        } else {
            attachName = confirmation.Account__r.SchoolCode__c + ' SS ' + confirmation.RecordType.Name + ' ' + 'DFL.pdf'; 
        }
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + conf.Id);
        return recordpage;
    }

    public PageReference getGalleryForm() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        
        Attachment attach = new Attachment();
        PageReference pdf = Page.OnlineGalleryOrderForm;
        pdf.getParameters().put('Id', recordId);

        Id rid = Id.valueOf(recordId);
        Blob body;
        String attachName = 'Online Gallery Order Form.pdf'; 
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + conf.Id);
        return recordpage;
    }

    public PageReference getEnglishSpanishDFLs() {
        getEnglishDFL();
        
        return getSpanishDFL();
    }

    public static List<ConfirmationProduct__c> parseJSONCPBody(String body)
    {
        JSONParser parser = JSON.createParser(body);

        List<ConfirmationProduct__c> lines = new List<ConfirmationProduct__c>();
        while (parser.nextToken() != null) 
        {
            // Start at the array of lines.
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
            {
                while (parser.nextToken() != null) 
                {
                    // Advance to the start object marker to find next line object.
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                    {
                        ConfirmationProduct__c line = (ConfirmationProduct__c)parser.readValueAs(ConfirmationProduct__c.class);
                        lines.add(line);
                    }
                }
            }
        }

        return lines;
    }
}