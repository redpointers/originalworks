public with sharing class ReportsPageController {
	/*
     * author: Redpoint
     * purpose: The apex class for the Reports Page lightning component. Each method calls a 
     *          standard or VF page report with the necessary parameters and returns a string
     *          URL that the Reports Page helper class will redirect to.
     * date: 4/18/2017
     * notes:
    */

     @AuraEnabled
    public static List<Season__c> getSeasons() {
        List<Season__c> seasons = [SELECT Id,Name, StartDate__c, EndDate__c FROM Season__c WHERE (NOT Name LIKE 'Demo%') AND (NOT Name LIKE 'test%') ORDER BY StartDate__c DESC];
        
        return seasons;
    }

    @AuraEnabled
    public static DateTime defaultDate() 
    {
        Datetime current = System.now();
 
        Date currDate = current.date();
        Time currTime = current.time();
 
        Datetime local = Datetime.newinstance(currDate,currTime);
        return local;
    }

    @AuraEnabled
    public static String dailyRevenueReport() {
        Report report = [SELECT Id, Name FROM Report WHERE Name = 'Daily Revenue' LIMIT 1];
        String instance = URL.getSalesforceBaseURL().toExternalForm();

        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return instance + '/one/one.app#/sObject/' + report.Id + '/view?a:t=1491592830779&fv0=' + season;
    }

    @AuraEnabled
    public static String itemCountReport(String seasonId) {
        PageReference item = new PageReference('/apex/ItemCountPDF');
        
        item.getParameters().put('id',seasonId);
    
        return item.getUrl();
    }

    @AuraEnabled
    public static String perCapReport(String seasonName) {
        PageReference percap = new PageReference('/apex/PerCapReport');
        
        percap.getParameters().put('season',seasonName);

        return percap.getUrl();
    }

    @AuraEnabled
    public static String leadSourceReport() {
        return '/apex/LeadSourceListReport';
    }

    @AuraEnabled
    public static String receivingReport(String start, String endd) {
        String s = formatDateRanges(Date.valueOf(start));
        String e = formatDateRanges(Date.valueOf(endd));
        
        PageReference receiving = new PageReference('/apex/ReceivingReport');

        receiving.getParameters().put('startrange',s);
        receiving.getParameters().put('endrange',e);

        return receiving.getUrl();
    }

    @AuraEnabled
    public static String salesAnalysisReport(String seasonName) {
        PageReference sales = new PageReference('/apex/SalesAnalysis');

        sales.getParameters().put('season',seasonName);

        return sales.getUrl();
    }

    @AuraEnabled
    public static String seasonForecastDetailReport(String seasonName, String cStatus) {
        PageReference forecast = new PageReference('/apex/SeasonForecastDetail');
        
        forecast.getParameters().put('season', seasonName);
        forecast.getParameters().put('status', cStatus);

        return forecast.getUrl();
    }

    @AuraEnabled
    public static String seasonForecastHQMGRDetailReport(String seasonName, String cStatus) {
        PageReference forecast = new PageReference('/apex/SeasonForecastHQMGRDetail');
        
        forecast.getParameters().put('season', seasonName);
        forecast.getParameters().put('status', cStatus);

        return forecast.getUrl();
    }

    @AuraEnabled
    public static String seasonForecastHQMGRSummaryReport(String seasonName, String cStatus) {
        PageReference forecast = new PageReference('/apex/SeasonForecastHQMGRSummary');
        
        forecast.getParameters().put('season', seasonName);
        forecast.getParameters().put('status', cStatus);

        return forecast.getUrl();
    }

    @AuraEnabled
    public static String seasonForecastSummaryReport(String seasonName, String cStatus) {
        PageReference forecast = new PageReference('/apex/SeasonForecastSummary');
        
        forecast.getParameters().put('season', seasonName);
        forecast.getParameters().put('status', cStatus);

        return forecast.getUrl();
    }

    @AuraEnabled
    public static String shipDateChangeLogReport() {
        Report report = [SELECT Id, Name FROM Report WHERE Name = 'Ship Date Change Log' LIMIT 1];
        String instance = URL.getSalesforceBaseURL().toExternalForm();

        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        return instance + '/one/one.app#/sObject/' + report.Id + '/view?a:t=1491592830779&fv0=' + season;
    }

    @AuraEnabled
    public static String shippingReport(String startDate, String endDate) {
        String s = formatDateRanges(Date.valueOf(startDate));
        String e = formatDateRanges(Date.valueOf(endDate));

        PageReference shipping = new PageReference('/apex/ShippingReport');
        
        shipping.getParameters().put('startrange',s);
        shipping.getParameters().put('endrange',e);

        return shipping.getUrl();
    }

    @AuraEnabled
    public static String wipReport(String seasonId, String cDate) {
        PageReference wip = new PageReference('/apex/WIPReportPDF');
        String formatted = cDate.replace('T', ' ');
        Integer index = formatted.indexOf('.');
        formatted = formatted.substring(0, index-1);
        
        wip.getParameters().put('seasonid',seasonId);
        wip.getParameters().put('date', formatted);
        
        return wip.getUrl();
    }

    public static String formatDateRanges(Date range) {
        String formatted = '';

        formatted = String.valueOf(range.month()) + '/' + String.valueOf(range.day()) + '/' + String.valueOf(range.year());

        return formatted;
    }
}