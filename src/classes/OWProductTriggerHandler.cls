public class OWProductTriggerHandler {

	public static void handleInsert(List<OWProduct__c> products) {

		List<OWProduct__c> productList = [SELECT Name FROM OWProduct__c];
		Set<String> productNames = new Set<String>();

		for(OWProduct__c pn : productList) {
			if(!productNames.contains(pn.Name)) {
				productNames.add(pn.Name);
			}
		}

		for(OWProduct__c p : products) {
			if(productNames.contains(p.Name)) {
				p.addError('Product already exists.');
			}
		}
	}

	public static void handleDelete(List<OWProduct__c> products) {

		for(OWProduct__c p : products) {
			p.addError('Record cannot be deleted.');
		}
	}
}