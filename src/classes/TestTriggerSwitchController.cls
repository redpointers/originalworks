@isTest
private class TestTriggerSwitchController {
    /*
	* author: Redpoint
	* date: 6/3/2017
	* purpose: Unit tests Trigger Switch Controller
	* note: 100% code coverage as of 6/2
	*/

	static void setup() {
		TriggerSwitches__c tswitch = TriggerSwitches__c.getOrgDefaults();

		tswitch.OWProductAll__c = False;
		tswitch.OWProductBeforeInsert__c = False;
		tswitch.OWProductBeforeDelete__c = False;

		upsert tswitch;
	}

    static Testmethod void testAccountSetting() {
        setup();

        TriggerSwitches__c testSwitch = TriggerSwitchController.accountSetting();
        System.assert(!testSwitch.OWProductAll__c);
        System.assert(!testSwitch.OWProductBeforeInsert__c);
        System.assert(!testSwitch.OWProductBeforeDelete__c);
    }
    
    static Testmethod void testOWProductSwitchAllOn() {
        setup();

        TriggerSwitches__c testSwitch = TriggerSwitchController.setOWProductSwitch(1);
     	System.assert(testSwitch.OWProductAll__c);
        System.assert(testSwitch.OWProductBeforeInsert__c);
        System.assert(testSwitch.OWProductBeforeDelete__c);   
    }

    static Testmethod void testOWProductSwitchAllOff() {
        
        TriggerSwitches__c tswitch = TriggerSwitches__c.getOrgDefaults();

		tswitch.OWProductAll__c = True;
		tswitch.OWProductBeforeInsert__c = True;
		tswitch.OWProductBeforeDelete__c = True;

		upsert tswitch;

        TriggerSwitches__c testSwitch = TriggerSwitchController.setOWProductSwitch(1);
     	System.assert(!testSwitch.OWProductAll__c);
        System.assert(!testSwitch.OWProductBeforeInsert__c);
        System.assert(!testSwitch.OWProductBeforeDelete__c);   
    }

    static Testmethod void testOWProductSwitchInsert() {
    	setup();

        TriggerSwitches__c testSwitch = TriggerSwitchController.setOWProductSwitch(2);
        System.assert(!testSwitch.OWProductAll__c);
        System.assert(testSwitch.OWProductBeforeInsert__c);
        System.assert(!testSwitch.OWProductBeforeDelete__c);
    }

    static Testmethod void testOWProductSwitchDelete() {
    	setup();

        TriggerSwitches__c testSwitch = TriggerSwitchController.setOWProductSwitch(3);
        System.assert(!testSwitch.OWProductAll__c);
        System.assert(!testSwitch.OWProductBeforeInsert__c);
        System.assert(testSwitch.OWProductBeforeDelete__c);
    }
    
}