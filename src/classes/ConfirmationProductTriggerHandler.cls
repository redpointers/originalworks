public with sharing class ConfirmationProductTriggerHandler 
{
	public static void handleAfterInsert(Map<Id, ConfirmationProduct__c> prodMap) 
	{
		ConfirmationProductTriggerHandler.sendRecordsToHeroku(prodMap.keySet(), 'addConfirmationProducts');
	}

	public static void handleAfterUpdate(Map<Id, ConfirmationProduct__c> prodMap) 
	{
		ConfirmationProductTriggerHandler.sendRecordsToHeroku(prodMap.keySet(), 'updateConfirmationProducts');
	}

	@future(callout=true)
	public static void sendRecordsToHeroku(Set<Id> lineIds, String endPt)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		List<ConfirmationProduct__c> lines = [select Id, Name, AdjustPriceToParent__c, AdjustPriceToSchool__c, Code__c, Code_MAS90__c, 
			Confirmation__c, CreatedById, CreatedDate, Description__c, DFLAdjusted__c, DFLPrice__c, DFLTax__c, LastModifiedById,
			LastModifiedDate, OWProduct__c, Price_to_Parent__c, Price_to_School__c, PrintOrder__c, ProductName__c, SeasonProduct_ID__c, Shortcut__c, 
			TaxParent__c, TaxSchool__c, TestProduct__c from ConfirmationProduct__c where Id in : lineIds];

		Http h = new Http();
    	HttpRequest req = new HttpRequest();
    	req.setEndpoint(var.Database_Endpoint__c+endPt);
    	req.setMethod('POST');
    	req.setHeader('Content-Type', 'application/json');
    	req.setBody(JSON.serialize(lines));
        System.debug('**** BODY: '+JSON.serialize(lines));

    	String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

    	HttpResponse res = new HttpResponse();

    	if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock();
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
	    {
	        if(res.getStatus() == 'Success' || res.getStatus() == 'OK')
	        {
	        	// Get rid of the ConfirmationProduct__c records. No need to keep them
	        	delete lines;
	        }
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	}

	/* This is not call by anything. Until we need to delete from the Heroku DB, leave this commented out
	@future(callout=true)
	public static void deleteRecordsFromHeroku(Set<Id> lineIds)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		String confirmids = '';
		for(Id lid : lineIds)
		{
			confirmids = confirmids + lid + ',';
		}
		confirmids = confirmids.substring(0,confirmids.length()-1);

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'deleteConfirmationProducts?confirmids='+confirmids);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock();
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }
	} */
}