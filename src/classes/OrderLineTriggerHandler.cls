public with sharing class OrderLineTriggerHandler 
{
	public static void handleAfterInsert(List<Order__c> orders) 
	{
		List<Id> ids = new List<Id>();
		for(Order__c line : orders)
			ids.add(line.Id);

		OrderLineTriggerHandler.sendRecordsToHeroku(ids);
	}

	@future(callout=true)
	public static void sendRecordsToHeroku(List<Id> lineIds)
	{
		Variables__c var = Variables__c.getOrgDefaults();
		String defSeason = var.Default_Season__c;
		Season__c currentSeason = null;

		if(!Test.isRunningTest())
			currentSeason = [select Id, Name from Season__c where Name =: defSeason];
		else
			currentSeason = UnitTestDataFactory.inflateSeason(defSeason);

		List<Order__c> lines = [select Id, Name, Code__c, Description__c, OrderNumber__c, Orientation__c, PaymentMethod__c, Quantity__c, Form__c, Form__r.Confirmation__r.Season__c,
			RetailSalesTax__c, Shipping__c, Subtotal__c, WholeSale__c, Tax__c, LastModifiedDate, LastModifiedById, CreatedDate, CreatedById, 
			Form__r.Carrier__c, DFLPrice__c from Order__c where Id in : lineIds];

		List<OrderLines> orderLines = new List<OrderLines>();
		for(Order__c line : lines)
		{
			OrderLines order = new OrderLines();
			order.hv = line.Orientation__c;
			order.paymt = line.PaymentMethod__c;
			order.qty = (Integer)line.Quantity__c;
			order.wholesale = line.WholeSale__c;
			order.total = (line.DFLPrice__c * line.Quantity__c);
			order.price = line.DFLPrice__c;
			order.suffix = line.Name;
			order.code = line.Code__c;
			order.description = line.Description__c;
			order.formId = line.Form__c;
			order.seasonid = line.Form__r.Confirmation__r.Season__c;
			if(order.seasonid == null)
			{
				order.seasonid = currentSeason.Id;
			}

			order.lastmoddate = line.LastModifiedDate;
			order.lastmodid = line.LastModifiedById;
        	order.shipping = line.Shipping__c;
        	order.shipvia = line.Form__r.Carrier__c;
        	order.discount = 0;
        	order.salestax = line.RetailSalesTax__c;
        	order.grandtotal = (order.price*order.qty)+order.discount+order.salestax+order.shipping;
			order.createddate = line.CreatedDate;
			order.createdid = line.CreatedById;
			
			orderLines.add(order);
		}

		

		Http h = new Http();
    	HttpRequest req = new HttpRequest();
    	req.setEndpoint(var.Database_Endpoint__c+'upsertRecords');
    	req.setMethod('POST');
    	req.setHeader('Content-Type', 'application/json');
    	req.setBody(JSON.serialize(orderLines));

    	String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

    	HttpResponse res = new HttpResponse();

    	if (Test.isRunningTest()) 
        {
            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock();
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
	    {
	        if(res.getStatus() == 'Success' || res.getStatus() == 'OK')
	        {
	        	// Get rid of the Order__c records. No need to keep them
	        	delete lines;
	        }
	    }
	    catch(Exception e)
	    {
	    	throw e;
	    }
	}
}