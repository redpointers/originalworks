public class SeasonForecastSummaryReportController {
	/*
     * author: Redpoint
     * date: 3/21/2017
     * purpose: Pulls the data from the Confirmation, Account, and Contact 
     *			object to create the Season Forecast Summary report and the Season Forecast Detail report. 
     * notes:
    */
    public List<Confirmation__c> confirms = new List<Confirmation__c>();
    public List<Confirmation__c> houseList = new List<Confirmation__c>();
    public List<Confirmation__c> nonHouseList = new List<Confirmation__c>();
    public Confirmation__c confirm = new Confirmation__c();
    public String season {get;set;}
    public String status {get;set;}

    public SeasonForecastSummaryReportController(ApexPages.StandardController stdcontroller) {
    	this.confirm = (Confirmation__c)stdcontroller.getRecord();
        this.season = ApexPages.currentPage().getParameters().get('season');
        this.status = ApexPages.currentPage().getParameters().get('status');
    }

    public void getqueryConfirmations() {
    	confirms = [SELECT Id, Account__r.Name, SeasonPopEstimate__c, Projection__c, SalesRep__r.Name, SalesRep__r.FirstName, 
                            SalesRep__r.LastName,RecordType.Name, SalesRep__r.RepId__c, SalesRep__r.HouseRep__c, 
                            Account__r.BillingState, Account__r.RepeatCustomer__c
    				FROM Confirmation__c
    				WHERE (Season__r.Name =: season) AND (SalesRep__c != null) AND (Status__c =: status)
    				ORDER BY SalesRep__r.LastName];
    }

    public void getseparateByHouse() {
        for(Confirmation__c c : confirms) {
            if(c.SalesRep__r.HouseRep__c == True) {
                houseList.add(c);
            } else {
                nonHouseList.add(c);
            }
        }
    }

    public Map<String, List<Confirmation__c>> groupByReps(List<Confirmation__c> toMap) {
        Map<String, List<Confirmation__c>> subLists = new Map<String, List<Confirmation__c>>();

    	for(Confirmation__c c : toMap) {
    		List<Confirmation__c> clist = subLists.get(c.SalesRep__r.Name);

    		if(clist == null) {
    			clist = new List<Confirmation__c>();
    			subLists.put(c.SalesRep__r.Name, clist);
    		}
    		clist.add(c);
    	}
    	return subLists;
    }

    public List<Double> totalCounts(List<Confirmation__c> conf) {
    	List<Double> sumList = new List<Double>();
    	Double popSum = 0;
    	Double sbybSum = 0;
        Double sssbybSum = 0;
    	Double opSum = 0;
    	Double gallerySum = 0;
        Double tileSum = 0;
    	Double totalSum = 0;
        Double percent = 0;

    	for(Confirmation__c c : conf) {
    		if(c.SeasonPopEstimate__c != null) {
    			popSum += c.SeasonPopEstimate__c;
    		}

    		if(c.RecordType.Name == 'SBYB' && c.Projection__c != null) {
    			sbybSum += c.Projection__c;
    		}

            if(c.RecordType.Name == 'SS SBYB' && c.Projection__c != null) {
                sssbybSum += c.Projection__c;
            }            

    		if(c.RecordType.Name == 'OP' && c.Projection__c != null) {
    			opSum += c.Projection__c;
    		}

    		if(c.RecordType.Name == 'Gallery' && c.Projection__c != null) {
    			gallerySum += c.Projection__c;
    		}

            if(c.RecordType.Name == 'Tile Wall' && c.Projection__c != null) {
                tileSum += c.Projection__c;
            }

    		if(c.Projection__c != null) {
    			totalSum += c.Projection__c;
    		}
    	}

        if(totalSum != 0) { 
            percent = (opSum / totalSum) * 100;
        }

    	sumList.add(popSum);
    	sumList.add(sbybSum);
        sumList.add(sssbybSum);
    	sumList.add(opSum);
    	sumList.add(gallerySum);
        sumList.add(tileSum);
    	sumList.add(totalSum);
        sumList.add(percent);

    	return sumList;
    }

    public Map<String, List<Confirmation__c>> sumByState(List<Confirmation__c> conf) {
    	Map<String, List<Confirmation__c>> summaries = new Map<String, List<Confirmation__c>>();

    	for(Confirmation__c c : conf) {
    		List<Confirmation__c> sumList = summaries.get(c.Account__r.BillingState);

    		if(sumList == null) {
    			sumList = new List<Confirmation__c>();
    			summaries.put(c.Account__r.BillingState, sumList);
    		}
    		sumList.add(c);
    	}
        return summaries;
    }

    public string houseImage(Boolean tof) {
        if(tof) {
            return 'Yes';
        }

        return 'No';
    }

    public List<Double> gethousePopulationTotal() {
        return totalCounts(houseList);
    }

    public List<Double> getnonHousePopulationTotal() {
        return totalCounts(nonHouseList);
    }

    public List<Double> getgrandTotals() {
        return totalCounts(confirms);
    }

    /*************************************** Summary Tables ***************************************/

    public String buildSummaryTables(List<Confirmation__c> repList) {

        Map<String, List<Confirmation__c>> subLists = groupByReps(repList);

        String tables = '';

        for(String s : subLists.keySet()) {
            
            List<Confirmation__c> summaryLists = subLists.get(s);
            Confirmation__c salesrep = summaryLists.get(0);
            List<Double> counts = totalCounts(summaryLists);
            Map<String, List<Confirmation__c>> states = sumByState(summaryLists);

            tables += '<table><thead>' +
                        '<tr>' +
                            '<th style="width:50%;" colspan="3">Sales Rep: ' + salesrep.SalesRep__r.LastName + ', ' + salesrep.SalesRep__r.FirstName + '</th>' +
                            '<th style="width:20%;text-align:center" colspan="3">House: ' + houseImage(salesrep.SalesRep__r.HouseRep__c) + '</th>' +
                            '<th style="width:30%; text-align:right;" colspan="3">REP ID: ' + salesrep.SalesRep__r.RepId__c + '</th>' +
                        '</tr>' +
                        '<tr>' +
                            '<th style="text-align:center" colspan="2"></th>' +
                            '<th style="text-align:center">Population</th>' +
                            '<th style="text-align:center">SBYB</th>' +
                            '<th style="text-align:center">SS SBYB</th>' +
                            '<th style="text-align:center">OP</th>' +
                            '<th style="text-align:center">Gallery</th>' +
                            '<th style="text-align:center">Tile Wall</th>' +
                            '<th style="text-align:center">Total</th>' +
                        '</tr>' +
                  '</thead><tbody>';
                
            for(String str : states.keySet()) {
                List<Confirmation__c> statesList = states.get(str);
                List<Double> proj = totalCounts(statesList);

                tables += '<tr>' +
                            '<td class="col1 bottomBorder" colspan="2">State Totals: ' + str + '</td>' +
                            '<td class="col2 bottomBorder">' + proj.get(0).format() + '</td>' +
                            '<td class="col3 bottomBorder">$' + proj.get(1).format() + '</td>' +
                            '<td class="col4 bottomBorder">$' + proj.get(2).format() + '</td>' +
                            '<td class="col5 bottomBorder">$' + proj.get(3).format() + '</td>' +
                            '<td class="col6 bottomBorder">$' + proj.get(4).format() + '</td>' +
                            '<td class="col7 bottomBorder">$' + proj.get(5).format() + '</td>' +
                            '<td class="col8 bottomBorder">$' + proj.get(6).format() + '</td>' +
                        '</tr>';
            }

            tables += '<tr>' + 
                        '<td class="col1" colspan="2">Rep Totals: </td>' +
                        '<td class="col2">' + counts.get(0).format() + '</td>' +
                        '<td class="col3">$' + counts.get(1).format() + '</td>' +
                        '<td class="col4">$' + counts.get(2).format() + '</td>' +
                        '<td class="col5">$' + counts.get(3).format() + '</td>' +
                        '<td class="col6">$' + counts.get(4).format() + '</td>' +
                        '<td class="col7">$' + counts.get(5).format() + '</td>' +
                        '<td class="col8">$' + counts.get(6).format() + '</td>' +
                      '</tr>' +
                    '</tbody></table>';   
        }
        return tables;
    }

    public String getHouseReps() {
        return buildSummaryTables(houseList);
    }

    public String getNonHouseReps() {
        return buildSummaryTables(nonHouseList);
    }

    /*************************************** Detail Tables**************************************/

    public String buildDetailTables(List<Confirmation__c> repList) {

        Map<String, List<Confirmation__c>> subLists = groupByReps(repList);

        String tables = '';

        for(String s : subLists.keySet()) {
            
            List<Confirmation__c> summaryLists = subLists.get(s);
            Confirmation__c salesrep = summaryLists.get(0);
            List<Double> counts = totalCounts(summaryLists);
            Map<String, List<Confirmation__c>> states = sumByState(summaryLists);

            tables += '<table><thead>' +
                '<tr>' +
                    '<th style="width:50%;" colspan="3">Sales Rep: ' + salesrep.SalesRep__r.LastName + ', ' + salesrep.SalesRep__r.FirstName + '</th>' +
                    '<th style="width:20%;text-align:center" colspan="3">House: ' + houseImage(salesrep.SalesRep__r.HouseRep__c) + '</th>' +
                    '<th style="width:30%;text-align:right;" colspan="3">REP ID: ' + salesrep.SalesRep__r.RepId__c + '</th>' +
                '</tr>' +
                '</thead><tbody>';
                
            for(String str : states.keySet()) {
                List<Confirmation__c> statesList = states.get(str);
                List<Double> proj = totalCounts(statesList);

                tables += 
                    '<tr>' +
                         '<th>State: </th>' +
                         '<th>' + str + '</th>' +
                    '</tr>' + 
                    '<tr>' +
                        '<th colspan="2">School Name</th>' +
                        '<th style="text-align:center">Population</th>' +
                        '<th style="text-align:center">SBYB</th>' +
                        '<th style="text-align:center">SS SBYB</th>' +
                        '<th style="text-align:center">Gallery</th>' +
                        '<th style="text-align:center">OP</th>' +
                        '<th style="text-align:center">Tile Wall</th>' +
                        '<th style="text-align:center">Total</th>' +
                        '<th style="text-align:center">Repeat</th>' +
                    '</tr>';    

                for(Confirmation__c cs : statesList) {

                List<Double> recType = showByRecordType(cs);
        
                tables += '<tr>' +
                    '<td class="col1 schoolColor" colspan="2">' + cs.Account__r.Name + '</td>' +
                    '<td class="col2 schoolColor">' + cs.SeasonPopEstimate__c + '</td>' +
                    '<td class="col3 schoolColor">$' + recType.get(1).format() + '</td>' +
                    '<td class="col4 schoolColor">$' + recType.get(2).format() + '</td>' +
                    '<td class="col5 schoolColor">$' + recType.get(3).format() + '</td>' +
                    '<td class="col6 schoolColor">$' + recType.get(0).format() + '</td>' +
                    '<td class="col7 schoolColor">$' + recType.get(4).format() + '</td>' +
                    '<td class="col8 schoolColor">$' + recType.get(5).format() + '</td>' +
                    '<td class="col9 schoolColor">' + repeatCustomer(cs.Account__r.RepeatCustomer__c) + '</td>' +
                    '</tr>';
                }

                tables += '<tr>' +
                    '<td class="col1half bottomBorder">State Totals:</td>' +
                    '<td class="col1half bottomBorder">' + str + '</td>' +
                    '<td class="col2 bottomBorder">' + proj.get(0).format() + '</td>' +
                    '<td class="col3 bottomBorder">$' + proj.get(1).format() + '</td>' +
                    '<td class="col4 bottomBorder">$' + proj.get(2).format() + '</td>' +
                    '<td class="col5 bottomBorder">$' + proj.get(4).format() + '</td>' +
                    '<td class="col6 bottomBorder">$' + proj.get(3).format() + '</td>' +
                    '<td class="col7 bottomBorder">$' + proj.get(5).format() + '</td>' +
                    '<td class="col8 bottomBorder">$' + proj.get(6).format() + '</td>' +
                    '<td class="col9 bottomBorder">' + '' + '</td>' +
                    '</tr>';
            }

            tables += '<tr>' + 
                '<td class="col1" colspan="2">Rep Totals: </td>' +
                '<td class="col2">' + counts.get(0).format() + '</td>' +
                '<td class="col3">$' + counts.get(1).format() + '</td>' +
                '<td class="col4">$' + counts.get(2).format() + '</td>' +
                '<td class="col5">$' + counts.get(4).format() + '</td>' +
                '<td class="col6">$' + counts.get(3).format() + '</td>' +
                '<td class="col7">$' + counts.get(5).format() + '</td>' +
                '<td class="col8">$' + counts.get(6).format() + '</td>' +
                '<td class="col9">' + '' + '</td>' +
                '</tr>' +
                '</tbody></table>';   
        }
        return tables;
    }
    
    public String getDetailHouseReps() {
        return buildDetailTables(houseList);
    }

    public String getDetailNonHouseReps() {
        return buildDetailTables(nonHouseList);
    }
    
    public String repeatCustomer(Boolean tof) {
        if(tof) {
            return 'X';
        }
        return '';
    }

    public List<Double> showByRecordType(Confirmation__c conf) {
        List<Double> projectionList = new List<Double>();
        Double op = 0;
        Double sbyb = 0;
        Double sssbyb = 0;
        Double gallery = 0;
        Double tile = 0;
        Double total = 0;
        
        if(conf.recordType.Name == 'OP' && conf.Projection__c != null) {
            op = conf.Projection__c;
        } else if(conf.recordType.Name == 'SBYB' && conf.Projection__c != null) {
            sbyb = conf.Projection__c;
        } else if (conf.recordType.Name == 'Gallery' && conf.Projection__c != null) {
            gallery = conf.Projection__c;
        } else if(conf.recordType.Name == 'Tile Wall' && conf.Projection__c != null) {
            tile = conf.Projection__c;
        } else if(conf.recordType.Name == 'SS SBYB' && conf.Projection__c != null) {
            sssbyb = conf.Projection__c;
        }

        total = op + sbyb + gallery + sssbyb + tile;

        projectionList.add(op);
        projectionList.add(sbyb);
        projectionList.add(sssbyb);
        projectionList.add(gallery);
        projectionList.add(tile);
        projectionList.add(total);

        return projectionList;
    }
}