public class SalesAnalysisReportController {
	/*
     * author: Redpoint
     * date: 3/16/2017
     * purpose: Pulls the data from the Confirmation object to create 
     *          the OWY Sales Analysis report. 
     * notes:
    */
    public List<Confirmation__c> confirmations = new List<Confirmation__c>();
    public Confirmation__c confirm = new Confirmation__c();
    public String season {get;set;}

    public SalesAnalysisReportController(ApexPages.StandardController stdcontroller) {
    	this.confirm = (Confirmation__c)stdcontroller.getRecord();
        this.season = ApexPages.currentPage().getParameters().get('season');
    }

    public List<Confirmation__c> getConfRecords() {
    	confirmations = [SELECT Account__r.SchoolCode__c, Account__r.BillingState, Account__r.Type__c,
				            Account__r.SubType__c, RecordType.Name, SeasonPopEstimate__c, ActualPop__c,
				            Projection__c, TotalRevenue__c, AverageOrder__c, LeadSource__c,
				            SalesRep__r.LastName, SubSalesRep__r.LastName, SalesRep__r.HQMGR__r.FirstName, 
                            SalesRep__r.HQMGR__r.LastName
		              FROM Confirmation__c
    		          WHERE Season__r.Name =: season
    		          ORDER BY Account__r.SchoolCode__c];
        return confirmations;
    }

     public Integer getTotalCount() {
        return confirmations.size();
    }
}