public class ReceivingReportController {
    /*
     * author: Redpoint
     * date: 2/13/2017
     * purpose: Pulls the data from the Form object based on user specified 
     *          start and end dates to create the Receiving report. 
     * notes:
    */
    public Map<Date, List<Form__c>> split = new Map<Date, List<Form__c>>();
    public List<Form__c> forms = new List<Form__c>();
    public Form__c form = new Form__c();
    public String start {get;set;}
    public String endd {get;set;}

    public ReceivingReportController(ApexPages.StandardController stdcontroller) {
        this.form = (Form__c)stdcontroller.getRecord();
    }

    public PageReference getReport() {
        PageReference receiving = new PageReference('/apex/ReceivingReport');
        receiving.getParameters().put('startrange',start);
        receiving.getParameters().put('endrange',endd);
        receiving.setRedirect(true);

        return receiving;
    }

    public void getFormRecords() {
        Date startRange = Date.parse(ApexPages.currentPage().getParameters().get('startrange'));
        Date endRange = Date.parse(ApexPages.currentPage().getParameters().get('endrange'));

        this.start = formatDateRanges(startRange);
        this.endd = formatDateRanges(endRange);
        
        forms = [SELECT Id, Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Account__r.Name,
                        Confirmation__r.Season__r.Name, Confirmation__r.RecordType.Name, 
                        Confirmation__r.Account__r.BillingState, Confirmation__r.OWYReceivesArtwork__c, 
                        Confirmation__r.OWYReceivesOrdersArtwork__c, ActualReceive__c,
                        EstimatedShip__c, FormType__c, FormSequence__c, Initials__c
                FROM Form__c
                WHERE (ActualReceive__c >: startRange) AND (ActualReceive__c <: endRange)
                ORDER BY ActualReceive__c];          
    }

    public void SubLists() {

        for (Form__c f : forms) {
            List<Form__c> flist = split.get(f.ActualReceive__c);
            
            if(flist == null) {
                flist = new List<Form__c>();
                split.put(f.ActualReceive__c, flist);
            }
            flist.add(f);
        }
    }

    public String getDynamicTables() {
        SubLists();

        String htmlBody = '';

        for(Date d : split.keySet()) {
            List<Form__c> subForm = split.get(d);
            htmlBody += '<tbody>';
            for(Form__c f : subForm) {
                htmlBody += '<tr>' + 
                            '<td>' + f.Confirmation__r.Account__r.SchoolCode__c + '</td>' + 
                            '<td>' + f.Confirmation__r.Account__r.Name + '</td>' +
                            '<td>' + formatDates(f.ActualReceive__c) + '</td>' + 
                            '<td>' + typeCheck(f) + '</td>' + 
                            '<td>' + formatDates(f.EstimatedShip__c) + '</td>' + 
                            '<td>' + f.Confirmation__r.Season__r.Name + '</td>' +
                            '<td>' + f.FormType__c + ' ' + f.FormSequence__c + '</td>' +
                            '<td>' + f.Confirmation__r.RecordType.Name + '</td>' + 
                            '<td>' + f.Confirmation__r.Account__r.BillingState + '</td>' +
                            '<td>' + displayValue(f.Initials__c) + '</td>' +
                            '</tr>';
            }
            htmlBody += '<tr>' + 
                        '<td class="separateTBody" colspan="10">Schools for ' + byDay(subForm) + ' = ' + subCount(subForm) +'</td>' + 
                        '</tr>' + 
                        '</tbody>';
        }
        return htmlBody;
    }

    public String displayValue(String s) {
        String ret;

        if(s == null) {
            ret = '';
        } else {
            ret = s;
        }

        return ret;
    }

    public String typeCheck(Form__c f) {
        String val;

        if(f.FormType__c == 'A') {
            val = formatDates(f.Confirmation__r.OWYReceivesArtwork__c);
        } else if(f.FormType__c == 'B' || f.FormType__c == 'D') {
            val = formatDates(f.Confirmation__r.OWYReceivesOrdersArtwork__c);
        } else
            val = '';

        return val;
    }

    public String byDay(List<Form__c> subForm) {
        Form__c firstForm = subForm.get(0);
        Date received = firstForm.ActualReceive__c;
        String day;
        day = received.month() + '/' + received.day() + '/' + received.year();
        return day;
    }

    public Integer subCount(List<Form__c> subForm) {
        Set<String> schools = new Set<String>();

        for(Form__c f : subForm) {
            if(!schools.contains(f.Confirmation__r.Account__r.SchoolCode__c)) {
                schools.add(f.Confirmation__r.Account__r.SchoolCode__c);
            } 
        }
        return schools.size();
    }

    public String formatDates(Date received) {
        String formatted = '';
        if(received != null) {
            formatted = String.valueOf(received.month()) + '/' + String.valueOf(received.day()) + '/' + String.valueOf(received.year());
        }
        return formatted;
    }

    public String formatDateRanges(Date range) {
        String formatted = '';

        formatted = String.valueOf(range.day()) + '-' + convertMonth(range.month()) + '-' + String.valueOf(range.year());

        return formatted;
    }

    public String convertMonth(Integer num) {
        String val;

        if(num == 1) {
            val = 'Jan';
        } else if(num == 2) {
            val = 'Feb';
        } else if(num == 3) {
            val = 'Mar';
        } else if(num == 4) {
            val = 'Apr';
        } else if(num == 5) {
            val = 'May';
        } else if(num == 6) {
            val = 'Jun';
        } else if(num == 7) {
            val = 'Jul';
        } else if(num == 8) {
            val = 'Aug';
        } else if(num == 9) {
            val = 'Sep';
        } else if(num == 10) {
            val = 'Oct';
        } else if(num == 11) {
            val = 'Nov';
        } else if(num == 12) {
            val = 'Dec';
        } 

        return val;
    }

    public Integer getTotalSchoolCount() {
        Set<String> schools = new Set<String>();

        for(Form__c f : forms) {
            if(!schools.contains(f.Confirmation__r.Account__r.SchoolCode__c)) {
                schools.add(f.Confirmation__r.Account__r.SchoolCode__c);
            } 
        }
        return schools.size();
    }
}