public class ConfirmationDocumentExtension {
	/*
	 * author: Redpoint
	 * date: 1/20/2017
	 * purpose: Pulls the necessary data from the Confirmation 
	 * 				and Account records to create the Confirmation document
	 * 				that is sent to the school for approval. Also contains methods
	 * 				that will save the document as a pdf attachment on the record.
	 * notes:
	*/
    public Confirmation__c confirmation;
    public String schoolPhone {get;set;}
    public String schoolFax {get;set;}
    public String coordPhone {get;set;}
    public String coordMobilePhone {get;set;}
    public String alternateCoordPhone {get;set;}
    public String artTeacherPhone {get;set;}
    public String repPhone {get;set;}
    public String dflPhone {get;set;}
    
    public ConfirmationDocumentExtension(ApexPages.StandardController stdcontroller) {
        this.confirmation = (Confirmation__c)stdcontroller.getRecord();
    }

    public Confirmation__c getInfo() {
        Confirmation__c confirm = new Confirmation__c();

        confirm = [SELECT RecordType.Name, Account__r.SchoolCode__c, Account__r.Name, Account__r.County__c, Account__r.BillingStreet, 
                        Account__r.BillingCity, Account__r.BillingState, Account__r.BillingPostalCode, Account__r.Phone,Account__r.Fax, 
                        Account__r.Type__c, Season__r.Name, ProgramMaterials__c, LastModifiedDate, DateOpened__c, SalesRep__r.Name,
                        SalesRep__r.Phone, SalesRep__r.Email, ChecksPayable__c, ActualPop__c, LastSchoolDate__c, ResumeDate__c, 
                        ClosedDates__c, OWYSendsProgramMaterials__c, SchoolReceivesProgramMaterials__c, ArtworkBegins__c,
                        SendLookWhatsComingLetterHome__c, SendArtworkPackagesHome__c, DeadlineForOrders__c,CoordinatorSendsArtworkToOWY__c,
                        OWYReceivesOrdersArtwork__c, OWYSendsOrdersArtwork__c, SendOrdersArtworkShippingFormToOWY__c, SchoolReceivesOrdersArtwork__c,
                        SchoolEmailsClassListForArtLabels__c, OWYReceivesArtwork__c, OWYSendsArtwork__c, SchoolReceivesArtwork__c,
                        SchoolSendsManualOrdersArtwork__c, OWYReceivesManualOrders__c, OWYSendsCompletedOrders__c, LateOrdersDue__c
                FROM Confirmation__c 
                WHERE Id =: confirmation.Id];

        schoolPhone = confirm.Account__r.Phone;
        schoolFax = confirm.Account__r.Fax;
        repPhone = confirm.SalesRep__r.Phone;

        return confirm;
    }

    public String getSchoolPhoneNumber() {
        String format = String.valueOf(schoolPhone);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }

    public String getSchoolFaxNumber() {
        String format = String.valueOf(schoolFax);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }

    public String getRepPhoneNumber() {
        String format = String.valueOf(repPhone);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }
    
    public List<ConfirmationProduct__c> getConfirmationProducts() {
        Variables__c var = Variables__c.getOrgDefaults();

        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getConfirmationProductsForConfirm?confirmid='+confirmation.Id);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseConfirmProdCalloutMock mock = new TestDatabaseConfirmProdCalloutMock(new List<Id>{confirmation.Id});
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'There was a problem getting products associated with this confirmation.'));
            }
        }

        List<ConfirmationProduct__c> cpList = DearFamilyLetterExtension.parseJSONCPBody(res.getBody());

        if(cpList.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'There are no products associated with this confirmation.'));
        }

        return cpList;
    }
    
     public List<ConfirmationContact__c> getConfirmationContacts() {
         
        return [SELECT Role__c, Contact__c
                           				FROM ConfirmationContact__c
                       					WHERE ConfirmationContact__c.Confirmation__c =: confirmation.Id];
    }
    
    public Contact getDFL() {
        Contact dfl = new Contact();
        for(ConfirmationContact__c ccon : getConfirmationContacts()) {
            if(ccon.Role__c == 'DFL Coord') {
                dfl = [SELECT Name, Phone FROM Contact WHERE Id =: ccon.Contact__c];
            } 
        }

        dflPhone = dfl.Phone;

        return dfl;
    }

    public String getDFLPhoneNumber() {
        String format = String.valueOf(dflPhone);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }
    
    public Contact getCoordinator() {
        Contact coord = new Contact();
        for(ConfirmationContact__c ccon : getConfirmationContacts()) {
            if(ccon.Role__c == 'Coordinator') {
                coord = [SELECT Id, Name, Phone,MobilePhone,Email,MailingStreet,MailingCity,MailingState,MailingPostalCode FROM Contact WHERE Id =: ccon.Contact__c];
            } 
        }

        coordPhone = coord.Phone;
        coordMobilePhone = coord.MobilePhone;

        return coord;
    }

    public String getCoordPhoneNumber() {
        String format = String.valueOf(coordPhone);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }

    public String getCoordMobilePhoneNumber() {
        String format = String.valueOf(coordMobilePhone);

        if(format != null) {
            System.debug(format);
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }
    
    public Contact getCoordinatorTwo() {
        Contact ctwo = new Contact();
        for(ConfirmationContact__c ccon : getConfirmationContacts()) {
            if(ccon.Role__c == 'Coordinator Sec') {
                ctwo = [SELECT Id, Name, Phone FROM Contact WHERE Id =: ccon.Contact__c];
            } 
        }

        alternateCoordPhone = ctwo.Phone;

        return ctwo;
    }

    public String getCoordSecPhone() {
        String format = String.valueOf(alternateCoordPhone);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }
    
    public Contact getArt() {
        Contact art = new Contact();
        for(ConfirmationContact__c ccon : getConfirmationContacts()) {
            if(ccon.Role__c == 'Art Teacher') {
                art = [SELECT Id, Name, Phone FROM Contact WHERE Id =: ccon.Contact__c];
            } 
        }

        artTeacherPhone = art.Phone;
        return art;
    }

    public String getArtPhone() {
        String format = String.valueOf(artTeacherPhone);

        if(format != null) {
            return '(' + format.substring(0, 3) + ') ' + format.substring(3, 6) + '-' + format.substring(6, 10);
        } 

        return '';
    }

    public pageReference getConfirmationDocument() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        Attachment attach = new Attachment();
        PageReference pdf = Page.ConfirmationPrintOut;
        pdf.getParameters().put('Id', recordId);

        Blob body;
        Id rid = Id.valueOf(recordId);

        Confirmation__c confirmation = [SELECT Account__r.SchoolCode__c, RecordType.Name 
                                            FROM Confirmation__c 
                                            WHERE Confirmation__c.Id =: rid];

        String attachName = confirmation.Account__r.SchoolCode__c + ' ' + confirmation.RecordType.Name + ' ' + 'Confirmation.pdf'; 
        
        if(Test.isRunningTest()) {
            body = Blob.valueOf('Unit Test');
        } else {
            body = pdf.getContentAsPDF();
        }
        attach.Name = attachName;
        attach.ContentType = 'pdf';
        attach.ParentId = rid;
        attach.IsPrivate = False;
        attach.Body = body;
        insert attach;

        PageReference recordpage = new PageReference('/' + confirmation.Id);
        return recordpage;
    }
}