@isTest
private class TestWarehouseAssignmentsController {
    /*
     * author: Redpoint
     * purpose: Unit Tests for the Warehouse Assignments Lightning component.
     * date: 3/9/2017
     * notes: 100% code coverage as of 3/9
    */

    static Confirmation__c confirm = new Confirmation__c();
    static Form__c form = new Form__c();
    static Form__c form2 = new Form__c();
    static Form__c form3 = new Form__c();
    static Form__c form4 = new Form__c();
    static Form__c form5 = new Form__c();

    static void setup() {

        Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP17';
        upsert var;
        
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.SchoolCode__c = 'UNITAC';
        acct.RepeatCustomer__c = True;
        insert acct;

        Contact ct = UnitTestDataFactory.inflateContact(null, acct);
        insert ct;
        Season__c season = UnitTestDataFactory.inflateSeason(var.Default_Season__c);
        insert season;

        confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
        insert confirm;

        form = UnitTestDataFactory.inflateForm(confirm);
        form.FormType__c = 'A';
        insert form;

        form2 = UnitTestDataFactory.inflateForm(confirm);
        form2.WarehouseAssigned__c = 'ST';
        form2.WarehouseTransferDate__c = System.today();
        form2.OrderValueWholesale__c = 5;
        form2.OrderCount__c = 5;
        insert form2;

        form3 = UnitTestDataFactory.inflateForm(confirm);
        form3.WarehouseAssigned__c = 'AM';
        form3.WarehouseTransferDate__c = System.today();
        insert form3;

        form4 = UnitTestDataFactory.inflateForm(confirm);
        form4.WarehouseAssigned__c = 'ST';
        insert form4;

        form5 = UnitTestDataFactory.inflateForm(confirm);
        form5.WarehouseAssigned__c = 'AM';
        insert form5;

    }

    static Testmethod void testHeadTable() {
        setup();

        List<Form__c> test = WarehouseAssignmentsController.headTable();
        Variables__c vars = Variables__c.getInstance();
        String season = vars.Default_Season__c;

        System.assertEquals(1, test.size());
        Form__c testForm = test.get(0);
        System.assertEquals(testForm.Confirmation__r.Season__r.Name, season);
        
    }

    static Testmethod void testSubTable() {
        setup();

        List<Form__c> test = WarehouseAssignmentsController.subTable('ST');
        System.assert(test.size() > 0);
    }

    static Testmethod void testTableBasedOnTransferDate() {
        setup();

        String sdate = String.valueOf(System.today());
        List<Form__c> test = WarehouseAssignmentsController.TableBasedOnTransferDate('ST', sdate);
        System.assert(test.size() > 0);
    }

    static Testmethod void testFindByName() {
        setup();

        List<Account> test = WarehouseAssignmentsController.findByName('UNITAC');
        System.assertEquals(1, test.size());
    }

    static Testmethod void testFormsBySchoolMain() {
        setup();

        List<Form__c> test = WarehouseAssignmentsController.formsBySchoolMain('UNITAC');
        System.assertEquals(1, test.size());
    }

    static Testmethod void testFormsBySchool() {
        setup();

        List<Form__c> test = WarehouseAssignmentsController.formsBySchool('UNITAC', 'ST');
        System.assert(test.size() > 0);
    }

    static Testmethod void testToStillwater() {
        setup();

        String tDate = String.valueOf(System.today());
        List<Id> idList = new List<Id>();
        idList.add(form3.Id);

        WarehouseAssignmentsController.toStillwater(idList, tDate);
        Form__c test = [SELECT Id, WarehouseAssigned__c, WarehouseTransferDate__c FROM Form__c WHERE Id =: form3.Id];
        System.assertEquals('ST', test.WarehouseAssigned__c);
        System.assertEquals(System.today(), test.WarehouseTransferDate__c);
    }

    static Testmethod void testToAmsterdam() {
        setup();

        String tDate = String.valueOf(System.today());
        List<Id> idList = new List<Id>();
        idList.add(form4.Id);

        WarehouseAssignmentsController.toAmsterdam(idList, tDate);
        Form__c test = [SELECT Id, WarehouseAssigned__c, WarehouseTransferDate__c FROM Form__c WHERE Id =: form4.Id];
        System.assertEquals('AM', test.WarehouseAssigned__c);
        System.assertEquals(System.today(), test.WarehouseTransferDate__c);
    }

    static Testmethod void testToMainTable() {
        setup();

        List<Id> idList = new List<Id>();
        idList.add(form5.Id);

        WarehouseAssignmentsController.toMainTable(idList);
        Form__c test = [SELECT Id, WarehouseAssigned__c, WarehouseTransferDate__c FROM Form__c WHERE Id =: form5.Id];
        System.assertEquals(null, test.WarehouseAssigned__c);
        System.assertEquals(null, test.WarehouseTransferDate__c);
    }

    static Testmethod void testGetTableCount() {
        setup();

        List<Form__c> testList = WarehouseAssignmentsController.headTable();
        Integer test = WarehouseAssignmentsController.getTableCount(testList);
        System.assertEquals(1, test);
    }

    static Testmethod void testGetTableValue() {
        setup();

        Confirmation__c temp = [SELECT AverageOrder__c FROM Confirmation__c WHERE Id =: confirm.Id];

        List<Form__c> testList = WarehouseAssignmentsController.headTable();
        Decimal test = WarehouseAssignmentsController.getTableValue(testList);
        Decimal actual = temp.AverageOrder__c;

        System.assertEquals(actual, test);
    }

    static Testmethod void testGetArtworkCount() {
        setup();

        List<Form__c> testList = WarehouseAssignmentsController.headTable();
        Decimal test = WarehouseAssignmentsController.getArtworkCount(testList);
        Decimal actual = form.ArtworkCount__c;
        System.assertEquals(actual, test);
    }
}