@isTest
private class TestSeasonForecastSummaryController {
	/*
	 * author: Redpoint
	 * date: 4/6/2017
	 * purpose: Test coverage for the SeasonForecastSummaryController class.
	 * note: 100% test coverage as of 4/6
	*/

	static Account acct = new Account();
	static Account acct2 = new Account();
	static Account acct3 = new Account();
	static Account acct4 = new Account();
	static Account acct5 = new Account();
	static Account acct6 = new Account();
    static Season__c season = new Season__c();
    static Confirmation__c confH = new Confirmation__c();
    static Confirmation__c confN = new Confirmation__c();
    static Confirmation__c confS = new Confirmation__c();
    static Confirmation__c confSS = new Confirmation__c();
    static Confirmation__c confG = new Confirmation__c();
    static Confirmation__c confT = new Confirmation__c();
    static Contact houseRep = new Contact();
    static Contact nonHouseRep = new Contact();
    static Contact mgr = new Contact();

	static void setup() {
		UnitTestDataFactory.insertShipDays();

		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP17';
        var.Tax_States__c = 'NM, CO';
        upsert var;

        houseRep.FirstName = 'Rep';
        houseRep.LastName = 'House';
        houseRep.HouseRep__c = True;
        insert houseRep;

		nonHouseRep.FirstName = 'Rep';
        nonHouseRep.LastName = 'NonHouse';
        insert nonHouseRep;        

		acct.Name = 'Unit Test Account';
        acct.SalesRep__c = houseRep.Id;
        acct.BillingState = 'NM';
        insert acct;

        acct2.Name = 'Unit Test Account 2';
        acct2.SalesRep__c = nonHouseRep.Id;
        acct2.BillingState = 'CO';
        insert acct2;

        acct3.Name = 'Unit Test Account 3';
        acct3.SalesRep__c = nonHouseRep.Id;
        acct3.BillingState = 'CO';
        insert acct3;

        acct4.Name = 'Unit Test Account 4';
        acct4.SalesRep__c = nonHouseRep.Id;
        acct4.BillingState = 'CO';
        insert acct4;

        acct5.Name = 'Unit Test Account 5';
        acct5.SalesRep__c = nonHouseRep.Id;
        acct5.BillingState = 'CO';
        insert acct5;

        acct6.Name = 'Unit Test Account 6';
        acct6.SalesRep__c = nonHouseRep.Id;
        acct6.BillingState = 'CO';
        insert acct6;

        season.Name = var.Default_Season__c;
        season.OPGross__c = 1.0;
        season.OPParticipation__c = 5;
        season.SBYBGross__c = 2.0;
        season.SBYBParticipation__c = 10;
        season.GalleryGross__c = 3.0;
        season.GalleryParticipation__c = 15;
        season.TileWallGross__c = 4.0;
        Season.TileWallParticipation__c = 20;
        insert season;

        RecordType recordtype = [SELECT Id, Name FROM RecordType WHERE Name = 'OP' LIMIT 1];
        
        confH.RecordType = recordtype;
        confH.Account__c = acct.Id;
        confH.Season__c = season.Id;
        confH.Status__c = 'Active';
        //confH.SalesRep__c = houseRep.Id;//Data migration
        confH.SeasonPopEstimate__c = 1;
        insert confH;

        confN.RecordType = recordtype;
        confN.Account__c = acct2.Id;
        confN.Season__c = season.Id;
        confN.Status__c = 'Active';
        //confN.SalesRep__c = nonHouseRep.Id;//Data migration
        confN.SeasonPopEstimate__c = 1;
        insert confN;

        ApexPages.currentPage().getParameters().put('season', season.Name);
        ApexPages.currentPage().getParameters().put('status', 'Active');
	}

	static void extraConfirmations() {

        confS.Account__c = acct3.Id;
        confS.Season__c = season.Id; 
        confS.RecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'SBYB' LIMIT 1];
        confS.Status__c = 'Active';
        //confS.SalesRep__c = nonHouseRep.Id;//Data migration
        confS.SeasonPopEstimate__c = 2;
        insert confS;

        confSS.Account__c = acct4.Id;
        confSS.Season__c = season.Id;
        confSS.RecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'SS SBYB' LIMIT 1];
        confSS.Status__c = 'Active';
        //confSS.SalesRep__c = nonHouseRep.Id;//Data migration
        confSS.SeasonPopEstimate__c = 3;
        insert confSS;

        confG.Account__c = acct5.Id;
        confG.Season__c = season.Id;
        confG.RecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Gallery' LIMIT 1];
        confG.Status__c = 'Active';
        //confG.SalesRep__c = nonHouseRep.Id;//Data migration
        confG.SeasonPopEstimate__c = 4;
        insert confG;

        confT.Account__c = acct6.Id;
        confT.Season__c = season.Id;
        confT.RecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1];
        confT.Status__c = 'Active';
        //confT.SalesRep__c = nonHouseRep.Id;//Data migration
        confT.SeasonPopEstimate__c = 5;
        insert confT;
	}

	static Testmethod void testGetQueryConfirmations() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();
		System.assertEquals(2, sfs.confirms.size());
	}

	static Testmethod void testGetSeparateByHouse() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();
		sfs.getseparateByHouse();
		System.assertEquals(1, sfs.houseList.size());
		System.assertEquals(1, sfs.nonHouseList.size());
	}

	static Testmethod void testGroupByReps() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();
		sfs.getseparateByHouse();

		Map<String, List<Confirmation__c>> test = sfs.groupByReps(sfs.houseList);
		System.assert(!test.isEmpty());

		Set<String> test2 = test.keySet();
		System.assert(test2.contains(houseRep.FirstName + ' ' + houseRep.LastName));
	}

	static Testmethod void testTotalCounts() {
		setup();
		
		confS.Account__c = acct3.Id;
        confS.Season__c = season.Id; 
        confS.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1].Id;
        confS.Status__c = 'Active';
        confS.SeasonPopEstimate__c = 2;
        insert confS;

        confSS.Account__c = acct4.Id;
        confSS.Season__c = season.Id;
        confSS.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'SS SBYB' LIMIT 1].Id;
        confSS.Status__c = 'Active';
        confSS.SeasonPopEstimate__c = 3;
        insert confSS;

        confG.Account__c = acct5.Id;
        confG.Season__c = season.Id;
        confG.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Gallery' LIMIT 1].Id;
        confG.Status__c = 'Active';
        confG.SeasonPopEstimate__c = 4;
        insert confG;

        confT.Account__c = acct6.Id;
        confT.Season__c = season.Id;
        confT.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1].Id;
        confT.Status__c = 'Active';
        confT.SeasonPopEstimate__c = 5;
        insert confT;

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();

		List<Double> test = sfs.totalCounts(sfs.confirms);
		System.assertEquals(16, test.get(0));
		System.assertEquals(0.4, test.get(1));
		System.assertEquals(0.6, test.get(2));
		System.assertEquals(0.1, test.get(3));
		System.assertEquals(1.8, test.get(4));
		System.assertEquals(4, test.get(5));
		System.assertEquals(6.9, test.get(6));

		List<Double> test2 = sfs.gethousePopulationTotal();
		System.assert(!test2.isEmpty());

		List<Double> test3 = sfs.getnonHousePopulationTotal();
		System.assert(!test3.isEmpty());

		List<Double> test4 = sfs.getgrandTotals();
		System.assert(!test4.isEmpty());
	}

	static Testmethod void testSumByState() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();

		Map<String, List<Confirmation__c>> test = sfs.sumByState(sfs.confirms);
		System.assertEquals(2, test.size());

		Set<String> test2 = test.keySet();
		System.assert(test2.contains('CO'));
		System.assert(test2.contains('NM'));
	}

	static Testmethod void testHouseImage() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		String test = sfs.houseImage(True);
		System.assertEquals('Yes', test);

		String test2 = sfs.houseImage(False);
		System.assertEquals('No', test2);
	}

	static Testmethod void testBuildSummaryTables() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();
		sfs.getseparateByHouse();

		String test = sfs.buildSummaryTables(sfs.nonHouseList);
		System.assertNotEquals('', test);
		System.assertNotEquals(null, test);

		String test2 = sfs.getHouseReps();
		System.assertNotEquals('', test2);
		System.assertNotEquals(null, test2);

		String test3 = sfs.getNonHouseReps();
		System.assertNotEquals('', test3);
		System.assertNotEquals(null, test3);
	}

	static Testmethod void testBuildDetailTables() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		sfs.getqueryConfirmations();
		sfs.getseparateByHouse();

		String test = sfs.buildDetailTables(sfs.houseList);
		System.assertNotEquals('', test);
		System.assertNotEquals(null, test);

		String test2 = sfs.getDetailHouseReps();
		System.assertNotEquals('', test2);
		System.assertNotEquals(null, test2);

		String test3 = sfs.getDetailNonHouseReps();
		System.assertNotEquals('', test3);
		System.assertNotEquals(null, test3);	
	}

	static Testmethod void testRepeatCustomer() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		String test = sfs.repeatCustomer(True);
		System.assertEquals('X', test);

		String test2 = sfs.repeatCustomer(False);
		System.assertEquals('', test2);
	}

	static Testmethod void testShowByRecordType() {
		setup();
		
		List<Confirmation__c> confirms = new List<Confirmation__c>();

		confS.Account__c = acct3.Id;
        confS.Season__c = season.Id; 
        confS.RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'SBYB' LIMIT 1].Id;
        confS.Status__c = 'Active';
        confS.SeasonPopEstimate__c = 2;
        confirms.add(confS);
        
        confSS.Account__c = acct4.Id;
        confSS.Season__c = season.Id;
        confSS.RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'SS SBYB' LIMIT 1].Id;
        confSS.Status__c = 'Active';
        confSS.SeasonPopEstimate__c = 3;
        confirms.add(confSS);

        confG.Account__c = acct5.Id;
        confG.Season__c = season.Id;
        confG.RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'Gallery' LIMIT 1].Id;
        confG.Status__c = 'Active';
        confG.SeasonPopEstimate__c = 4;
        confirms.add(confG);

        confT.Account__c = acct6.Id;
        confT.Season__c = season.Id;
        confT.RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1].Id;
        confT.Status__c = 'Active';
        confT.SeasonPopEstimate__c = 5;
        confirms.add(confT);

        insert confirms;

		ApexPages.StandardController sc = new ApexPages.StandardController(confH);
		SeasonForecastSummaryReportController sfs = new SeasonForecastSummaryReportController(sc);

		Confirmation__c c1 = [SELECT Id, RecordType.Name, Projection__c FROM Confirmation__c WHERE Id =: confH.Id LIMIT 1];
		List<Double> test1 = sfs.showByRecordType(c1);//OP
		System.assertEquals(0.05, test1.get(0));
		System.assertEquals(0, test1.get(1));
		System.assertEquals(0, test1.get(2));
		System.assertEquals(0, test1.get(3));
		System.assertEquals(0, test1.get(4));
		System.assertEquals(0.05, test1.get(5));

		Confirmation__c c2 = [SELECT Id, RecordType.Name, Projection__c FROM Confirmation__c WHERE Id =: confS.Id LIMIT 1];
		List<Double> test2 = sfs.showByRecordType(c2);//SBYB

		System.assertEquals(0, test2.get(0));
		System.assertEquals(0.4, test2.get(1));
		System.assertEquals(0, test2.get(2));
		System.assertEquals(0, test2.get(3));
		System.assertEquals(0, test2.get(4));
		System.assertEquals(0.4, test2.get(5));

		Confirmation__c c3 = [SELECT Id, RecordType.Name, Projection__c FROM Confirmation__c WHERE Id =: confSS.Id LIMIT 1];
		List<Double> test3 = sfs.showByRecordType(c3);//SS SBYB
		System.assertEquals(0, test3.get(0));
		System.assertEquals(0, test3.get(1));
		System.assertEquals(0.6, test3.get(2));
		System.assertEquals(0, test3.get(3));
		System.assertEquals(0, test3.get(4));
		System.assertEquals(0.6, test3.get(5));

		Confirmation__c c4 = [SELECT Id, RecordType.Name, Projection__c FROM Confirmation__c WHERE Id =: confG.Id LIMIT 1];
		List<Double> test4 = sfs.showByRecordType(c4);//Gallery
		System.assertEquals(0, test4.get(0));
		System.assertEquals(0, test4.get(1));
		System.assertEquals(0, test4.get(2));
		System.assertEquals(1.8, test4.get(3));
		System.assertEquals(0, test4.get(4));
		System.assertEquals(1.8, test4.get(5));

		Confirmation__c c5 = [SELECT Id, RecordType.Name, Projection__c FROM Confirmation__c WHERE Id =: confT.Id LIMIT 1];
		List<Double> test5 = sfs.showByRecordType(c5);//Tile Wall
		System.assertEquals(0, test5.get(0));
		System.assertEquals(0, test5.get(1));
		System.assertEquals(0, test5.get(2));
		System.assertEquals(0, test5.get(3));
		System.assertEquals(4.0, test5.get(4));
		System.assertEquals(4.0, test5.get(5));

	}
}