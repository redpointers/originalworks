@isTest
public class UnitTestDataFactory
{
	public static void insertVariables()
	{
		Variables__c var = Variables__c.getOrgDefaults();
		var.Default_Season__c = 'SP17';
		var.Paper_SBYB__c = 5;
		var.Paper_OP__c = 5;
		var.Paper_TW4__c = 5;
		var.Paper_TW6__c = 5;
		var.Tax_States__c = 'NY,GA,TN,TX,CA,FL,MD,CO,SC,MI,NC,VA';

		upsert var;
	}

	public static void insertShipDays()
	{
		List<ShipDays__c> shipList = new List<ShipDays__c>();
		shipList.add(new ShipDays__c(Name = 'CO', numDays__c = 6));
		insert shipList;
	}

	public static void insertPlanetPress()
	{
		List<Planet_Press_Export__c> ppList = new List<Planet_Press_Export__c>();
		ppList.add(new Planet_Press_Export__c(Name = 'QN', Column__c = 72));
		ppList.add(new Planet_Press_Export__c(Name = '24NC', Column__c = 73));
		ppList.add(new Planet_Press_Export__c(Name = '5NOTE', Column__c = 74));
		insert ppList;
	}

	public static Account inflateAccount(String accountName) 
	{
		Account acc = new Account (
				name=accountName, 
			 	Phone = '111-222-3333',
			 	BillingStreet = '12223 Main St',
			 	BillingState = 'CO',
			 	BillingCity = 'Denver',
			 	BillingPostalCode = '80204', 
			 	BillingCountry = 'United States',
			 	TaxExempt__c = true,
			 	TaxCode__c = 'CO DENVER',
			 	TaxExemptValidUntil__c = System.today().addDays(10),
			 	TaxExemptReceived__c = System.today()
			);
		return acc;


	}

	public static Contact inflateContact(Contact masterContact, Account testAccount) 
	{
		Contact cont;
		
		if(masterContact == null)
		{
			cont = new Contact (LastName='TestContact', 
								Phone='303-111-2222',
								AccountId = testAccount.Id,
								MailingStreet =  '123 Main st',
								MailingCity = 'Parker',
								MailingState = 'Colorado',
								MailingPostalCode = '80134',
								MailingCountry = 'United States'
								);
		} 
		else 
		{
			cont = masterContact; 
		}
		return cont;
	}

	public static Season__c inflateSeason(String name) 
	{
		Season__c season = new Season__c();
		season.Name = name;
		season.StartDate__c = System.today();
		season.EndDate__c = System.today().addDays(100);
		return season;
	}

	public static Confirmation__c inflateConfirmation(Account acct, Season__c season, Contact rep)
	{
		Confirmation__c confirm = new Confirmation__c();
		confirm.Season__c = season.Id;
		confirm.SalesRep__c = rep.id;
		confirm.Account__c = acct.Id;
		
		confirm.PurchasePaper__c = true;
        confirm.ShipInstructions__c = 'Ship It';
        confirm.ManInstructions__c = 'Man It';
        confirm.MatchInstructions__c = 'Match It';
        confirm.NumberOfReams__c = '5';

        RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1];

        confirm.RecordTypeId = recordtype.Id;

        return confirm; 
	}

	public static Form__c inflateForm(Confirmation__c confirm)
	{
		Form__c form = new Form__c();

		form.FormType__c = 'A';
		form.FormSequence__c = 1;
		form.ActualShip__c = System.today(); 
		form.ActualReceive__c = System.today(); 
		form.OrderEntry__c = System.today(); 
		form.ArtworkCount__c = 10; 
        form.EstimatedShip__c = System.today(); 
        form.MagsBought__c = 5;
        form.MagsReturned__c = 0;
        form.MagValueRetail__c = 0;
        form.Initials__c = 'TG';
        form.QuestionFolders__c = 0;
        form.ArtworkCountOE__c = 10;
        form.Confirmation__c = confirm.Id;
        form.DaysLate__c = 0;

        return form;
	}

	public static OWProduct__c inflateOWProduct()
	{
		OWProduct__c prod = new OWProduct__c();
		prod.Name = 'Test Product';
		prod.CodeMAS90__c = '1234';
		prod.Code__c = '1234';
		prod.Shortcut__c = '11';
		prod.Description__c = 'Test Desc';

		return prod;
	}

	public static SeasonProduct__c inflateSeasonProduct(OWProduct__c prod, Season__c season)
	{
		SeasonProduct__c sp = new SeasonProduct__c();
		sp.OWProduct__c = prod.Id;
		sp.PriceToParent__c = 10;
		sp.PriceToSchool__c = 5;
		sp.Season__c = season.Id;

		return sp;
	}

	public static ConfirmationProduct__c inflateConfirmationProduct(Confirmation__c confirm, SeasonProduct__c sp)
	{
		ConfirmationProduct__c cp = new ConfirmationProduct__c();
        cp.Confirmation__c = confirm.Id;
        cp.Price_to_Parent__c = sp.PriceToParent__c;
        cp.Price_to_School__c = sp.PriceToSchool__c;
        cp.Code__c = sp.OWProduct__r.Code__c;
        cp.Code_MAS90__c = sp.OWProduct__r.CodeMAS90__c;
        cp.Description__c = sp.OWProduct__r.Description__c;
        cp.Shortcut__c = sp.OWProduct__r.Shortcut__c;
        cp.SeasonProduct_ID__c = String.valueOf(sp.Id);
        cp.ProductName__c = sp.OWProduct__r.Name;

        return cp;
	}

	public static User inflateUser() {

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];

		User usr = new User();
		usr.Username = 'UnitTest@notreal.com';
		usr.Email = 'fake@notreal.com';
		usr.LastName = 'User';
		usr.Alias = 'testuser';
		usr.TimeZoneSidkey = 'America/New_York';
		usr.LocaleSidKey = 'en_US';
		usr.EmailEncodingKey = 'UTF-8';
		usr.ProfileId = p.Id;
		usr.LanguageLocaleKey = 'en_US';
		return usr;
	}

	public static OrderLines inflateOrder(String suffixStr, Form__c form, Season__c season)
	{
		OrderLines ol = new OrderLines();
		ol.formid = form.Id;
		ol.qty = 1;
		ol.seasonid = season.Id;
		ol.wholesale = 10;
		ol.total = 20;
		ol.suffix = suffixStr;
		ol.shipvia = 'UPS';
		ol.shipping = 5;
		ol.salestax = 2;
		ol.price = 20;
		ol.paymt = 'O';
		ol.lineno = 1;
		ol.lastmoddate = System.today();
		ol.createddate = System.today();
		ol.hv = 'V';
		ol.grandtotal = 27;
		ol.discount = 0;
		ol.code = 'OW';

		return ol;
	}
}