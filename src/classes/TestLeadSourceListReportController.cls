@isTest
private class TestLeadSourceListReportController {
	/*
     * author: Redpoint
     * date: 4/5/2017
     * purpose: Test coverage for the LeadSourceListReportController class 
     * notes: 100% test coverage as of 4/5.
    */

    static Testmethod void testGetLeadListings() {
    	Account acct = new Account(Name = 'Unit Test');
    	insert acct;

    	ApexPages.StandardController sc = new ApexPages.StandardController(acct);
    	LeadSourceListReportController lead = new LeadSourceListReportController(sc);

    	List<String> test = lead.getLeadListings();
    	System.assertNotEquals(0, test.size());
    }
}