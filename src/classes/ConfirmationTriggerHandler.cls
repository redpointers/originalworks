public with sharing class ConfirmationTriggerHandler 
{
	public static void handleBeforeInsert(List<Confirmation__c> newConfs)
	{	
		if(UserInfo.getUserId() != 'placeholder') {
			Set<Id> confAccountIds = new Set<Id>();
			for(Confirmation__c conf : newConfs) 
		    {
		    	if(!confAccountIds.contains(conf.Account__c))
		    		confAccountIds.add(conf.Account__c);
		    }

		    Variables__c vars = Variables__c.getInstance();
	        String currentSeason = vars.Default_Season__c;

	        Season__c season = [SELECT Id FROM Season__c WHERE Name =: currentSeason LIMIT 1];

			List<Account> accts = [SELECT Id, SalesRep__c, SubSalesRep__c, (SELECT Id, Season__c FROM Confirmations__r) FROM Account WHERE Account.Id in : confAccountIds];
			for(Confirmation__c conf : newConfs) 
			{
				conf.Season__c = season.Id;

		        for(Account acct : accts)
		        {
			        if(conf.Account__c == acct.Id)
			        {
			        	conf.SalesRep__c = acct.SalesRep__c;
			        	conf.SubSalesRep__c = acct.SubSalesRep__c;

			        	for(Confirmation__c con : acct.Confirmations__r) 
			        	{
			        		if(conf.Season__c == con.Season__c) 
			        		{
			        			conf.addError('Only one Confirmation per Season is permitted.');
			        		}
			        	}
			        	break;
			        }
			    }
		    }
		}
	}

	public static void handleAfterInsert(List<Confirmation__c> newConfs, Map<id, Confirmation__c> oldMap) 
	{
		
		// First, when a new confirmation record is created, automatically add all season products to the Confirmation Product related list that match the season the confirmation is for.
		List<ConfirmationProduct__c> cpList = new List<ConfirmationProduct__c>();

		Set<Id> confAccountIds = new Set<Id>();
	    Set<Id> confSeasonIds = new Set<Id>();
	    for(Confirmation__c conf : newConfs) 
	    {
	    	if(!confAccountIds.contains(conf.Account__c))
	    		confAccountIds.add(conf.Account__c);

	        if(!confSeasonIds.contains(conf.Season__c))
	            confSeasonIds.add(conf.Season__c);
	    }

	    if(UserInfo.getUserId() != 'placeholder') {

		    List<SeasonProduct__c> spList = [SELECT Id,PriceToParent__c, PriceToSchool__c, OWProduct__c, OWProduct__r.Name, OWProduct__r.Code__c, OWProduct__r.CodeMAS90__c, Season__c,
		            OWProduct__r.Description__c, OWProduct__r.Shortcut__c, PrintOrder__c, TestProduct__c FROM SeasonProduct__c WHERE SeasonProduct__c.Season__c in : confSeasonIds AND SeasonProduct__c.TestProduct__c = false];
		    RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1];
		    
		    for(Confirmation__c conf : newConfs) 
		    {
		    	if(conf.RecordTypeId != recordtype.Id)
		    	{
			        for(SeasonProduct__c sp : spList) 
			        {
			            if(sp.Season__c == conf.Season__c)
			            {
			                ConfirmationProduct__c cp = new ConfirmationProduct__c();
			                cp.Confirmation__c = conf.Id;
			                cp.AdjustPriceToParent__c = sp.PriceToParent__c;
			                cp.AdjustPriceToSchool__c = sp.PriceToSchool__c;
			                cp.Price_to_Parent__c = sp.PriceToParent__c;
			                cp.Price_to_School__c = sp.PriceToSchool__c;
			                cp.DFLAdjusted__c = sp.PriceToParent__c; // Is this the right value for these? We're not sure
			                cp.DFLPrice__c = sp.PriceToParent__c;
			                cp.DFLTax__c = sp.PriceToParent__c;
			                cp.Code__c = sp.OWProduct__r.Code__c;
			                cp.Code_MAS90__c = sp.OWProduct__r.CodeMAS90__c;
			                cp.Description__c = sp.OWProduct__r.Description__c;
			                cp.Shortcut__c = sp.OWProduct__r.Shortcut__c;
			                cp.SeasonProduct_ID__c = String.valueOf(sp.Id);
			                cp.ProductName__c = sp.OWProduct__r.Name;
			                cp.OWProduct__c = sp.OWProduct__c;
			                cp.PrintOrder__c = sp.PrintOrder__c;
	                        cp.TestProduct__c = sp.TestProduct__c;
			                cpList.add(cp);
			            }
			        }
		    	} else 
		    	{
		    		for(SeasonProduct__c sp : spList) 
			        {
			            if(sp.Season__c == conf.Season__c && sp.OWProduct__r.Name.containsIgnoreCase('Tile'))
			            {
			                ConfirmationProduct__c cp = new ConfirmationProduct__c();
			                cp.Confirmation__c = conf.Id;
			                cp.AdjustPriceToParent__c = sp.PriceToParent__c;
			                cp.AdjustPriceToSchool__c = sp.PriceToSchool__c;
			                cp.Price_to_Parent__c = sp.PriceToParent__c;
			                cp.Price_to_School__c = sp.PriceToSchool__c;
			                cp.DFLAdjusted__c = sp.PriceToParent__c; // Is this the right value for these? We're not sure
			                cp.DFLPrice__c = sp.PriceToParent__c;
			                cp.DFLTax__c = sp.PriceToParent__c;
			                cp.Code__c = sp.OWProduct__r.Code__c;
			                cp.Code_MAS90__c = sp.OWProduct__r.CodeMAS90__c;
			                cp.Description__c = sp.OWProduct__r.Description__c;
			                cp.Shortcut__c = sp.OWProduct__r.Shortcut__c;
			                cp.SeasonProduct_ID__c = String.valueOf(sp.Id);
			                cp.ProductName__c = sp.OWProduct__r.Name;
			                cp.OWProduct__c = sp.OWProduct__c;
			                cp.PrintOrder__c = sp.PrintOrder__c;
	                        cp.TestProduct__c = sp.TestProduct__c;
			                cpList.add(cp);
			            }
			        }
		    	}
		    }
		    
		    if(cpList.size() > 0) 
		    {
		        insert cpList;
		    }
		}

	    // Next, The trigger makes a check to determine how many confirmations an account has in its related list. If an account has more than one
	 	// confirmation, then the RepeatCustomer__c checkbox field is set to true to indicate that the account (school) is a repeat customer of 
		// Original Works. This checkbox field is for specific use with the Production Planner tool, which allows data filtering by new and/or repeat customers.
		List<Account> accts = [SELECT Id, SchoolCode__c, (SELECT Id FROM Confirmations__r) FROM Account WHERE Account.Id in : confAccountIds];
		List<Account> updateAccts = new List<Account>();
		Set<Id> updateAcctIds = new Set<Id>();

		RecordType opRT = [SELECT Id FROM RecordType WHERE Name = 'OP' LIMIT 1];
		RecordType sbybRT = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1];
		RecordType galleryRT = [SELECT Id FROM RecordType WHERE Name = 'Gallery' LIMIT 1];
		RecordType twRT = [SELECT Id FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1];

		for(Confirmation__c conf : newConfs) 
		{
	        List<Confirmation__c> confList = new List<Confirmation__c>();
	        for(Account acct : accts) 
	        {
	            for(Confirmation__c con : acct.Confirmations__r) 
	            {
	                confList.add(con);
	            }
	            
	            if(confList.size() > 1) 
	            {
	                acct.RepeatCustomer__c = True;
	                if(!updateAcctIds.contains(acct.Id))
	                {
	                	updateAccts.add(acct);
	                	updateAcctIds.add(acct.Id);
	                }
	            }
	        }

	        if(UserInfo.getUserId() != 'placeholder') {
		        // Create the required forms for this confirmation. The method takes into account the type of confirmation
		        ConfirmationTriggerHandler.createForms(conf, accts, opRT, sbybRT, galleryRT, twRT);
		    }
	    }

	    if(!updateAccts.isEmpty())
       		update updateAccts;  
	}

	private static void createForms(Confirmation__c confirm, List<Account> accts, RecordType opRT, RecordType sbybRT, RecordType galleryRT, RecordType twRT)
	{
		// SBYB gets a 'A', 'B', and 'S' form
		// OP and TW get a 'D' and 'S' form
		// Gallery gets a 'A', and 'B' form
		String schoolCode = null;
		for(Account acct : accts)
		{
			if(confirm.Account__c == acct.Id)
			{
				schoolCode = acct.SchoolCode__c;
				break;
			}
		}

		// Each create call does an insert. This is because Form insert order matters. See Form trigger
		if(confirm.RecordTypeId == sbybRT.Id)
		{
			ConfirmationTriggerHandler.createAForm(schoolCode, confirm.Id);
			ConfirmationTriggerHandler.createBForm(schoolCode, confirm.Id);
			ConfirmationTriggerHandler.createSForm(schoolCode, confirm.Id);
		}

		if(confirm.RecordTypeId == opRT.Id || confirm.RecordTypeId == twRT.Id)
		{
			ConfirmationTriggerHandler.createDForm(schoolCode, confirm.Id);
			ConfirmationTriggerHandler.createSForm(schoolCode, confirm.Id);
		}

		if(confirm.RecordTypeId == galleryRT.Id)
		{
			ConfirmationTriggerHandler.createAForm(schoolCode, confirm.Id);
			ConfirmationTriggerHandler.createBForm(schoolCode, confirm.Id);
		}
	}

	private static void createAForm(String code, Id confirmId)
	{
		Form__c formA = new Form__c();
		formA.Name = code+' A';
    	formA.FormType__c = 'A';
    	formA.Confirmation__c = confirmId;
    	formA.FormSequence__c = 1;
    	formA.ActualReceive__c = System.today();
    	formA.OKInstructions__c = true;
    	formA.OKDFL__c = true;
    	formA.OKShipAddress__c = true;
    	formA.GreenSheet__c = false;

    	insert formA;
	}

	private static void createBForm(String code, Id confirmId)
	{
		Form__c formB = new Form__c();
		formB.Name = code+' B';
    	formB.FormType__c = 'B';
    	formB.Confirmation__c = confirmId;
    	formB.FormSequence__c = 1;
    	formB.ActualReceive__c = System.today();
    	formB.OKInstructions__c = true;
    	formB.OKDFL__c = true;
    	formB.OKShipAddress__c = true;
    	formB.GreenSheet__c = false;

    	insert formB;
	}

	private static void createDForm(String code, Id confirmId)
	{
		Form__c formD = new Form__c();
		formD.Name = code+' D';
    	formD.FormType__c = 'D';
    	formD.Confirmation__c = confirmId;
    	formD.FormSequence__c = 1;
    	formD.ActualReceive__c = System.today();
    	formD.OKInstructions__c = true;
    	formD.OKDFL__c = true;
    	formD.OKShipAddress__c = true;
    	formD.GreenSheet__c = false;

    	insert formD;
	}

	private static void createSForm(String code, Id confirmId)
	{
		Form__c formS = new Form__c();
		formS.Name = code+' S';
    	formS.FormType__c = 'S';
    	formS.Confirmation__c = confirmId;
    	formS.FormSequence__c = 1;
    	formS.WarehouseAssigned__c = 'ST';
    	formS.ActualReceive__c = System.today();
    	formS.OKInstructions__c = true;
    	formS.OKDFL__c = true;
    	formS.OKShipAddress__c = true;
    	formS.GreenSheet__c = false;
    	formS.OrderEntry__c = System.today();
    	formS.WarehouseTransferDate__c = System.today();
    	formS.MAS90Export__c = System.today();

    	insert formS;

    	Tracking__c track = new Tracking__c();
    	track.Form__c = formS.Id;
    	track.SLXFormID__c = String.valueOf(formS.Id).substring(0, 15); // field only 15 chars long
    	track.Warehouse__c = formS.WarehouseAssigned__c;
    	insert track;
	}
}