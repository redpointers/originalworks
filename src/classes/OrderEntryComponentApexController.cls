public with sharing class OrderEntryComponentApexController 
{
	@AuraEnabled
	public static Confirmation__c getConfirmation(Id recordId)
	{
		Confirmation__c confirm = [select Id, Name, Account__c, Account__r.Name, Account__r.SchoolCode__c, Season__c, Season__r.Name, RecordType.Name,
			Notes__c from Confirmation__c where Id =: recordId];
		return confirm;
	}

	@AuraEnabled
	public static List<Form__c> getForms(Id recordId)
	{
		List<Form__c> forms = [select Id, Name, FormType__c, FormSequence__c, ActualShip__c, ActualReceive__c, OrderEntry__c, ArtworkCount__c, GreenSheet__c,
			EstimatedShip__c, MagsBought__c, MagsReturned__c, Initials__c, QuestionFolders__c, ArtworkCountOE__c, Carrier__c from Form__c where Confirmation__c =: recordId and
			FormType__c != 'S'];
		return forms;
	}

	@AuraEnabled
	public static List<OrderLines> getOrdersForForm(Id formId)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getRecordsForForm?formid='+formId);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock(new List<Id>{formId});
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            List<OrderLines> lines = parseJSONLinesBody(res.getBody());
            return lines;
        }
        catch(Exception e)
        {
        	throw new AuraException(e.getMessage());
        }

		return null;
	}

	@AuraEnabled
	public static List<ConfirmationProduct__c> getProductsForConfirmation(Id confId)
	{
		Variables__c var = Variables__c.getOrgDefaults();

		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(var.Database_Endpoint__c+'getConfirmationProductsForConfirm?confirmid='+confId);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');

        String username = var.Database_Username__c;
        String password = var.Database_Password__c;

        Blob headerValue = Blob.valueOf(username + ':' + password);
	    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
	    req.setHeader('Authorization', authorizationHeader);

        HttpResponse res = new HttpResponse();

        if (Test.isRunningTest()) 
        {
            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock(new List<Id>{confId});
            res = mock.respond(req);
        }
        else
        {
            try
            {
                res = h.send(req);
            }
            catch(Exception e)
            {
                throw new AuraException(e.getMessage());
            }
        }

        try
        {
            List<ConfirmationProduct__c> lines = parseJSONCPBody(res.getBody());
            return lines;
        }
        catch(Exception e)
        {
        	throw new AuraException(e.getMessage());
        }

		return null;
	}

	@AuraEnabled
	public static void saveOrders(Form__c form, String inOrders, String orderCount)
	{
		try
		{
			Double orderValueWholesale = 0;
			Double orderValueRetail = 0;

			if(inOrders!=null && inOrders.length()>0)
			{
				List<TableLine> tableLines = (List<TableLine>)JSON.deserialize(inOrders, List<TableLine>.Class);

				if(tableLines!=null && tableLines.size()>0)
				{
					List<OrderLines> orderLines = new List<OrderLines>();

					for(TableLine line : tableLines)
					{
						OrderLines order = new OrderLines();
						order.hv = line.hv;
						order.paymt = line.pmt;
						order.qty = line.qty;
						order.wholesale = line.wholesale;
						orderValueWholesale = orderValueWholesale + (order.wholesale*order.qty);
						order.total = line.total;
						orderValueRetail = orderValueRetail + order.total;
						order.price = line.price;
						order.suffix = line.suffix;
						order.code = line.code;
						order.description = line.description;
						order.formId = form.Id;
						order.seasonid = line.seasonid;
						order.lineno = line.lineno;
						order.lastmoddate = System.now();
						order.lastmodid = UserInfo.getUserId();
		            	order.shipping = 0;
		            	order.shipvia = form.Carrier__c;
		            	order.discount = 0;
		            	order.salestax = 0;
		            	order.grandtotal = (line.price*line.qty)+order.discount+order.salestax+order.shipping;

						if(order.lineno == null)
						{
							order.createddate = System.now();
							order.createdid = UserInfo.getUserId();
						}
						
						orderLines.add(order);
					}

					Variables__c var = Variables__c.getOrgDefaults();

					Http h = new Http();
		        	HttpRequest req = new HttpRequest();
		        	req.setEndpoint(var.Database_Endpoint__c+'upsertRecords');
		        	req.setMethod('POST');
		        	req.setHeader('Content-Type', 'application/json');
		        	req.setBody(JSON.serialize(orderLines));

		        	String username = var.Database_Username__c;
			        String password = var.Database_Password__c;

			        Blob headerValue = Blob.valueOf(username + ':' + password);
				    String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
				    req.setHeader('Authorization', authorizationHeader);

		        	System.debug('**** Request: '+req);

		        	HttpResponse res = new HttpResponse();

		        	if (Test.isRunningTest()) 
			        {
			            TestDatabaseCalloutMock mock = new TestDatabaseCalloutMock();
			            res = mock.respond(req);
			        }
			        else
			        {
			            try
			            {
			                res = h.send(req);
			            }
			            catch(Exception e)
			            {
			                throw new AuraException(e.getMessage());
			            }
			        }

			        try
			        {
			            if(res.getStatus() != 'Success' && res.getStatus() != 'OK')
			            	throw new AuraException('Server responded with error: '+res.getStatus());	            
			        }
			        catch(Exception e)
			        {
			        	throw new AuraException(e.getMessage());
			        }
			    }
		    }

			Form__c dbForm = [select Id from Form__c where Id =: form.Id];
			dbForm.Initials__c = form.Initials__c;
			dbForm.QuestionFolders__c = form.QuestionFolders__c;
			dbForm.ArtworkCountOE__c = form.ArtworkCountOE__c;
			dbForm.OrderValueWholesale__c = orderValueWholesale;
			dbForm.OrderValueRetail__c = orderValueRetail;

			if(orderCount!=null && orderCount.length()>0)
				dbForm.OrderCount__c = Integer.valueOf(orderCount);

			update dbForm;
		}
		catch(Exception e)
		{
			throw new AuraException('There was an error while saving the orders:'+e.getMessage());
		}
	}

	public static List<Orderlines> parseJSONLinesBody(String body)
    {
    	JSONParser parser = JSON.createParser(body);

    	List<Orderlines> lines = new List<Orderlines>();
    	while (parser.nextToken() != null) 
    	{
        	// Start at the array of lines.
        	if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
        	{
            	while (parser.nextToken() != null) 
            	{
               		// Advance to the start object marker to find next line object.
                	if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                	{
                		Orderlines line = (Orderlines)parser.readValueAs(Orderlines.class);
                    	lines.add(line);
                	}
                }
            }
        }

        return lines;
    }

    public static List<ConfirmationProduct__c> parseJSONCPBody(String body)
    {
    	JSONParser parser = JSON.createParser(body);

    	List<ConfirmationProduct__c> lines = new List<ConfirmationProduct__c>();
    	while (parser.nextToken() != null) 
    	{
        	// Start at the array of lines.
        	if (parser.getCurrentToken() == JSONToken.START_ARRAY) 
        	{
            	while (parser.nextToken() != null) 
            	{
               		// Advance to the start object marker to find next line object.
                	if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
                	{
                		ConfirmationProduct__c line = (ConfirmationProduct__c)parser.readValueAs(ConfirmationProduct__c.class);
                    	lines.add(line);
                	}
                }
            }
        }

        return lines;
    }

    public class TableLine
	{
		public Long lineno{get;set;}
		public String suffix{get;set;}
		public String hv{get;set;}
		public String pmt{get;set;}
		public String code{get;set;}
		public String description{get;set;}
		public Integer qty{get;set;}
		public Double price{get;set;}
		public Double total{get;set;}
		public Double wholesale{get;set;}
		public String seasonid{get;set;}
	}


}