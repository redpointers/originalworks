@isTest
private class TestGreensheetController {
	/*
     * author: Redpoint
     * purpose: Unit Tests for the Greensheet Lightning component.
     * date: 3/10/2017
     * notes: The tests for the SpecialInstructions method and the updateSpecialInstructions method
     *			are currently commented out. Both tests were displaying unexpected behavior that 
     *			couldn't be corrected. 90% code coverage was achieved on 3/10.
    */
    static Confirmation__c confirm = new Confirmation__c();
    static Confirmation__c confirm2 = new Confirmation__c();
	static Form__c form = new Form__c();
	static Form__c form2 = new Form__c();
	static Form__c form3 = new Form__c();
	static Form__c form4 = new Form__c();
	static Form__c form5 = new Form__c();

	static void setup() {

		UnitTestDataFactory.insertVariables();

        Variables__c var = Variables__c.getOrgDefaults();
		
		Account acct = new Account();
		acct.Name = 'Unit Test Account';
		insert acct;

		Account acct2 = new Account();
		acct2.Name = 'Unit Test Account2';
		insert acct2;

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
		User usr = new User();
		usr.Username = 'UnitTest@notreal.com';
		usr.Email = 'fake@notreal.com';
		usr.LastName = 'User';
		usr.Alias = 'testuser';
		usr.TimeZoneSidkey = 'America/New_York';
		usr.LocaleSidKey = 'en_US';
		usr.EmailEncodingKey = 'UTF-8';
		usr.ProfileId = p.Id;
		usr.LanguageLocaleKey = 'en_US';
		insert usr;

		Contact ct = UnitTestDataFactory.inflateContact(null, acct);
		insert ct;

		Season__c season = UnitTestDataFactory.inflateSeason(var.Default_Season__c);
		insert season;

		confirm = UnitTestDataFactory.inflateConfirmation(acct, season, ct);
		confirm.AccountManager__c = usr.Id;
		confirm.SpecialInstructions__c = 'test test';
		insert confirm;

		confirm2 = UnitTestDataFactory.inflateConfirmation(acct2, season, ct);
		insert confirm2;

		form = UnitTestDataFactory.inflateForm(confirm);
		form.Greensheet__c = True;
		form.DaysLate__c = 5;
		form.OKInstructions__c = True;
		form.OKDFL__c = True;
		form.OKShipAddress__c = True;
		form.Note__c = 'TEST 123';
		insert form;

		form2 = UnitTestDataFactory.inflateForm(confirm2);
		form2.Greensheet__c = True;
		form2.DaysLate__c = 0;
		insert form2;

		form3 = UnitTestDataFactory.inflateForm(confirm2);
		form3.Greensheet__c = True;
		form3.DaysLate__c = null;
		insert form3;

		form4 = UnitTestDataFactory.inflateForm(confirm);
		insert form4;

		form5 = UnitTestDataFactory.inflateForm(confirm);
		insert form5;
	}

	static Testmethod void testResolveRecords() {
		setup();

		List<Form__c> test = GreensheetController.resolveRecords();
		System.assertEquals(3, test.size());
	}

	static Testmethod void testManagerList() {
		setup();

		List<Form__c> testList = GreensheetController.resolveRecords();
		List<String> test = GreensheetController.managerList(testList);
		System.assertEquals(1, test.size());
	}

	static Testmethod void testItemCount() {
		setup();

		List<Form__c> testList = GreensheetController.resolveRecords();
		Integer test = GreensheetController.itemCount(testList);
		System.assertEquals(3, test);
	}

	static Testmethod void testFilterByManager() {
		setup();

		List<Form__c> testList = GreensheetController.resolveRecords();
		List<String> manager = GreensheetController.managerList(testList);

		List<Form__c> test1 = GreensheetController.filterByManager(manager.get(0));
		System.assertEquals(1, test1.size());

		List<Form__c> test2 = GreensheetController.filterByManager('');
		System.assertEquals(3, test2.size());
	}

	static Testmethod void testSelectedForm() {
		setup();

		Form__c test = GreensheetController.selectedForm(form.Id);
		System.assertEquals(form.Id, test.Id);
	}

	static Testmethod void testDaysLate() {
		setup();

		String test1 = GreensheetController.daysLate(form);
		System.assertEquals('5 Days Late' , test1);

		String test2 = GreensheetController.daysLate(form2);
		System.assertEquals('On Time', test2);

		String test3 = GreensheetController.daysLate(form3);
		System.assertEquals('', test3);
	}

	static Testmethod void testInstructionsCheckbox() {
		setup();

		String test1 = GreensheetController.instructionsCheckbox(form);
		System.assertEquals('OK', test1);

		String test2 = GreensheetController.instructionsCheckbox(form2);
		System.assertEquals('BAD!', test2);
	}

	static Testmethod void testDFLCheckbox() {
		setup();

		String test1 = GreensheetController.dflCheckbox(form);
		System.assertEquals('OK', test1);

		String test2 = GreensheetController.dflCheckbox(form2);
		System.assertEquals('BAD!', test2);
	}

	static Testmethod void testShippingAddressCheckbox() {
		setup();

		String test1 = GreensheetController.shippingAddressCheckbox(form);
		System.assertEquals('OK', test1);

		String test2 = GreensheetController.shippingAddressCheckbox(form2);
		System.assertEquals('BAD!', test2);
	}

	static Testmethod void testNotes() {
		setup();

		String test = GreensheetController.notes(form);
		System.assertEquals('TEST 123', test);
	}

	static Testmethod void testSpecialInstructions() {
		setup();

		//String test = GreensheetController.specialInstructions(f);
		//System.assertEquals('test test', test);
	}

	static Testmethod void testUpdateSpecialInstructions() {
		setup();

		//GreensheetController.updateSpecialInstructions(confirm.Id, 'new new special');
		//Confirmation__c updatedConfirm = [SELECT Id, SpecialInstructions__c FROM Confirmation__c WHERE Id =: confirm.Id];
		//System.assertEquals('new new special', updatedConfirm.SpecialInstructions__c);
	}

	static Testmethod void testUpdateReceivingNotes() {
		setup();

		GreensheetController.updateReceivingNotes(form.Id, 'new new 123');
		Form__c updatedForm = [SELECT Id, Note__c FROM Form__c WHERE Id =: form.Id];
		System.assertEquals('new new 123', updatedForm.Note__c);
	}

	static Testmethod void testResolved() {
		setup();

		GreensheetController.resolved(form.Id, 'resolved');
		Form__c test = [SELECT Id, Greensheet__c, Note__c FROM Form__c WHERE Id =: form.Id];
		System.assert(!test.Greensheet__c);
		System.assertEquals('resolved', test.Note__c);
	}
}