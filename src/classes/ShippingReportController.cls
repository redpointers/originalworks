public class ShippingReportController {
	/*
     * author: Redpoint
     * date: 4/21/2017
     * purpose: Provides information to the Shipping Report Visualforce page such as the range of dates
     *          to pull reports from as well as summary counts of total projections. 
     * notes:
    */
    public Map<Date, List<Form__c>> splitAM = new Map<Date, List<Form__c>>();
    public Map<Date, List<Form__c>> splitST = new Map<Date, List<Form__c>>();
    public List<Form__c> formsAM = new List<Form__c>();
    public List<Form__c> formsST = new List<Form__c>();
    public Form__c form = new Form__c();
    public String start {get;set;}
    public String endd {get;set;}

    public ShippingReportController(ApexPages.StandardController stdcontroller) {
    	this.form = (Form__c)stdcontroller.getRecord();
    }

    public void getshippingReportForAM() {
    	Date startDate = Date.parse(ApexPages.currentPage().getParameters().get('startrange'));
        Date endDate = Date.parse(ApexPages.currentPage().getParameters().get('endrange'));
        this.start = formatDateRanges(startDate);
        this.endd = formatDateRanges(endDate);

    	formsAM = [SELECT Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
    					Confirmation__r.RecordType.Name, WarehouseAssigned__c, ActualShip__c, 
    					EstimatedShip__c, FormType__c, FormSequence__c, MagsBought__c, 
    					OrderValueWholesale__c, MagValueWholesale__c
				FROM Form__c
				WHERE (ActualShip__c >: startDate AND ActualShip__c <: endDate) AND
						WarehouseAssigned__c = 'AM' AND 
						(FormType__c = 'A' OR FormType__c = 'B' OR FormType__c = 'C' OR FormType__c = 'D' OR FormType__c = 'F')
				ORDER BY ActualShip__c, Confirmation__r.Account__r.SchoolCode__c];
    }

    public void getshippingReportForST() {
    	Date startDate = Date.parse(ApexPages.currentPage().getParameters().get('startrange'));
        Date endDate = Date.parse(ApexPages.currentPage().getParameters().get('endrange'));

    	formsST = [SELECT Confirmation__r.Account__r.SchoolCode__c, Confirmation__r.Season__r.Name, 
    					Confirmation__r.RecordType.Name, WarehouseAssigned__c, ActualShip__c, 
    					EstimatedShip__c, FormType__c, FormSequence__c, MagsBought__c, 
    					OrderValueWholesale__c, MagValueWholesale__c 
				FROM Form__c
				WHERE (ActualShip__c >: startDate AND ActualShip__c <: endDate) AND
						WarehouseAssigned__c = 'ST' AND 
						(FormType__c = 'A' OR FormType__c = 'B' OR FormType__c = 'C' OR FormType__c = 'D' OR FormType__c = 'F')
				ORDER BY ActualShip__c, Confirmation__r.Account__r.SchoolCode__c];
    }

    public void SubListsAM() {

        for (Form__c f : formsAM) {
            List<Form__c> flist = splitAM.get(f.ActualShip__c);
            
            if(flist == null) {
                flist = new List<Form__c>();
                splitAM.put(f.ActualShip__c, flist);
            }
            flist.add(f);
        }
    }

    public void SubListsST() {

        for (Form__c f : formsST) {
            List<Form__c> flist = splitST.get(f.ActualShip__c);
            
            if(flist == null) {
                flist = new List<Form__c>();
                splitST.put(f.ActualShip__c, flist);
            }
            flist.add(f);
        }
    }

    public String getDynamicAM() {
    	SubListsAM();
    	return getDynamicTables(splitAM);
    }

    public String getDynamicST() {
    	SubListsST();
    	return getDynamicTables(splitST);
    }

    public String getDynamicTables(Map<Date, List<Form__c>> warehouse) {

        String htmlBody = '';

        for(Date d : warehouse.keySet()) {
            List<Form__c> subForm = warehouse.get(d);
            Form__c first = subForm.get(0);
            List<Double> values = TotalsByDay(subForm);
            
            htmlBody += '<tbody>';
            
            for(Form__c f : subForm) {
                htmlBody += '<tr>' + 
                            '<td class="textAlignCenter">' + f.Confirmation__r.Season__r.Name + '</td>' + 
                            '<td class="textAlignCenter">' + f.Confirmation__r.Account__r.SchoolCode__c + '</td>' +
                            '<td class="textAlignCenter">' + formatDates(f.ActualShip__c) + '</td>' + 
                            '<td class="textAlignCenter">' + formatDates(f.EstimatedShip__c) + '</td>' + 
                            '<td class="textAlignCenter">' + f.Confirmation__r.RecordType.Name + '</td>' + 
                            '<td class="textAlignCenter">' + f.FormType__c + ' ' + f.FormSequence__c + '</td>' +
                            '<td class="textAlignCenter">$' + formTotalValue(f.Confirmation__r.RecordType.Name, f.OrderValueWholesale__c, f.MagValueWholesale__c) + '</td>' +
                            '<td class="textAlignCenter">$' + f.OrderValueWholesale__c + '</td>' + 
                            '</tr>';
            }
            
            htmlBody += '<tr style="height:15px"></tr><tr>' +
                        '<td class="textAlignCenter" colspan="6">' + formatDates(first.ActualShip__c) + ' Totals' +'</td>' +
                        '<td class="textAlignCenter">$' + values.get(5).format() + '</td>' +
                        '<td class="textAlignCenter">$' + values.get(6).format() + '</td>' + 
                        '</tr>' +
                        '<tr>' +
                        '<td colspan="3"></td>' +
                        '<td>A: $' + values.get(0).format() + '</td>' +
                        '<td>B: $' + values.get(1).format() + '</td>' +
                        '<td>C: $' + values.get(2).format() + '</td>' +
                        '<td>D: $' + values.get(3).format() + '</td>' +
                        '<td>F: $' + values.get(4).format() + '</td>' + 
                        '</tr><tr style="height:15px"><td></td></tr></tbody>';
            
        }
        return htmlBody;
    }

    public List<Double> TotalsByDay(List<Form__c> formsTotal) {
    	List<Double> values = new List<Double>();
    	Double formA = 0;
    	Double formB = 0;
    	Double formC = 0;
    	Double formD = 0;
    	Double formF = 0;
    	Double total = 0;
    	Double order = 0;

    	for(Form__c f : formsTotal) {

    		if(f.OrderValueWholesale__c != null) {
    			order += f.OrderValueWholesale__c;

                if(f.Confirmation__r.RecordType.Name == 'SBYB') {
                    if(f.MagValueWholesale__c != null) {
                        total += (f.OrderValueWholesale__c + f.MagValueWholesale__c);
                    }
                } else {
                    total += f.OrderValueWholesale__c;
                } 
    		}

    		if(f.FormType__c == 'A' && f.OrderValueWholesale__c != null) {
    			formA += f.OrderValueWholesale__c;
    		}

    		if(f.FormType__c == 'B' && f.OrderValueWholesale__c != null) {
    			formB += f.OrderValueWholesale__c;
    		}

    		if(f.FormType__c == 'C' && f.OrderValueWholesale__c != null) {	
    			formC += f.OrderValueWholesale__c;
    		}

    		if(f.FormType__c == 'D' && f.OrderValueWholesale__c != null) {
    			formD += f.OrderValueWholesale__c;
    		}

    		if(f.FormType__c == 'F' && f.OrderValueWholesale__c != null) {
    			formF += f.OrderValueWholesale__c;
    		} 
    	}
    	values.add(formA);//0
    	values.add(formB);//1
    	values.add(formC);//2
    	values.add(formD);//3
    	values.add(formF);//4
    	values.add(total);//5
    	values.add(order);//6

    	return values;
    }

    public List<Double> getAMWarehouseTotals() {
        return TotalsByDay(formsAM);
    }

    public List<Double> getSTWarehouseTotals() {
        return TotalsByDay(formsST);
    }

    public Decimal formTotalValue(String type, Decimal orderVal, Decimal magVal) {
    	Decimal sum = 0;
    	if(type == 'SBYB') {
    		if(orderVal != null && magVal != null) {
    			sum = orderVal + magVal;
    		} 
    	} else {
    		sum = orderVal;
    	}
    	return sum;
    }

    public String formatDates(Date received) {
        String formatted = '';
        if(received != null) {
            formatted = String.valueOf(received.month()) + '/' + String.valueOf(received.day()) + '/' + String.valueOf(received.year());
        }
        return formatted;
    }

    public String formatDateRanges(Date range) {
        String formatted = '';

        formatted = String.valueOf(range.day()) + '-' + convertMonth(range.month()) + '-' + String.valueOf(range.year());

        return formatted;
    }

    public String convertMonth(Integer num) {
        String val;

        if(num == 1) {
            val = 'Jan';
        } else if(num == 2) {
            val = 'Feb';
        } else if(num == 3) {
            val = 'Mar';
        } else if(num == 4) {
            val = 'Apr';
        } else if(num == 5) {
            val = 'May';
        } else if(num == 6) {
            val = 'Jun';
        } else if(num == 7) {
            val = 'Jul';
        } else if(num == 8) {
            val = 'Aug';
        } else if(num == 9) {
            val = 'Sep';
        } else if(num == 10) {
            val = 'Oct';
        } else if(num == 11) {
            val = 'Nov';
        } else if(num == 12) {
            val = 'Dec';
        } 

        return val;
    }

}