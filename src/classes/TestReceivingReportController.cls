@isTest
private class TestReceivingReportController {
	/*
	 * author: Redpoint
	 * date: 4/5/2017
	 * purpose: Test coverage for the ReceivingReportController class.
	 * note: 100% test coverage as of 4/5
	*/

	static Account acct = new Account();
    static Season__c season = new Season__c();
    static Confirmation__c conf = new Confirmation__c();
    static Form__c form = new Form__c();
    static Form__c formA = new Form__c();
    static Form__c formB = new Form__c();
    static Form__c formD = new Form__c();
    static Form__c formF = new Form__c();
    static ApexPages.StandardController sc = new ApexPages.StandardController(form);
    static ReceivingReportController rrc = new ReceivingReportController(sc);

	static void setup() {
		UnitTestDataFactory.insertVariables();

        Variables__c var = Variables__c.getOrgDefaults();
        
		acct.Name = 'Unit Test Account';
        season.Name = var.Default_Season__c;
        RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'OP' LIMIT 1];
        
        insert acct;
        insert season;

        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        conf.OWYReceivesArtwork__c = Date.newInstance(1960, 2, 3);
        conf.OWYReceivesOrdersArtwork__c = Date.newInstance(1960, 4, 5);
        insert conf;

        form.Name = 'Unit Test';
        form.Confirmation__c = conf.Id;
        form.FormType__c = 'A';
        form.ActualReceive__c = Date.newInstance(1960, 5, 1);
        insert form;

        formA.Name = 'Unit Test';
        formA.Confirmation__c = conf.Id;
        formA.FormType__c = 'A';
        formA.ActualReceive__c = Date.newInstance(1960, 5, 1);
        insert formA;

        formB.Name = 'Unit Test';
        formB.Confirmation__c = conf.Id;
        formB.FormType__c = 'B';
        formB.ActualReceive__c = Date.newInstance(1960, 7, 1);
        insert formB;

        formD.Name = 'Unit Test';
        formD.Confirmation__c = conf.Id;
        formD.FormType__c = 'D';
        formD.ActualReceive__c = Date.newInstance(1960, 3, 1);
        insert formD;

        formF.Name = 'Unit Test';
        formF.Confirmation__c = conf.Id;
        formF.FormType__c = 'F';
        formF.ActualReceive__c = Date.newInstance(1960, 6, 1);
        insert formF;

        ApexPages.currentPage().getParameters().put('startrange', '1/1/1960');
		ApexPages.currentPage().getParameters().put('endrange', '10/1/1960');
	}

	static Testmethod void testGetReport() {
		setup();

		String test = rrc.getReport().getURL();
		System.assertEquals('/apex/ReceivingReport', test);
	}

	static Testmethod void testGetFormRecords() {
		setup();
		rrc.getFormRecords();

		List<Form__c> test = rrc.forms;
		System.assertEquals('1-Jan-1960', rrc.start);
		System.assertEquals('1-Oct-1960', rrc.endd);
		System.assertEquals(5, test.size());
	}

	static Testmethod void testSubLists() {
		setup();
		rrc.getFormRecords();

		rrc.SubLists();
		Map<Date, List<Form__c>> test = rrc.split;
		Set<Date> testKeys = test.keySet();
		
		System.assert(!rrc.split.IsEmpty());
		System.assert(testKeys.contains(Date.newInstance(1960, 5, 1)));
	}

	static Testmethod void testGetDynamicTables() {
		setup();
		rrc.getFormRecords();

		String test = rrc.getDynamicTables();
		System.assertNotEquals('', test);
	}

	static Testmethod void testDisplayValue() {
		setup();
		rrc.getFormRecords();

		String test = rrc.displayValue('');
		System.assertEquals('', test);

		String test2 = rrc.displayValue('test');
		System.assertEquals('test', test2);
	}

	static Testmethod void testByDay() {
		setup();
		rrc.getFormRecords();

		List<Form__c> test = new List<Form__c>();
		test.add(form);
		test.add(formA);
		test.add(formB);
		test.add(formD);
		test.add(formF);

		String testDay = rrc.byDay(test);
		System.assertEquals('5/1/1960', testDay);
	}

	static Testmethod void testConvertMonth() {
		setup();
		rrc.getFormRecords();

		String test1 = rrc.convertMonth(1);
		String test2 = rrc.convertMonth(2);
		String test3 = rrc.convertMonth(3);
		String test4 = rrc.convertMonth(4);
		String test5 = rrc.convertMonth(5);
		String test6 = rrc.convertMonth(6);
		String test7 = rrc.convertMonth(7);
		String test8 = rrc.convertMonth(8);
		String test9 = rrc.convertMonth(9);
		String test10 = rrc.convertMonth(10);
		String test11 = rrc.convertMonth(11);
		String test12 = rrc.convertMonth(12);

		System.assertEquals('Jan', test1);
		System.assertEquals('Feb', test2);
		System.assertEquals('Mar', test3);
		System.assertEquals('Apr', test4);
		System.assertEquals('May', test5);
		System.assertEquals('Jun', test6);
		System.assertEquals('Jul', test7);
		System.assertEquals('Aug', test8);
		System.assertEquals('Sep', test9);
		System.assertEquals('Oct', test10);
		System.assertEquals('Nov', test11);
		System.assertEquals('Dec', test12);
	}

	static Testmethod void testGetTotalSchoolCount() {
		setup();
		rrc.getFormRecords();

		Integer test = rrc.getTotalSchoolCount();
		System.assertEquals(1,test);
	}
}