/*
This is a container for the records in the Heroku database.
*/
public with sharing class Orderlines 
{
	@AuraEnabled
	public Long lineno{get;set;}
    
    @AuraEnabled
    public String formid{get;set;}
    
    @AuraEnabled
    public String seasonid{get;set;}
    
    @AuraEnabled
    public String suffix{get;set;}
    
    @AuraEnabled
    public String hv{get;set;}
    
    @AuraEnabled
    public String paymt{get;set;}
    
    @AuraEnabled
    public String code{get;set;}
    
    @AuraEnabled
    public String description{get;set;}
    
    @AuraEnabled
    public Integer qty{get;set;}
    
    @AuraEnabled
    public Double price{get;set;}

    @AuraEnabled
    public Double wholesale{get;set;}

    @AuraEnabled
    public Double total{get;set;}

    @AuraEnabled
    public Double grandtotal{get;set;}

    @AuraEnabled
    public Double salestax{get;set;}

    @AuraEnabled
    public Double shipping{get;set;}

    @AuraEnabled
    public Double discount{get;set;}

    @AuraEnabled
    public String shipvia{get;set;}
    
    @AuraEnabled
    public Id createdid{get;set;}
    
    @AuraEnabled
    public DateTime createddate{get;set;}
    
    @AuraEnabled
    public Id lastmodid{get;set;}
    
    @AuraEnabled
    public DateTime lastmoddate{get;set;}
}