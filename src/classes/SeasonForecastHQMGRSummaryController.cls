public class SeasonForecastHQMGRSummaryController {
	/*
     * author: Redpoint
     * date: 3/30/2017
     * purpose: Pulls the data from the Confirmation, Account, and Contact 
     *			object to create the Season Forecast HQ MGR Summary Report report. 
     * notes:
    */
    Map<String, List<Confirmation__c>> subLists = new Map<String, List<Confirmation__c>>();
    public List<Confirmation__c> confirmations = new List<Confirmation__c>();
    public Confirmation__c confirmation = new Confirmation__c();
    public String season {get;set;}
    public String status {get;set;}

	public SeasonForecastHQMGRSummaryController(ApexPages.StandardController stdcontroller) {
		this.confirmation = (Confirmation__c)stdcontroller.getRecord();
        this.season = ApexPages.currentPage().getParameters().get('season');
        this.status = ApexPages.currentPage().getParameters().get('status');
	}

	public void getqueryConfirmations() {
    	confirmations = [SELECT Id, SeasonPopEstimate__c, Projection__c, SalesRep__r.FirstName, SalesRep__r.LastName, 
                            SalesRep__r.HQMGR__r.FirstName, SalesRep__r.HQMGR__r.LastName, SalesRep__r.HQMGR__r.Name, 
                            RecordType.Name, Account__r.BillingState
    				FROM Confirmation__c
    				WHERE (Season__r.Name =: season) AND (SalesRep__c != null) AND (Status__c =: status)
    				ORDER BY SalesRep__r.HQMGR__r.LastName];
    }

    public Map<String, List<Confirmation__c>> groupByReps() {
    	getqueryConfirmations();

    	for(Confirmation__c c : confirmations) {
    		List<Confirmation__c> clist = subLists.get(c.SalesRep__r.HQMGR__r.Name);

    		if(clist == null) {
    			clist = new List<Confirmation__c>();
    			subLists.put(c.SalesRep__r.HQMGR__r.Name, clist);
    		}
    		clist.add(c);
    	}
    	return subLists;
    }

     public String getHouseReps() {
        groupByReps();

        String tables = '';

        for(String s : subLists.keySet()) {
            
            List<Confirmation__c> summaryLists = subLists.get(s);
            Confirmation__c salesrep = summaryLists.get(0);
            List<Double> counts = totalCounts(summaryLists);

            tables += '<table><thead>' +
                        '<tr>' +
                            '<th colspan="9">HQ MGR: ' + salesrep.SalesRep__r.HQMGR__r.LastName + ', ' + salesrep.SalesRep__r.HQMGR__r.FirstName + '</th>' +
                        '</tr>' +
                        '<tr>' +
                            '<th></th>' +
                            '<th></th>' +
                            '<th class="textAlign">Population</th>' +
                            '<th class="textAlign">SBYB</th>' +
                            '<th class="textAlign">SS SBYB</th>' +
                            '<th class="textAlign">OP</th>' +
                            '<th class="textAlign">GALLERY</th>' +
                            '<th class="textAlign">Tile Wall</th>' +
                            '<th class="textAlign">Total</th>' +
                        '</tr>' +
                  '</thead>' + 
                  '<tbody>';
                  
        for(Confirmation__c c : summaryLists) {

            tables += '<tr>' +
                '<td>' + c.SalesRep__r.LastName + ', ' + c.SalesRep__r.FirstName + '</td>' + 
                '<td class="textAlign">' + c.Account__r.BillingState + '</td>' +
                '<td class="textAlign">' + c.SeasonPopEstimate__c + '</td>' +
                '<td class="textAlign">$' + (c.RecordType.Name == 'SBYB' && c.Projection__c != null ? c.Projection__c : 0.00) + '</td>' +
                '<td class="textAlign">$' + (c.RecordType.Name == 'SS SBYB' && c.Projection__c != null ? c.Projection__c : 0.00) + '</td>' +
                '<td class="textAlign">$' + (c.RecordType.Name == 'OP' && c.Projection__c != null ? c.Projection__c : 0.00) + '</td>' +
                '<td class="textAlign">$' + (c.RecordType.Name == 'Gallery' && c.Projection__c != null ? c.Projection__c : 0.00) + '</td>' +
                '<td class="textAlign">$' + (c.RecordType.Name == 'Tile Wall' && c.Projection__c != null ? c.Projection__c : 0.00) + '</td>' +
                '<td class="textAlign">$' + (c.Projection__c != null ? c.Projection__c : 0.00) + '</td>' + 
            '</tr>'; 
        }

                tables += '<tr>' + 
                        '<td class="col1 totalBorder">HQ MGR Totals: </td>' +
                        '<td class="col2 totalBorder"></td>' +
                        '<td class="col3 totalBorder">' + counts.get(5).format() + '</td>' +
                        '<td class="col4 totalBorder">$' + counts.get(6).format() + '</td>' +
                        '<td class="col5 totalBorder">$' + counts.get(7).format() + '</td>' +
                        '<td class="col6 totalBorder">$' + counts.get(8).format() + '</td>' +
                        '<td class="col7 totalBorder">$' + counts.get(9).format() + '</td>' +
                        '<td class="col8 totalBorder">$' + counts.get(10).format() + '</td>' +
                        '<td class="col9 totalBorder">$' + counts.get(11).format() + '</td>' +
                    '</tr>' +
                    '</tbody></table>';   
        }
        return tables;
    }

    public Map<String, List<Double>> getDisplayReps() {
        groupByReps();
        Map<String, List<Double>> trythis = new Map<String, List<Double>>();

        for(String s : subLists.keySet()) {
            
            List<Confirmation__c> summaryLists = subLists.get(s);
            Confirmation__c salesrep = summaryLists.get(0);
            List<Double> counts = totalCounts(summaryLists);

            String rep = salesrep.SalesRep__r.HQMGR__r.LastName + ', ' + salesrep.SalesRep__r.HQMGR__r.FirstName;

            trythis.put(rep,counts);
        }
        return trythis;
    }

    public List<Double> totalCounts(List<Confirmation__c> conf) {
    	List<Double> sumList = new List<Double>();
    	Double sbybPop = 0;
        Double sssbybPop = 0;
        Double opPop = 0;
        Double galleryPop = 0;
        Double tilePop = 0;
        Double popSum = 0;
    	Double sbybSum = 0;
        Double sssbybSum = 0;
    	Double opSum = 0;
    	Double gallerySum = 0;
        Double tileSum = 0;
    	Double totalSum = 0;

    	for(Confirmation__c c : conf) {
    		if(c.SeasonPopEstimate__c != null) {
    			popSum += c.SeasonPopEstimate__c;
    		}

    		if(c.RecordType.Name == 'SBYB' && c.Projection__c != null) {
    			sbybSum += c.Projection__c;
    		}

            if(c.RecordType.Name == 'SS SBYB' && c.Projection__c != null) {
                sssbybSum += c.Projection__c;
            }

    		if(c.RecordType.Name == 'OP' && c.Projection__c != null) {
    			opSum += c.Projection__c;
    		}

    		if(c.RecordType.Name == 'Gallery' && c.Projection__c != null) {
    			gallerySum += c.Projection__c;
    		}

            if(c.RecordType.Name == 'Tile Wall' && c.Projection__c != null) {
                tileSum += c.Projection__c;
            }

    		if(c.Projection__c != null) {
    			totalSum += c.Projection__c;
    		}

            if(c.RecordType.Name == 'SBYB' && c.SeasonPopEstimate__c != null) {
                sbybPop += c.SeasonPopEstimate__c;
            }

            if(c.RecordType.Name == 'OP' && c.SeasonPopEstimate__c != null) {
                opPop += c.SeasonPopEstimate__c;
            }

            if(c.RecordType.Name == 'Gallery' && c.SeasonPopEstimate__c != null) {
                galleryPop += c.SeasonPopEstimate__c;
            }

            if(c.RecordType.Name == 'SS SBYB' && c.SeasonPopEstimate__c != null) {
                sssbybPop += c.SeasonPopEstimate__c;
            }

            if(c.RecordType.Name == 'Tile Wall' && c.SeasonPopEstimate__c != null) {
                tilePop += c.SeasonPopEstimate__c;
            }
    	}

        sumList.add(sbybPop);//0
        sumList.add(sssbybPop);//1
        sumList.add(opPop);//2
        sumList.add(galleryPop);//3
        sumList.add(tilePop);//4
    	sumList.add(popSum);//5
    	sumList.add(sbybSum);//6
        sumList.add(sssbybSum);//7
    	sumList.add(opSum);//8
    	sumList.add(gallerySum);//9
        sumList.add(tileSum);//10
    	sumList.add(totalSum);//11

    	return sumList;
    }

    public List<Double> getgrandTotals() {
        List<Double> sumList = new List<Double>();
        Double popSum = 0;
        Double sbybSum = 0;
        Double sssbybSum = 0;
        Double opSum = 0;
        Double gallerySum = 0;
        Double tileSum = 0;
        Double totalSum = 0;
        Double percent = 0;

        for(Confirmation__c c : confirmations) {

            if(c.SeasonPopEstimate__c != null) {
                popSum += c.SeasonPopEstimate__c;
            }

            if(c.RecordType.Name == 'SBYB' && c.Projection__c != null) {
                sbybSum += c.Projection__c;
            }

            if(c.RecordType.Name == 'SS SBYB' && c.Projection__c != null) {
                sssbybSum += c.Projection__c;
            }

            if(c.RecordType.Name == 'OP' && c.Projection__c != null) {
                opSum += c.Projection__c;
            }

            if(c.RecordType.Name == 'Gallery' && c.Projection__c != null) {
                gallerySum += c.Projection__c;
            }

            if(c.RecordType.Name == 'Tile Wall' && c.Projection__c != null) {
                tileSum += c.Projection__c;
            }

            if(c.Projection__c != null) {
                totalSum += c.Projection__c;
            }
        }

        if(totalSum != 0) {
            percent = (opSum / totalSum) * 100;
        }

        sumList.add(popSum);//0
        sumList.add(sbybSum);//1
        sumList.add(sssbybSum);//2
        sumList.add(opSum);//3
        sumList.add(gallerySum);//4
        sumList.add(tileSum);//5
        sumList.add(totalSum);//6
        sumList.add(percent);//7
        return sumList;
    }    
}