public with sharing class PlanetPressApexController 
{
	@AuraEnabled
	public static List<Form__c> getForms()
	{
		List<String> formTypes = new List<String>{'B','C','D','F'};
		List<String> warehouses = new List<String>{'AM','ST'};

		List<Form__c> forms = [select Id, Name, FormType__c, ActualShip__c,WarehouseAssigned__c,FormSequence__c,MagsBought__c,
			WarehouseTransferDate__c, isScanned__c, isPPFileCreated__c, Confirmation__r.Account__r.SchoolCode__c from Form__c where FormType__c in : formTypes and
			ActualShip__c = null and WarehouseAssigned__c in : warehouses and (isPPFileCreated__c = false or isPPFileCreated__c = null)];
		return forms;
	}

	@AuraEnabled
	public static Boolean getMarkScanned(String formId, Boolean mark)
	{
		try
		{
			Form__c form = [select Id, Name, isScanned__c from Form__c where Id =: formId];
			form.isScanned__c = mark;

			update form;
		}
		catch(Exception e)
		{
			throw new AuraException('The Form\'s scan status could not be updated. Please see your System Administrator');
		}

		return true;
	}

	@AuraEnabled
	public static Boolean getMarkFileCreated(String formId, Boolean mark)
	{
		try
		{
			Form__c form = [select Id, Name, isPPFileCreated__c from Form__c where Id =: formId];
			form.isPPFileCreated__c = mark;

			update form;
		}
		catch(Exception e)
		{
			throw new AuraException('The Form\'s file creation status could not be updated. Please see your System Administrator');
		}

		return true;
	}

	@AuraEnabled
	public static String getExport(String formId, String schoolCode)
	{
		String retStr = null;
		try
		{
			List<Planet_Press_Export__c> ppList = Planet_Press_Export__c.getAll().values();

			String file = 'data:text/csv;charset=utf-8,';

			String header = 'Code,Suffix,Orientation,';
			for(Integer i=0;i<=69;i++)
			{
				String key = PlanetPressApexController.getItemAtColumn(ppList, i);
				header = header + key + ',';
			}
			header = header + '\n';

			retStr = file+header;

			List<OrderLines> oLines = OrderEntryComponentApexController.getOrdersForForm(formId);
			
			if(oLines!=null && oLines.size() > 0)
			{
				Map<String, Map<String,Integer>> suffixMap = new Map<String, Map<String,Integer>>();
				Set<String> uniqueSuffixes = PlanetPressApexController.getUniqueSuffixes(oLines);
				Map<String,String> suffixOrientMap = PlanetPressApexController.getSuffixOrients(uniqueSuffixes,oLines);

				for(String suffix : uniqueSuffixes)
				{
					Map<String,Integer> itemMap = setDefaultMapValues(ppList);
					suffixMap.put(suffix, itemMap);
				}

				for(OrderLines line : oLines)
				{
					System.debug('**** Line: '+line.suffix+' code: '+line.code);
					Map<String,Integer> itemMap = suffixMap.get(line.suffix);
					Integer mapCount = itemMap.get(line.code);
					mapCount = mapCount + line.qty;
					itemMap.put(line.code, mapCount);

					Map<String, Integer> kitQtys = PlanetPressApexController.handleKits(line.code, ppList, line.qty);
					if(kitQtys != null) // line code is a kit
					{
						for(String itemKey : kitQtys.keySet())
						{
							mapCount = itemMap.get(itemKey);
							mapCount = mapCount + kitQtys.get(itemKey);
							itemMap.put(itemKey, mapCount);
						}
					}
				}

				List<String> suffixLines = PlanetPressApexController.getSuffixLines(suffixMap, ppList, schoolCode, suffixOrientMap);
				for(String sl : suffixLines)
					retStr = retStr + sl;
			}
		}
		catch(Exception e)
		{
			throw new AuraException('A Planet Press file could not be created. Please see your System Administrator '+e.getStackTraceString());
		}

		try
		{
			Boolean success = PlanetPressApexController.getMarkFileCreated(formId,true);
		}
		catch(Exception e)
		{
			throw new AuraException(e.getMessage());
		}

		return retStr;
	}

	private static String getItemAtColumn(List<Planet_Press_Export__c> exps, Integer column)
	{
		for(Planet_Press_Export__c exp : exps)
		{
			if(exp.Column__c == column)
				return exp.Name;
		}

		return null;
	}

	private static Set<String> getUniqueSuffixes(List<OrderLines> oLines)
	{
		Set<String> sufs = new Set<String>();
		for(OrderLines line : oLines)
		{
			if(!sufs.contains(line.suffix))
				sufs.add(line.suffix);
		}

		return sufs;
	}

	private static Map<String,String> getSuffixOrients(Set<String> uniqueSuffixes, List<OrderLines> oLines)
	{
		Map<String,String> orientMap = new Map<String,String>();
		for(String suf : uniqueSuffixes)
		{
		 	for(OrderLines ol : oLines) // there should only be one orientation for a suffix. Grab the first one.
		 	{
		 		if(ol.suffix == suf)
		 		{
		 			orientMap.put(suf,ol.hv);
		 			break;
		 		}
		 	}
		}

		return orientMap;
	}

	private static Map<String,Integer> setDefaultMapValues(List<Planet_Press_Export__c> ppList)
	{
		Map<String,Integer> itemMap = new Map<String,Integer>();

		for(Planet_Press_Export__c exp : ppList)
		{
			itemMap.put(exp.Name, 0);
		}

		return itemMap;
	}

	private static List<String> getSuffixLines(Map<String, Map<String,Integer>> suffixMap, List<Planet_Press_Export__c> ppList, String schoolCode, Map<String,String> suffixOrientMap)
	{
		List<String> lines = new List<String>();
		for(String suffix : suffixMap.keySet())
		{
			Map<String,Integer> sufData = suffixMap.get(suffix);
			String orientation = suffixOrientMap.get(suffix);
			String line = '"'+schoolCode+'","'+suffix+'","'+orientation+'",';

			for(Integer i=0;i<=69;i++)
			{
				String item = PlanetPressApexController.getItemAtColumn(ppList, i);
				Integer itemCt = sufData.get(item);
				line = line + '"'+itemCt+'",';
			}
			line = line + '\n';
			lines.add(line);
		}

		return lines;
	}

	private static Map<String, Integer> handleKits(String item, List<Planet_Press_Export__c> ppList, Integer qty)
	{
		if(item == '24NC')
		{
			Map<String, Integer> colQtyMap = new Map<String, Integer>();
			colQtyMap.put('5NOTE',3*qty);
			return colQtyMap;
		}
		else if(item == 'OP')
		{
			Map<String, Integer> colQtyMap = new Map<String, Integer>();
			colQtyMap.put('MPAD',1*qty);
			colQtyMap.put('GM',1*qty);
			colQtyMap.put('JL',1*qty);
			return colQtyMap;
		}
		else if(item == 'WRAP')
		{
			Map<String, Integer> colQtyMap = new Map<String, Integer>();
			colQtyMap.put('5NOTE',1*qty);
			colQtyMap.put('SB',1*qty);
			colQtyMap.put('JL',1*qty);
			return colQtyMap;
		}
		else if(item == 'PM')
		{
			Map<String, Integer> colQtyMap = new Map<String, Integer>();
			colQtyMap.put('3M',2*qty);
			colQtyMap.put('MW6 x 5',1*qty);
			return colQtyMap;
		}

		return null;
	}
}