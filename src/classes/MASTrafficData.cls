public with sharing class MASTrafficData 
{
	@AuraEnabled
    public List<Form__c> forms { get;set; }

    @AuraEnabled
    public List<Id> formIds { get;set; } // used to pass back for CSV output. Can't serialize the forms with their orders, so we'll send back IDs when needed

    @AuraEnabled
    public List<Confirmation__c> confirms { get;set; }

    @AuraEnabled
    public List<Account> accounts { get;set; }

    @AuraEnabled
    public Integer numSchools{get;set;}

    @AuraEnabled
    public Integer numForms{get;set;}

    @AuraEnabled
    public Integer numTaxes{get;set;}

    @AuraEnabled
    public Set<Account> taxAccounts { get;set; }

    @AuraEnabled
    public Integer numQuestions{get;set;}

    @AuraEnabled
    public Set<Form__c> questionForms { get;set; }

    @AuraEnabled
    public Integer numSalesReps{get;set;}

    @AuraEnabled
    public Set<Confirmation__c> saleRepConfirms { get;set; }

    @AuraEnabled
    public Integer numNoMags{get;set;}

    @AuraEnabled
    public Set<Form__c> noMagsForms { get;set; }

    @AuraEnabled
    public Integer numHighMags{get;set;}

    @AuraEnabled
    public Set<Confirmation__c> hiMagsConfirms { get;set; }

    @AuraEnabled
    public Boolean goodData{get;set;}

	public MASTrafficData(List<Form__c> informs, List<Confirmation__c> inconfirms, List<Account> inaccounts) 
    {
        this.forms = informs;
        this.confirms = inconfirms;
        this.accounts = inaccounts;

        this.numSchools = calcSchoolsNumber();
        this.numForms = calcFormsNumber();
        this.numTaxes = calcTaxNumber();
        this.numQuestions = calcQuestionNumber();
        this.numSalesReps = calcSalesRepsNumber();
        this.numNoMags = calcNoMagsNumber();
        this.numHighMags = calcHiMagsNumber();
        this.goodData = isGoodData();

        formIds = new List<Id>();
        for(Form__c form : forms)
            formIds.add(form.Id);
    }

    public Boolean isGoodData()
    {
        if(numSchools >0 || numForms >0 || numTaxes >0 || numQuestions >0 || numSalesReps>0 || numNoMags>0 || numHighMags >0)
            return true;
        else
            return false;
    }

    private Integer calcSchoolsNumber()
    {
        if(accounts != null && accounts.size()>0)
            return accounts.size();
        else
            return 0;
    }

    private Integer calcFormsNumber()
    {
        if(forms != null && forms.size()>0)
            return forms.size();
        else
            return 0;
    }

    private Integer calcQuestionNumber()
    {
        Integer foundQuestion = 0;

        if(forms != null && forms.size()>0)
        {
            questionForms = new Set<Form__c>();

            for(Form__c form : forms)
            {
                if(form.QuestionFolders__c != null && form.QuestionFolders__c > 0)
                {
                    foundQuestion++;
                    questionForms.add(form);
                }
            }
        }
        
        return foundQuestion;
    }

    private Integer calcTaxNumber()
    {
        Integer foundTax = 0;
        taxAccounts = new Set<Account>();
        Variables__c vars = Variables__c.getInstance();

        if(accounts != null && accounts.size()>0)
        {
            for(Account acct : accounts)
            {
                System.debug('**** Exempt: '+acct.TaxExempt__c);
                System.debug('**** TaxExemptReceived__c: '+acct.TaxExemptReceived__c);
                System.debug('**** TaxExemptValidUntil__c: '+acct.TaxExemptValidUntil__c);
                System.debug('**** TaxCode__c: '+acct.TaxCode__c);
                System.debug('**** BillingState: '+acct.BillingState);

                if((acct.TaxExempt__c !=null && acct.TaxExempt__c) && (acct.TaxExemptReceived__c == null || acct.TaxExemptValidUntil__c == null || acct.TaxExemptValidUntil__c < System.today()))
                {
                    foundTax++;
                    taxAccounts.add(acct);
                }
                else if((acct.TaxExempt__c == null || !acct.TaxExempt__c || String.isEmpty(acct.TaxCode__c)) && (vars.Tax_States__c.indexOf(acct.BillingState) >= 0))
                {
                    foundTax++;
                    taxAccounts.add(acct);
                }

            }
        }

        return foundTax;
    }

    private Integer calcSalesRepsNumber()
    {
        Integer foundReps = 0;

        if(confirms != null && confirms.size()>0)
        {
            Set<Id> uniqueReps = new Set<Id>();
            saleRepConfirms = new Set<Confirmation__c>();

            for(Confirmation__c confirm : confirms)
            {
                if(confirm.SalesRep__c!=null && !uniqueReps.contains(confirm.SalesRep__c))
                {
                    uniqueReps.add(confirm.SalesRep__c);
                    saleRepConfirms.add(confirm);
                }
            }

            foundReps = uniqueReps.size();
        }

        return foundReps;

    }

    private Integer calcNoMagsNumber()
    {
        Integer foundNoMag = 0;

        if(forms != null && forms.size()>0)
        {
            noMagsForms = new Set<Form__c>();

            for(Form__c form : forms)
            {
                if(form.FormType__c == 'B' && form.FormSequence__c == 1 && (form.MagsBought__c == null || form.MagsBought__c <= 0))
                {
                    foundNoMag++;
                    noMagsForms.add(form);
                }    
            }
        }
        
        return foundNoMag;
    }

    private Integer calcHiMagsNumber()
    {
        Integer foundHiMag = 0;

        if(confirms != null && confirms.size() >0 && forms != null && forms.size()>0)
        {
            hiMagsConfirms = new Set<Confirmation__c>();

            for(Confirmation__c confirm : confirms)
            {
                List<Form__c> formsInConfirm = new List<Form__c>();
                for(Form__c form : forms)
                {
                    if(form.Confirmation__c == confirm.Id)
                        formsInConfirm.add(form);
                }

                if(formsInConfirm.size() > 0)
                {
                    Integer formBMags = 0;
                    Integer formAArtCount = 0;

                    for(Form__c form : formsInConfirm)
                    {
                        if(form.FormType__c == 'B' && form.FormSequence__c == 1)
                            formBMags = (Integer)form.MagsBought__c;
                        else if (form.FormType__c == 'A' && form.FormSequence__c == 1)
                            formAArtCount = (Integer)form.ArtworkCountOE__c;
                    }

                    if(formBMags > formAArtCount)
                    {
                        foundHiMag++;
                        hiMagsConfirms.add(confirm);
                    }
                }
            }
        }
        
        return foundHiMag;
    }
}