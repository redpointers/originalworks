@isTest
private class TestPerCapReportController {
	/*
	* author: Redpoint
	* date: 4/25/2017
	* purpose: Unit tests for the Per Cap Report Controller
	* note: 93% code coverage as of 4/25
	*/

	static Confirmation__c conf = new Confirmation__c();
    static Confirmation__c confO = new Confirmation__c();
    static Confirmation__c confT = new Confirmation__c();
    static Confirmation__c confG = new Confirmation__c();
    static Season__c season = new Season__c();
    static Account acct = new Account();
    static Account acct2 = new Account();
    static Account acct3 = new Account();
    static Account acct4 = new Account();
    static Form__c form = new Form__c();
    static Form__c formS = new Form__c();
    static Form__c formA = new Form__c();
    static Form__c formB = new Form__c();
    static Form__c formC = new Form__c();
    static Form__c formD = new Form__c();
    static Form__c formF = new Form__c();

	static void setup() {
		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP17';
        upsert var;

		acct.Name = 'Unit Test Account';
        insert acct;

        acct2.Name = 'Unit Test Account 2';
        insert acct2;

        acct3.Name = 'Unit Test Account 3';
        insert acct3;

        acct4.Name = 'Unit Test Account 4';
        insert acct4;

        season.Name = var.Default_Season__c;
        insert season;

        RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'SBYB' LIMIT 1];
        
        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        conf.Status__c = 'Active';
        conf.SeasonPopEstimate__c = 2;
        insert conf;

        RecordType recordtype2 = [SELECT Id FROM RecordType WHERE Name = 'OP' LIMIT 1];

        confO.RecordTypeId = recordtype2.Id;
        confO.Account__c = acct2.Id;
        confO.Season__c = season.Id;
        confO.Status__c = 'Active';
        confO.SeasonPopEstimate__c = 3;
        insert confO;

        RecordType recordtype3 = [SELECT Id FROM RecordType WHERE Name = 'Tile Wall' LIMIT 1];

        confT.RecordTypeId = recordtype3.Id;
        confT.Account__c = acct3.Id;
        confT.Season__c = season.Id;
        confT.Status__c = 'Active';
        confT.SeasonPopEstimate__c = 4;
        insert confT;

        RecordType recordtype4 = [SELECT Id FROM RecordType WHERE Name = 'Gallery' LIMIT 1];

        confG.RecordTypeId = recordtype4.Id;
        confG.Account__c = acct4.Id;
        confG.Season__c = season.Id;
        confG.Status__c = 'Active';
        confG.SeasonPopEstimate__c = 5;
        insert confG;

        formF.Name = 'Unit Test';
        formF.Confirmation__c = conf.Id;
        formF.FormType__c = 'A';
        formF.ArtworkCount__c = 2;
        formF.OrderCount__c = 2;
        formF.OrderValueWholesale__c = 6;
        insert formF;

        form.Name = 'Unit Test Form';
        form.Confirmation__c = conf.Id;
        form.FormType__c = 'B';
        form.ArtworkCount__c = 2;
        form.OrderCount__c = 2;
        form.MagValueWholesale__c = 1;
        form.OrderValueWholesale__c = 2;
        insert form;

        formS.Name = 'Unit Test';
        formS.Confirmation__c = confO.Id;
        formS.FormType__c = 'D';
        formS.ArtworkCount__c = 2;
        formS.OrderCount__c = 1;
        formS.MagValueWholesale__c = 1;
        formS.OrderValueWholesale__c = 2;
        insert formS;

        formA.Name = 'Unit Test';
        formA.Confirmation__c = confT.Id;
        formA.FormType__c = 'D';
        formA.ArtworkCount__c = 2;
        formA.OrderCount__c = 3;
        formA.OrderValueWholesale__c = 2;
        insert formA;

        formB.Name = 'Unit Test';
        formB.Confirmation__c = confG.Id;
        formB.FormType__c = 'E';
        formB.ArtworkCount__c = 2;
        formB.OrderCount__c = 2;
        formB.OrderValueWholesale__c = 3;
        insert formB;

        formC.Name = 'Unit Test';
        formC.Confirmation__c = confG.Id;
        formC.FormType__c = 'G';
        formC.ArtworkCount__c = 4;
        formC.OrderCount__c = 2;
        formC.OrderValueWholesale__c = 4;
        insert formC;

        formD.Name = 'Unit Test';
        formD.Confirmation__c = confO.Id;
        formD.FormType__c = 'D';
        formD.ArtworkCount__c = 2;
        formD.OrderCount__c = 2;
        formD.OrderValueWholesale__c = 5;
        insert formD;

        ApexPages.currentPage().getParameters().put('season', season.Name);
	}

	static Testmethod void testSeasonConfirmations() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(conf);
		PerCapReportController pcr = new PerCapReportController(sc);

		pcr.seasonConfirmations();

		System.assertEquals(4, pcr.confirmations.size());
	}

	static Testmethod void testSeasonForms() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(conf);
		PerCapReportController pcr = new PerCapReportController(sc);

		pcr.seasonForms();

		System.assert(pcr.forms.size() > 0);
	}

	static Testmethod void testSchoolCount() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(conf);
		PerCapReportController pcr = new PerCapReportController(sc);

		pcr.seasonConfirmations();
		pcr.schoolCount();

		System.assertEquals(1, pcr.sbybCount);
		System.assertEquals(1, pcr.opCount);
		System.assertEquals(1, pcr.tileCount);
		System.assertEquals(1, pcr.galleryCount);

	}

    static Testmethod void testEstimatedPopulations() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        PerCapReportController pcr = new PerCapReportController(sc);

        pcr.seasonConfirmations();
        pcr.estimatedPopulations();

        System.assertEquals(2,pcr.sbybEstPop);
        System.assertEquals(3,pcr.opEstPop);
        System.assertEquals(4,pcr.tileEstPop);
        System.assertEquals(5,pcr.galleryEstPop);
    }

    static Testmethod void testActualPopulation() {
        setup();

        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        PerCapReportController pcr = new PerCapReportController(sc);

        pcr.seasonForms();
        pcr.actualPopulation();

        System.assertEquals(2,pcr.sbybActualPop);
        System.assertEquals(3,pcr.opActualPop);
        System.assertEquals(3,pcr.tileActualPop);        
        System.assertEquals(4,pcr.galleryActualPop);
    }

    static Testmethod void testTotalOrdered() {
        setup();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        PerCapReportController pcr = new PerCapReportController(sc);

        pcr.seasonForms();
        pcr.totalOrdered();

        System.assertEquals(9,pcr.sbybTotal);
        System.assertEquals(7,pcr.opTotal);
        System.assertEquals(2,pcr.tileTotal);
        System.assertEquals(7,pcr.galleryTotal);        
    }

    static Testmethod void testGetPerCap() {
        setup();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        PerCapReportController pcr = new PerCapReportController(sc);

        pcr.seasonForms();
        pcr.actualPopulation();
        pcr.totalOrdered();

        Double test1 = pcr.getSBYBPerCap();
        Double test2 = pcr.getOPPercap();
        Double test3 = pcr.getTilePercap();
        Double test4 = pcr.getGalleryPercap();

        System.assertEquals(4.5,test1);
        System.assertEquals(2,test2.round());
        System.assertEquals(1,test3.round());
        System.assertEquals(1.75,test4);
    }

    static Testmethod void testGetSBYBAverageOrderValue() {
        setup();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        PerCapReportController pcr = new PerCapReportController(sc);

        pcr.seasonForms();
        pcr.totalOrdered();

        Double test = pcr.getSBYBAverageOrderValue();

        System.assertEquals(4.5, test);
    }

    static Testmethod void testGetGalleryAverageOrderValue() {
        setup();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(conf);
        PerCapReportController pcr = new PerCapReportController(sc);

        pcr.seasonForms();
        pcr.totalOrdered();

        Double test = pcr.getGalleryAverageOrderValue();

        System.assertEquals(1.75, test);
    }
}