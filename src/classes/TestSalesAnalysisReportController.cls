@isTest
private class TestSalesAnalysisReportController {
	/*
	 * author: Redpoint
	 * date: 4/5/2017
	 * purpose: Test coverage for the SalesAnalysisController class.
	 * note: 100% test coverage as of 4/5
	*/

	static Account acct = new Account();
    static Season__c season = new Season__c();
    static Confirmation__c conf = new Confirmation__c();

	static void setup() {
		Variables__c var = Variables__c.getOrgDefaults();
        var.Default_Season__c = 'SP17';
        upsert var;

		acct.Name = 'Unit Test Account';
		acct.SchoolCode__c = 'UTAC';
        season.Name = var.Default_Season__c;
        RecordType recordtype = [SELECT Id FROM RecordType WHERE Name = 'OP' LIMIT 1];
        
        insert acct;
        insert season;

        conf.RecordTypeId = recordtype.Id;
        conf.Account__c = acct.Id;
        conf.Season__c = season.Id;
        insert conf;

        ApexPages.currentPage().getParameters().put('season', season.Name);
	}

	static Testmethod void testGetConfRecords() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(conf);
    	SalesAnalysisReportController sar = new SalesAnalysisReportController(sc);

		List<Confirmation__c> test = sar.getConfRecords();
		Confirmation__c test2 = test.get(0);

		System.assertEquals(1, test.size());
		System.assertEquals(conf.Id, test2.Id);
	}
	
	static Testmethod void testGetTotalCounts() {
		setup();

		ApexPages.StandardController sc = new ApexPages.StandardController(conf);
    	SalesAnalysisReportController sar = new SalesAnalysisReportController(sc);

		sar.getConfRecords();

		Integer test = sar.getTotalCount();
		System.assertEquals(1, test);
	}
}